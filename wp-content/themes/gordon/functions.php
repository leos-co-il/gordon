<?php
defined('ABSPATH') or die("No script kiddies please!");
require_once('inc/menu-walker.php');
require_once('inc/product-menu-walker.php');
define('THEMEDIR', get_template_directory_uri());
define('THEMEPATH', get_template_directory());
define('PARTIALS', THEMEPATH . '/views/partials/');
define('STYLES', THEMEDIR . '/assets/styles/');
define('SCRIPTS', THEMEDIR . '/assets/scripts/');
define('IMG', THEMEDIR . '/assets/img/');
define('ICONS', THEMEDIR . '/assets/iconsall/');
define('PLUGINS', THEMEDIR . '/assets/plugins/');
define('NODE', THEMEDIR . '/node_modules/');
define('CS_PAGE_ASSETS', THEMEDIR . '/assets/coming-soon/');
define('CATALOG', false);
define('PROJECTS', false);
define('ENV', get_option('stage'));
define('TEXTDOMAIN', 'leos');
define('UPLOADED_FILE_SIZE', 1024 * 10000);
define('FONTS', STYLES . 'fonts/');
define('FILES', THEMEPATH . '/files/');
define('PRODUCTS_PER_PAGE', 30);


function add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}

add_action( 'after_setup_theme', 'add_woocommerce_support' );

// *********************************** INIT ***********************************
load_theme_textdomain(TEXTDOMAIN, get_template_directory() . '/languages');

function disable_wp_emojicons($plugins){
	if (is_array($plugins)) {
		return array_diff($plugins, array('wpemoji'));
	} else {
		return array();
	}
}
function filter_site_upload_size_limit( $size ) {
	$size = UPLOADED_FILE_SIZE;
	return $size;
}
function wpcontent_svg_mime_type( $mimes = array() ) {
	$mimes['svg']  = 'image/svg+xml';
	$mimes['svgz'] = 'image/svg+xml';
	return $mimes;
}
function remove_wp_block_library_css(){
	wp_dequeue_style( 'wp-block-library' );
	wp_dequeue_style( 'wp-block-library-theme' );
	wp_dequeue_style( 'wc-block-style' );
}

function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );

	// Remove from TinyMCE
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
}
function my_deregister_scripts(){
	wp_dequeue_script( 'wp-embed' );
}
function remove_scripts() {
	if (!is_admin()) {
		wp_deregister_script('jquery');
//		wp_deregister_script('wp-api-fetch-js-after');
//		wp_deregister_script('wp-api-fetch-js');
//		wp_deregister_script('wp-url-js');
//		wp_deregister_script('lodash-js');
//		wp_deregister_script('wp-hooks-js');


		wp_register_script('jquery', false);

	}
}
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

function scriptsInit() {


	wp_enqueue_script('js-bundle', SCRIPTS . 'dist/app.js', '', 1.1);
	wp_enqueue_style('app-style', STYLES . 'styles.css', false, 1.0);


	$object = array(
		'wp_ajax' => admin_url( 'admin-ajax.php' ),
		'nonce' => wp_create_nonce('search-query-nonce')
	);

	wp_enqueue_script('mustache', NODE . 'mustache/mustache.min.js', array('jquery'), 1.0, true);
	wp_enqueue_script('app-scripts', SCRIPTS . 'scripts.js', array('jquery'), 1.0, true);
	wp_enqueue_script('app-scripts');
	wp_localize_script('app-scripts', 'JSObject', $object);
	//wp_enqueue_style('primary-style', get_stylesheet_uri(), '', '1.0');

	if(is_product_category()){
		wp_enqueue_style('jquery-ui', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', false, 1.0);
		wp_enqueue_style('jquery-ui-rtl', STYLES . 'jquery-ui-rtl.css', false, 1.0);
	}
}



function my_style_loader_tag_filter($html, $handle) {
	if ($handle === 'app-style') {
		return str_replace("rel='stylesheet'",
			"rel='stylesheet preload' as='style'", $html);
	}
	return $html;
}


function admin_style(){

	$cm_settings['codeEditor'] = wp_enqueue_code_editor(array('type' => 'text/css'));
	wp_localize_script('jquery', 'cm_settings', $cm_settings);

	wp_enqueue_style('admin-bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css');

	wp_enqueue_script('wp-theme-plugin-editor');
	wp_enqueue_style('wp-codemirror');



	wp_enqueue_script('admin-custom', SCRIPTS . 'admin.js', array('jquery'), 1.0, false);
	wp_enqueue_style('admin-styles', STYLES . 'admin.css');
}
if (function_exists('acf_add_options_page')) {

	acf_add_options_page(array(
		'page_title' => 'הגדרות נוספות',
		'menu_title' => 'הגדרות נוספות',
		'menu_slug' => 'theme-general-settings',
		'capability' => 'edit_posts',
		'redirect' => false
	));

	acf_add_options_page(array(
		'page_title' => 'הודעות רצות',
		'menu_title' => 'הודעות רצות',
		'menu_slug' => 'top-flash-msg',
		'capability' => 'edit_posts',
		'redirect' => false
	));
}
function acf_json_save_point( $path ) {
	$path = get_stylesheet_directory() . '/acf';
	return $path;
}
function acf_json_load_point( $paths ) {
	unset($paths[0]);
	$paths[] = get_stylesheet_directory() . '/acf';
	return $paths;
}


function preload_resources(){
	$output = '<link rel="preconnect" href="https://fonts.gstatic.com" />';

	//Almoni
	//$output .= '<link rel="preload" as="font" href="'.FONTS.'almoni/almoni-dl-aaa-300.woff" type="font/woff" crossorigin="anonymous">';
	//$output .= '<link rel="preload" as="font" href="'.FONTS.'almoni/almoni-dl-aaa-400.woff" type="font/woff" crossorigin="anonymous">';
	//$output .= '<link rel="preload" as="font" href="'.FONTS.'almoni/almoni-dl-aaa-900.woff" type="font/woff" crossorigin="anonymous">';
	//$output .= '<link rel="preload" as="font" href="'.FONTS.'almoni/almoni-dl-aaa-700.woff" type="font/woff" crossorigin="anonymous">';

	//Almoni tzar
	//$output .= '<link rel="preload" as="font" href="'.FONTS.'almoni-tzar/almoni-tzar-aaa-100.woff" type="font/woff" crossorigin="anonymous">';
	//$output .= '<link rel="preload" as="font" href="'.FONTS.'almoni-tzar/almoni-tzar-aaa-200.woff" type="font/woff" crossorigin="anonymous">';
	//$output .= '<link rel="preload" as="font" href="'.FONTS.'almoni-tzar/almoni-tzar-aaa-300.woff" type="font/woff" crossorigin="anonymous">';
	//$output .= '<link rel="preload" as="font" href="'.FONTS.'almoni-tzar/almoni-tzar-aaa-400.woff" type="font/woff" crossorigin="anonymous">';
	//$output .= '<link rel="preload" as="font" href="'.FONTS.'almoni-tzar/almoni-tzar-aaa-700.woff" type="font/woff" crossorigin="anonymous">';

	//Assistant
	$output .= '<link rel="preload" as="font" href="'.FONTS.'Assistant/Assistant-Regular.ttf"  crossorigin="anonymous">';
	$output .= '<link rel="preload" as="font" href="'.FONTS.'Assistant/Assistant-Bold.ttf"  crossorigin="anonymous">';
	$output .= '<link rel="preload" as="font" href="'.FONTS.'Assistant/Assistant-SemiBold.ttf"  crossorigin="anonymous">';

	//Rubik
	$output .= '<link rel="preload" as="font" href="'.FONTS.'Rubik/Rubik-VariableFont_wght.ttf"  crossorigin="anonymous">';

	//Slick
	$output .= '<link rel="preload" as="font" href="'.FONTS.'slick/slick.ttf"  crossorigin="anonymous">';
	$output .= '<link rel="preload" as="font" href="'.FONTS.'slick/slick.woff" type="font/woff"  crossorigin="anonymous">';

	//Google icons
	$output .= '<link rel="preload" as="font" href="https://fonts.gstatic.com/s/materialicons/v81/flUhRq6tzZclQEJ-Vdg-IuiaDsNcIhQ8tQ.woff2" type="font/woff2" crossorigin="anonymous">';

	echo $output;
}

add_theme_support('soil', [
	'clean-up',
	'disable-rest-api',
	'disable-asset-versioning',
	'disable-trackbacks',
	'js-to-footer',
]);

add_action('wp_enqueue_scripts', 'scriptsInit', 0);
add_filter( 'big_image_size_threshold', '__return_false' );
add_action('init', 'disable_wp_emojicons');
add_filter( 'upload_size_limit', 'filter_site_upload_size_limit', 20 );
add_filter( 'upload_mimes', 'wpcontent_svg_mime_type' );
add_action( 'wp_enqueue_scripts', 'remove_wp_block_library_css', 100 );
add_action( 'wp_footer', 'my_deregister_scripts' );
add_action( 'init', 'disable_emojis' );
add_action('init', 'remove_scripts');
add_filter('style_loader_tag', 'my_style_loader_tag_filter', 10, 2);
add_action('admin_enqueue_scripts', 'admin_style');
add_action('init', 'registerMenus');
add_theme_support( 'post-thumbnails' );
add_filter('acf/settings/load_json', 'acf_json_load_point');
add_filter('acf/settings/save_json', 'acf_json_save_point');

//add_filter( 'wpcf7_load_js', '__return_false' );
//add_filter( 'wpcf7_load_css', '__return_false' );
// *********************************** INIT ***********************************





// *********************************** SETUP ***********************************
function registerMenus() {
	register_nav_menus(
		array(
			'footer-main-menu' => __('Footer Main Menu', TEXTDOMAIN),
			'footer-second-menu' => __('Footer Second Menu', TEXTDOMAIN),
			'footer-third-menu' => __('Footer Third Menu', TEXTDOMAIN),
			'footer-fourth-menu' => __('Footer Fourth Menu', TEXTDOMAIN),
			'header-menu' => __('Header Menu', TEXTDOMAIN),
			'sidebar-menu' => __('Sidebar Menu', TEXTDOMAIN),

			'mega-menu-top' => __('Mega Menu Top', TEXTDOMAIN),
			'mega-menu-right' => __('Mega Menu Right', TEXTDOMAIN),
			'mega-menu-left' => __('Mega Menu Left (Products)', TEXTDOMAIN),
		)
	);
}
add_image_size('gallery-thumb', 134, 94);

// *********************************** SETUP ***********************************


add_filter( 'manage_edit-product_columns', 'change_columns_filter',10, 1 );
function change_columns_filter( $columns ) {
	unset($columns['product_tag']);
	unset($columns['wpseo-score-readability']);
	unset($columns['wpseo-score']);
	unset($columns['wpseo-links']);
	unset($columns['wpseo-linked']);
	unset($columns['sku']);
	return $columns;
}

function mgr_create_custom_field() {
	$args = array(
		'id'            => 'price_per_100',
		'label'         => 'מחיר ל-100 מל',
		'class'			=> 'price_per_100',
		'desc_tip'      => true,
	);
	woocommerce_wp_text_input( $args );
}
add_action( 'woocommerce_product_options_general_product_data', 'mgr_create_custom_field' );

function mgr_save_custom_field( $post_id ) {
	$product = wc_get_product( $post_id );
	$title = isset( $_POST['price_per_100'] ) ? $_POST['price_per_100'] : '';
	$product->update_meta_data( 'price_per_100', sanitize_text_field( $title ) );
	$product->save();
}
add_action( 'woocommerce_process_product_meta', 'mgr_save_custom_field' );

//add_option('search_json_version','0.0');



function generate_products_file(){

	$products = new WP_Query([
		'post_type' => 'product',
		'posts_per_page' => -1
	]);

	$_query_tax = [
		'brand',
		'size',
		'case',
		'smell',
		'concentration',
		'deodorant_type',
		'product_cat'
	];
	$products_array = [];

	if($products->have_posts()){
		ob_start();
		foreach ($products->posts as $p){
			$_pf = new WC_Product_Factory();
			$_product = $_pf->get_product($p->ID);
			$_product_thumb = get_webp_img(get_post_thumbnail_id( $_product->get_id() ), 'single-post-thumbnail', '');

			$keywords =  $p->post_title .'|';
			foreach ($_query_tax as $t){
				$_terms = get_the_terms($p, $t);
				if($_terms){
					foreach ($_terms as $_t){
						$keywords .= $_t->name . '|';
					}
				}
			}


			$products->the_post();
			wc_get_template_part( 'search', 'product' );
			$results_html = ob_get_contents();


			$products_array[] = [
				'id' => $p->ID,
				'title' => $p->post_title,
				'regular_price' => $_product->get_regular_price(),
				'sale_price' => $_product->is_on_sale() ? $_product->get_sale_price() : null,
				'keyword' => $keywords,
				'image' => $_product_thumb,
				'url' => get_permalink($p),
				'price_per_100' => get_post_meta($_product->get_id(), 'price_per_100', true )
				//'html' => $results_html
			];
		}
		ob_end_clean();
		wp_reset_postdata();
	}

	$version = (float) get_option('search_json_version') + 0.1;
	update_option(  'search_json_version', $version, true);
	$products_array['version'] = number_format($version, 1, '.', '');
	$products_array['count'] = $products->post_count;


	$json =  json_encode($products_array);
	$bytes = file_put_contents(FILES . "products.json", $json);

}

add_action('save_post','generate_product_json');
function generate_product_json($post_id){
	global $post;
	if ($post->post_type === 'product'){
		generate_products_file();
	}
}


//WOO
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action('woocommerce_review_before', 'woocommerce_review_display_gravatar', 10);
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);

add_action('add_meta_boxes', 'add_metabox_to_product');
add_action('woocommerce_single_title_custom', 'woocommerce_template_single_title');


function add_metabox_to_product(){
	add_meta_box(   'product_video', 'קישור לסרטון',  'woo_product_video', 'product', 'side', 'low');
}


function woo_product_video($post) {
	$yt_link = get_post_meta($post->ID, '_product_video', TRUE);

	echo '<input type="hidden" name="gallery_type_noncename" id="gallery_type_noncename" value="'.wp_create_nonce( 'product'. $post->ID ) . '" />';
	echo '<input type="text" style="width: 100%;" name="product_video" id="product_video" value="'.$yt_link.'">';

	$yt_id = null;
	if($yt_link){
		$yt_id = getYoutubeId($yt_link);
	}
	if($yt_id){
		echo '<iframe style="width: 100%; height: 300px; margin-top: 10px;" src="https://www.youtube.com/embed/'.$yt_id.'" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
	}
}


// Add to admin_init function
add_action('save_post', 'save_gallery_data' );

function save_gallery_data($post_id) {
	// verify this came from the our screen and with proper authorization.
	if ( !wp_verify_nonce( $_POST['gallery_type_noncename'], 'product'.$post_id )) {
		return $post_id;
	}

	// verify if this is an auto save routine. If it is our form has not been submitted, so we dont want to do anything
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
		return $post_id;

	// Check permissions
	if ( !current_user_can( 'edit_post', $post_id ) )
		return $post_id;


	// OK, we're authenticated: we need to find and save the data
	$post = get_post($post_id);
	if ($post->post_type == 'product') {
		update_post_meta($post_id, '_product_video', esc_attr($_POST['product_video']) );
		return(esc_attr($_POST['product_video']));
	}
	return $post_id;
}

//add_filter( 'comment_post_redirect', 'redirect_after_comment', 10, 2 );
//function redirect_after_comment($location, $comment ){
//	return add_query_arg('action', 'cmtpnd', $location);
//}

add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );
function new_loop_shop_per_page( $cols ){
	return PRODUCTS_PER_PAGE;
}


function custom_array_unique($array, $keep_key_assoc = false){
	$duplicate_keys = array();
	$tmp = array();

	foreach ($array as $key => $val){
		if (is_object($val))
			$val = (array)$val;

		if (!in_array($val, $tmp))
			$tmp[] = $val;
		else
			$duplicate_keys[] = $key;
	}

	foreach ($duplicate_keys as $key)
		unset($array[$key]);

	return $keep_key_assoc ? $array : array_values($array);
}

function build_data_attr($terms){
	$attr = '';

	foreach ($terms as $t){
		$attr .= ' ' . $t->taxonomy . '-' . $t->term_id;
	}

	return $attr;
}

function custom_override_checkout_fields( $fields ) {
	unset($fields['billing']['billing_country']);
	return $fields;
}

function custom_override_billing_fields( $fields ) {
	unset($fields['billing_country']);
	return $fields;
}

add_filter('woocommerce_checkout_fields','custom_override_checkout_fields');
add_filter( 'woocommerce_billing_fields' , 'custom_override_billing_fields' );
add_filter( 'woocommerce_shipping_fields' , 'custom_override_checkout_fields' );

require_once "inc/options.php";
require_once "inc/custom-types.php";
require_once "inc/users.php";
require_once "inc/ajax_calls.php";
require_once 'inc/helper.php';
require_once "inc/dashboard-widgets.php";
