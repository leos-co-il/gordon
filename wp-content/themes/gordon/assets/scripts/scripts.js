(function ($) {
	var products = '';
	function twinkle(id, duration) {
		var top = (Math.floor(Math.random() * 85)) + '%';
		var left = (Math.floor(Math.random() * 85)) + '%';
		var _el = $('#speck' + id);

		_el.remove();
		$('#specks').append("<div class='speck' id='speck" + id + "'></div>");
		_el.css({
			'top': top,
			'left': left,
			'animation-duration': duration + 's',
			'animation-timing-function': 'cubic-bezier(0.250, 0.250, 0.750, 0.750)',
			'animation-name': 'twinkle',
		});
	}
	function twinkleLoop(i) {
		var speed = 400;
		var duration = ((Math.random() * 5) + 3);

		duration = duration - ((495 - speed) / 100);
		twinkle(i, duration);

		setTimeout(function () {
			twinkleLoop(i);
		}, duration * 1000);
	}
	function ajaxSearch($query) {
		jQuery.ajax({
			url: JSObject.wp_ajax,
			type: 'post',
			dataType: "json",
			data: {
				action: 'search_query',
				query: $query,
				nonce: JSObject.nonce,
			},
			success: function (data) {

			}
		});
	}
	function debounce(func, wait, immediate) {
		var timeout;
		return function() {
			var context = this, args = arguments;
			var later = function() {
				timeout = null;
				if (!immediate) func.apply(context, args);
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			if (callNow) func.apply(context, args);
		};
	};
	function readTextFile(file, callback) {
		var rawFile = new XMLHttpRequest();
		rawFile.overrideMimeType("application/json");
		rawFile.open("GET", file, true);
		rawFile.onreadystatechange = function() {
			if (rawFile.readyState === 4 && rawFile.status == "200") {
				callback(rawFile.responseText);
			}
		}
		rawFile.send(null);
	}
	function loadData($_version){
		localStorage.setItem("data-version" , $_version);
		readTextFile("/wp-content/themes/duty/files/products.json", function(text){
			var data = JSON.parse(text);
			localStorage.setItem('data-object', JSON.stringify(data));
			products = JSON.parse(localStorage.getItem("data-object"));
		});
	}
	function getRandom(arr, n) {
		var result = new Array(n),
			len = arr.length,
			taken = new Array(len);
		if (n > len)
			throw new RangeError("getRandom: more elements taken than available");
		while (n--) {
			var x = Math.floor(Math.random() * len);
			result[n] = arr[x in taken ? taken[x] : x];
			taken[x] = --len in taken ? taken[len] : len;
		}
		return result;
	}
	function generateYoutubeIframe(_id, _w, _h, _title, _clean, _autoplay){


		var params = {
			id: _id,
			width: _w,
			height: _h,
			title: _title,
			modestbranding: _clean ? 1 : 0,
			showinfo: _clean ? 0 : 1,
			autoplay: _autoplay ? 1: 0
		};

		return `<iframe width="${params.width}" height="${params.height}"

							src="https://www.youtube.com/embed/${params.id}
							?autoplay=${params.autoplay}&
							modestbranding=${params.modestbranding}&
							showinfo=${params.showinfo}&
							controls=0&
							loop=1&
							rel=0&
							playlist=${params.id}&
							mute=1"
							title="${params.title}"
							allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
					</iframe>`;

	}
	function addToCartAnimation(button){
		var target        = $('#mini-cart'),
			target_offset = target.offset();

		var target_x = target_offset.left,
			target_y = target_offset.top;

		var obj_id = 1 + Math.floor(Math.random() * 100000),
			obj_class = 'cart-animation-helper',
			obj_class_id = obj_class + '_' + obj_id;

		var obj = $("<div>", {
			'class': obj_class + ' ' + obj_class_id
		});

		button.parent().parent().append(obj);

		var obj_offset = obj.offset(),
			dist_x = target_x - obj_offset.left + 10,
			dist_y = target_y - obj_offset.top + 10,
			delay  = 0.8; // seconds

		setTimeout(function(){
			obj.css({
				'transition': 'transform ' + delay + 's ease-in',
				'transform' : "translateX(" + dist_x + "px)"
			});
			$('<style>.' + obj_class_id + ':after{ \
				transform: translateY(' + dist_y + 'px); \
				opacity: 1; \
				z-index: 99999999999; \
				border-radius: 100%; \
				height: 20px; \
				width: 20px; margin: 0; \
			}</style>').appendTo('head');
		}, 0);


		obj.show(1).delay((delay + 0.02) * 1000).hide(1, function() {
			$(obj).remove();
		});
	}
	function updateMiniCart(key, qty){

		var cartEl = $('.mini-cart-wrap');

		jQuery.ajax({
			url: JSObject.wp_ajax,
			type: 'POST',
			data: {
				action: 'update_mini_cart',
				id: key,
				qty: qty
			},
			success: function (results) {
				cartEl.html(results);
				update_cart_count();
				$('.preloader').removeClass('now-loading');
			}
		});
	}
	function update_cart_count(){
		var count = $('#cart-count');
		count.html('<i class="far fa-smile-beam fa-spin" style="color: #fff"></i>');

		jQuery.ajax({
			url: JSObject.wp_ajax,
			type: 'POST',
			data: {
				action: 'update_cart_count',
			},
			success: function (results) {
				count.html(results/10);
			}
		});

	}
	function play_video(){
		var id = $('#video-thumb').data('id');
		var pr_frame = generateYoutubeIframe(id, '', '', '', 1, 1);

		$('#video-holder').append(pr_frame);
	}
	function clear_frame(){
		$('#video-holder').empty();
	}
	function productFilter(){
		var checked = $('.filter-select:checkbox:checked');
		var filterClass = '';
		var _col = $('.product-item-col');
		if(checked.length > 0){
			_col.removeClass('show-filter').addClass('prepend-filter')
		}else{
			_col.removeClass('show-filter').removeClass('prepend-filter')
		}

		checked.each(function (index, value){
			filterClass += '.' + $(this).data('tax') + '-' +$(this).val();
			var _toFilter = $(filterClass);

			_toFilter.each(function (index, value){
				$(this).parent().addClass('show-filter');
			});
		});
		priceFilter();
	}
	function priceFilter(){
		var minPrice = parseInt($('#price-min').val());
		var maxPrice = parseInt($('#price-max').val());
		var _filtered = $('.show-filter .product-small-card');

		_filtered.each(function(index, item){
			var _price = parseInt($(this).data('price'));

			if(!isNaN(_price)){
				if(!(_price >= minPrice && _price <= maxPrice)){
					$(this).parent().addClass('price-filter');
				}else{
					$(this).parent().removeClass('price-filter');
				}
			}

		});
	}

	$(document).ready(function () {

		$(".filter-select").change(function() {
			productFilter();
		});

		var btnForm = $('.product-summary .add-to-cart-single-item').html();
		if(btnForm !== 'undefined'){
			$('.bottom-sticky-section .container .row .col-12').html(btnForm);
		}

		$( "#slider-range" ).slider({
			range: true,
			isRTL: true,
			min: 0,
			max: 5000,
			values: [ 0, 5000 ],
			slide: function( event, ui ) {
				$('#price-min').val(ui.values[0]);
				$('#price-max').val(ui.values[1]);
				priceFilter();
			}
		});

		function addCommas(nStr){
			nStr += '';
			x = nStr.split('.');
			x1 = x[0];
			x2 = x.length > 1 ? '.' + x[1] : '';
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(x1)) {
				x1 = x1.replace(rgx, '$1' + ',' + '$2');
			}
			return x1 + x2;
		}


		var dataVersion = $('head').data('version');

		if(typeof(Storage)!=="undefined") {

			var localVersion = localStorage.getItem("data-version");

			if(localVersion === null){
				loadData(dataVersion)
			}else{
				if(parseFloat(localVersion) < parseFloat(dataVersion)){
					loadData(dataVersion);
				}else{
					products = JSON.parse(localStorage.getItem("data-object"));
				}

			}


			console.log(products);
		}

		var products_slider = $('#products-slider').slick({
			infinite: true,
			slidesToShow: 3,
			slidesToScroll: 3,
			rtl: true,
			arrows: true,
			centerMode: true,
			centerPadding: '60px',
			responsive: [
				{
					breakpoint: 900,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						centerPadding: '10px',
					}
				},
			]
		});

		products_slider.on('beforeChange', function(slick, currentSlide, nextSlide){

			var _yt = currentSlide.$slides.get(currentSlide.currentSlide).children[0].children;
			var _frame = generateYoutubeIframe(_yt[0].dataset['yt'], '', '', '', true, true)
			var scene = $('#video-scene');
			scene.empty().append(_frame);

		});


		var products_slider_partial = $('.product-slider-partial').slick({
			infinite: true,
			slidesToShow: 4,
			slidesToScroll: 4,
			rtl: true,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 1500,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});

		$('.sl-nav.right').click(function (){
			products_slider_partial.slick('slickPrev')
		});
		$('.sl-nav.left').click(function (){
			products_slider_partial.slick('slickNext')
		});

		$('#mini-cart').click(function (e){
			e.preventDefault();

			$('#mini-cart-el').addClass('show-cart');
		})

		$(document).on('click', '#close-cart', function (){
			$('#mini-cart-el').removeClass('show-cart');
		})


		$('.product-small-card .single_add_to_cart_button').click(function (e) {
			e.preventDefault();
			addToCartAnimation($(this));
			var cartEl = $('.mini-cart-wrap');
			cartEl.html('<div class="cart-loading"><i class="fas fa-spinner fa-pulse"></i></div>');


			var btn = $(this);
			btn.addClass('loading');


			$(this).addClass('adding-cart');
			var product_id = $(this).data('id');
			var quantity = $('.qty-for-' + product_id).val();


			jQuery.ajax({
				url: JSObject.wp_ajax,
				type: 'POST',
				data: {
					action: 'add_product_to_cart',
					product_id: product_id,
					quantity: quantity,
				},

				success: function (results) {
					btn.removeClass('loading').addClass('clicked');
					cartEl.html(results);
					update_cart_count();
				}
			});

		});

		var large_slider = $('#gallery-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			fade: false,
			asNavFor: '#gallery-thumbs',
			rtl: true,
			onAfterChange: function(slide, index){
				// console.log( $(this).attr('id') );
				// console.log( $(slide).attr('id') );
			}
		});
		var thumb_slider = $('#gallery-thumbs').slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			asNavFor: '#gallery-slider',
			dots: false,
			arrows: false,
			centerMode: false,
			vertical: true,
			focusOnSelect: true,
			responsive: [
				{
					breakpoint: 900,
					settings: {
						slidesToShow: 2,
					}
				},
			]
		});


		$('#open-comment-form').click(function (){
			$('#review_form_wrapper').addClass('show-review-form')
		});

		$('#close-review-form').click(function (){
			$('#review_form_wrapper').removeClass('show-review-form')
		})

		large_slider.on('afterChange', function(slick, currentSlide, nextSlide){

			var active = $('.gallery-slider .slick-current .slider-item').data('video');

			if(active){
				play_video();
			}else{
				clear_frame();
			}

		});


		$('#accordion').on('shown.bs.collapse', function (e) {
			$('#accordion .collapse.show').parent().addClass('active');
		});

		$('#accordion').on('hide.bs.collapse', function (e) {
			$('#accordion .collapse.show').parent().removeClass('active');
		});

		$('.flash-msg-slider').slick({
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false,
			autoplay: true,
			autoplaySpeed: 3000,
			rtl: true
		});

		$('.slick-text').slick({
			infinite: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false,
			arrows: true,
			rtl: true
		});
		$('.base-slider').slick({
			infinite: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false,
			arrows: true,
			rtl: true
		});

		$('.top-slider').slick({
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: true,
			arrows: false,
			autoplay: true,
			autoplaySpeed: 5000,
			rtl: true,
			fade: true,
			cssEase: 'linear'
		});

		$(window).scroll(function () {
			var sticky = $('.sticky'),
				stickyBottom = $('.bottom-sticky-section')
			scroll = $(window).scrollTop();

			if (scroll >= 400) {
				sticky.addClass('fixed');
				stickyBottom.addClass('sticky-bottom');
				$('body').addClass('header-is-sticky');
			} else {
				stickyBottom.removeClass('sticky-bottom');
				sticky.removeClass('fixed');
				$('body').removeClass('header-is-sticky');
			}
		});



		AOS.init();
		$('.hamburger').click(function () {
			$(this).toggleClass('is-active');
			$('.top-navigation-menu').toggleClass('menu-active');

			$('.mega-menu').toggleClass('show');
			$('body').toggleClass('menu-is-active');

		});
		$('#search-btn').click(function () {
			$('.search-wrap').toggleClass('show-search');
			$('body').addClass('menu-is-active');
			$('.sticky').addClass('fixed');
		});
		$('#search-close').click(function () {
			$('.search-wrap').removeClass('show-search');
			$('body').removeClass('menu-is-active');
			$('.sticky').removeClass('fixed');
		});
		$('#close-menu').click(function () {
			$(this).toggleClass('is-active');
			$('.hamburger').toggleClass('is-active');
			$('.top-navigation-menu').toggleClass('menu-active');
			$('.mega-menu').toggleClass('show');
			$('body').removeClass('menu-is-active');
		});
		$('li svg').click(function () {
			$(this).parent().toggleClass('mob-sub-active');
		});
		$('.slick-test').slick({
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true
		});
		$('.banner-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			vertical: true,
			centerMode: false,
			verticalSwiping: true,
			centerPadding: 0,
			arrows: true,
			dots: false,
		});
		$('.number-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: false,
			dots: true,
			customPaging : function(slider, i) {
				var thumb = $(slider.$slides[i]).data();
				return '<a>'+(i+1)+'</a>';
			},
		});
		var myEfficientFn = debounce(function() {
			var _query = $('#search-query').val();
			var query_result = [];
			var result_el = $('.search-query-result');
			result_el.empty();

			console.log(products);

			for(var item in products){
				if(!products[item].hasOwnProperty('keyword'))
					continue;

				if(products[item].keyword.indexOf(_query) !== -1){
					query_result.push(products[item]);
				}
			}

			var random_result = query_result;

			for (let res in random_result){

				fetch('wp-content/themes/duty/assets/scripts/templates/product.mustache')
					.then((response) => response.text())
					.then((template) => {
						var render = Mustache.render(template, {
							id: random_result[res].id,
							name: random_result[res].title,
							image: random_result[res].image,
							regular_price: random_result[res].regular_price,
							sale_price: random_result[res].sale_price,
							is_sale: !!random_result[res].sale_price,
							is_regular: !random_result[res].sale_price,
							title: random_result[res].title,
							url: random_result[res].url,
							price_per_100: random_result[res].price_per_100
						});
						result_el.append(render);
					});
			}


		}, 500);

		var brandSearchFn = debounce(function() {
			var _query = $('#brand-search').val();
			const brandList = document.querySelector('.brand-list');
			let brands = document.querySelectorAll('.brand-list li');

			if(_query !== ''){

				brandList.classList.add('apply-search');
				brands.forEach(function (item){
					item.classList.remove('show-brand');
					if(item.getAttribute('data-title').indexOf(_query) !== -1){
						item.classList.add("show-brand");
					}
				})

			}else{
				brandList.classList.remove('apply-search');
			}


		}, 300)

		$('#search-query').keyup(function(event) {
			myEfficientFn();
		});

		$('#brand-search').keyup(function (){
			brandSearchFn();
		});

		function preloadFirstVideo(){
			var scene = $('#video-scene');
			var iframe = generateYoutubeIframe(scene.data('id'), '', '', '', true, true);
			scene.append(iframe);
		}

		var stop = 0;
		$(window).on( 'scroll', function(){

			if ( $('.new-product-presentation').length ) {

				var scrollTop     = $(window).scrollTop(),
					elementOffset = $('.new-product-presentation').offset().top,
					distance      = (elementOffset - scrollTop);
				var vh = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);

				if (distance < (vh + 400) && !stop){
					stop = 1;
					preloadFirstVideo();
				}

			}

		});

		function mobileNav() {
			$('.mobile-navigation').toggleClass('show-nav');
			$('.mob-nav-toggle').toggleClass('nav-is-open');
		}

		var mobileNavF = $('.mob-nav-toggle');
		mobileNavF.click(function () {
			mobileNav();
		});
		mobileNavF.on('swipedown', function () {
			mobileNav();
		});
		// mobile_nav.on('swipeup',function(){
		// 	mobileNav();
		// });


		$('body').on('click', '.remove_from_cart_x', function (e) {
			e.preventDefault();
			var productID = $(this).data('key');
			var cartEl = $('.mini-cart-wrap');
			cartEl.html('<div class="cart-loading"><i class="fas fa-spinner fa-pulse"></i></div>');
			jQuery.ajax({
				url: JSObject.wp_ajax,
				type: 'POST',
				data: {
					action: 'remove_item_from_cart',
					product_id: productID,
				},

				success: function (results) {
					update_cart_count()
					cartEl.html(results);
				}
			});
		});

		$(document).on('click', '.plus, .minus', function () {

			var $_class = $(this).hasClass('mini-cart-ctrl') ? '.mini-cart-qty-for-' : '.qty-for-';
			var qty =  $($_class + $(this).data('id'));
			var val = parseFloat(qty.val());
			var max = 9999;
			var min = 1;
			var step = 1;

			if ($(this).is('.plus')) {
				if (max && (max <= val)) {
					qty.val(max);
				} else {
					qty.val(val + step);
				}
			} else {
				if (min && (min >= val)) {
					qty.val(min);
				} else if (val > 1) {
					qty.val(val - step);
				}
			}

			if($(this).hasClass('mini-cart-ctrl')){
				$('.preloader').addClass('now-loading');
				updateMiniCart($(this).data('key'), qty.val())
			}

		});


	});



	for (var i = 1; i < 6; i++) {
		twinkleLoop(i);
	}

	$(document).on('added_to_wishlist removed_from_wishlist', function () {
		jQuery.ajax({
			url: JSObject.wp_ajax,
			type: 'post',
			dataType: "json",
			data: {
				action: 'yith_wcwl_update_wishlist_count',
			},
			success: function (data) {
				$('#yith-w-count').html(data.count);
			}
		});
	});


})(jQuery);
