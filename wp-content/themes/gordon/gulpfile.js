var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var gulp = require('gulp');
var order = require('gulp-order');




gulp.task('js', function() {
	return gulp.src('./assets/scripts/source/*.js')
		.pipe(order([
			'assets/scripts/source/1jquery.js',
			'assets/scripts/source/bootstrap.js',
			'assets/scripts/source/*.js'
		]))
		.pipe(concat('app.js'))
		.pipe(uglify().on('error', function(e){ console.log(e);  }))
		.pipe(gulp.dest('./assets/scripts/dist/'));
});
