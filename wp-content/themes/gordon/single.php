<?php

the_post();
get_header();
$fields = get_fields();

?>



<div class="inner-header">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1><?= the_title() ?></h1>
			</div>
			<div class="col-12">
				<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
				}
				?>
			</div>
		</div>
	</div>
</div>
<article class="post-body">
	<div class="container">
		<div class="row align-items-start">
			<div class="col-lg-8 col-12">
				<?php if (has_post_thumbnail()) : ?>
					<div class="thumb-post-wrap">
						<img src="<?= postThumb(); ?>" alt="post-img">
					</div>
				<?php endif; ?>
				<div class="post-text-output">
					<?php the_content(); ?>
				</div>
				<?php if ($fields['post_gallery']) : ?>
					<div class="row justify-content-start align-items-stretch">
						<?php foreach ($fields['post_gallery'] as $gal_img) : ?>
							<div class="col-md-4 col-12 mb-3">
								<div class="post-gallery-img">
									<a class="gallery-img-inside" href="<?= $gal_img['url']; ?>" data-lightbox="post-img"
									style="background-image: url('<?= $gal_img['url']; ?>')"></a>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif;
				if ($fields['post_link_item']) : ?>
					<div class="row mt-5">
						<div class="col-12">
							<h2 class="post-links-title">
								<?= $fields['post_links_title'] ? $fields['post_links_title'] : 'קישורים בנושא:'; ?>
							</h2>
						</div>
						<?php foreach ($fields['post_link_item'] as $file) : if ((isset($file['link_url'])) && $file['link_url'])?>
							<div class="col-12">
								<a href="<?= $file['link_url']['url']; ?>" class="post-file-item">
									<?php $link_title = (isset($file['link_url']['title']) && $file['link_url']['title']) ?? $file['link_url']['title'];
									echo $file['link_title'] ? $file['link_title'] : $link_title; ?>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<?php if ($shop = opt('shop_link')) : ?>
				<div class="col-lg-4 col-12 shop-col-single">
					<div class="single-shop-wrap">
						<a href="<?= $shop['url']; ?>" class="prod-slide-link-single">
							<?= (isset($shop['title']) && $shop['title']) ? $shop['title']
									: 'חזרה לחנות'; ?>
						</a>
						<?php if ($shop_img = opt('shop_img')) : ?>
							<img src="<?= $shop_img['url']; ?>" alt="shop-online" class="shop-single-img">
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php
$postId = get_the_ID();
$currentType = get_post_type($postId);
$post_terms = wp_get_object_terms($postId, 'category', ['fields' => 'ids']);
$samePosts = [];
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} else {
	$samePosts = get_posts([
			'posts_per_page' => 4,
			'post_type' => 'post',
			'post__not_in' => array($postId),
			'tax_query' => [
					[
							'taxonomy' => 'category',
							'field' => 'term_id',
							'terms' => $post_terms,
					],
			],
	]);
}
if ($samePosts === NULL) {
	$samePosts = get_posts([
			'posts_per_page' => 4,
			'orderby' => 'rand',
			'post_type' => 'post',
			'post__not_in' => array($postId),
	]);
}
if ($samePosts) : ?>
	<section class="posts-same-block">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<h2 class="same-posts-title">
						<?= $fields['same_title'] ? $fields['same_title'] : 'מאמרים נוספים שכדאי לכם לקרוא:'; ?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($samePosts as $x => $post) : ?>
					<div class="col-xl-3 col-md-6 col-12 mb-3 ">
						<?php get_template_part('views/partials/card', 'post',
								[
										'post' => $post,
								]); ?>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php endif;
if ($slider_seo = $fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $slider_seo,
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
