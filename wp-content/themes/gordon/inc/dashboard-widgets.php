<?php



function remove_dashboard_widgets() {
	global $wp_meta_boxes;

	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_site_health']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['wpseo-dashboard-overview']);

}

add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );
add_action('wp_dashboard_setup', 'remove_wpseo_dashboard_overview' );

function remove_wpseo_dashboard_overview() {
	// In some cases, you may need to replace 'side' with 'normal' or 'advanced'.
	remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'side' );
}



add_action( 'admin_footer', 'rv_custom_dashboard_widget' );
function rv_custom_dashboard_widget() {

	$site_stage = get_option('stage');

	if ( get_current_screen()->base !== 'dashboard' && $site_stage !== 'prod') {
		return;
	}

	$check_list = [
		'admin_mail'
	];

	$woo_mails = class_exists('WooCommerce') ? checkWooMails() : null;

	$status_badge = null;
	if($site_stage === 'dev'){
		$status_badge = '<span class="badge bg-danger">Development</span>';
	}elseif($site_stage === 'prod'){
		$status_badge = '<span class="badge bg-success">Online</span>';
	}


	?>

	<div id="custom-id" class="py-3 px-1 border" style="display: none;">
		<div class="welcome-panel-content container-fluid">
			<div class="row">
				<div class="col-12">
					<h2 class="d-flex justify-content-between mb-1">
						<span>סטטוס האתר</span>
						<span>
							<?= $status_badge ?>
							<?= class_exists('WooCommerce') ? '<span class="badge bg-info text-dark">Woocommerce Site</span>' : '' ?>
						</span>
					</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<h5>Site Settings</h5>
					<ul class="list-group">
						<li class="list-group-item">
							מנהל האתר:
							<?= get_option('admin_email') ?>
						</li>
					</ul>
				</div>
				<?php if(class_exists('WooCommerce')): ?>
				<div class="col-md-4">
					<h5>Woocommerce Mails</h5>
					<ul class="list-group">
						<?php if($woo_mails): ?>
						<li class="list-group-item">
							<?php foreach ($woo_mails as $m): ?>
								<div class="alert <?= $m['warn'] === false ? 'alert-success' : 'alert-danger' ?> " role="alert">
									<span><?= $m['title'] . ': ' . $m['recipient'] ?></span>
								</div>
							<?php endforeach; ?>
						</li>
						<?php endif; ?>
					</ul>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<script>
		jQuery(document).ready(function($) {
			$('#welcome-panel').after($('#custom-id').show());
		});
	</script>

<?php }

function checkWooMails(){
	$emails = [];


		$emails_objects = wc()->mailer()->emails;
		if($emails_objects){
			foreach ($emails_objects as $obj){
				if($obj->enabled === 'yes'){
					$emails[] = [
							'title' => 	$obj->title,
							'recipient' => $obj->recipient,
							'warn' => strpos($obj->recipient, 'maxf@leos.co.il') !== false || empty($obj->recipient)
					];
				}
			}
		}

	return $emails;
}
