<?php

add_action('wp_ajax_nopriv_search_query', 'search_query');
add_action('wp_ajax_search_query', 'search_query');

function search_query()
{

    if (!wp_verify_nonce($_REQUEST['nonce'], "search-query-nonce")) {
        exit("No naughty business please");
    }

    $_query_tax = [
		'brand',
		'product_cat'
	];
    $_query = $_REQUEST['query'];

    $tax_query = [
    	'relation' => 'OR'
	];

    foreach ($_query_tax as $_tax){
		$terms = wp_list_pluck(get_terms([
			'taxonomy' 	 => $_tax,
			'name__like' => $_query,
			'hide_empty' => false,
		]), 'term_id');

		if($terms){
			$tax_query[] = [
				'taxonomy' => $_tax,
				'field' => 'term_id',
				'terms' => $terms
			];
		}
	}

	var_dump($tax_query);

    $query_object = new WP_Query([
		'post_type' => 'product',
		'cat' => $terms
	]);

    var_dump($query_object->posts);

    $result['type'] = "error";
    $result['var'] = 1;

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    } else {
        header("Location: " . $_SERVER["HTTP_REFERER"]);
    }
    die();
}

if ( defined( 'YITH_WCWL' ) ) {
	function yith_wcwl_ajax_update_count() {
		wp_send_json( array(
			'count' => yith_wcwl_count_all_products()
		) );

	}
	add_action( 'wp_ajax_yith_wcwl_update_wishlist_count', 'yith_wcwl_ajax_update_count' );
	add_action( 'wp_ajax_nopriv_yith_wcwl_update_wishlist_count', 'yith_wcwl_ajax_update_count' );
}


function remove_item_from_cart(){

	$key = isset($_REQUEST['product_id']) ? $_REQUEST['product_id'] : null;

	if($key){
		WC()->cart->remove_cart_item($key);
	}

	wc_get_template( 'cart/mini-cart.php' );
	die();
}


add_action('wp_ajax_remove_item_from_cart', 'remove_item_from_cart');
add_action('wp_ajax_nopriv_remove_item_from_cart', 'remove_item_from_cart');

function add_product_to_cart() {

	$product_id = isset($_REQUEST['product_id']) ?  intval($_REQUEST['product_id']) : null;
	$quantity = isset($_REQUEST['quantity']) ? intval($_REQUEST['quantity']) : null;
	$custom_data = [];
	$_product = wc_get_product( $product_id );


	$cart = WC()->cart;

	if($_POST['delete'] === 'true'){
		$cart->remove_cart_item($product_id);
	}else{
		$cart->add_to_cart( $product_id, $quantity, '0', array(), $custom_data );
	}

	$result['type'] = "success";

	wc_get_template( 'cart/mini-cart.php' );

	die();
}

add_action('wp_ajax_add_product_to_cart', 'add_product_to_cart');
add_action('wp_ajax_nopriv_add_product_to_cart', 'add_product_to_cart');


add_action('wp_ajax_nopriv_update_cart_count', 'update_cart_count');
add_action('wp_ajax_update_cart_count', 'update_cart_count');
function update_cart_count(){
	global $woocommerce;
	echo $woocommerce->cart->cart_contents_count;
}


add_action('wp_ajax_nopriv_update_mini_cart', 'update_mini_cart');
add_action('wp_ajax_update_mini_cart', 'update_mini_cart');
function update_mini_cart(){
	$id = filter_var($_POST['id'], FILTER_SANITIZE_STRING);
	$qty = intval($_POST['qty']);

	global $woocommerce;
	$woocommerce->cart->set_quantity($id,$qty);


	$result['type'] = "success";

	wc_get_template( 'cart/mini-cart.php' );

	die();
}
