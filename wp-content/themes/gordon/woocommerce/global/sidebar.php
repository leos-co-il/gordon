<?php
/**
 * Sidebar
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/sidebar.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $wp_query;
$query_brands = [];
$query_sizes = [];
$query_cases = [];
$query_smells = [];
$query_concentrations = [];
$query_deodorant_types = [];
$query_cat = [];

if($wp_query->have_posts()){
	foreach ($wp_query->posts as $_p){
		$_brands = get_the_terms($_p, 'brand');
		if($_brands){
			foreach ($_brands as $_b){
				$query_brands[] = $_b;
			}
		}

		$_category = get_the_terms($_p, 'product_cat');
		if($_category){
			foreach ($_category as $_cat){
				$query_cat[] = $_cat;
			}
		}
	}
}

$brands = custom_array_unique($query_brands);

$filters = [
		[
				'title' => 'קטגוריה',
				'data' => custom_array_unique($query_cat)
		],
];





?>

<div id="sidebar">
	<?php if(!empty($brands)): ?>
	<div class="sidebar-box">
		<div class="box-title">
			<h6>מותג</h6>
		</div>
		<div class="box-content">
			<input type="text" id="brand-search" placeholder="חפש מותג">
			<ul class="brand-list box-list">
				<?php foreach ($brands as $_brand_item): ?>
					<li data-title="<?= $_brand_item->name ?>">
						<input type="checkbox"
							   data-tax="brand"
							   id="brand-<?= $_brand_item->term_id ?>" value="<?= $_brand_item->term_id ?>" class="brand-select filter-select">
						<label for="brand-<?= $_brand_item->term_id ?>">
							<?= $_brand_item->name ?>
						</label>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
	<?php endif; ?>
	<?php foreach ($filters as $filter): if(empty($filter['data'])) continue; ?>
		<div class="sidebar-box">
			<div class="box-title">
				<h6><?= $filter['title'] ?></h6>
			</div>
			<div class="box-content">
				<ul class="box-list">
					<?php foreach ($filter['data'] as $_data_item): ?>
						<li data-title="<?= $_data_item->name ?>">
							<input type="checkbox" data-tax="<?= $_data_item->taxonomy ?>"
								   value="<?= $_data_item->term_id ?>"
								   id="<?= $_data_item->taxonomy . '-' . $_data_item->term_id ?>" class="filter-select">
							<label for="brand-<?= $_data_item->term_id ?>">
								<?= $_data_item->name ?>
							</label>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
	<?php endforeach; ?>
	<div class="sidebar-box">
		<div class="box-title">
			<h6>מחיר</h6>
		</div>
		<div class="box-content">
			<p class="range-title d-flex align-items-center justify-content-center mt-2">
				<span>מחיר: </span>
				<input type="text" class="price-field" value="0" id="price-min">
				<span>-</span>
				<input type="text" class="price-field" value="5000" id="price-max">
				<span><?= get_woocommerce_currency_symbol() ?></span>
			</p>
			<div id="slider-range" class="mb-3 mx-2"></div>
		</div>
	</div>
</div>
