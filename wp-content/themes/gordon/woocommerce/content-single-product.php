<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
$post_id = $product->get_id();
$category = get_the_terms($post_id, 'product_cat');


$brand = get_the_terms($post_id, 'brand');
$product_thumb = get_webp_img(get_post_thumbnail_id($post_id), 'large', '');
$product_gallery = $product->get_gallery_image_ids();
$product_gallery_images = [];
if($product_gallery){
	foreach ($product_gallery as $_item){
		$product_gallery_images[] = get_webp_img($_item, 'large', '');
	}
}
$product_video =  get_post_meta($post_id, '_product_video', true );
$fields = get_fields();
$info = $fields['info_tabs'];
$related_products = $fields['related_products'];

?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>

	<section class="single-product-header">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h4 class="product-cat-title">
						<?= $category[0]->name ?>
					</h4>
				</div>
				<div class="col-12">
					<div class="single-product-breadcrumbs">
						<a href="/">דף הבית</a>|
						<?php if($category): ?>
							<a href="<?= get_term_link($category[0]) ?>">
								<?= $category[0]->name ?>
							</a>|
						<?php endif; ?>
						<?php if($brand): ?>
							<a href="<?= get_term_link($brand[0]) ?>">
								<?= $brand[0]->name ?>
							</a>|
						<?php endif; ?>
						<span><?php the_title(); ?></span>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="product-summary">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-12">
					<?php do_action('woocommerce_single_title_custom'); ?>
					<h6 class="product-brand">
						<?= $brand[0]->name ?>
					</h6>
					<?php
					/**
					 * Hook: woocommerce_single_product_summary.
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 * @hooked WC_Structured_Data::generate_product_data() - 60
					 */
					do_action( 'woocommerce_single_product_summary' );
					?>

					<?php if ($info): ?>
					<div class="product-item-info">
						<div id="accordion">
							<?php foreach ($info as $c => $i_item): ?>
							<div class="card">
								<div class="card-header" id="heading<?= $c ?>">
									<h5 class="mb-0">
										<button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?= $c ?>" aria-expanded="true" aria-controls="collapse<?= $c ?>">
											<?= $i_item['title'] ?>
										</button>
									</h5>
								</div>
								<div id="collapse<?= $c ?>" class="collapse" aria-labelledby="heading<?= $c ?>" data-parent="#accordion">
									<div class="card-body">
										<?= $i_item['content']; ?>
									</div>
								</div>
							</div>
							<?php endforeach; ?>
						</div>
					</div>
					<?php endif; ?>

				</div>
				<div class="col-lg-6 col-12 mt-lg-0 mt-4">
					<div class="product-gallery d-flex">
						<div class="gallery-slider" id="gallery-slider">
							<?php if(has_post_thumbnail($post_id)): ?>
								<div class="slider-item">
									<?= $product_thumb ?>
								</div>
							<?php endif; ?>
							<?php foreach ($product_gallery_images as $img): ?>
								<div class="slider-item">
									<?= $img ?>
								</div>
							<?php endforeach; ?>
							<?php if($product_video): ?>
								<div class="slider-item" id="video-holder" data-video="1">

								</div>
							<?php endif; ?>
						</div>
						<div class="thumbs pr-1" id="gallery-thumbs">
							<?php if(has_post_thumbnail($post_id)): ?>
								<div class="thumb-item centered">
									<?= $product_thumb ?>
								</div>
							<?php endif; ?>
							<?php foreach ($product_gallery_images as $img): ?>
								<div class="thumb-item">
									<?= $img ?>
								</div>
							<?php endforeach; ?>
							<?php if($product_video): ?>
								<div class="thumb-item" id="video-thumb" data-id="<?= getYoutubeId($product_video) ?>">
									<div class="play-overlay">
										<?= svg_simple(THEMEPATH . '/assets/images/olay.svg') ?>
									</div>
									<picture>
										<img src="<?= getYoutubeThumb($product_video) ?>" alt="">
									</picture>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<?php if($related_products): ?>
			<div class="col-12 related-products-col px-md-0">
				<div class="d-flex flex-column justify-content-start align-items-center">
					<h4 class="related-products-title">
						<?= $fields['related_products_title'] ? $fields['related_products_title'] : 'המוצרים אצלנו בגורדון לרכישה מיידית'; ?>
					</h4>
					<?php if ($fields['related_products_text']) : ?>
					<p class="related-products-text">
						<?= $fields['related_products_text']; ?>
					</p>
					<?php endif; ?>
				</div>
				<div class="related-slider">
					<?php

					echo get_template_part('views/partials/product', 'slider', [
							'products' => $related_products,
					]);

					?>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</section>

	<?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	do_action( 'woocommerce_after_single_product_summary' );
	?>
</div>
<?php
if ($slider_seo = $fields['prod_slider_seo']) {
	get_template_part('views/partials/content', 'slider_shop', [
			'content' => $slider_seo,
			'img' => $fields['prod_slider_img'],
			'title' => $fields['prod_slider_title'],
			'text' => $fields['prod_slider_text'],
			'link' => $fields['prod_slider_link'],
	]);
}
do_action( 'woocommerce_after_single_product' ); ?>
