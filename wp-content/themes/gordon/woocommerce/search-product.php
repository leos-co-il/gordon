<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

$price_per_100 =  get_post_meta($product->get_id(), 'price_per_100', true );
$in_wishlist = false;
if ( function_exists( 'YITH_WCWL' ) ) {
	$in_wishlist = YITH_WCWL()->is_product_in_wishlist( $product->get_id() );
}

?>

<div class="col-md-3">

	<div <?php wc_product_class( 'product-small-card', $product ); ?>>
		<?php if($in_wishlist): ?>
			<a href="<?= get_permalink(get_option('yith_wcwl_wishlist_page_id')); ?>" class="in-wishlist">
				<?= svg_simple(THEMEPATH . '/assets/images/like.svg') ?>
			</a>
		<?php endif; ?>

		<?php

			woocommerce_template_loop_product_link_open();
			woocommerce_show_product_loop_sale_flash();
			woocommerce_template_loop_product_thumbnail();
			woocommerce_template_loop_product_title();
			woocommerce_template_loop_rating();
			woocommerce_template_loop_price();
			woocommerce_template_loop_product_link_close();
			woocommerce_template_loop_add_to_cart();

		?>




		<span class="price-per-100">
				<?php if($price_per_100): ?>
					(
					<?= get_woocommerce_currency_symbol() . $price_per_100 ?>ל-100 מ״ל
					)
				<?php endif; ?>
			</span>

	</div>
</div>
