<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>

<section class="home-main-block" <?php if ($fields['home_main_img']) : ?>
		 style="background-image: url('<?= $fields['home_main_img']['url']; ?>')"
<?php endif; ?>>
	<div class="home-main-overlay">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 col-lg-8 col-auto">
					<h3 class="home-small-title"><?= $fields['home_small_title']; ?></h3>
					<h2 class="home-big-title"><?= $fields['home_big_title']; ?></h2>
					<?php if ($fields['home_main_link']) : ?>
						<div class="row justify-content-end">
							<div class="col-auto">
								<a href="<?= $fields['home_main_link']['url']; ?>" class="base-link">
									<?= (isset($fields['home_main_link']['title']) && $fields['home_main_link']['title']) ?
											$fields['home_main_link']['title'] : 'מעבר לקטגוריה לורם אפסום'; ?>
								</a>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php if ($fields['home_serv_prod_text'] || $fields['home_serv_prod_item']) : ?>
	<section class="serv-prod-block pad-70">
		<div class="container">
			<div class="row justify-content-center">
				<?php if ($fields['home_serv_prod_text']) : ?>
					<div class="col-xl-7 col-lg-8 col-12 col-serv-arrow">
						<div class="block-text">
							<?= $fields['home_serv_prod_text']; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
			<?php if ($fields['home_serv_prod_item']) : ?>
				<div class="row justify-content-center align-items-stretch prod-items-row">
					<?php foreach ($fields['home_serv_prod_item'] as $item) : ?>
						<div class="col-xl-6 col-12 serv-prod-col">
							<div class="serv-prod-wrap">
								<div class="row align-items-stretch h-100 row-serv-reverse">
									<?php if ($item['block_item_img']) : ?>
										<div class="col-sm-6 col-12 d-flex align-items-end">
											<img src="<?= $item['block_item_img']['url']; ?>" alt="team">
										</div>
									<?php endif; ?>
									<div class="col-sm-6 col-12 d-flex flex-column justify-content-center align-items-center order-content">
										<?php if ($item['block_item_small_title']) : ?>
											<h4 class="gveret-title"><?= $item['block_item_small_title']; ?></h4>
										<?php endif;
										if ($item['block_item_big_title']) : ?>
											<h3 class="item-title-serv"><?= $item['block_item_big_title']; ?></h3>
										<?php endif;
										if ($item['block_item_link']) : ?>
											<a href="<?= $item['block_item_link']['url']; ?>" class="base-link base-link-small">
												<?= (isset($item['block_item_link']['title']) && $item['block_item_link']['title']) ?
														$item['block_item_link']['title'] : 'עוד מידע'; ?>
											</a>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif; ?>
<section class="search-block pad-70">
	<div class="container">
		<div class="row justify-content-center">
			<?php if ($fields['home_search_text']) : ?>
			<div class="col-xl-7 col-lg-8 col-12">
				<div class="block-text">
					<?= $fields['home_search_text']; ?>
				</div>
			</div>
			<?php endif; ?>
			<div class="col-xl-10 col-lg-11 col-12 col-search-arrow">
				<?php
				$prod_areas = get_terms([
						'taxonomy' => 'prod_area',
						'hide_empty' => false,
				]);
				$prod_techs = get_terms([
						'taxonomy' => 'prod_tech',
						'hide_empty' => false,
				]);
				$ages = get_terms([
						'taxonomy' => 'prod_age',
						'hide_empty' => false,
						'parent' => 0,
				]); ?>
				<div class="search-section-back">
					<form method="get" action="">
						<div class="row align-items-stretch w-100">
							<?php if ($prod_areas): ?>
								<div class="form-group col-xl col-sm-6 col-12">
									<select id="inputArea" name="place-location" class="form-control">
										<option selected disabled><?= esc_html__('תחומי דעת','leos')  ?></option>
										<?php foreach($prod_areas as $area): ?>
											<option value="<?= $area->term_id ?>" data-id="<?= $area->term_id ?>">
												<?= $area->name ?>
											</option>
										<?php endforeach ?>
									</select>
								</div>
							<?php endif;
							if ($ages): ?>
								<div class="form-group col-xl-2 col-sm-6 col-12">
									<select id="inputAge" name="place-department" class="form-control age-input">
										<option selected disabled><?= esc_html__('גילאים','leos')  ?></option>
										<?php foreach($ages as $age): ?>
											<option value="<?= $age->term_id ?>" data-id="<?= $age->term_id ?>">
												<?= $age->name ?>
											</option>
										<?php endforeach ?>
									</select>
								</div>
							<?php endif;
							if ($prod_techs): ?>
								<div class="form-group col-xl-3 col-12">
									<select id="inputTypeTech" name="place-type" class="form-control tech-input">
										<option selected disabled><?= esc_html__('טכנולוגיות','leos')  ?></option>
										<?php foreach($prod_techs as $tech): ?>
											<option value="<?= $tech->term_id ?>"><?= $tech->name ?></option>
										<?php endforeach ?>
									</select>
								</div>
							<?php endif; ?>
							<div class="form-group col-xl col-12">
								<button type="submit" class="btn btn-search"><?= esc_html__('מצא לי תוצאה','leos') ?></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<?php if ($fields['slider_banner']) : ?>
	<section class="pad-70 slider-banner-block">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-12">
					<div class="banner-slider">
						<?php foreach ($fields['slider_banner'] as $x => $slide) : ?>
							<div>
								<div class="slide-banner-item row align-items-stretch">
									<?php if ($slide['slider_images']) : $first_img = array_shift($slide['slider_images']); ?>
										<div class="<?= ($x > 0) ? 'col-lg-6' : 'col-12'; ?> col-12 first-banner-wrap">
											<?php if ($first_img) : ?>
												<img src="<?= $first_img['url']; ?>" alt="banner">
											<?php endif; ?>
										</div>
									<?php endif; ?>
									<?php if ($slide['slider_images']) : ?>
										<div class="col-lg-6 col-12 d-flex flex-column justify-content-between col-2-banners">
											<?php foreach ($slide['slider_images'] as $image) : ?>
												<div class="slider-image-banner"
													 style="background-image: url('<?= $image['url']; ?>')"></div>
											<?php endforeach; ?>
										</div>
									<?php endif; ?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['h_block_prod_text'] || $fields['h_block_prod_custom']) : ?>
	<section class="custom-products-block courses-block">
		<div class="container">
			<div class="row align-items-center row-no-wrap">
				<div class="<?= $fields['h_block_prod_custom'] ? 'col-lg-5' : 'col-12'; ?> col-12 mb-lg-0 mb-4">
					<?php if ($fields['h_block_prod_text']) : ?>
					<div class="courses-block-text">
						<?= $fields['h_block_prod_text']; ?>
					</div>
					<?php endif;
					if ($fields['h_block_prod_link']) : ?>
						<div class="row justify-content-start">
							<div class="col-auto">
								<a href="<?= $fields['h_block_prod_link']['url']; ?>" class="base-link">
									<?= (isset($fields['h_block_prod_link']['title']) && $fields['h_block_prod_link']['title']) ?
											$fields['h_block_prod_link']['title'] : 'לכל המוצרים'; ?>
								</a>
							</div>
						</div>
					<?php endif; ?>
				</div>
				<?php if ($fields['h_block_prod_custom']) : ?>
					<div class="col-lg-9 col-12">
						<div class="row justify-content-center align-items-stretch">
							<?php foreach ($fields['h_block_prod_custom'] as $course) : ?>
								<div class="col-md-4 col-12 mb-4">
									<a class="course-item" href="<?= isset($course['prod_custom_link']) ? $course['prod_custom_link'] : '#'; ?>">
										<?php if ($course['prod_custom_img']) : ?>
											<div class="course-item-img"
												 style="background-image: url('<?= $course['prod_custom_img']['url']; ?>')"></div>
										<?php endif; ?>
										<span class="course-title"><?= $course['prod_custom_title']; ?></span>
									</a>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['home_course_text'] || $fields['home_course_item']) : ?>
	<section class="courses-block">
		<div class="container">
			<div class="row">
				<div class="<?= $fields['home_course_img'] ? 'col-lg-6' : 'col-12'; ?> col-12 mb-lg-0 mb-4">
					<?php if ($fields['home_course_text']) : ?>
						<div class="courses-block-text">
							<?= $fields['home_course_text']; ?>
						</div>
					<?php endif;
					if ($fields['home_course_item']) : ?>
						<div class="row justify-content-center align-items-stretch">
							<?php foreach ($fields['home_course_item'] as $course) : ?>
								<div class="col-md-6 col-12 mb-4">
									<a class="course-item" href="<?= isset($course['course_link']) ? $course['course_link'] : '#'; ?>">
										<?php if ($course['course_img']) : ?>
											<div class="course-item-img"
												 style="background-image: url('<?= $course['course_img']['url']; ?>')"></div>
										<?php endif; ?>
										<span class="course-title"><?= $course['course_title']; ?></span>
									</a>
								</div>
							<?php endforeach; ?>
						</div>
					<?php endif;
					if ($fields['home_course_link']) : ?>
					<div class="row justify-content-end">
						<div class="col-auto">
							<a href="<?= $fields['home_course_link']['url']; ?>" class="base-link">
								<?= (isset($fields['home_course_link']['title']) && $fields['home_course_link']['title']) ?
										$fields['home_course_link']['title'] : 'לכל הקורסים'; ?>
							</a>
						</div>
					</div>
					<?php endif; ?>
				</div>
				<?php if ($fields['home_course_img']) : ?>
					<div class="col-lg-6 d-flex align-items-end justify-content-center">
						<img src="<?= $fields['home_course_img']['url']; ?>" alt="courses">
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif;
if($fields['home_prod_slider'] || $fields['home_prod_slider_text']): ?>
	<section class="home-slider-products">
		<div class="container">
			<div class="row">
				<?php if ($fields['home_prod_slider_text']) : ?>
					<div class="col-12">
						<div class="block-text">
							<?= $fields['home_prod_slider_text']; ?>
						</div>
					</div>
				<?php endif; ?>
				<div class="col-12 px-md-0">
					<div class="related-slider">
						<?php

						echo get_template_part('views/partials/product', 'slider', [
								'products' => $fields['home_prod_slider'],
						]);

						?>
					</div>
				</div>
			</div>
			<?php if ($fields['home_prod_slider_link']) : ?>
				<div class="row justify-content-end">
					<div class="col-auto">
						<a href="<?= $fields['home_prod_slider_link']['url']; ?>" class="base-link">
							<?= (isset($fields['home_prod_slider_link']['title']) && $fields['home_prod_slider_link']['title']) ?
									$fields['home_prod_slider_link']['title'] : 'לכל המוצרים'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if($fields['slider_benefits']) : ?>
	<section class="benefits-block">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="benefits-slider-block numbered-slider">
						<div class="row">
							<div class="<?= ($fields['slider_benefits_img']) ? 'col-xl' : 'col-12' ?> col-12">
								<?php if ($fields['slider_benefits_title']) : ?>
									<h2 class="ben-block-title"><?= $fields['slider_benefits_title']; ?></h2>
								<?php endif; ?>
								<div class="number-slider" dir="rtl">
									<?php foreach ($fields['slider_benefits'] as $benefit) : ?>
										<div>
											<div class="benefit-text-wrap">
												<div class="benefit-text">
													<?= $benefit['benefit_content']; ?>
												</div>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
							<?php if ($fields['slider_benefits_img']) : ?>
								<div class="col-xl-auto col-hide-benefits">
									<img src="<?= $fields['slider_benefits_img']['url']; ?>" alt="benefits">
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($slider_seo = $fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $slider_seo,
			'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
