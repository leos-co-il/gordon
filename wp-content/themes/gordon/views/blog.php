<?php
/*
Template Name: מאמרים
*/

get_header();
$fields = get_fields();
$posts = get_posts([
		'numberposts' => -1,
		'post_type' => 'post',
		'suppress_filters' => false
]);
?>



<div class="inner-header">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1><?= the_title() ?></h1>
			</div>
			<div class="col-12">
				<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
				}
				?>
			</div>
		</div>
	</div>
</div>
<article class="post-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<div class="post-text-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($posts) : ?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($posts as $post) : ?>
					<div class="col-lg-4 col-sm-6 col-12 mb-3">
						<?php get_template_part('views/partials/card', 'post',
								[
										'post' => $post,
								]); ?>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php
if ($slider_seo = $fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $slider_seo,
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
