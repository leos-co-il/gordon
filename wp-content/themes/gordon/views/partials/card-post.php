<?php if(isset($args['post']) && $args['post']) :
	$link = get_the_permalink($args['post']); ?>
	<div class="post-item">
		<a class="post-image" <?php if (has_post_thumbnail($args['post'])) : ?>
			style="background-image: url('<?= postThumb($args['post']); ?>')" <?php endif; ?>
		   href="<?= $link; ?>"></a>
		<div class="post-item-content">
			<a class="post-item-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
			<p class="post-item-text">
				<?= text_preview($args['post']->post_content, 10); ?>
			</p>
		</div>
		<a href="<?= $link; ?>" class="post-link">
			המשך למאמר
		</a>
	</div>
<?php endif; ?>
