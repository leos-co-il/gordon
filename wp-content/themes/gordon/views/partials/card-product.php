<?php // Ensure visibility.
$product = (isset($args['product']) && $args['product']) ? wc_get_product($args['product']) : '';
if ( empty( $product ) || ! $product->is_visible() ) {
return;
}

$in_wishlist = false;
if ( function_exists( 'YITH_WCWL' ) ) {
$in_wishlist = YITH_WCWL()->is_product_in_wishlist( $product->get_id() );
}
$item_terms = wp_get_post_terms($product->get_id(), [
'brand', 'product_cat'
]);


$data_attr = build_data_attr($item_terms);

?>
<div <?php wc_product_class( 'product-small-card ' . $data_attr, $product ); ?> data-price="<?= $product->get_price() ?>">
	<?php if($in_wishlist): ?>
		<a href="<?= get_permalink(get_option('yith_wcwl_wishlist_page_id')); ?>" class="in-wishlist">
			<?= svg_simple(THEMEPATH . '/assets/images/like_full.svg') ?>
		</a>
	<?php endif; ?>
	<div class="top">
		<?php
		/**
		 * Hook: woocommerce_before_shop_loop_item.
		 *
		 * @hooked woocommerce_template_loop_product_link_open - 10
		 */
		do_action( 'woocommerce_before_shop_loop_item' );

		/**
		 * Hook: woocommerce_before_shop_loop_item_title.
		 *
		 * @hooked woocommerce_show_product_loop_sale_flash - 10
		 * @hooked woocommerce_template_loop_product_thumbnail - 10
		 */


		do_action( 'woocommerce_before_shop_loop_item_title' );

		?>
	</div>

	<div class="bottom">
		<?php

		/**
		 * Hook: woocommerce_shop_loop_item_title.
		 *
		 * @hooked woocommerce_template_loop_product_title - 10
		 */
		do_action( 'woocommerce_shop_loop_item_title' );

		/**
		 * Hook: woocommerce_after_shop_loop_item_title.
		 *
		 * @hooked woocommerce_template_loop_rating - 5
		 * @hooked woocommerce_template_loop_price - 10
		 */
		do_action( 'woocommerce_after_shop_loop_item_title' );

		/**
		 * Hook: woocommerce_after_shop_loop_item.
		 *
		 * @hooked woocommerce_template_loop_product_link_close - 5
		 * @hooked woocommerce_template_loop_add_to_cart - 10
		 */
		do_action( 'woocommerce_after_shop_loop_item' );
		?>
	</div>
</div>
