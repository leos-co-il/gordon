<?php if (isset($args['content']) && $args['content']) :
	$sliderImage = (isset($args['img']) && $args['img']) ? $args['img'] : NULL; ?>
	<section class="slider-base-wrap arrows-slider">
		<div class="container">
			<div class="row justify-content-center align-items-center">
				<div class="<?= $sliderImage ? 'col-lg-6' : 'col-12'; ?> col-12 slider-wrap-col">
					<div class="slider-text-wrap">
						<div class="base-slider" dir="rtl">
							<?php foreach ($args['content'] as $content) : ?>
								<div>
									<div class="base-output-slider"><?= $content['content']; ?></div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
				<?php if ($sliderImage) : ?>
					<div class="col-lg-6 d-flex justify-content-center align-items-center">
						<img src="<?= $sliderImage['url']; ?>" class="slider-img">
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif; ?>
