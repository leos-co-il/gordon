<?php if (isset($args['content']) && $args['content']) :
	$sliderImage = (isset($args['img']) && $args['img']) ? $args['img'] : NULL; ?>
	<section class="slider-base-wrap arrows-slider">
		<div class="container">
			<div class="row justify-content-center align-items-center">
				<div class="<?= $sliderImage ? 'col-lg-6' : 'col-12'; ?> col-12 slider-wrap-col">
					<div class="slider-text-wrap">
						<div class="base-slider" dir="rtl">
							<?php foreach ($args['content'] as $content) : ?>
								<div>
									<div class="base-output-slider"><?= $content['content']; ?></div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
				<?php if ($sliderImage) : ?>
					<div class="col-lg-6 d-flex justify-content-center align-items-center">
						<div class="prod-slider-image" style="background-image: url('<?= $sliderImage['url']; ?>')">
							<div class="overlay-slide">
								<div class="d-flex flex-column justify-content-center align-items-center">
									<?php if (isset($args['title']) && $args['title']) : ?>
										<h3 class="prod-slide-title">
											<?= $args['title']; ?>
										</h3>
									<?php endif;
									if (isset($args['text']) && $args['text']) : ?>
										<p class="prod-slide-text">
											<?= $args['text']; ?>
										</p>
									<?php endif;
									if (isset($args['link']) && $args['link']) : ?>
										<a class="prod-slide-link" href="<?= $args['link']['url']; ?>">
											<?= (isset($args['link']['title']) && $args['link']['title']) ? $args['link']['title']
												: 'shop now'; ?>
										</a>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif; ?>
