<?php
/*
Template Name: גלריה
*/

get_header();
$fields = get_fields();

?>



<div class="inner-header">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1><?= the_title() ?></h1>
			</div>
			<div class="col-12">
				<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
				}
				?>
			</div>
		</div>
	</div>
</div>
<?php if ($fields['gallery_main']) : ?>
	<section class="main-gallery">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="block-text">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($fields['gallery_main'] as $img) : ?>
				<div class="col-xl-3 col-lg-4 col-sm-6 col-12 mb-1">
					<a href="<?= $img['url']; ?>" data-lightbox="gallery" class="gallery-item"
					style="background-image: url('<?= $img['url']; ?>')"></a>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php endif;
if ($slider_seo = $fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $slider_seo,
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
