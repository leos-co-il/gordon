<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$mail = opt('mail');
$fax = opt('fax');
$address = opt('address');
$open_hours = opt('open_hours');
$map = opt('map_image');


?>


<div class="inner-header">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1><?= the_title() ?></h1>
			</div>
			<div class="col-12">
				<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
				}
				?>
			</div>
		</div>
	</div>
</div>
<section class="contact-form">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-lg-10 col-11">
				<div class="row">
					<div class="col-12">
						<div class="contact-text">
							<?php the_content(); ?>
						</div>
					</div>
					<div class="col-12 main-contact-form">
						<?php getForm('12');
						if (has_post_thumbnail()) : ?>
							<div class="contact-img">
								<img src="<?= postThumb(); ?>" alt="contact-img">
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php if ($map || $tel || $address || $mail) : ?>
	<section class="my-4">
		<div class="container-fluid">
			<?php if ($fields['map_block_title']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<h2 class="contacts-block-title"><?= $fields['map_block_title']; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center">
				<div class="col-lg-10 col-11 contacts-block">
					<div class="row align-items-center">
						<?php if ($tel || $address || $mail) : ?>
							<div class="col-lg-6 col-12 contact-items-col">
								<?php if ($address) : ?>
									<a href="tel:<?= $tel; ?>" class="contact-info">
										<span class="geo-wrap">
											<?= svg_simple(ICONS.'foo-geo.svg'); ?>
										</span>
										<span class="base-text-contact"><?= $address; ?></span>
									</a>
								<?php endif;
								if ($tel) : ?>
									<a href="tel:<?= $tel; ?>" class="contact-info">
										<img src="<?= ICONS ?>form-tel.png" alt="tel">
										<span class="base-text-contact"><?= $tel; ?></span>
									</a>
								<?php endif;
								if ($mail) : ?>
									<a href="tel:<?= $tel; ?>" class="contact-info">
										<img src="<?= ICONS ?>form-mail.png" alt="mail">
										<span class="base-text-contact"><?= $mail; ?></span>
									</a>
								<?php endif; ?>
							</div>
						<?php endif;
						if ($map) : ?>
							<div class="col-lg-6 col-12 map-col">
								<img src="<?= $map['url']; ?>" alt="map" class="map-img">
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['clients_block_text'] || $fields['clients']) : ?>
	<section class="contact-clients-block">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-lg-10 col-11">
					<?php if ($fields['clients_block_text']) : ?>
						<div class="row justify-content-center">
							<div class="col-auto">
								<div class="block-text">
									<?= $fields['clients_block_text']; ?>
								</div>
							</div>
						</div>
					<?php endif;
					if ($fields['clients']) : ?>
						<div class="row justify-content-center align-items-stretch">
							<?php foreach ($fields['clients'] as $client) : ?>
							<div class="col-auto">
								<div class="client-logo-wrap">
									<img src="<?= $client['url']; ?>" alt="logo" class="client-logo">
								</div>
							</div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>

<?php get_footer(); ?>
