<!DOCTYPE HTML>
<html
		<?php language_attributes(); ?>>
<head data-version="<?= get_option('search_json_version') ?>">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
	<?php
	preload_resources();
	wp_head();
	?>
</head>
<body <?php body_class(ENV); ?>>
<?php

$flash_msg = opt('flash_msg');
$_pre_search_products = new WP_Query([
		'post_type' 	 => 'product',
		'posts_per_page' => 4,
		'orderby'        => 'rand'
]);

?>
<header>
	<?php if($flash_msg): ?>
		<div class="flash-msg-wrap" data-aos="fade-down">
			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col-xl-10">
						<div class="row justify-content-center align-items-center">
							<div class="col-md col-auto"></div>
							<div class="col-md-6 col-11">
								<div class="flash-msg-slider">
									<?php foreach ($flash_msg as $msg): ?>
										<div class="single-msg d-flex centered">
											<?php if($msg['link']): ?>
											<a href="<?= $msg['link'] ?>" title="<?= $msg['title'] ?>">
												<?php endif; ?>
												<span class="msg-text"><?= $msg['title'] ?></span>
												<?php if($msg['link']): ?>
											</a>
										<?php endif; ?>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
							<?php if ($tel = opt('tel')) : ?>
								<div class="col-md col-auto tel-col">
									<a href="tel:<?= $tel; ?>" class="header-tel">
										<span class="text">התקשרו </span>
										<span><?= ' '.$tel; ?></span>
									</a>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container-fluid sticky top-nav-container">
		<div class="row h-100 justify-content-center">
			<div class="col-xl-10 col-md-11 col-12 h-100">
				<div class="row h-100 justify-content-between align-items-center">
					<?php if ($logo = opt('logo')) : ?>
					<div class="col-xl-2 col-md-3 col-4">
						<a href="/">
							<img src="<?= $logo['url']; ?>" alt="logo">
						</a>
					</div>
					<?php endif; ?>
					<div class="col-md d-flex col-auto justify-content-start align-items-center">
						<button class="hamburger mega-menu-btn hamburger--collapse" type="button">
  					<span class="hamburger-box">
    					<span class="hamburger-inner"></span>
  					</span>
							<span class="menu-btn-txt">תפריט</span>
						</button>
						<button id="search-btn">
							<?= svg_simple(THEMEPATH . '/assets/images/loupe.svg') ?>
						</button>
					</div>
					<div class="col-md-4 col d-flex justify-content-end align-items-center header-btns">
						<a href="<?= get_permalink(get_option('woocommerce_myaccount_page_id')); ?>" class="header-btn">
							<?= svg_simple(THEMEPATH . '/assets/images/user.svg') ?>
						</a>
						<?php if(defined( 'YITH_WCWL' )): ?>
							<a href="<?= get_permalink(get_option('yith_wcwl_wishlist_page_id')); ?>" class="header-btn">
								<?= svg_simple(THEMEPATH . '/assets/images/heart.svg') ?>
								<span class="count-circle" id="yith-w-count">
							<?= esc_html(yith_wcwl_count_all_products()) ?>
						</span>
							</a>
						<?php endif; ?>
						<a href="#" class="header-btn" id="mini-cart">
							<?= svg_simple(THEMEPATH . '/assets/images/shopping-cart.svg') ?>
							<span class="count-circle" id="cart-count">
						<!--@TODO update cart count-->
						<?= WC()->cart->get_cart_contents_count(); ?>
					</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<?php if ($tel = opt('tel')) : ?>
	<a href="tel:<?= $tel; ?>" class="header-tel-fixed">
		<img src="<?= ICONS ?>form-tel.png" alt="tel-icon" class="tel-icon">
	</a>
<?php endif; ?>
<div class="mega-menu">
	<div class="menu-background">
		<div class="tile"></div>
		<div class="tile"></div>
		<div class="tile"></div>
		<div class="tile"></div>
		<div class="tile"></div>
		<div class="tile"></div>
		<div class="tile"></div>
		<div class="tile"></div>
	</div>
	<div class="container-fluid menu-foreground py-md-5 py-4">
		<div class="row justify-content-start">
			<div class="col-md-1 position-relative">
				<button id="close-menu">
					<?= svg_simple(THEMEPATH . '/assets/images/cancel.svg') ?>
				</button>
			</div>
			<div class="col-md-auto mega-top">
				<?php getMenu('mega-menu-top', 1) ?>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-md-6 mega-right">
				<?php getMenu('mega-menu-right', 1) ?>
			</div>
			<div class="col-md-6 mega-left">
				<?php getMenu('mega-menu-left', 1, '', '', 'product') ?>
			</div>
		</div>
	</div>
</div>
<div class="search-wrap">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-md-9 d-flex mobile-search-header">
				<button id="search-close">
					<?= svg_simple(THEMEPATH . '/assets/images/cancel.svg') ?>
				</button>
				<form action="">
					<input type="text" placeholder="חיפוש" name="search-query" id="search-query">
					<button type="submit">
						<?= svg_simple(THEMEPATH . '/assets/images/search.svg') ?>
					</button>
				</form>
			</div>
			<div class="col-12">
				<div class="search-query-result row" id="search-query-result">
					<?php

					if($_pre_search_products->have_posts()){
						foreach ($_pre_search_products->posts as $_p){
							echo '<div class="col-xl-3 col-lg-4 col-md-6 col-12 mb-3">';
							setup_postdata($GLOBALS['post'] =& $_p);
							wc_get_template_part( 'content', 'product' );
							echo '</div>';
						}
						wp_reset_postdata();
					}

					?>
				</div>
			</div>
		</div>
	</div>
</div>
<!---->
<!--<div class="mini-cart-wrap" id="mini-cart-el">-->
<!--	--><?php //wc_get_template( 'cart/mini-cart.php' ); ?>
<!--</div>-->
<!---->


<?php
$facebook = opt('facebook');
$youtube = opt('youtube');
$linkedin = opt('linkedin');
$socials = get_social_links(['facebook', 'youtube', 'linkedin']);
if ($socials) : ?>
<div class="fixed-socials">
	<?php foreach ($socials as $fix_social) : ?>
		<a href="<?= $fix_social['url']; ?>" class="fix-social-link">
			<?= svg_simple(ICONS.$fix_social['name'].'.svg'); ?>
		</a>
	<?php endforeach; ?>
	<div class="fix-social-link pop-trigger">
		<img src="<?= ICONS ?>mail.png">
	</div>
</div>
<?php endif; ?>
