-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jul 13, 2021 at 02:54 PM
-- Server version: 5.7.26
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gordon`
--

-- --------------------------------------------------------

--
-- Table structure for table `fmn_actionscheduler_actions`
--

CREATE TABLE `fmn_actionscheduler_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `hook` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `scheduled_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `scheduled_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `args` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `schedule` longtext COLLATE utf8mb4_unicode_520_ci,
  `group_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_attempt_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `claim_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `extended_args` varchar(8000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_actionscheduler_actions`
--

INSERT INTO `fmn_actionscheduler_actions` (`action_id`, `hook`, `status`, `scheduled_date_gmt`, `scheduled_date_local`, `args`, `schedule`, `group_id`, `attempts`, `last_attempt_gmt`, `last_attempt_local`, `claim_id`, `extended_args`) VALUES
(7, 'action_scheduler/migration_hook', 'complete', '2021-07-10 10:44:38', '2021-07-10 13:44:38', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1625913878;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1625913878;}', 1, 1, '2021-07-10 10:44:46', '2021-07-10 13:44:46', 0, NULL),
(8, 'wc-admin_import_customers', 'complete', '2021-07-11 10:13:51', '2021-07-11 13:13:51', '[1]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1625998431;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1625998431;}', 2, 1, '2021-07-11 10:14:38', '2021-07-11 13:14:38', 0, NULL),
(9, 'wc-admin_import_customers', 'complete', '2021-07-12 07:43:42', '2021-07-12 10:43:42', '[1]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1626075822;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1626075822;}', 2, 1, '2021-07-12 07:44:11', '2021-07-12 10:44:11', 0, NULL),
(10, 'wc-admin_import_customers', 'complete', '2021-07-13 05:47:49', '2021-07-13 08:47:49', '[1]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1626155269;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1626155269;}', 2, 1, '2021-07-13 05:48:49', '2021-07-13 08:48:49', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_actionscheduler_claims`
--

CREATE TABLE `fmn_actionscheduler_claims` (
  `claim_id` bigint(20) UNSIGNED NOT NULL,
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_actionscheduler_groups`
--

CREATE TABLE `fmn_actionscheduler_groups` (
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_actionscheduler_groups`
--

INSERT INTO `fmn_actionscheduler_groups` (`group_id`, `slug`) VALUES
(1, 'action-scheduler-migration'),
(2, 'wc-admin-data');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_actionscheduler_logs`
--

CREATE TABLE `fmn_actionscheduler_logs` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `log_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_actionscheduler_logs`
--

INSERT INTO `fmn_actionscheduler_logs` (`log_id`, `action_id`, `message`, `log_date_gmt`, `log_date_local`) VALUES
(1, 7, 'action created', '2021-07-10 10:43:38', '2021-07-10 13:43:38'),
(2, 7, 'action started via Async Request', '2021-07-10 10:44:46', '2021-07-10 13:44:46'),
(3, 7, 'action complete via Async Request', '2021-07-10 10:44:46', '2021-07-10 13:44:46'),
(4, 8, 'הפעולה נוצרה', '2021-07-11 10:13:46', '2021-07-11 13:13:46'),
(5, 8, 'הפעולה התחילה דרך WP Cron', '2021-07-11 10:14:38', '2021-07-11 13:14:38'),
(6, 8, 'הפעולה הושלמה דרך WP Cron', '2021-07-11 10:14:38', '2021-07-11 13:14:38'),
(7, 9, 'הפעולה נוצרה', '2021-07-12 07:43:37', '2021-07-12 10:43:37'),
(8, 9, 'הפעולה התחילה דרך Async Request', '2021-07-12 07:44:11', '2021-07-12 10:44:11'),
(9, 9, 'הפעולה הושלמה דרך Async Request', '2021-07-12 07:44:11', '2021-07-12 10:44:11'),
(10, 10, 'הפעולה נוצרה', '2021-07-13 05:47:44', '2021-07-13 08:47:44'),
(11, 10, 'הפעולה התחילה דרך WP Cron', '2021-07-13 05:48:49', '2021-07-13 08:48:49'),
(12, 10, 'הפעולה הושלמה דרך WP Cron', '2021-07-13 05:48:49', '2021-07-13 08:48:49');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_commentmeta`
--

CREATE TABLE `fmn_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_comments`
--

CREATE TABLE `fmn_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_comments`
--

INSERT INTO `fmn_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'מגיב וורדפרס', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2021-07-08 20:04:37', '2021-07-08 17:04:37', 'היי, זו תגובה.\nכדי לשנות, לערוך, או למחוק תגובות, יש לגשת למסך התגובות בלוח הבקרה.\nצלמית המשתמש של המגיב מגיעה מתוך <a href=\"https://gravatar.com\">גראווטר</a>.', 0, '1', '', 'comment', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_links`
--

CREATE TABLE `fmn_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_options`
--

CREATE TABLE `fmn_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_options`
--

INSERT INTO `fmn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://gordon:8888', 'yes'),
(2, 'home', 'http://gordon:8888', 'yes'),
(3, 'blogname', 'gordon', 'yes'),
(4, 'blogdescription', 'אתר וורדפרס חדש', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'maxf@leos.co.il', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j בF Y', 'yes'),
(24, 'time_format', 'G:i', 'yes'),
(25, 'links_updated_date_format', 'j בF Y G:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:8:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:33:\"classic-editor/classic-editor.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:23:\"debug-bar/debug-bar.php\";i:4;s:33:\"jquery-updater/jquery-updater.php\";i:5;s:27:\"woocommerce/woocommerce.php\";i:6;s:24:\"wordpress-seo/wp-seo.php\";i:7;s:34:\"yith-woocommerce-wishlist/init.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'gordon', 'yes'),
(41, 'stylesheet', 'gordon', 'yes'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '49752', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'Asia/Jerusalem', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '67', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'admin_email_lifespan', '1641315876', 'yes'),
(94, 'initial_db_version', '45805', 'yes'),
(95, 'fmn_user_roles', 'a:10:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:115:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;s:20:\"wpseo_manage_options\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:11:\"leos_client\";a:2:{s:4:\"name\";s:11:\"Leos Client\";s:12:\"capabilities\";a:124:{s:16:\"activate_plugins\";b:0;s:19:\"delete_others_pages\";b:1;s:19:\"delete_others_posts\";b:1;s:12:\"delete_pages\";b:1;s:12:\"delete_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:14:\"edit_dashboard\";b:1;s:17:\"edit_others_pages\";b:1;s:17:\"edit_others_posts\";b:1;s:10:\"edit_pages\";b:1;s:10:\"edit_posts\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:18:\"edit_theme_options\";b:1;s:6:\"export\";b:0;s:6:\"import\";b:0;s:10:\"list_users\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:13:\"promote_users\";b:0;s:13:\"publish_pages\";b:1;s:13:\"publish_posts\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:4:\"read\";b:1;s:12:\"remove_users\";b:0;s:13:\"switch_themes\";b:0;s:12:\"upload_files\";b:1;s:11:\"update_core\";b:0;s:14:\"update_plugins\";b:0;s:13:\"update_themes\";b:0;s:15:\"install_plugins\";b:0;s:14:\"install_themes\";b:0;s:13:\"delete_themes\";b:0;s:14:\"delete_plugins\";b:0;s:12:\"edit_plugins\";b:0;s:11:\"edit_themes\";b:0;s:10:\"edit_files\";b:0;s:10:\"edit_users\";b:0;s:9:\"add_users\";b:0;s:12:\"create_users\";b:0;s:12:\"delete_users\";b:0;s:15:\"unfiltered_html\";b:1;s:18:\"manage_woocommerce\";b:1;s:25:\"manage_woocommerce_orders\";b:1;s:26:\"manage_woocommerce_coupons\";b:1;s:27:\"manage_woocommerce_products\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:10:\"copy_posts\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;s:17:\"edit_shop_webhook\";b:1;s:17:\"read_shop_webhook\";b:1;s:19:\"delete_shop_webhook\";b:1;s:18:\"edit_shop_webhooks\";b:1;s:25:\"edit_others_shop_webhooks\";b:1;s:21:\"publish_shop_webhooks\";b:1;s:26:\"read_private_shop_webhooks\";b:1;s:20:\"delete_shop_webhooks\";b:1;s:28:\"delete_private_shop_webhooks\";b:1;s:30:\"delete_published_shop_webhooks\";b:1;s:27:\"delete_others_shop_webhooks\";b:1;s:26:\"edit_private_shop_webhooks\";b:1;s:28:\"edit_published_shop_webhooks\";b:1;s:25:\"manage_shop_webhook_terms\";b:1;s:23:\"edit_shop_webhook_terms\";b:1;s:25:\"delete_shop_webhook_terms\";b:1;s:25:\"assign_shop_webhook_terms\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"edit_theme_options\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}s:13:\"wpseo_manager\";a:2:{s:4:\"name\";s:11:\"SEO Manager\";s:12:\"capabilities\";a:38:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;s:20:\"wpseo_manage_options\";b:1;s:23:\"view_site_health_checks\";b:1;}}s:12:\"wpseo_editor\";a:2:{s:4:\"name\";s:10:\"SEO Editor\";s:12:\"capabilities\";a:36:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;}}}', 'yes'),
(96, 'fresh_site', '0', 'yes'),
(97, 'WPLANG', 'he_IL', 'yes'),
(98, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'sidebars_widgets', 'a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(104, 'cron', 'a:22:{i:1626177214;a:1:{s:26:\"action_scheduler_run_queue\";a:1:{s:32:\"0d04ed39571b55704c122d726248bbac\";a:3:{s:8:\"schedule\";s:12:\"every_minute\";s:4:\"args\";a:1:{i:0;s:7:\"WP Cron\";}s:8:\"interval\";i:60;}}}i:1626177311;a:1:{s:25:\"wpseo_ping_search_engines\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1626177878;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1626180154;a:1:{s:34:\"yith_wcwl_delete_expired_wishlists\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1626180217;a:1:{s:33:\"wc_admin_process_orders_milestone\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1626180224;a:1:{s:29:\"wc_admin_unsnooze_admin_notes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1626180692;a:1:{s:32:\"woocommerce_cancel_unpaid_orders\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1626183815;a:1:{s:24:\"woocommerce_cleanup_logs\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1626194615;a:1:{s:28:\"woocommerce_cleanup_sessions\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1626195877;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1626195878;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1626195887;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1626195888;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1626196122;a:1:{s:18:\"wp_https_detection\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1626210000;a:1:{s:27:\"woocommerce_scheduled_sales\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1626216129;a:2:{s:13:\"wpseo-reindex\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:31:\"wpseo_permalink_structure_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1626259417;a:1:{s:14:\"wc_admin_daily\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1626259425;a:2:{s:33:\"woocommerce_cleanup_personal_data\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:30:\"woocommerce_tracker_send_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1626455322;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}i:1626648129;a:1:{s:16:\"wpseo_ryte_fetch\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}i:1627209875;a:1:{s:25:\"woocommerce_geoip_updater\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:11:\"fifteendays\";s:4:\"args\";a:0:{}s:8:\"interval\";i:1296000;}}}s:7:\"version\";i:2;}', 'yes'),
(105, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'recovery_keys', 'a:1:{s:22:\"PpTxQVpf5GSDoE5X8lHty5\";a:2:{s:10:\"hashed_key\";s:34:\"$P$BtZszT/A0ytJ0s/rWTEsvWHqn4bce4.\";s:10:\"created_at\";i:1626088973;}}', 'yes'),
(123, '_site_transient_timeout_browser_619828cbc593f946769500632b389ffc', '1626368687', 'no'),
(124, '_site_transient_browser_619828cbc593f946769500632b389ffc', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"91.0.4472.101\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(125, '_site_transient_timeout_php_check_fb6df547cfb7d95cb9b49b8301cad3ab', '1626368688', 'no'),
(126, '_site_transient_php_check_fb6df547cfb7d95cb9b49b8301cad3ab', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(139, 'disallowed_keys', '', 'no'),
(140, 'comment_previously_approved', '1', 'yes'),
(141, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(142, 'auto_update_core_dev', 'enabled', 'yes'),
(143, 'auto_update_core_minor', 'enabled', 'yes'),
(144, 'auto_update_core_major', 'unset', 'yes'),
(145, 'finished_updating_comment_type', '1', 'yes'),
(146, 'db_upgraded', '', 'yes'),
(150, 'https_detection_errors', 'a:1:{s:20:\"https_request_failed\";a:1:{i:0;s:26:\"בקשת HTTPS נכשלה.\";}}', 'yes'),
(151, 'can_compress_scripts', '1', 'no'),
(156, 'recently_activated', 'a:0:{}', 'yes'),
(157, 'theme_mods_twentytwenty', 'a:1:{s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1625764541;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}', 'yes'),
(158, 'current_theme', 'Gordon', 'yes'),
(159, 'theme_mods_twentytwentyone', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1625764543;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(160, 'theme_switched', '', 'yes'),
(162, 'theme_mods_gordon', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:8:{s:11:\"header-menu\";i:16;s:16:\"footer-main-menu\";i:17;s:18:\"footer-second-menu\";i:18;s:17:\"footer-third-menu\";i:19;s:18:\"footer-fourth-menu\";i:20;s:13:\"mega-menu-top\";i:21;s:15:\"mega-menu-right\";i:22;s:14:\"mega-menu-left\";i:23;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(170, 'acf_version', '5.9.8', 'yes'),
(171, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.4.1\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1625764646;s:7:\"version\";s:5:\"5.4.1\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(190, '_transient_health-check-site-status-result', '{\"good\":12,\"recommended\":7,\"critical\":1}', 'yes'),
(195, 'acf_pro_license', 'YToyOntzOjM6ImtleSI7czo3MjoiYjNKa1pYSmZhV1E5TmpRNE5USjhkSGx3WlQxd1pYSnpiMjVoYkh4a1lYUmxQVEl3TVRVdE1Ea3RNak1nTURVNk1qQTZNVFE9IjtzOjM6InVybCI7czoxODoiaHR0cDovL2dvcmRvbjo4ODg4Ijt9', 'yes'),
(206, 'action_scheduler_hybrid_store_demarkation', '6', 'yes'),
(207, 'schema-ActionScheduler_StoreSchema', '3.0.1625913814', 'yes'),
(208, 'schema-ActionScheduler_LoggerSchema', '2.0.1625913814', 'yes'),
(211, 'woocommerce_schema_version', '430', 'yes'),
(212, 'woocommerce_store_address', 'חיפה', 'yes'),
(213, 'woocommerce_store_address_2', '', 'yes'),
(214, 'woocommerce_store_city', 'חיפה', 'yes'),
(215, 'woocommerce_default_country', 'IL', 'yes'),
(216, 'woocommerce_store_postcode', '3086100', 'yes'),
(217, 'woocommerce_allowed_countries', 'all', 'yes'),
(218, 'woocommerce_all_except_countries', 'a:0:{}', 'yes'),
(219, 'woocommerce_specific_allowed_countries', 'a:0:{}', 'yes'),
(220, 'woocommerce_ship_to_countries', '', 'yes'),
(221, 'woocommerce_specific_ship_to_countries', 'a:0:{}', 'yes'),
(222, 'woocommerce_default_customer_address', 'base', 'yes'),
(223, 'woocommerce_calc_taxes', 'no', 'yes'),
(224, 'woocommerce_enable_coupons', 'yes', 'yes'),
(225, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(226, 'woocommerce_currency', 'ILS', 'yes'),
(227, 'woocommerce_currency_pos', 'left', 'yes'),
(228, 'woocommerce_price_thousand_sep', '', 'yes'),
(229, 'woocommerce_price_decimal_sep', '', 'yes'),
(230, 'woocommerce_price_num_decimals', '0', 'yes'),
(231, 'woocommerce_shop_page_id', '7', 'yes'),
(232, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(233, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(234, 'woocommerce_placeholder_image', '6', 'yes'),
(235, 'woocommerce_weight_unit', 'kg', 'yes'),
(236, 'woocommerce_dimension_unit', 'cm', 'yes'),
(237, 'woocommerce_enable_reviews', 'yes', 'yes'),
(238, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(239, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(240, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(241, 'woocommerce_review_rating_required', 'yes', 'no'),
(242, 'woocommerce_manage_stock', 'yes', 'yes'),
(243, 'woocommerce_hold_stock_minutes', '60', 'no'),
(244, 'woocommerce_notify_low_stock', 'yes', 'no'),
(245, 'woocommerce_notify_no_stock', 'yes', 'no'),
(246, 'woocommerce_stock_email_recipient', 'maxf@leos.co.il', 'no'),
(247, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(248, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(249, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(250, 'woocommerce_stock_format', '', 'yes'),
(251, 'woocommerce_file_download_method', 'force', 'no'),
(252, 'woocommerce_downloads_require_login', 'no', 'no'),
(253, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(254, 'woocommerce_downloads_add_hash_to_filename', 'yes', 'yes'),
(255, 'woocommerce_prices_include_tax', 'no', 'yes'),
(256, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(257, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(258, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(259, 'woocommerce_tax_classes', '', 'yes'),
(260, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(261, 'woocommerce_tax_display_cart', 'excl', 'yes'),
(262, 'woocommerce_price_display_suffix', '', 'yes'),
(263, 'woocommerce_tax_total_display', 'itemized', 'no'),
(264, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(265, 'woocommerce_shipping_cost_requires_address', 'no', 'yes'),
(266, 'woocommerce_ship_to_destination', 'billing', 'no'),
(267, 'woocommerce_shipping_debug_mode', 'no', 'yes'),
(268, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(269, 'woocommerce_enable_checkout_login_reminder', 'no', 'no'),
(270, 'woocommerce_enable_signup_and_login_from_checkout', 'no', 'no'),
(271, 'woocommerce_enable_myaccount_registration', 'no', 'no'),
(272, 'woocommerce_registration_generate_username', 'yes', 'no'),
(273, 'woocommerce_registration_generate_password', 'yes', 'no'),
(274, 'woocommerce_erasure_request_removes_order_data', 'no', 'no'),
(275, 'woocommerce_erasure_request_removes_download_data', 'no', 'no'),
(276, 'woocommerce_allow_bulk_remove_personal_data', 'no', 'no'),
(277, 'woocommerce_registration_privacy_policy_text', 'Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our [privacy_policy].', 'yes'),
(278, 'woocommerce_checkout_privacy_policy_text', 'Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our [privacy_policy].', 'yes'),
(279, 'woocommerce_delete_inactive_accounts', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(280, 'woocommerce_trash_pending_orders', '', 'no'),
(281, 'woocommerce_trash_failed_orders', '', 'no'),
(282, 'woocommerce_trash_cancelled_orders', '', 'no'),
(283, 'woocommerce_anonymize_completed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(284, 'woocommerce_email_from_name', 'gordon', 'no'),
(285, 'woocommerce_email_from_address', 'maxf@leos.co.il', 'no'),
(286, 'woocommerce_email_header_image', '', 'no'),
(287, 'woocommerce_email_footer_text', '{site_title} &mdash; Built with {WooCommerce}', 'no'),
(288, 'woocommerce_email_base_color', '#96588a', 'no'),
(289, 'woocommerce_email_background_color', '#f7f7f7', 'no'),
(290, 'woocommerce_email_body_background_color', '#ffffff', 'no'),
(291, 'woocommerce_email_text_color', '#3c3c3c', 'no'),
(292, 'woocommerce_merchant_email_notifications', 'no', 'no'),
(293, 'woocommerce_cart_page_id', '8', 'no'),
(294, 'woocommerce_checkout_page_id', '9', 'no'),
(295, 'woocommerce_myaccount_page_id', '10', 'no'),
(296, 'woocommerce_terms_page_id', '', 'no'),
(297, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(298, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(299, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(300, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(301, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(302, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(303, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(304, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(305, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(306, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(307, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(308, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(309, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(310, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(311, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(312, 'woocommerce_api_enabled', 'no', 'yes'),
(313, 'woocommerce_allow_tracking', 'no', 'no'),
(314, 'woocommerce_show_marketplace_suggestions', 'yes', 'no'),
(315, 'woocommerce_single_image_width', '600', 'yes'),
(316, 'woocommerce_thumbnail_image_width', '300', 'yes'),
(317, 'woocommerce_checkout_highlight_required_fields', 'yes', 'yes'),
(318, 'woocommerce_demo_store', 'no', 'no'),
(319, 'woocommerce_permalinks', 'a:5:{s:12:\"product_base\";s:7:\"product\";s:13:\"category_base\";s:16:\"product-category\";s:8:\"tag_base\";s:11:\"product-tag\";s:14:\"attribute_base\";s:0:\"\";s:22:\"use_verbose_page_rules\";b:0;}', 'yes'),
(320, 'current_theme_supports_woocommerce', 'yes', 'yes'),
(321, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes'),
(322, '_transient_wc_attribute_taxonomies', 'a:0:{}', 'yes'),
(324, 'default_product_cat', '15', 'yes'),
(327, 'woocommerce_version', '5.4.1', 'yes'),
(328, 'woocommerce_db_version', '5.4.1', 'yes'),
(329, 'woocommerce_inbox_variant_assignment', '1', 'yes'),
(333, '_transient_jetpack_autoloader_plugin_paths', 'a:1:{i:0;s:29:\"{{WP_PLUGIN_DIR}}/woocommerce\";}', 'yes'),
(334, 'action_scheduler_lock_async-request-runner', '1626177213', 'yes'),
(335, 'woocommerce_admin_notices', 'a:0:{}', 'yes'),
(336, 'woocommerce_maxmind_geolocation_settings', 'a:1:{s:15:\"database_prefix\";s:32:\"GULbTDVGizUvvyL91knf73r2IFDGzgWq\";}', 'yes'),
(337, '_transient_woocommerce_webhook_ids_status_active', 'a:0:{}', 'yes'),
(338, 'widget_woocommerce_widget_cart', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(339, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(340, 'widget_woocommerce_layered_nav', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(341, 'widget_woocommerce_price_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(342, 'widget_woocommerce_product_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(343, 'widget_woocommerce_product_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(344, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(345, 'widget_woocommerce_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(346, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(347, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(348, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(349, 'widget_woocommerce_rating_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(352, 'woocommerce_admin_version', '2.3.1', 'yes'),
(353, 'woocommerce_admin_install_timestamp', '1625913817', 'yes'),
(354, 'wc_remote_inbox_notifications_wca_updated', '', 'yes');
INSERT INTO `fmn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(355, 'wc_remote_inbox_notifications_specs', 'a:28:{s:19:\"eu_vat_changes_2021\";O:8:\"stdClass\":8:{s:4:\"slug\";s:19:\"eu_vat_changes_2021\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:54:\"Get your business ready for the new EU tax regulations\";s:7:\"content\";s:617:\"On July 1, 2021, new taxation rules will come into play when the <a href=\"https://ec.europa.eu/taxation_customs/business/vat/modernising-vat-cross-border-ecommerce_en\">European Union (EU) Value-Added Tax (VAT) eCommerce package</a> takes effect.<br/><br/>The new regulations will impact virtually every B2C business involved in cross-border eCommerce trade with the EU.<br/><br/>We therefore recommend <a href=\"https://woocommerce.com/posts/new-eu-vat-regulations\">familiarizing yourself with the new updates</a>, and consult with a tax professional to ensure your business is following regulations and best practice.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:19:\"eu_vat_changes_2021\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:39:\"Learn more about the EU tax regulations\";}}s:3:\"url\";s:52:\"https://woocommerce.com/posts/new-eu-vat-regulations\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-06-24 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:19:\"publish_before_time\";s:14:\"publish_before\";s:19:\"2021-07-11 00:00:00\";}i:2;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:3:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:29:\"woocommerce_allowed_countries\";s:5:\"value\";s:3:\"all\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:1;a:2:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:29:\"woocommerce_allowed_countries\";s:5:\"value\";s:10:\"all_except\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:27:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"BE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"BG\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"CZ\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"DK\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:4;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"DE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:5;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"EE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:6;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"IE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:7;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"EL\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:8;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"ES\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:9;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"FR\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:10;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"HR\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:11;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"IT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:12;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"CY\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:13;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"LV\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:14;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"LT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:15;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"LU\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:16;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"HU\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:17;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"MT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:18;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"NL\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:19;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"AT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:20;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"PL\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:21;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"PT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:22;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"RO\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:23;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"SI\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:24;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"SK\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:25;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"FI\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}i:26;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:32:\"woocommerce_all_except_countries\";s:5:\"value\";s:2:\"SE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:9:\"!contains\";}}}}i:2;a:3:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:29:\"woocommerce_allowed_countries\";s:5:\"value\";s:3:\"all\";s:7:\"default\";b:0;s:9:\"operation\";s:2:\"!=\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:29:\"woocommerce_allowed_countries\";s:5:\"value\";s:10:\"all_except\";s:7:\"default\";b:0;s:9:\"operation\";s:2:\"!=\";}i:2;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:27:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"BE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"BG\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"CZ\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"DK\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:4;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"DE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:5;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"EE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:6;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"IE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:7;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"EL\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:8;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"ES\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:9;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"FR\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:10;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"HR\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:11;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"IT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:12;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"CY\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:13;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"LV\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:14;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"LT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:15;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"LU\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:16;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"HU\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:17;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"MT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:18;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"NL\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:19;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"AT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:20;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"PL\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:21;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"PT\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:22;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"RO\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:23;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"SI\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:24;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"SK\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:25;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"FI\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}i:26;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:38:\"woocommerce_specific_allowed_countries\";s:5:\"value\";s:2:\"SE\";s:7:\"default\";a:0:{}s:9:\"operation\";s:8:\"contains\";}}}}}}}}s:20:\"paypal_ppcp_gtm_2021\";O:8:\"stdClass\":8:{s:4:\"slug\";s:20:\"paypal_ppcp_gtm_2021\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:38:\"Offer more options with the new PayPal\";s:7:\"content\";s:113:\"Get the latest PayPal extension for a full suite of payment methods with extensive currency and country coverage.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:36:\"open_wc_paypal_payments_product_page\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:61:\"https://woocommerce.com/products/woocommerce-paypal-payments/\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:4:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-04-05 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:19:\"publish_before_time\";s:14:\"publish_before\";s:19:\"2021-04-21 00:00:00\";}i:2;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:7:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:43:\"woocommerce-gateway-paypal-express-checkout\";}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:30:\"woocommerce-gateway-paypal-pro\";}}i:2;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:37:\"woocommerce-gateway-paypal-pro-hosted\";}}i:3;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:35:\"woocommerce-gateway-paypal-advanced\";}}i:4;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:40:\"woocommerce-gateway-paypal-digital-goods\";}}i:5;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:31:\"woocommerce-paypal-here-gateway\";}}i:6;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:44:\"woocommerce-gateway-paypal-adaptive-payments\";}}}}i:3;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:27:\"woocommerce-paypal-payments\";}}}}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:27:\"woocommerce-paypal-payments\";s:7:\"version\";s:5:\"1.2.1\";s:8:\"operator\";s:1:\"<\";}}}}}s:23:\"facebook_pixel_api_2021\";O:8:\"stdClass\":8:{s:4:\"slug\";s:23:\"facebook_pixel_api_2021\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:44:\"Improve the performance of your Facebook ads\";s:7:\"content\";s:152:\"Enable Facebook Pixel and Conversions API through the latest version of Facebook for WooCommerce for improved measurement and ad targeting capabilities.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:30:\"upgrade_now_facebook_pixel_api\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:11:\"Upgrade now\";}}s:3:\"url\";s:67:\"plugin-install.php?tab=plugin-information&plugin=&section=changelog\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-05-17 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:19:\"publish_before_time\";s:14:\"publish_before\";s:19:\"2021-06-14 00:00:00\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:24:\"facebook-for-woocommerce\";s:7:\"version\";s:5:\"2.4.0\";s:8:\"operator\";s:2:\"<=\";}}}s:16:\"facebook_ec_2021\";O:8:\"stdClass\":8:{s:4:\"slug\";s:16:\"facebook_ec_2021\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:59:\"Sync your product catalog with Facebook to help boost sales\";s:7:\"content\";s:170:\"A single click adds all products to your Facebook Business Page shop. Product changes are automatically synced, with the flexibility to control which products are listed.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:22:\"learn_more_facebook_ec\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:42:\"https://woocommerce.com/products/facebook/\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-03-01 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:19:\"publish_before_time\";s:14:\"publish_before\";s:19:\"2021-03-15 00:00:00\";}i:2;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:24:\"facebook-for-woocommerce\";}}}}s:37:\"ecomm-need-help-setting-up-your-store\";O:8:\"stdClass\":8:{s:4:\"slug\";s:37:\"ecomm-need-help-setting-up-your-store\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:32:\"Need help setting up your Store?\";s:7:\"content\";s:350:\"Schedule a free 30-min <a href=\"https://wordpress.com/support/concierge-support/\">quick start session</a> and get help from our specialists. We’re happy to walk through setup steps, show you around the WordPress.com dashboard, troubleshoot any issues you may have, and help you the find the features you need to accomplish your goals for your site.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:16:\"set-up-concierge\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:21:\"Schedule free session\";}}s:3:\"url\";s:34:\"https://wordpress.com/me/concierge\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:3:{i:0;s:35:\"woocommerce-shipping-australia-post\";i:1;s:32:\"woocommerce-shipping-canada-post\";i:2;s:30:\"woocommerce-shipping-royalmail\";}}}}s:20:\"woocommerce-services\";O:8:\"stdClass\":8:{s:4:\"slug\";s:20:\"woocommerce-services\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:26:\"WooCommerce Shipping & Tax\";s:7:\"content\";s:255:\"WooCommerce Shipping & Tax helps get your store “ready to sell” as quickly as possible. You create your products. We take care of tax calculation, payment processing, and shipping label printing! Learn more about the extension that you just installed.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:84:\"https://docs.woocommerce.com/document/woocommerce-shipping-and-tax/?utm_source=inbox\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:20:\"woocommerce-services\";}}i:1;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:1:\"<\";s:4:\"days\";i:2;}}}s:32:\"ecomm-unique-shopping-experience\";O:8:\"stdClass\":8:{s:4:\"slug\";s:32:\"ecomm-unique-shopping-experience\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:53:\"For a shopping experience as unique as your customers\";s:7:\"content\";s:274:\"Product Add-Ons allow your customers to personalize products while they’re shopping on your online store. No more follow-up email requests—customers get what they want, before they’re done checking out. Learn more about this extension that comes included in your plan.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:43:\"learn-more-ecomm-unique-shopping-experience\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:71:\"https://docs.woocommerce.com/document/product-add-ons/?utm_source=inbox\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:3:{i:0;s:35:\"woocommerce-shipping-australia-post\";i:1;s:32:\"woocommerce-shipping-canada-post\";i:2;s:30:\"woocommerce-shipping-royalmail\";}}i:1;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:1:\"<\";s:4:\"days\";i:2;}}}s:37:\"wc-admin-getting-started-in-ecommerce\";O:8:\"stdClass\":8:{s:4:\"slug\";s:37:\"wc-admin-getting-started-in-ecommerce\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:38:\"Getting Started in eCommerce - webinar\";s:7:\"content\";s:174:\"We want to make eCommerce and this process of getting started as easy as possible for you. Watch this webinar to get tips on how to have our store up and running in a breeze.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:17:\"watch-the-webinar\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:17:\"Watch the webinar\";}}s:3:\"url\";s:28:\"https://youtu.be/V_2XtCOyZ7o\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:18:\"onboarding_profile\";s:5:\"index\";s:12:\"setup_client\";s:9:\"operation\";s:2:\"!=\";s:5:\"value\";b:1;}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:3:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:18:\"onboarding_profile\";s:5:\"index\";s:13:\"product_count\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:1:\"0\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:18:\"onboarding_profile\";s:5:\"index\";s:7:\"revenue\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:4:\"none\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:18:\"onboarding_profile\";s:5:\"index\";s:7:\"revenue\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:10:\"up-to-2500\";}}}}}s:18:\"your-first-product\";O:8:\"stdClass\":8:{s:4:\"slug\";s:18:\"your-first-product\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:18:\"Your first product\";s:7:\"content\";s:461:\"That\'s huge! You\'re well on your way to building a successful online store — now it’s time to think about how you\'ll fulfill your orders.<br/><br/>Read our shipping guide to learn best practices and options for putting together your shipping strategy. And for WooCommerce stores in the United States, you can print discounted shipping labels via USPS with <a href=\"https://href.li/?https://woocommerce.com/shipping\" target=\"_blank\">WooCommerce Shipping</a>.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:82:\"https://woocommerce.com/posts/ecommerce-shipping-solutions-guide/?utm_source=inbox\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:4:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:12:\"stored_state\";s:5:\"index\";s:22:\"there_were_no_products\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";b:1;}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:12:\"stored_state\";s:5:\"index\";s:22:\"there_are_now_products\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";b:1;}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:13:\"product_count\";s:9:\"operation\";s:2:\">=\";s:5:\"value\";i:1;}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:18:\"onboarding_profile\";s:5:\"index\";s:13:\"product_types\";s:9:\"operation\";s:8:\"contains\";s:5:\"value\";s:8:\"physical\";s:7:\"default\";a:0:{}}}}s:31:\"wc-square-apple-pay-boost-sales\";O:8:\"stdClass\":8:{s:4:\"slug\";s:31:\"wc-square-apple-pay-boost-sales\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:26:\"Boost sales with Apple Pay\";s:7:\"content\";s:191:\"Now that you accept Apple Pay® with Square you can increase conversion rates by letting your customers know that Apple Pay® is available. Here’s a marketing guide to help you get started.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:27:\"boost-sales-marketing-guide\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:19:\"See marketing guide\";}}s:3:\"url\";s:97:\"https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=square-boost-sales\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:9:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.8\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:18:\"woocommerce-square\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"2.3\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:27:\"wc_square_apple_pay_enabled\";s:5:\"value\";i:1;s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:38:\"wc-square-apple-pay-grow-your-business\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:4;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:38:\"wc-square-apple-pay-grow-your-business\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:5;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:7;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}s:38:\"wc-square-apple-pay-grow-your-business\";O:8:\"stdClass\":8:{s:4:\"slug\";s:38:\"wc-square-apple-pay-grow-your-business\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:45:\"Grow your business with Square and Apple Pay \";s:7:\"content\";s:178:\"Now more than ever, shoppers want a fast, simple, and secure online checkout experience. Increase conversion rates by letting your customers know that you now accept Apple Pay®.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:34:\"grow-your-business-marketing-guide\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:19:\"See marketing guide\";}}s:3:\"url\";s:104:\"https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=square-grow-your-business\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:9:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.8\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:18:\"woocommerce-square\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"2.3\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:27:\"wc_square_apple_pay_enabled\";s:5:\"value\";i:2;s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:31:\"wc-square-apple-pay-boost-sales\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:4;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:31:\"wc-square-apple-pay-boost-sales\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:5;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:7;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}s:32:\"wcpay-apple-pay-is-now-available\";O:8:\"stdClass\":8:{s:4:\"slug\";s:32:\"wcpay-apple-pay-is-now-available\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:53:\"Apple Pay is now available with WooCommerce Payments!\";s:7:\"content\";s:397:\"Increase your conversion rate by offering a fast and secure checkout with <a href=\"https://woocommerce.com/apple-pay/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_applepay\" target=\"_blank\">Apple Pay</a>®. It’s free to get started with <a href=\"https://woocommerce.com/payments/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_applepay\" target=\"_blank\">WooCommerce Payments</a>.\";}}s:7:\"actions\";a:2:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:13:\"add-apple-pay\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:13:\"Add Apple Pay\";}}s:3:\"url\";s:69:\"/admin.php?page=wc-settings&tab=checkout&section=woocommerce_payments\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}i:1;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:121:\"https://docs.woocommerce.com/document/payments/apple-pay/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_applepay\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.8\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:20:\"woocommerce-payments\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:5:\"2.3.0\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:26:\"wcpay_is_apple_pay_enabled\";s:5:\"value\";b:0;s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}}}s:27:\"wcpay-apple-pay-boost-sales\";O:8:\"stdClass\":8:{s:4:\"slug\";s:27:\"wcpay-apple-pay-boost-sales\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:26:\"Boost sales with Apple Pay\";s:7:\"content\";s:205:\"Now that you accept Apple Pay® with WooCommerce Payments you can increase conversion rates by letting your customers know that Apple Pay® is available. Here’s a marketing guide to help you get started.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:27:\"boost-sales-marketing-guide\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:19:\"See marketing guide\";}}s:3:\"url\";s:96:\"https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=wcpay-boost-sales\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:4:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.8\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:26:\"wcpay_is_apple_pay_enabled\";s:5:\"value\";i:1;s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}s:34:\"wcpay-apple-pay-grow-your-business\";O:8:\"stdClass\":8:{s:4:\"slug\";s:34:\"wcpay-apple-pay-grow-your-business\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:58:\"Grow your business with WooCommerce Payments and Apple Pay\";s:7:\"content\";s:178:\"Now more than ever, shoppers want a fast, simple, and secure online checkout experience. Increase conversion rates by letting your customers know that you now accept Apple Pay®.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:34:\"grow-your-business-marketing-guide\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:19:\"See marketing guide\";}}s:3:\"url\";s:103:\"https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=wcpay-grow-your-business\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:4:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.8\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:26:\"wcpay_is_apple_pay_enabled\";s:5:\"value\";i:2;s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}s:37:\"wc-admin-optimizing-the-checkout-flow\";O:8:\"stdClass\":8:{s:4:\"slug\";s:37:\"wc-admin-optimizing-the-checkout-flow\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:28:\"Optimizing the checkout flow\";s:7:\"content\";s:171:\"It\'s crucial to get your store\'s checkout as smooth as possible to avoid losing sales. Let\'s take a look at how you can optimize the checkout experience for your shoppers.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:28:\"optimizing-the-checkout-flow\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:78:\"https://woocommerce.com/posts/optimizing-woocommerce-checkout?utm_source=inbox\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:1:\">\";s:4:\"days\";i:3;}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:45:\"woocommerce_task_list_tracked_completed_tasks\";s:9:\"operation\";s:8:\"contains\";s:5:\"value\";s:8:\"payments\";s:7:\"default\";a:0:{}}}}s:39:\"wc-admin-first-five-things-to-customize\";O:8:\"stdClass\":8:{s:4:\"slug\";s:39:\"wc-admin-first-five-things-to-customize\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:45:\"The first 5 things to customize in your store\";s:7:\"content\";s:173:\"Deciding what to start with first is tricky. To help you properly prioritize, we\'ve put together this short list of the first few things you should customize in WooCommerce.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:82:\"https://woocommerce.com/posts/first-things-customize-woocommerce/?utm_source=inbox\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:1:\">\";s:4:\"days\";i:2;}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:45:\"woocommerce_task_list_tracked_completed_tasks\";s:5:\"value\";s:9:\"NOT EMPTY\";s:7:\"default\";s:9:\"NOT EMPTY\";s:9:\"operation\";s:2:\"!=\";}}}s:32:\"wc-payments-qualitative-feedback\";O:8:\"stdClass\":8:{s:4:\"slug\";s:32:\"wc-payments-qualitative-feedback\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:55:\"WooCommerce Payments setup - let us know what you think\";s:7:\"content\";s:146:\"Congrats on enabling WooCommerce Payments for your store. Please share your feedback in this 2 minute survey to help us improve the setup process.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:35:\"qualitative-feedback-from-new-users\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:14:\"Share feedback\";}}s:3:\"url\";s:39:\"https://automattic.survey.fm/wc-pay-new\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:1:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:45:\"woocommerce_task_list_tracked_completed_tasks\";s:9:\"operation\";s:8:\"contains\";s:5:\"value\";s:20:\"woocommerce-payments\";s:7:\"default\";a:0:{}}}}s:29:\"share-your-feedback-on-paypal\";O:8:\"stdClass\":8:{s:4:\"slug\";s:29:\"share-your-feedback-on-paypal\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:29:\"Share your feedback on PayPal\";s:7:\"content\";s:127:\"Share your feedback in this 2 minute survey about how we can make the process of accepting payments more useful for your store.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:14:\"share-feedback\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:14:\"Share feedback\";}}s:3:\"url\";s:43:\"http://automattic.survey.fm/paypal-feedback\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:26:\"woocommerce-gateway-stripe\";}}}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:43:\"woocommerce-gateway-paypal-express-checkout\";}}}}s:31:\"wcpay_instant_deposits_gtm_2021\";O:8:\"stdClass\":8:{s:4:\"slug\";s:31:\"wcpay_instant_deposits_gtm_2021\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:69:\"Get paid within minutes – Instant Deposits for WooCommerce Payments\";s:7:\"content\";s:384:\"Stay flexible with immediate access to your funds when you need them – including nights, weekends, and holidays. With <a href=\"https://woocommerce.com/products/woocommerce-payments/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_instant_deposits\">WooCommerce Payments\'</a> new Instant Deposits feature, you’re able to transfer your earnings to a debit card within minutes.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:40:\"Learn about Instant Deposits eligibility\";}}s:3:\"url\";s:136:\"https://docs.woocommerce.com/document/payments/instant-deposits/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_instant_deposits\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:4:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-05-18 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:19:\"publish_before_time\";s:14:\"publish_before\";s:19:\"2021-06-01 00:00:00\";}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:5:\"value\";s:2:\"US\";s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:20:\"woocommerce-payments\";}}}}s:31:\"google_listings_and_ads_install\";O:8:\"stdClass\":8:{s:4:\"slug\";s:31:\"google_listings_and_ads_install\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:35:\"Drive traffic and sales with Google\";s:7:\"content\";s:123:\"Reach online shoppers to drive traffic and sales for your store by showcasing products across Google, for free or with ads.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:11:\"get-started\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:11:\"Get started\";}}s:3:\"url\";s:56:\"https://woocommerce.com/products/google-listings-and-ads\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-06-09 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:23:\"google_listings_and_ads\";}}}}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:11:\"order_count\";s:9:\"operation\";s:1:\">\";s:5:\"value\";i:10;}}}s:39:\"wc-subscriptions-security-update-3-0-15\";O:8:\"stdClass\":8:{s:4:\"slug\";s:39:\"wc-subscriptions-security-update-3-0-15\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:42:\"WooCommerce Subscriptions security update!\";s:7:\"content\";s:736:\"We recently released an important security update to WooCommerce Subscriptions. To ensure your site\'s data is protected, please upgrade <strong>WooCommerce Subscriptions to version 3.0.15</strong> or later.<br/><br/>Click the button below to view and update to the latest Subscriptions version, or log in to <a href=\"https://woocommerce.com/my-dashboard\">WooCommerce.com Dashboard</a> and navigate to your <strong>Downloads</strong> page.<br/><br/>We recommend always using the latest version of WooCommerce Subscriptions, and other software running on your site, to ensure maximum security.<br/><br/>If you have any questions we are here to help — just <a href=\"https://woocommerce.com/my-account/create-a-ticket/\">open a ticket</a>.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:30:\"update-wc-subscriptions-3-0-15\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:19:\"View latest version\";}}s:3:\"url\";s:30:\"&page=wc-addons&section=helper\";s:18:\"url_is_admin_query\";b:1;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:1:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:25:\"woocommerce-subscriptions\";s:8:\"operator\";s:1:\"<\";s:7:\"version\";s:6:\"3.0.15\";}}}s:29:\"woocommerce-core-update-5-4-0\";O:8:\"stdClass\":8:{s:4:\"slug\";s:29:\"woocommerce-core-update-5-4-0\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:31:\"Update to WooCommerce 5.4.1 now\";s:7:\"content\";s:140:\"WooCommerce 5.4.1 addresses a checkout issue discovered in WooCommerce 5.4. We recommend upgrading to WooCommerce 5.4.1 as soon as possible.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:20:\"update-wc-core-5-4-0\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:25:\"How to update WooCommerce\";}}s:3:\"url\";s:64:\"https://docs.woocommerce.com/document/how-to-update-woocommerce/\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:1:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.4.0\";}}}s:19:\"wcpay-promo-2020-11\";O:8:\"stdClass\":7:{s:4:\"slug\";s:19:\"wcpay-promo-2020-11\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:19:\"wcpay-promo-2020-11\";s:7:\"content\";s:19:\"wcpay-promo-2020-11\";}}s:5:\"rules\";a:0:{}}s:19:\"wcpay-promo-2020-12\";O:8:\"stdClass\":7:{s:4:\"slug\";s:19:\"wcpay-promo-2020-12\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:19:\"wcpay-promo-2020-12\";s:7:\"content\";s:19:\"wcpay-promo-2020-12\";}}s:5:\"rules\";a:0:{}}s:30:\"wcpay-promo-2021-6-incentive-1\";O:8:\"stdClass\":8:{s:4:\"slug\";s:30:\"wcpay-promo-2021-6-incentive-1\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:74:\"Special offer: Save 50% on transaction fees for up to $125,000 in payments\";s:7:\"content\";s:715:\"Save big when you add <a href=\"https://woocommerce.com/payments/?utm_medium=notification&utm_source=product&utm_campaign=wcpay_exp222\">WooCommerce Payments</a> to your store today.\n                Get a discounted rate of 1.5% + $0.15 on all transactions – that’s 50% off the standard fee on up to $125,000 in processed payments (or six months, whichever comes first). Act now – this offer is available for a limited time only.\n                <br/><br/>By clicking \"Get WooCommerce Payments,\" you agree to the promotional <a href=\"https://woocommerce.com/terms-conditions/woocommerce-payments-half-off-six-promotion/?utm_medium=notification&utm_source=product&utm_campaign=wcpay_exp222\">Terms of Service</a>.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:25:\"get-woo-commerce-payments\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:24:\"Get WooCommerce Payments\";}}s:3:\"url\";s:57:\"admin.php?page=wc-admin&action=setup-woocommerce-payments\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:12:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:6:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"1\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"3\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"5\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"7\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:4;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"9\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:5;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:2:\"11\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:4:{i:0;s:17:\"crowdsignal-forms\";i:1;s:11:\"layout-grid\";i:2;s:17:\"full-site-editing\";i:3;s:13:\"page-optimize\";}}}}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:5:\"value\";s:2:\"US\";s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:26:\"woocommerce_allow_tracking\";s:5:\"value\";s:3:\"yes\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:4;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:2:\">=\";s:4:\"days\";i:31;}i:5;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:20:\"woocommerce-payments\";}}}}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.0\";}i:7;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:10:\"db_version\";s:5:\"value\";s:5:\"45805\";s:7:\"default\";i:0;s:9:\"operation\";s:2:\">=\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-11\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:9;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-11\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:10;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-12\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:11;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-12\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}s:30:\"wcpay-promo-2021-6-incentive-2\";O:8:\"stdClass\":8:{s:4:\"slug\";s:30:\"wcpay-promo-2021-6-incentive-2\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:58:\"Special offer: No transaction fees* for up to three months\";s:7:\"content\";s:684:\"Save big when you add <a href=\"https://woocommerce.com/payments/?utm_medium=notification&utm_source=product&utm_campaign=wcpay_exp233\">WooCommerce Payments</a> to your store today. Pay zero transaction fees* on up to $25,000 in processed payments (or three months, whichever comes first). Act now – this offer is available for a limited time only. *Currency conversion fees excluded.\n                <br/><br/>By clicking \"Get WooCommerce Payments,\" you agree to the promotional <a href=\"https://woocommerce.com/terms-conditions/woocommerce-payments-no-transaction-fees-for-three-promotion/?utm_medium=notification&utm_source=product&utm_campaign=wcpay_exp233\">Terms of Service</a>.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:24:\"get-woocommerce-payments\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:24:\"Get WooCommerce Payments\";}}s:3:\"url\";s:57:\"admin.php?page=wc-admin&action=setup-woocommerce-payments\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:12:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:6:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"2\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"4\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"6\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"8\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:4;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:2:\"10\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:5;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:2:\"12\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:4:{i:0;s:17:\"crowdsignal-forms\";i:1;s:11:\"layout-grid\";i:2;s:17:\"full-site-editing\";i:3;s:13:\"page-optimize\";}}}}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:5:\"value\";s:2:\"US\";s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:26:\"woocommerce_allow_tracking\";s:5:\"value\";s:3:\"yes\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:4;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:2:\">=\";s:4:\"days\";i:31;}i:5;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:20:\"woocommerce-payments\";}}}}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.0\";}i:7;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:10:\"db_version\";s:5:\"value\";s:5:\"45805\";s:7:\"default\";i:0;s:9:\"operation\";s:2:\">=\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-11\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:9;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-11\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:10;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-12\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:11;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-12\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}s:34:\"ppxo-pps-upgrade-paypal-payments-1\";O:8:\"stdClass\":8:{s:4:\"slug\";s:34:\"ppxo-pps-upgrade-paypal-payments-1\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:47:\"Get the latest PayPal extension for WooCommerce\";s:7:\"content\";s:440:\"Heads up! There\'s a new PayPal on the block!<br/><br/>Now is a great time to upgrade to our latest <a href=\"https://woocommerce.com/products/woocommerce-paypal-payments/\" target=\"_blank\">PayPal extension</a> to continue to receive support and updates with PayPal.<br/><br/>Get access to a full suite of PayPal payment methods, extensive currency and country coverage, and pay later options with the all-new PayPal extension for WooCommerce.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:34:\"ppxo-pps-install-paypal-payments-1\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:18:\"View upgrade guide\";}}s:3:\"url\";s:96:\"https://docs.woocommerce.com/document/woocommerce-paypal-payments/paypal-payments-upgrade-guide/\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:27:\"woocommerce-paypal-payments\";}}}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:43:\"woocommerce-gateway-paypal-express-checkout\";}}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:27:\"woocommerce_paypal_settings\";s:9:\"operation\";s:2:\"!=\";s:5:\"value\";b:0;s:7:\"default\";b:0;}}}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";i:7;s:7:\"default\";i:1;s:9:\"operation\";s:1:\"<\";}}}s:34:\"ppxo-pps-upgrade-paypal-payments-2\";O:8:\"stdClass\":8:{s:4:\"slug\";s:34:\"ppxo-pps-upgrade-paypal-payments-2\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:31:\"Upgrade your PayPal experience!\";s:7:\"content\";s:513:\"We\'ve developed a whole new <a href=\"https://woocommerce.com/products/woocommerce-paypal-payments/\" target=\"_blank\">PayPal extension for WooCommerce</a> that combines the best features of our many PayPal extensions into just one extension.<br/><br/>Get access to a full suite of PayPal payment methods, extensive currency and country coverage, offer subscription and recurring payments, and the new PayPal pay later options.<br/><br/>Start using our latest PayPal today to continue to receive support and updates.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:34:\"ppxo-pps-install-paypal-payments-2\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:18:\"View upgrade guide\";}}s:3:\"url\";s:96:\"https://docs.woocommerce.com/document/woocommerce-paypal-payments/paypal-payments-upgrade-guide/\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:27:\"woocommerce-paypal-payments\";}}}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:43:\"woocommerce-gateway-paypal-express-checkout\";}}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:27:\"woocommerce_paypal_settings\";s:9:\"operation\";s:2:\"!=\";s:5:\"value\";b:0;s:7:\"default\";b:0;}}}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";i:6;s:7:\"default\";i:1;s:9:\"operation\";s:1:\">\";}}}}', 'yes');
INSERT INTO `fmn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(356, 'wc_remote_inbox_notifications_stored_state', 'O:8:\"stdClass\":3:{s:22:\"there_were_no_products\";b:1;s:22:\"there_are_now_products\";b:1;s:17:\"new_product_count\";i:0;}', 'yes'),
(360, '_transient_wc_count_comments', 'O:8:\"stdClass\":7:{s:14:\"total_comments\";i:1;s:3:\"all\";i:1;s:8:\"approved\";s:1:\"1\";s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(361, 'wc_blocks_db_schema_version', '260', 'yes'),
(362, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(366, '_transient_woocommerce_reports-transient-version', '1625913887', 'yes'),
(367, '_transient_timeout_orders-all-statuses', '1626518758', 'no'),
(368, '_transient_orders-all-statuses', 'a:2:{s:7:\"version\";s:10:\"1625913887\";s:5:\"value\";a:0:{}}', 'no'),
(376, 'action_scheduler_migration_status', 'complete', 'yes'),
(378, 'woocommerce_onboarding_profile', 'a:8:{s:12:\"setup_client\";b:1;s:8:\"industry\";a:1:{i:0;a:1:{s:4:\"slug\";s:22:\"education-and-learning\";}}s:13:\"product_types\";a:1:{i:0;s:8:\"physical\";}s:13:\"product_count\";s:8:\"101-1000\";s:14:\"selling_venues\";s:2:\"no\";s:19:\"business_extensions\";a:0:{}s:5:\"theme\";s:6:\"gordon\";s:9:\"completed\";b:1;}', 'yes'),
(379, '_transient_timeout_wc_report_orders_stats_9bbe806c461ce8886c0a04ada2c00159', '1626518762', 'no'),
(380, '_transient_wc_report_orders_stats_9bbe806c461ce8886c0a04ada2c00159', 'a:2:{s:7:\"version\";s:10:\"1625913887\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-28\";s:10:\"date_start\";s:19:\"2021-07-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-09 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-10 13:45:59\";s:12:\"date_end_gmt\";s:19:\"2021-07-10 10:45:59\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(381, '_transient_timeout_wc_report_orders_stats_4c18dadf0a16397f532fae567f7273fb', '1626518762', 'no'),
(382, '_transient_wc_report_orders_stats_4c18dadf0a16397f532fae567f7273fb', 'a:2:{s:7:\"version\";s:10:\"1625913887\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-28\";s:10:\"date_start\";s:19:\"2021-07-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-09 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-10 13:45:59\";s:12:\"date_end_gmt\";s:19:\"2021-07-10 10:45:59\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(383, 'woocommerce_task_list_tracked_completed_tasks', 'a:2:{i:0;s:13:\"store_details\";i:1;s:8:\"products\";}', 'yes'),
(384, '_transient_timeout_wc_report_orders_stats_a2dc4a1b923318707dc5888ef8715eb3', '1626518762', 'no'),
(385, '_transient_wc_report_orders_stats_a2dc4a1b923318707dc5888ef8715eb3', 'a:2:{s:7:\"version\";s:10:\"1625913887\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-28\";s:10:\"date_start\";s:19:\"2021-07-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-08 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-07-09 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(386, '_transient_timeout_wc_report_orders_stats_01288a6218bb4097ddd252338d356001', '1626518762', 'no'),
(387, '_transient_wc_report_orders_stats_01288a6218bb4097ddd252338d356001', 'a:2:{s:7:\"version\";s:10:\"1625913887\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-28\";s:10:\"date_start\";s:19:\"2021-07-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-08 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-07-09 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(388, 'woocommerce_task_list_welcome_modal_dismissed', 'yes', 'yes'),
(390, '_transient_timeout_wc_report_orders_stats_8e2f639e5beb392b0a9f41f79926d791', '1626518853', 'no'),
(391, '_transient_wc_report_orders_stats_8e2f639e5beb392b0a9f41f79926d791', 'a:2:{s:7:\"version\";s:10:\"1625913887\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-28\";s:10:\"date_start\";s:19:\"2021-07-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-09 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-10 13:47:32\";s:12:\"date_end_gmt\";s:19:\"2021-07-10 10:47:32\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(392, '_transient_timeout_wc_report_orders_stats_280f062c3a2dcac8fe45d1c515d5ac48', '1626518853', 'no'),
(393, '_transient_wc_report_orders_stats_280f062c3a2dcac8fe45d1c515d5ac48', 'a:2:{s:7:\"version\";s:10:\"1625913887\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-28\";s:10:\"date_start\";s:19:\"2021-07-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-09 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-10 13:47:32\";s:12:\"date_end_gmt\";s:19:\"2021-07-10 10:47:32\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(416, '_transient_product_query-transient-version', '1626177024', 'yes'),
(458, '_transient_product-transient-version', '1626171029', 'yes'),
(467, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(490, 'options_tel', '050-00-0000', 'no'),
(491, '_options_tel', 'field_5dd3a33f42e0c', 'no'),
(492, 'options_mail', 'infoffff@fffffff.co.il', 'no'),
(493, '_options_mail', 'field_5dd3a35642e0d', 'no'),
(494, 'options_fax', '', 'no'),
(495, '_options_fax', 'field_5dd3a36942e0e', 'no'),
(496, 'options_address', 'כתובת העסק לורם איפסום', 'no'),
(497, '_options_address', 'field_5dd3a4d142e0f', 'no'),
(498, 'options_map_image', '97', 'no'),
(499, '_options_map_image', 'field_5ddbd67734310', 'no'),
(500, 'options_facebook', 'https://www.facebook.com/facebook', 'no'),
(501, '_options_facebook', 'field_5ddbd6b3cc0cd', 'no'),
(502, 'options_whatsapp', '050-00-0000', 'no'),
(503, '_options_whatsapp', 'field_5ddbd6cdcc0ce', 'no'),
(504, 'options_instagram', 'https://www.instagram.com/', 'no'),
(505, '_options_instagram', 'field_60e988ff7e1ad', 'no'),
(506, 'options_youtube', 'https://www.youtube.com/', 'no'),
(507, '_options_youtube', 'field_60e9890b7e1ae', 'no'),
(508, 'options_linkedin', 'https://www.linkedin.com/', 'no'),
(509, '_options_linkedin', 'field_60e989197e1af', 'no'),
(510, 'options_open_hours', 'א-ה 10:00 - 18:30', 'no'),
(511, '_options_open_hours', 'field_60e9892d7e1b0', 'no'),
(512, 'options_logo', '114', 'no'),
(513, '_options_logo', 'field_5dd3a310760b9', 'no'),
(515, 'recovery_mode_email_last_sent', '1626088973', 'yes'),
(594, 'yit_recently_activated', 'a:1:{i:0;s:34:\"yith-woocommerce-wishlist/init.php\";}', 'yes'),
(595, 'yith_wcwl_wishlist_page_id', '98', 'yes'),
(596, 'yith_wcwl_version', '3.0.23', 'yes'),
(597, 'yith_wcwl_db_version', '3.0.0', 'yes'),
(599, 'yith_wcwl_ajax_enable', 'no', 'yes'),
(600, 'yith_wfbt_enable_integration', 'yes', 'yes'),
(601, 'yith_wcwl_after_add_to_wishlist_behaviour', 'view', 'yes'),
(602, 'yith_wcwl_show_on_loop', 'no', 'yes'),
(603, 'yith_wcwl_loop_position', 'after_add_to_cart', 'yes'),
(604, 'yith_wcwl_button_position', 'after_add_to_cart', 'yes'),
(605, 'yith_wcwl_add_to_wishlist_text', 'Add to wishlist', 'yes'),
(606, 'yith_wcwl_product_added_text', 'המוצר נשמר!', 'yes'),
(607, 'yith_wcwl_browse_wishlist_text', 'Browse wishlist', 'yes'),
(608, 'yith_wcwl_already_in_wishlist_text', 'The product is already in your wishlist!', 'yes'),
(609, 'yith_wcwl_add_to_wishlist_style', 'link', 'yes'),
(610, 'yith_wcwl_rounded_corners_radius', '16', 'yes'),
(611, 'yith_wcwl_add_to_wishlist_icon', 'fa-heart-o', 'yes'),
(612, 'yith_wcwl_add_to_wishlist_custom_icon', '', 'yes'),
(613, 'yith_wcwl_added_to_wishlist_icon', 'fa-heart', 'yes'),
(614, 'yith_wcwl_added_to_wishlist_custom_icon', '', 'yes'),
(615, 'yith_wcwl_custom_css', '', 'yes'),
(616, 'yith_wcwl_variation_show', '', 'yes'),
(617, 'yith_wcwl_price_show', 'yes', 'yes'),
(618, 'yith_wcwl_stock_show', 'yes', 'yes'),
(619, 'yith_wcwl_show_dateadded', '', 'yes'),
(620, 'yith_wcwl_add_to_cart_show', 'yes', 'yes'),
(621, 'yith_wcwl_show_remove', 'yes', 'yes'),
(622, 'yith_wcwl_repeat_remove_button', '', 'yes'),
(623, 'yith_wcwl_redirect_cart', 'no', 'yes'),
(624, 'yith_wcwl_remove_after_add_to_cart', 'yes', 'yes'),
(625, 'yith_wcwl_enable_share', 'yes', 'yes'),
(626, 'yith_wcwl_share_fb', 'yes', 'yes'),
(627, 'yith_wcwl_share_twitter', 'yes', 'yes'),
(628, 'yith_wcwl_share_pinterest', 'yes', 'yes'),
(629, 'yith_wcwl_share_email', 'yes', 'yes'),
(630, 'yith_wcwl_share_whatsapp', 'yes', 'yes'),
(631, 'yith_wcwl_share_url', 'no', 'yes'),
(632, 'yith_wcwl_socials_title', 'רשימת המשאלות שלי ב-gordon', 'yes'),
(633, 'yith_wcwl_socials_text', '', 'yes'),
(634, 'yith_wcwl_socials_image_url', '', 'yes'),
(635, 'yith_wcwl_wishlist_title', 'My wishlist', 'yes'),
(636, 'yith_wcwl_add_to_cart_text', 'Add to cart', 'yes'),
(637, 'yith_wcwl_add_to_cart_style', 'link', 'yes'),
(638, 'yith_wcwl_add_to_cart_rounded_corners_radius', '16', 'yes'),
(639, 'yith_wcwl_add_to_cart_icon', 'fa-shopping-cart', 'yes'),
(640, 'yith_wcwl_add_to_cart_custom_icon', '', 'yes'),
(641, 'yith_wcwl_color_headers_background', '#F4F4F4', 'yes'),
(642, 'yith_wcwl_fb_button_icon', 'fa-facebook', 'yes'),
(643, 'yith_wcwl_fb_button_custom_icon', '', 'yes'),
(644, 'yith_wcwl_tw_button_icon', 'fa-twitter', 'yes'),
(645, 'yith_wcwl_tw_button_custom_icon', '', 'yes'),
(646, 'yith_wcwl_pr_button_icon', 'fa-pinterest', 'yes'),
(647, 'yith_wcwl_pr_button_custom_icon', '', 'yes'),
(648, 'yith_wcwl_em_button_icon', 'fa-envelope-o', 'yes'),
(649, 'yith_wcwl_em_button_custom_icon', '', 'yes'),
(650, 'yith_wcwl_wa_button_icon', 'fa-whatsapp', 'yes'),
(651, 'yith_wcwl_wa_button_custom_icon', '', 'yes'),
(652, 'yit_plugin_fw_panel_wc_default_options_set', 'a:1:{s:15:\"yith_wcwl_panel\";b:1;}', 'yes'),
(653, 'yith_plugin_fw_promo_2019_bis', '1', 'yes'),
(654, '_site_transient_timeout_yith_promo_message', '3252111792', 'no'),
(655, '_site_transient_yith_promo_message', '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!-- Default border color: #acc327 -->\n<!-- Default background color: #ecf7ed -->\n\n<promotions>\n    <expiry_date>2019-12-10</expiry_date>\n    <promo>\n        <promo_id>yithblackfriday2019</promo_id>\n        <title><![CDATA[<strong>YITH Black Friday</strong>]]></title>\n        <description><![CDATA[\n            Don\'t miss our <strong>30% discount</strong> on all our products! No coupon needed in cart. Valid from <strong>28th November</strong> to <strong>2nd December</strong>.\n        ]]></description>\n        <link>\n            <label>Get your deals now!</label>\n            <url><![CDATA[https://yithemes.com]]></url>\n        </link>\n        <style>\n            <image_bg_color>#272121</image_bg_color>\n            <border_color>#272121</border_color>\n            <background_color>#ffffff</background_color>\n        </style>\n        <start_date>2019-11-27 23:59:59</start_date>\n        <end_date>2019-12-03 08:00:00</end_date>\n    </promo>\n</promotions>', 'no'),
(656, '_transient_timeout_yith_wcwl_hidden_products', '1628599298', 'no'),
(657, '_transient_yith_wcwl_hidden_products', 'a:0:{}', 'no'),
(720, 'product_cat_children', 'a:0:{}', 'yes'),
(722, 'search_json_version', '1.5', 'yes'),
(725, '_transient_shipping-transient-version', '1626012315', 'yes'),
(726, '_transient_timeout_wc_shipping_method_count_legacy', '1628604315', 'no'),
(727, '_transient_wc_shipping_method_count_legacy', 'a:2:{s:7:\"version\";s:10:\"1626012315\";s:5:\"value\";i:0;}', 'no'),
(747, 'brand_children', 'a:0:{}', 'yes'),
(772, '_transient_timeout_wc_report_orders_stats_26d037228f0b21e09b0433f120d995f7', '1626620938', 'no'),
(773, '_transient_wc_report_orders_stats_26d037228f0b21e09b0433f120d995f7', 'a:2:{s:7:\"version\";s:10:\"1625913887\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-29\";s:10:\"date_start\";s:19:\"2021-07-11 03:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-11 00:00:00\";s:8:\"date_end\";s:19:\"2021-07-11 18:08:55\";s:12:\"date_end_gmt\";s:19:\"2021-07-11 15:08:55\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(774, '_transient_timeout_wc_report_orders_stats_7e8c841dec5f1d1a560d4aedddf4f62b', '1626620938', 'no'),
(775, '_transient_wc_report_orders_stats_7e8c841dec5f1d1a560d4aedddf4f62b', 'a:2:{s:7:\"version\";s:10:\"1625913887\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-28\";s:10:\"date_start\";s:19:\"2021-07-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-09 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-10 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-07-10 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(776, '_transient_timeout_wc_report_orders_stats_1a00a74727e9ad57888fbd33967ffe99', '1626620938', 'no'),
(777, '_transient_wc_report_orders_stats_1a00a74727e9ad57888fbd33967ffe99', 'a:2:{s:7:\"version\";s:10:\"1625913887\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-28\";s:10:\"date_start\";s:19:\"2021-07-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-09 21:00:00\";s:8:\"date_end\";s:19:\"2021-07-10 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-07-10 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(778, '_transient_timeout_wc_report_orders_stats_5154280181ebad7ba543ff5c82fc55b3', '1626620938', 'no'),
(779, '_transient_wc_report_orders_stats_5154280181ebad7ba543ff5c82fc55b3', 'a:2:{s:7:\"version\";s:10:\"1625913887\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-29\";s:10:\"date_start\";s:19:\"2021-07-11 03:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-07-11 00:00:00\";s:8:\"date_end\";s:19:\"2021-07-11 18:08:55\";s:12:\"date_end_gmt\";s:19:\"2021-07-11 15:08:55\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(789, 'options_flash_msg_0_link', 'https://www.google.com', 'no'),
(790, '_options_flash_msg_0_link', 'field_60eb0bbab9694', 'no'),
(791, 'options_flash_msg_0_title', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג 1', 'no'),
(792, '_options_flash_msg_0_title', 'field_60eb0bc4b9695', 'no'),
(793, 'options_flash_msg_1_link', 'https://www.google.com', 'no'),
(794, '_options_flash_msg_1_link', 'field_60eb0bbab9694', 'no'),
(795, 'options_flash_msg_1_title', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג 2', 'no'),
(796, '_options_flash_msg_1_title', 'field_60eb0bc4b9695', 'no'),
(797, 'options_flash_msg', '2', 'no'),
(798, '_options_flash_msg', 'field_60eb0ba1b9693', 'no'),
(810, 'options_menu_titles_0_foo_menu_title', 'ניווט מהיר', 'no'),
(811, '_options_menu_titles_0_foo_menu_title', 'field_60eb0e321c29f', 'no'),
(812, 'options_menu_titles_1_foo_menu_title', 'שירותינו', 'no'),
(813, '_options_menu_titles_1_foo_menu_title', 'field_60eb0e321c29f', 'no'),
(814, 'options_menu_titles_2_foo_menu_title', 'חם מהבלוג', 'no'),
(815, '_options_menu_titles_2_foo_menu_title', 'field_60eb0e321c29f', 'no'),
(816, 'options_menu_titles_3_foo_menu_title', 'כאן תופיע כותרת לורם', 'no'),
(817, '_options_menu_titles_3_foo_menu_title', 'field_60eb0e321c29f', 'no'),
(818, 'options_menu_titles', '4', 'no'),
(819, '_options_menu_titles', 'field_60eb0e1a1c29e', 'no'),
(879, 'options_foo_form_title', 'המקום הנכון ללמידה חדשה ועדכנית', 'no'),
(880, '_options_foo_form_title', 'field_60eb71af9d079', 'no'),
(893, 'yoast_migrations_free', 'a:1:{s:7:\"version\";s:6:\"16.6.1\";}', 'yes'),
(894, 'wpseo', 'a:44:{s:8:\"tracking\";b:0;s:22:\"license_server_version\";b:0;s:15:\"ms_defaults_set\";b:0;s:40:\"ignore_search_engines_discouraged_notice\";b:0;s:19:\"indexing_first_time\";b:1;s:16:\"indexing_started\";b:0;s:15:\"indexing_reason\";s:26:\"permalink_settings_changed\";s:29:\"indexables_indexing_completed\";b:0;s:7:\"version\";s:6:\"16.6.1\";s:16:\"previous_version\";s:0:\"\";s:20:\"disableadvanced_meta\";b:1;s:30:\"enable_headless_rest_endpoints\";b:1;s:17:\"ryte_indexability\";b:1;s:11:\"baiduverify\";s:0:\"\";s:12:\"googleverify\";s:0:\"\";s:8:\"msverify\";s:0:\"\";s:12:\"yandexverify\";s:0:\"\";s:9:\"site_type\";s:0:\"\";s:20:\"has_multiple_authors\";s:0:\"\";s:16:\"environment_type\";s:0:\"\";s:23:\"content_analysis_active\";b:1;s:23:\"keyword_analysis_active\";b:1;s:21:\"enable_admin_bar_menu\";b:1;s:26:\"enable_cornerstone_content\";b:1;s:18:\"enable_xml_sitemap\";b:1;s:24:\"enable_text_link_counter\";b:1;s:22:\"show_onboarding_notice\";b:1;s:18:\"first_activated_on\";i:1626043329;s:13:\"myyoast-oauth\";b:0;s:26:\"semrush_integration_active\";b:1;s:14:\"semrush_tokens\";a:0:{}s:20:\"semrush_country_code\";s:2:\"us\";s:19:\"permalink_structure\";s:36:\"/%year%/%monthnum%/%day%/%postname%/\";s:8:\"home_url\";s:18:\"http://gordon:8888\";s:18:\"dynamic_permalinks\";b:0;s:17:\"category_base_url\";s:0:\"\";s:12:\"tag_base_url\";s:0:\"\";s:21:\"custom_taxonomy_slugs\";a:6:{s:5:\"brand\";s:5:\"brand\";s:12:\"product_type\";s:12:\"product_type\";s:18:\"product_visibility\";s:18:\"product_visibility\";s:11:\"product_cat\";s:16:\"product-category\";s:11:\"product_tag\";s:11:\"product-tag\";s:22:\"product_shipping_class\";s:22:\"product_shipping_class\";}s:29:\"enable_enhanced_slack_sharing\";b:1;s:25:\"zapier_integration_active\";b:0;s:19:\"zapier_subscription\";a:0:{}s:14:\"zapier_api_key\";s:0:\"\";s:23:\"enable_metabox_insights\";b:1;s:23:\"enable_link_suggestions\";b:1;}', 'yes'),
(895, 'wpseo_titles', 'a:163:{s:17:\"forcerewritetitle\";b:0;s:9:\"separator\";s:7:\"sc-dash\";s:16:\"title-home-wpseo\";s:42:\"%%sitename%% %%page%% %%sep%% %%sitedesc%%\";s:18:\"title-author-wpseo\";s:43:\"%%name%%, מחבר ב-%%sitename%% %%page%%\";s:19:\"title-archive-wpseo\";s:38:\"%%date%% %%page%% %%sep%% %%sitename%%\";s:18:\"title-search-wpseo\";s:66:\"חיפוש עבור %%searchphrase%% %%page%% %%sep%% %%sitename%%\";s:15:\"title-404-wpseo\";s:45:\"העמוד לא נמצא %%sep%% %%sitename%%\";s:25:\"social-title-author-wpseo\";s:8:\"%%name%%\";s:26:\"social-title-archive-wpseo\";s:8:\"%%date%%\";s:31:\"social-description-author-wpseo\";s:0:\"\";s:32:\"social-description-archive-wpseo\";s:0:\"\";s:29:\"social-image-url-author-wpseo\";s:0:\"\";s:30:\"social-image-url-archive-wpseo\";s:0:\"\";s:28:\"social-image-id-author-wpseo\";i:0;s:29:\"social-image-id-archive-wpseo\";i:0;s:19:\"metadesc-home-wpseo\";s:0:\"\";s:21:\"metadesc-author-wpseo\";s:0:\"\";s:22:\"metadesc-archive-wpseo\";s:0:\"\";s:9:\"rssbefore\";s:0:\"\";s:8:\"rssafter\";s:66:\"הפוסט %%POSTLINK%% הופיע לראשונה ב-%%BLOGLINK%%.\";s:20:\"noindex-author-wpseo\";b:0;s:28:\"noindex-author-noposts-wpseo\";b:1;s:21:\"noindex-archive-wpseo\";b:1;s:14:\"disable-author\";b:0;s:12:\"disable-date\";b:0;s:19:\"disable-post_format\";b:0;s:18:\"disable-attachment\";b:1;s:23:\"is-media-purge-relevant\";b:0;s:20:\"breadcrumbs-404crumb\";s:36:\"שגיאה 404: הדף לא נמצא\";s:29:\"breadcrumbs-display-blog-page\";b:0;s:20:\"breadcrumbs-boldlast\";b:0;s:25:\"breadcrumbs-archiveprefix\";s:21:\"ארכיון עבור\";s:18:\"breadcrumbs-enable\";b:1;s:16:\"breadcrumbs-home\";s:13:\"דף הבית\";s:18:\"breadcrumbs-prefix\";s:0:\"\";s:24:\"breadcrumbs-searchprefix\";s:10:\"חיפשת\";s:15:\"breadcrumbs-sep\";s:1:\"|\";s:12:\"website_name\";s:0:\"\";s:11:\"person_name\";s:0:\"\";s:11:\"person_logo\";s:0:\"\";s:22:\"alternate_website_name\";s:0:\"\";s:12:\"company_logo\";s:0:\"\";s:12:\"company_name\";s:0:\"\";s:17:\"company_or_person\";s:7:\"company\";s:25:\"company_or_person_user_id\";b:0;s:17:\"stripcategorybase\";b:0;s:26:\"open_graph_frontpage_title\";s:12:\"%%sitename%%\";s:25:\"open_graph_frontpage_desc\";s:0:\"\";s:26:\"open_graph_frontpage_image\";s:0:\"\";s:10:\"title-post\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-post\";s:0:\"\";s:12:\"noindex-post\";b:0;s:23:\"display-metabox-pt-post\";b:1;s:23:\"post_types-post-maintax\";i:0;s:21:\"schema-page-type-post\";s:7:\"WebPage\";s:24:\"schema-article-type-post\";s:7:\"Article\";s:17:\"social-title-post\";s:9:\"%%title%%\";s:23:\"social-description-post\";s:0:\"\";s:21:\"social-image-url-post\";s:0:\"\";s:20:\"social-image-id-post\";i:0;s:10:\"title-page\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-page\";s:0:\"\";s:12:\"noindex-page\";b:0;s:23:\"display-metabox-pt-page\";b:1;s:23:\"post_types-page-maintax\";s:1:\"0\";s:21:\"schema-page-type-page\";s:7:\"WebPage\";s:24:\"schema-article-type-page\";s:4:\"None\";s:17:\"social-title-page\";s:9:\"%%title%%\";s:23:\"social-description-page\";s:0:\"\";s:21:\"social-image-url-page\";s:0:\"\";s:20:\"social-image-id-page\";i:0;s:16:\"title-attachment\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:19:\"metadesc-attachment\";s:0:\"\";s:18:\"noindex-attachment\";b:0;s:29:\"display-metabox-pt-attachment\";b:1;s:29:\"post_types-attachment-maintax\";s:1:\"0\";s:27:\"schema-page-type-attachment\";s:7:\"WebPage\";s:30:\"schema-article-type-attachment\";s:4:\"None\";s:18:\"title-tax-category\";s:57:\"ארכיון %%term_title%% %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-category\";s:0:\"\";s:28:\"display-metabox-tax-category\";b:1;s:20:\"noindex-tax-category\";b:0;s:25:\"social-title-tax-category\";s:27:\"ארכיון %%term_title%%\";s:31:\"social-description-tax-category\";s:0:\"\";s:29:\"social-image-url-tax-category\";s:0:\"\";s:28:\"social-image-id-tax-category\";i:0;s:18:\"title-tax-post_tag\";s:57:\"ארכיון %%term_title%% %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-post_tag\";s:0:\"\";s:28:\"display-metabox-tax-post_tag\";b:1;s:20:\"noindex-tax-post_tag\";b:0;s:25:\"social-title-tax-post_tag\";s:27:\"ארכיון %%term_title%%\";s:31:\"social-description-tax-post_tag\";s:0:\"\";s:29:\"social-image-url-tax-post_tag\";s:0:\"\";s:28:\"social-image-id-tax-post_tag\";i:0;s:21:\"title-tax-post_format\";s:57:\"ארכיון %%term_title%% %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-post_format\";s:0:\"\";s:31:\"display-metabox-tax-post_format\";b:0;s:23:\"noindex-tax-post_format\";b:1;s:28:\"social-title-tax-post_format\";s:27:\"ארכיון %%term_title%%\";s:34:\"social-description-tax-post_format\";s:0:\"\";s:32:\"social-image-url-tax-post_format\";s:0:\"\";s:31:\"social-image-id-tax-post_format\";i:0;s:13:\"title-product\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:16:\"metadesc-product\";s:0:\"\";s:15:\"noindex-product\";b:0;s:26:\"display-metabox-pt-product\";b:1;s:26:\"post_types-product-maintax\";i:0;s:24:\"schema-page-type-product\";s:7:\"WebPage\";s:27:\"schema-article-type-product\";s:4:\"None\";s:20:\"social-title-product\";s:9:\"%%title%%\";s:26:\"social-description-product\";s:0:\"\";s:24:\"social-image-url-product\";s:0:\"\";s:23:\"social-image-id-product\";i:0;s:23:\"title-ptarchive-product\";s:56:\"ארכיון %%pt_plural%% %%page%% %%sep%% %%sitename%%\";s:26:\"metadesc-ptarchive-product\";s:0:\"\";s:25:\"bctitle-ptarchive-product\";s:0:\"\";s:25:\"noindex-ptarchive-product\";b:0;s:30:\"social-title-ptarchive-product\";s:26:\"ארכיון %%pt_plural%%\";s:36:\"social-description-ptarchive-product\";s:0:\"\";s:34:\"social-image-url-ptarchive-product\";s:0:\"\";s:33:\"social-image-id-ptarchive-product\";i:0;s:15:\"title-tax-brand\";s:57:\"ארכיון %%term_title%% %%page%% %%sep%% %%sitename%%\";s:18:\"metadesc-tax-brand\";s:0:\"\";s:25:\"display-metabox-tax-brand\";b:1;s:17:\"noindex-tax-brand\";b:0;s:22:\"social-title-tax-brand\";s:27:\"ארכיון %%term_title%%\";s:28:\"social-description-tax-brand\";s:0:\"\";s:26:\"social-image-url-tax-brand\";s:0:\"\";s:25:\"social-image-id-tax-brand\";i:0;s:23:\"taxonomy-brand-ptparent\";i:0;s:21:\"title-tax-product_cat\";s:57:\"ארכיון %%term_title%% %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-product_cat\";s:0:\"\";s:31:\"display-metabox-tax-product_cat\";b:1;s:23:\"noindex-tax-product_cat\";b:0;s:28:\"social-title-tax-product_cat\";s:27:\"ארכיון %%term_title%%\";s:34:\"social-description-tax-product_cat\";s:0:\"\";s:32:\"social-image-url-tax-product_cat\";s:0:\"\";s:31:\"social-image-id-tax-product_cat\";i:0;s:29:\"taxonomy-product_cat-ptparent\";i:0;s:21:\"title-tax-product_tag\";s:57:\"ארכיון %%term_title%% %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-product_tag\";s:0:\"\";s:31:\"display-metabox-tax-product_tag\";b:1;s:23:\"noindex-tax-product_tag\";b:0;s:28:\"social-title-tax-product_tag\";s:27:\"ארכיון %%term_title%%\";s:34:\"social-description-tax-product_tag\";s:0:\"\";s:32:\"social-image-url-tax-product_tag\";s:0:\"\";s:31:\"social-image-id-tax-product_tag\";i:0;s:29:\"taxonomy-product_tag-ptparent\";i:0;s:32:\"title-tax-product_shipping_class\";s:57:\"ארכיון %%term_title%% %%page%% %%sep%% %%sitename%%\";s:35:\"metadesc-tax-product_shipping_class\";s:0:\"\";s:42:\"display-metabox-tax-product_shipping_class\";b:1;s:34:\"noindex-tax-product_shipping_class\";b:0;s:39:\"social-title-tax-product_shipping_class\";s:27:\"ארכיון %%term_title%%\";s:45:\"social-description-tax-product_shipping_class\";s:0:\"\";s:43:\"social-image-url-tax-product_shipping_class\";s:0:\"\";s:42:\"social-image-id-tax-product_shipping_class\";i:0;s:40:\"taxonomy-product_shipping_class-ptparent\";i:0;s:14:\"person_logo_id\";i:0;s:15:\"company_logo_id\";i:0;s:29:\"open_graph_frontpage_image_id\";i:0;s:26:\"taxonomy-category-ptparent\";s:1:\"0\";s:26:\"taxonomy-post_tag-ptparent\";s:1:\"0\";s:29:\"taxonomy-post_format-ptparent\";s:1:\"0\";}', 'yes'),
(896, 'wpseo_social', 'a:18:{s:13:\"facebook_site\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:11:\"myspace_url\";s:0:\"\";s:16:\"og_default_image\";s:0:\"\";s:19:\"og_default_image_id\";s:0:\"\";s:18:\"og_frontpage_title\";s:0:\"\";s:17:\"og_frontpage_desc\";s:0:\"\";s:18:\"og_frontpage_image\";s:0:\"\";s:21:\"og_frontpage_image_id\";s:0:\"\";s:9:\"opengraph\";b:1;s:13:\"pinterest_url\";s:0:\"\";s:15:\"pinterestverify\";s:0:\"\";s:7:\"twitter\";b:1;s:12:\"twitter_site\";s:0:\"\";s:17:\"twitter_card_type\";s:19:\"summary_large_image\";s:11:\"youtube_url\";s:0:\"\";s:13:\"wikipedia_url\";s:0:\"\";}', 'yes'),
(909, 'wpseo_ryte', 'a:2:{s:6:\"status\";i:-1;s:10:\"last_fetch\";i:1626043332;}', 'yes'),
(914, '_transient_timeout_yoast_i18n_wordpress-seo_he_IL', '1626129843', 'no'),
(915, '_transient_yoast_i18n_wordpress-seo_he_IL', 'O:8:\"stdClass\":14:{s:2:\"id\";i:396162;s:4:\"name\";s:6:\"Hebrew\";s:4:\"slug\";s:7:\"default\";s:10:\"project_id\";i:3158;s:6:\"locale\";s:2:\"he\";s:13:\"current_count\";i:1209;s:18:\"untranslated_count\";i:86;s:13:\"waiting_count\";i:1;s:11:\"fuzzy_count\";i:10;s:9:\"all_count\";i:1306;s:14:\"warnings_count\";i:0;s:18:\"percent_translated\";i:92;s:9:\"wp_locale\";s:5:\"he_IL\";s:13:\"last_modified\";s:19:\"2021-03-22 15:34:46\";}', 'no'),
(1342, 'new_admin_email', 'maxf@leos.co.il', 'yes'),
(1343, 'stage', 'prod', 'yes'),
(1353, '_transient_timeout_acf_plugin_updates', '1626262028', 'no'),
(1354, '_transient_acf_plugin_updates', 'a:4:{s:7:\"plugins\";a:0:{}s:10:\"expiration\";i:172800;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.9.8\";}}', 'no'),
(1435, '_site_transient_timeout_available_translations', '1626131598', 'no');
INSERT INTO `fmn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1436, '_site_transient_available_translations', 'a:126:{s:2:\"af\";a:8:{s:8:\"language\";s:2:\"af\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-05-13 15:59:22\";s:12:\"english_name\";s:9:\"Afrikaans\";s:11:\"native_name\";s:9:\"Afrikaans\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.2/af.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"af\";i:2;s:3:\"afr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Gaan voort\";}}s:2:\"ar\";a:8:{s:8:\"language\";s:2:\"ar\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-07-08 18:29:33\";s:12:\"english_name\";s:6:\"Arabic\";s:11:\"native_name\";s:14:\"العربية\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.2/ar.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:2;s:3:\"ara\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:3:\"ary\";a:8:{s:8:\"language\";s:3:\"ary\";s:7:\"version\";s:6:\"4.8.17\";s:7:\"updated\";s:19:\"2017-01-26 15:42:35\";s:12:\"english_name\";s:15:\"Moroccan Arabic\";s:11:\"native_name\";s:31:\"العربية المغربية\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/translation/core/4.8.17/ary.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:3;s:3:\"ary\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:2:\"as\";a:8:{s:8:\"language\";s:2:\"as\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-14 14:01:58\";s:12:\"english_name\";s:8:\"Assamese\";s:11:\"native_name\";s:21:\"অসমীয়া\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.2/as.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"as\";i:2;s:3:\"asm\";i:3;s:3:\"asm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:3:\"azb\";a:8:{s:8:\"language\";s:3:\"azb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-12 20:34:31\";s:12:\"english_name\";s:17:\"South Azerbaijani\";s:11:\"native_name\";s:29:\"گؤنئی آذربایجان\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/azb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:3;s:3:\"azb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"az\";a:8:{s:8:\"language\";s:2:\"az\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-06 00:09:27\";s:12:\"english_name\";s:11:\"Azerbaijani\";s:11:\"native_name\";s:16:\"Azərbaycan dili\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/az.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:2;s:3:\"aze\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Davam\";}}s:3:\"bel\";a:8:{s:8:\"language\";s:3:\"bel\";s:7:\"version\";s:6:\"4.9.18\";s:7:\"updated\";s:19:\"2019-10-29 07:54:22\";s:12:\"english_name\";s:10:\"Belarusian\";s:11:\"native_name\";s:29:\"Беларуская мова\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/translation/core/4.9.18/bel.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"be\";i:2;s:3:\"bel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Працягнуць\";}}s:5:\"bg_BG\";a:8:{s:8:\"language\";s:5:\"bg_BG\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-20 19:46:45\";s:12:\"english_name\";s:9:\"Bulgarian\";s:11:\"native_name\";s:18:\"Български\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/bg_BG.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bg\";i:2;s:3:\"bul\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:22:\"Продължение\";}}s:5:\"bn_BD\";a:8:{s:8:\"language\";s:5:\"bn_BD\";s:7:\"version\";s:5:\"5.4.6\";s:7:\"updated\";s:19:\"2020-10-31 08:48:37\";s:12:\"english_name\";s:20:\"Bengali (Bangladesh)\";s:11:\"native_name\";s:15:\"বাংলা\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.6/bn_BD.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"bn\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:23:\"এগিয়ে চল.\";}}s:2:\"bo\";a:8:{s:8:\"language\";s:2:\"bo\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2020-10-30 03:24:38\";s:12:\"english_name\";s:7:\"Tibetan\";s:11:\"native_name\";s:21:\"བོད་ཡིག\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.2/bo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bo\";i:2;s:3:\"tib\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"མུ་མཐུད།\";}}s:5:\"bs_BA\";a:8:{s:8:\"language\";s:5:\"bs_BA\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-04-25 07:27:37\";s:12:\"english_name\";s:7:\"Bosnian\";s:11:\"native_name\";s:8:\"Bosanski\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/bs_BA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bs\";i:2;s:3:\"bos\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:2:\"ca\";a:8:{s:8:\"language\";s:2:\"ca\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-07-06 08:29:04\";s:12:\"english_name\";s:7:\"Catalan\";s:11:\"native_name\";s:7:\"Català\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.2/ca.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ca\";i:2;s:3:\"cat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:3:\"ceb\";a:8:{s:8:\"language\";s:3:\"ceb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-02 17:25:51\";s:12:\"english_name\";s:7:\"Cebuano\";s:11:\"native_name\";s:7:\"Cebuano\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/ceb.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"ceb\";i:3;s:3:\"ceb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Padayun\";}}s:5:\"cs_CZ\";a:8:{s:8:\"language\";s:5:\"cs_CZ\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-30 15:14:54\";s:12:\"english_name\";s:5:\"Czech\";s:11:\"native_name\";s:9:\"Čeština\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/cs_CZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cs\";i:2;s:3:\"ces\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:11:\"Pokračovat\";}}s:2:\"cy\";a:8:{s:8:\"language\";s:2:\"cy\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-04-15 10:32:41\";s:12:\"english_name\";s:5:\"Welsh\";s:11:\"native_name\";s:7:\"Cymraeg\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.2/cy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cy\";i:2;s:3:\"cym\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Parhau\";}}s:5:\"da_DK\";a:8:{s:8:\"language\";s:5:\"da_DK\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-07-09 08:43:53\";s:12:\"english_name\";s:6:\"Danish\";s:11:\"native_name\";s:5:\"Dansk\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/da_DK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"da\";i:2;s:3:\"dan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Forts&#230;t\";}}s:5:\"de_CH\";a:8:{s:8:\"language\";s:5:\"de_CH\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-03-14 20:06:23\";s:12:\"english_name\";s:20:\"German (Switzerland)\";s:11:\"native_name\";s:17:\"Deutsch (Schweiz)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/de_CH.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Fortfahren\";}}s:5:\"de_AT\";a:8:{s:8:\"language\";s:5:\"de_AT\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-08 07:32:43\";s:12:\"english_name\";s:16:\"German (Austria)\";s:11:\"native_name\";s:21:\"Deutsch (Österreich)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/de_AT.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:5:\"de_DE\";a:8:{s:8:\"language\";s:5:\"de_DE\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-07-07 23:24:00\";s:12:\"english_name\";s:6:\"German\";s:11:\"native_name\";s:7:\"Deutsch\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/de_DE.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Fortfahren\";}}s:12:\"de_DE_formal\";a:8:{s:8:\"language\";s:12:\"de_DE_formal\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-07-07 23:23:07\";s:12:\"english_name\";s:15:\"German (Formal)\";s:11:\"native_name\";s:13:\"Deutsch (Sie)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/5.7.2/de_DE_formal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Fortfahren\";}}s:14:\"de_CH_informal\";a:8:{s:8:\"language\";s:14:\"de_CH_informal\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-03-14 20:06:52\";s:12:\"english_name\";s:30:\"German (Switzerland, Informal)\";s:11:\"native_name\";s:21:\"Deutsch (Schweiz, Du)\";s:7:\"package\";s:73:\"https://downloads.wordpress.org/translation/core/5.7.2/de_CH_informal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:3:\"dsb\";a:8:{s:8:\"language\";s:3:\"dsb\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-04-15 13:33:04\";s:12:\"english_name\";s:13:\"Lower Sorbian\";s:11:\"native_name\";s:16:\"Dolnoserbšćina\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.7.2/dsb.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"dsb\";i:3;s:3:\"dsb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Dalej\";}}s:3:\"dzo\";a:8:{s:8:\"language\";s:3:\"dzo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-06-29 08:59:03\";s:12:\"english_name\";s:8:\"Dzongkha\";s:11:\"native_name\";s:18:\"རྫོང་ཁ\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/dzo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"dz\";i:2;s:3:\"dzo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"el\";a:8:{s:8:\"language\";s:2:\"el\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-30 19:50:02\";s:12:\"english_name\";s:5:\"Greek\";s:11:\"native_name\";s:16:\"Ελληνικά\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.2/el.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"el\";i:2;s:3:\"ell\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Συνέχεια\";}}s:5:\"en_CA\";a:8:{s:8:\"language\";s:5:\"en_CA\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-04-15 04:12:51\";s:12:\"english_name\";s:16:\"English (Canada)\";s:11:\"native_name\";s:16:\"English (Canada)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/en_CA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_AU\";a:8:{s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-04-15 04:12:40\";s:12:\"english_name\";s:19:\"English (Australia)\";s:11:\"native_name\";s:19:\"English (Australia)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/en_AU.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_ZA\";a:8:{s:8:\"language\";s:5:\"en_ZA\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-04-15 07:22:30\";s:12:\"english_name\";s:22:\"English (South Africa)\";s:11:\"native_name\";s:22:\"English (South Africa)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/en_ZA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_GB\";a:8:{s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-16 08:10:36\";s:12:\"english_name\";s:12:\"English (UK)\";s:11:\"native_name\";s:12:\"English (UK)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/en_GB.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_NZ\";a:8:{s:8:\"language\";s:5:\"en_NZ\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-04-15 04:12:28\";s:12:\"english_name\";s:21:\"English (New Zealand)\";s:11:\"native_name\";s:21:\"English (New Zealand)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/en_NZ.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"eo\";a:8:{s:8:\"language\";s:2:\"eo\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-04-18 09:35:35\";s:12:\"english_name\";s:9:\"Esperanto\";s:11:\"native_name\";s:9:\"Esperanto\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.2/eo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eo\";i:2;s:3:\"epo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Daŭrigi\";}}s:5:\"es_EC\";a:8:{s:8:\"language\";s:5:\"es_EC\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-04-15 02:05:34\";s:12:\"english_name\";s:17:\"Spanish (Ecuador)\";s:11:\"native_name\";s:19:\"Español de Ecuador\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/es_EC.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CL\";a:8:{s:8:\"language\";s:5:\"es_CL\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-14 16:02:22\";s:12:\"english_name\";s:15:\"Spanish (Chile)\";s:11:\"native_name\";s:17:\"Español de Chile\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/es_CL.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_ES\";a:8:{s:8:\"language\";s:5:\"es_ES\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-10 20:43:51\";s:12:\"english_name\";s:15:\"Spanish (Spain)\";s:11:\"native_name\";s:8:\"Español\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/es_ES.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CO\";a:8:{s:8:\"language\";s:5:\"es_CO\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-04 22:59:08\";s:12:\"english_name\";s:18:\"Spanish (Colombia)\";s:11:\"native_name\";s:20:\"Español de Colombia\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/es_CO.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CR\";a:8:{s:8:\"language\";s:5:\"es_CR\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-02 03:00:51\";s:12:\"english_name\";s:20:\"Spanish (Costa Rica)\";s:11:\"native_name\";s:22:\"Español de Costa Rica\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/es_CR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_PE\";a:8:{s:8:\"language\";s:5:\"es_PE\";s:7:\"version\";s:5:\"5.6.4\";s:7:\"updated\";s:19:\"2020-12-11 02:12:59\";s:12:\"english_name\";s:14:\"Spanish (Peru)\";s:11:\"native_name\";s:17:\"Español de Perú\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.6.4/es_PE.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_PR\";a:8:{s:8:\"language\";s:5:\"es_PR\";s:7:\"version\";s:5:\"5.4.6\";s:7:\"updated\";s:19:\"2020-04-29 15:36:59\";s:12:\"english_name\";s:21:\"Spanish (Puerto Rico)\";s:11:\"native_name\";s:23:\"Español de Puerto Rico\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.6/es_PR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_GT\";a:8:{s:8:\"language\";s:5:\"es_GT\";s:7:\"version\";s:6:\"5.2.11\";s:7:\"updated\";s:19:\"2019-03-02 06:35:01\";s:12:\"english_name\";s:19:\"Spanish (Guatemala)\";s:11:\"native_name\";s:21:\"Español de Guatemala\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/5.2.11/es_GT.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_MX\";a:8:{s:8:\"language\";s:5:\"es_MX\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-04-16 13:07:32\";s:12:\"english_name\";s:16:\"Spanish (Mexico)\";s:11:\"native_name\";s:19:\"Español de México\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/es_MX.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_AR\";a:8:{s:8:\"language\";s:5:\"es_AR\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-04-16 02:17:21\";s:12:\"english_name\";s:19:\"Spanish (Argentina)\";s:11:\"native_name\";s:21:\"Español de Argentina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/es_AR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_VE\";a:8:{s:8:\"language\";s:5:\"es_VE\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-04-15 02:05:15\";s:12:\"english_name\";s:19:\"Spanish (Venezuela)\";s:11:\"native_name\";s:21:\"Español de Venezuela\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/es_VE.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_UY\";a:8:{s:8:\"language\";s:5:\"es_UY\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-03-31 18:33:26\";s:12:\"english_name\";s:17:\"Spanish (Uruguay)\";s:11:\"native_name\";s:19:\"Español de Uruguay\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/es_UY.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"et\";a:8:{s:8:\"language\";s:2:\"et\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2020-08-12 08:38:59\";s:12:\"english_name\";s:8:\"Estonian\";s:11:\"native_name\";s:5:\"Eesti\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.2/et.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"et\";i:2;s:3:\"est\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Jätka\";}}s:2:\"eu\";a:8:{s:8:\"language\";s:2:\"eu\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-24 16:08:10\";s:12:\"english_name\";s:6:\"Basque\";s:11:\"native_name\";s:7:\"Euskara\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.2/eu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eu\";i:2;s:3:\"eus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Jarraitu\";}}s:5:\"fa_AF\";a:8:{s:8:\"language\";s:5:\"fa_AF\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-14 12:40:09\";s:12:\"english_name\";s:21:\"Persian (Afghanistan)\";s:11:\"native_name\";s:31:\"(فارسی (افغانستان\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/fa_AF.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fa\";i:2;s:3:\"fas\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:5:\"fa_IR\";a:8:{s:8:\"language\";s:5:\"fa_IR\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-28 10:15:19\";s:12:\"english_name\";s:7:\"Persian\";s:11:\"native_name\";s:10:\"فارسی\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/fa_IR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fa\";i:2;s:3:\"fas\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:2:\"fi\";a:8:{s:8:\"language\";s:2:\"fi\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-05-06 05:21:48\";s:12:\"english_name\";s:7:\"Finnish\";s:11:\"native_name\";s:5:\"Suomi\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.2/fi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fi\";i:2;s:3:\"fin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Jatka\";}}s:5:\"fr_CA\";a:8:{s:8:\"language\";s:5:\"fr_CA\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-04-30 13:29:35\";s:12:\"english_name\";s:15:\"French (Canada)\";s:11:\"native_name\";s:19:\"Français du Canada\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/fr_CA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_FR\";a:8:{s:8:\"language\";s:5:\"fr_FR\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-24 08:28:29\";s:12:\"english_name\";s:15:\"French (France)\";s:11:\"native_name\";s:9:\"Français\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/fr_FR.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"fr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_BE\";a:8:{s:8:\"language\";s:5:\"fr_BE\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-02-22 13:54:46\";s:12:\"english_name\";s:16:\"French (Belgium)\";s:11:\"native_name\";s:21:\"Français de Belgique\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/fr_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:3:\"fur\";a:8:{s:8:\"language\";s:3:\"fur\";s:7:\"version\";s:6:\"4.8.17\";s:7:\"updated\";s:19:\"2018-01-29 17:32:35\";s:12:\"english_name\";s:8:\"Friulian\";s:11:\"native_name\";s:8:\"Friulian\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/translation/core/4.8.17/fur.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"fur\";i:3;s:3:\"fur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"gd\";a:8:{s:8:\"language\";s:2:\"gd\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-08-23 17:41:37\";s:12:\"english_name\";s:15:\"Scottish Gaelic\";s:11:\"native_name\";s:9:\"Gàidhlig\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/gd.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"gd\";i:2;s:3:\"gla\";i:3;s:3:\"gla\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"Lean air adhart\";}}s:5:\"gl_ES\";a:8:{s:8:\"language\";s:5:\"gl_ES\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-21 09:58:55\";s:12:\"english_name\";s:8:\"Galician\";s:11:\"native_name\";s:6:\"Galego\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/gl_ES.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gl\";i:2;s:3:\"glg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"gu\";a:8:{s:8:\"language\";s:2:\"gu\";s:7:\"version\";s:6:\"4.9.18\";s:7:\"updated\";s:19:\"2018-09-14 12:33:48\";s:12:\"english_name\";s:8:\"Gujarati\";s:11:\"native_name\";s:21:\"ગુજરાતી\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.18/gu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gu\";i:2;s:3:\"guj\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:31:\"ચાલુ રાખવું\";}}s:3:\"haz\";a:8:{s:8:\"language\";s:3:\"haz\";s:7:\"version\";s:6:\"4.4.25\";s:7:\"updated\";s:19:\"2015-12-05 00:59:09\";s:12:\"english_name\";s:8:\"Hazaragi\";s:11:\"native_name\";s:15:\"هزاره گی\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/translation/core/4.4.25/haz.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"haz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:5:\"he_IL\";a:8:{s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-05-28 16:42:59\";s:12:\"english_name\";s:6:\"Hebrew\";s:11:\"native_name\";s:16:\"עִבְרִית\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/he_IL.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"he\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"להמשיך\";}}s:5:\"hi_IN\";a:8:{s:8:\"language\";s:5:\"hi_IN\";s:7:\"version\";s:5:\"5.4.6\";s:7:\"updated\";s:19:\"2020-11-06 12:34:38\";s:12:\"english_name\";s:5:\"Hindi\";s:11:\"native_name\";s:18:\"हिन्दी\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.6/hi_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hi\";i:2;s:3:\"hin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"जारी\";}}s:2:\"hr\";a:8:{s:8:\"language\";s:2:\"hr\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-05-13 08:03:31\";s:12:\"english_name\";s:8:\"Croatian\";s:11:\"native_name\";s:8:\"Hrvatski\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.2/hr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hr\";i:2;s:3:\"hrv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:3:\"hsb\";a:8:{s:8:\"language\";s:3:\"hsb\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-04-15 13:34:18\";s:12:\"english_name\";s:13:\"Upper Sorbian\";s:11:\"native_name\";s:17:\"Hornjoserbšćina\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.7.2/hsb.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"hsb\";i:3;s:3:\"hsb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:4:\"Dale\";}}s:5:\"hu_HU\";a:8:{s:8:\"language\";s:5:\"hu_HU\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-26 14:52:10\";s:12:\"english_name\";s:9:\"Hungarian\";s:11:\"native_name\";s:6:\"Magyar\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/hu_HU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hu\";i:2;s:3:\"hun\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Tovább\";}}s:2:\"hy\";a:8:{s:8:\"language\";s:2:\"hy\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-03 16:21:10\";s:12:\"english_name\";s:8:\"Armenian\";s:11:\"native_name\";s:14:\"Հայերեն\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/hy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hy\";i:2;s:3:\"hye\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Շարունակել\";}}s:5:\"id_ID\";a:8:{s:8:\"language\";s:5:\"id_ID\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-04-24 02:11:27\";s:12:\"english_name\";s:10:\"Indonesian\";s:11:\"native_name\";s:16:\"Bahasa Indonesia\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/id_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"id\";i:2;s:3:\"ind\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Lanjutkan\";}}s:5:\"is_IS\";a:8:{s:8:\"language\";s:5:\"is_IS\";s:7:\"version\";s:6:\"4.9.18\";s:7:\"updated\";s:19:\"2018-12-11 10:40:02\";s:12:\"english_name\";s:9:\"Icelandic\";s:11:\"native_name\";s:9:\"Íslenska\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.18/is_IS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"is\";i:2;s:3:\"isl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Áfram\";}}s:5:\"it_IT\";a:8:{s:8:\"language\";s:5:\"it_IT\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-18 13:45:37\";s:12:\"english_name\";s:7:\"Italian\";s:11:\"native_name\";s:8:\"Italiano\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/it_IT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"it\";i:2;s:3:\"ita\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:2:\"ja\";a:8:{s:8:\"language\";s:2:\"ja\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-05-20 01:00:08\";s:12:\"english_name\";s:8:\"Japanese\";s:11:\"native_name\";s:9:\"日本語\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.2/ja.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"ja\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"続ける\";}}s:5:\"jv_ID\";a:8:{s:8:\"language\";s:5:\"jv_ID\";s:7:\"version\";s:6:\"4.9.18\";s:7:\"updated\";s:19:\"2019-02-16 23:58:56\";s:12:\"english_name\";s:8:\"Javanese\";s:11:\"native_name\";s:9:\"Basa Jawa\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.18/jv_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"jv\";i:2;s:3:\"jav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nutugne\";}}s:5:\"ka_GE\";a:8:{s:8:\"language\";s:5:\"ka_GE\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-07-03 08:25:10\";s:12:\"english_name\";s:8:\"Georgian\";s:11:\"native_name\";s:21:\"ქართული\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/ka_GE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ka\";i:2;s:3:\"kat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"გაგრძელება\";}}s:3:\"kab\";a:8:{s:8:\"language\";s:3:\"kab\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-30 20:40:17\";s:12:\"english_name\";s:6:\"Kabyle\";s:11:\"native_name\";s:9:\"Taqbaylit\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.7.2/kab.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"kab\";i:3;s:3:\"kab\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:2:\"kk\";a:8:{s:8:\"language\";s:2:\"kk\";s:7:\"version\";s:6:\"4.9.18\";s:7:\"updated\";s:19:\"2018-07-10 11:35:44\";s:12:\"english_name\";s:6:\"Kazakh\";s:11:\"native_name\";s:19:\"Қазақ тілі\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.18/kk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"kk\";i:2;s:3:\"kaz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Жалғастыру\";}}s:2:\"km\";a:8:{s:8:\"language\";s:2:\"km\";s:7:\"version\";s:6:\"5.2.11\";s:7:\"updated\";s:19:\"2019-06-10 16:18:28\";s:12:\"english_name\";s:5:\"Khmer\";s:11:\"native_name\";s:27:\"ភាសាខ្មែរ\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2.11/km.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"km\";i:2;s:3:\"khm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"បន្ត\";}}s:2:\"kn\";a:8:{s:8:\"language\";s:2:\"kn\";s:7:\"version\";s:6:\"4.9.18\";s:7:\"updated\";s:19:\"2020-09-30 14:08:59\";s:12:\"english_name\";s:7:\"Kannada\";s:11:\"native_name\";s:15:\"ಕನ್ನಡ\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.18/kn.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"kn\";i:2;s:3:\"kan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"ಮುಂದುವರೆಸಿ\";}}s:5:\"ko_KR\";a:8:{s:8:\"language\";s:5:\"ko_KR\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-07-08 04:56:41\";s:12:\"english_name\";s:6:\"Korean\";s:11:\"native_name\";s:9:\"한국어\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/ko_KR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ko\";i:2;s:3:\"kor\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"계속\";}}s:3:\"ckb\";a:8:{s:8:\"language\";s:3:\"ckb\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-17 10:36:11\";s:12:\"english_name\";s:16:\"Kurdish (Sorani)\";s:11:\"native_name\";s:13:\"كوردی‎\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.7.2/ckb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ku\";i:3;s:3:\"ckb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"به‌رده‌وام به‌\";}}s:2:\"lo\";a:8:{s:8:\"language\";s:2:\"lo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 09:59:23\";s:12:\"english_name\";s:3:\"Lao\";s:11:\"native_name\";s:21:\"ພາສາລາວ\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/lo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lo\";i:2;s:3:\"lao\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"ຕໍ່\";}}s:5:\"lt_LT\";a:8:{s:8:\"language\";s:5:\"lt_LT\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-03-23 12:35:40\";s:12:\"english_name\";s:10:\"Lithuanian\";s:11:\"native_name\";s:15:\"Lietuvių kalba\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/lt_LT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lt\";i:2;s:3:\"lit\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Tęsti\";}}s:2:\"lv\";a:8:{s:8:\"language\";s:2:\"lv\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-27 17:07:17\";s:12:\"english_name\";s:7:\"Latvian\";s:11:\"native_name\";s:16:\"Latviešu valoda\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.2/lv.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lv\";i:2;s:3:\"lav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Turpināt\";}}s:5:\"mk_MK\";a:8:{s:8:\"language\";s:5:\"mk_MK\";s:7:\"version\";s:5:\"5.4.6\";s:7:\"updated\";s:19:\"2020-07-01 09:16:57\";s:12:\"english_name\";s:10:\"Macedonian\";s:11:\"native_name\";s:31:\"Македонски јазик\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.4.6/mk_MK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mk\";i:2;s:3:\"mkd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Продолжи\";}}s:5:\"ml_IN\";a:8:{s:8:\"language\";s:5:\"ml_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:43:32\";s:12:\"english_name\";s:9:\"Malayalam\";s:11:\"native_name\";s:18:\"മലയാളം\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ml_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ml\";i:2;s:3:\"mal\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"തുടരുക\";}}s:2:\"mn\";a:8:{s:8:\"language\";s:2:\"mn\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-12 07:29:35\";s:12:\"english_name\";s:9:\"Mongolian\";s:11:\"native_name\";s:12:\"Монгол\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/mn.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mn\";i:2;s:3:\"mon\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"Үргэлжлүүлэх\";}}s:2:\"mr\";a:8:{s:8:\"language\";s:2:\"mr\";s:7:\"version\";s:6:\"4.9.18\";s:7:\"updated\";s:19:\"2019-11-22 15:32:08\";s:12:\"english_name\";s:7:\"Marathi\";s:11:\"native_name\";s:15:\"मराठी\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.18/mr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mr\";i:2;s:3:\"mar\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"सुरु ठेवा\";}}s:5:\"ms_MY\";a:8:{s:8:\"language\";s:5:\"ms_MY\";s:7:\"version\";s:6:\"4.9.18\";s:7:\"updated\";s:19:\"2018-08-31 11:57:07\";s:12:\"english_name\";s:5:\"Malay\";s:11:\"native_name\";s:13:\"Bahasa Melayu\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.18/ms_MY.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ms\";i:2;s:3:\"msa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Teruskan\";}}s:5:\"my_MM\";a:8:{s:8:\"language\";s:5:\"my_MM\";s:7:\"version\";s:6:\"4.2.30\";s:7:\"updated\";s:19:\"2017-12-26 11:57:10\";s:12:\"english_name\";s:17:\"Myanmar (Burmese)\";s:11:\"native_name\";s:15:\"ဗမာစာ\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.2.30/my_MM.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"my\";i:2;s:3:\"mya\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:54:\"ဆက်လက်လုပ်ေဆာင်ပါ။\";}}s:5:\"nb_NO\";a:8:{s:8:\"language\";s:5:\"nb_NO\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-08 06:18:21\";s:12:\"english_name\";s:19:\"Norwegian (Bokmål)\";s:11:\"native_name\";s:13:\"Norsk bokmål\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/nb_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nb\";i:2;s:3:\"nob\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsett\";}}s:5:\"ne_NP\";a:8:{s:8:\"language\";s:5:\"ne_NP\";s:7:\"version\";s:6:\"5.2.11\";s:7:\"updated\";s:19:\"2020-05-31 16:07:59\";s:12:\"english_name\";s:6:\"Nepali\";s:11:\"native_name\";s:18:\"नेपाली\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/5.2.11/ne_NP.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ne\";i:2;s:3:\"nep\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:31:\"जारीराख्नु \";}}s:12:\"nl_NL_formal\";a:8:{s:8:\"language\";s:12:\"nl_NL_formal\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-05-10 14:42:01\";s:12:\"english_name\";s:14:\"Dutch (Formal)\";s:11:\"native_name\";s:20:\"Nederlands (Formeel)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/5.7.2/nl_NL_formal.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nl_BE\";a:8:{s:8:\"language\";s:5:\"nl_BE\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-18 08:55:40\";s:12:\"english_name\";s:15:\"Dutch (Belgium)\";s:11:\"native_name\";s:20:\"Nederlands (België)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/nl_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nl_NL\";a:8:{s:8:\"language\";s:5:\"nl_NL\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-17 11:46:51\";s:12:\"english_name\";s:5:\"Dutch\";s:11:\"native_name\";s:10:\"Nederlands\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/nl_NL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nn_NO\";a:8:{s:8:\"language\";s:5:\"nn_NO\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-03-18 10:59:16\";s:12:\"english_name\";s:19:\"Norwegian (Nynorsk)\";s:11:\"native_name\";s:13:\"Norsk nynorsk\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/nn_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nn\";i:2;s:3:\"nno\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Hald fram\";}}s:3:\"oci\";a:8:{s:8:\"language\";s:3:\"oci\";s:7:\"version\";s:6:\"4.8.17\";s:7:\"updated\";s:19:\"2017-08-25 10:03:08\";s:12:\"english_name\";s:7:\"Occitan\";s:11:\"native_name\";s:7:\"Occitan\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/translation/core/4.8.17/oci.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"oc\";i:2;s:3:\"oci\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Contunhar\";}}s:5:\"pa_IN\";a:8:{s:8:\"language\";s:5:\"pa_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-16 05:19:43\";s:12:\"english_name\";s:7:\"Punjabi\";s:11:\"native_name\";s:18:\"ਪੰਜਾਬੀ\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/pa_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pa\";i:2;s:3:\"pan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"ਜਾਰੀ ਰੱਖੋ\";}}s:5:\"pl_PL\";a:8:{s:8:\"language\";s:5:\"pl_PL\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-07-10 17:09:40\";s:12:\"english_name\";s:6:\"Polish\";s:11:\"native_name\";s:6:\"Polski\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/pl_PL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pl\";i:2;s:3:\"pol\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Kontynuuj\";}}s:2:\"ps\";a:8:{s:8:\"language\";s:2:\"ps\";s:7:\"version\";s:6:\"4.3.26\";s:7:\"updated\";s:19:\"2015-12-02 21:41:29\";s:12:\"english_name\";s:6:\"Pashto\";s:11:\"native_name\";s:8:\"پښتو\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.3.26/ps.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ps\";i:2;s:3:\"pus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"دوام\";}}s:10:\"pt_PT_ao90\";a:8:{s:8:\"language\";s:10:\"pt_PT_ao90\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-04-15 08:18:42\";s:12:\"english_name\";s:27:\"Portuguese (Portugal, AO90)\";s:11:\"native_name\";s:17:\"Português (AO90)\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/translation/core/5.7.2/pt_PT_ao90.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_BR\";a:8:{s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-02 12:53:00\";s:12:\"english_name\";s:19:\"Portuguese (Brazil)\";s:11:\"native_name\";s:20:\"Português do Brasil\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/pt_BR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pt\";i:2;s:3:\"por\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_AO\";a:8:{s:8:\"language\";s:5:\"pt_AO\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-05-30 09:51:29\";s:12:\"english_name\";s:19:\"Portuguese (Angola)\";s:11:\"native_name\";s:20:\"Português de Angola\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/pt_AO.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_PT\";a:8:{s:8:\"language\";s:5:\"pt_PT\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-07-09 09:41:48\";s:12:\"english_name\";s:21:\"Portuguese (Portugal)\";s:11:\"native_name\";s:10:\"Português\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/pt_PT.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:3:\"rhg\";a:8:{s:8:\"language\";s:3:\"rhg\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-16 13:03:18\";s:12:\"english_name\";s:8:\"Rohingya\";s:11:\"native_name\";s:8:\"Ruáinga\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/rhg.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"rhg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"ro_RO\";a:8:{s:8:\"language\";s:5:\"ro_RO\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-07-12 10:53:36\";s:12:\"english_name\";s:8:\"Romanian\";s:11:\"native_name\";s:8:\"Română\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/ro_RO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ro\";i:2;s:3:\"ron\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuă\";}}s:5:\"ru_RU\";a:8:{s:8:\"language\";s:5:\"ru_RU\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-14 18:30:49\";s:12:\"english_name\";s:7:\"Russian\";s:11:\"native_name\";s:14:\"Русский\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/ru_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ru\";i:2;s:3:\"rus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продолжить\";}}s:3:\"sah\";a:8:{s:8:\"language\";s:3:\"sah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-21 02:06:41\";s:12:\"english_name\";s:5:\"Sakha\";s:11:\"native_name\";s:14:\"Сахалыы\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/sah.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"sah\";i:3;s:3:\"sah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Салҕаа\";}}s:3:\"snd\";a:8:{s:8:\"language\";s:3:\"snd\";s:7:\"version\";s:5:\"5.4.6\";s:7:\"updated\";s:19:\"2020-07-07 01:53:37\";s:12:\"english_name\";s:6:\"Sindhi\";s:11:\"native_name\";s:8:\"سنڌي\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.4.6/snd.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"sd\";i:2;s:3:\"snd\";i:3;s:3:\"snd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"اڳتي هلو\";}}s:5:\"si_LK\";a:8:{s:8:\"language\";s:5:\"si_LK\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 06:00:52\";s:12:\"english_name\";s:7:\"Sinhala\";s:11:\"native_name\";s:15:\"සිංහල\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/si_LK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"si\";i:2;s:3:\"sin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:44:\"දිගටම කරගෙන යන්න\";}}s:5:\"sk_SK\";a:8:{s:8:\"language\";s:5:\"sk_SK\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-07-11 15:17:09\";s:12:\"english_name\";s:6:\"Slovak\";s:11:\"native_name\";s:11:\"Slovenčina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/sk_SK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sk\";i:2;s:3:\"slk\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Pokračovať\";}}s:3:\"skr\";a:8:{s:8:\"language\";s:3:\"skr\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-04-23 11:54:14\";s:12:\"english_name\";s:7:\"Saraiki\";s:11:\"native_name\";s:14:\"سرائیکی\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.7.2/skr.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"skr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:17:\"جاری رکھو\";}}s:5:\"sl_SI\";a:8:{s:8:\"language\";s:5:\"sl_SI\";s:7:\"version\";s:6:\"5.1.10\";s:7:\"updated\";s:19:\"2019-04-30 13:03:56\";s:12:\"english_name\";s:9:\"Slovenian\";s:11:\"native_name\";s:13:\"Slovenščina\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/5.1.10/sl_SI.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sl\";i:2;s:3:\"slv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Nadaljujte\";}}s:2:\"sq\";a:8:{s:8:\"language\";s:2:\"sq\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-05-13 15:40:47\";s:12:\"english_name\";s:8:\"Albanian\";s:11:\"native_name\";s:5:\"Shqip\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.2/sq.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sq\";i:2;s:3:\"sqi\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Vazhdo\";}}s:5:\"sr_RS\";a:8:{s:8:\"language\";s:5:\"sr_RS\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-14 19:47:34\";s:12:\"english_name\";s:7:\"Serbian\";s:11:\"native_name\";s:23:\"Српски језик\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/sr_RS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sr\";i:2;s:3:\"srp\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:14:\"Настави\";}}s:5:\"sv_SE\";a:8:{s:8:\"language\";s:5:\"sv_SE\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-07-02 21:27:49\";s:12:\"english_name\";s:7:\"Swedish\";s:11:\"native_name\";s:7:\"Svenska\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/sv_SE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sv\";i:2;s:3:\"swe\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Fortsätt\";}}s:2:\"sw\";a:8:{s:8:\"language\";s:2:\"sw\";s:7:\"version\";s:5:\"5.3.8\";s:7:\"updated\";s:19:\"2019-10-13 15:35:35\";s:12:\"english_name\";s:7:\"Swahili\";s:11:\"native_name\";s:9:\"Kiswahili\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.3.8/sw.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sw\";i:2;s:3:\"swa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Endelea\";}}s:3:\"szl\";a:8:{s:8:\"language\";s:3:\"szl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-24 19:58:14\";s:12:\"english_name\";s:8:\"Silesian\";s:11:\"native_name\";s:17:\"Ślōnskŏ gŏdka\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/szl.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"szl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:13:\"Kōntynuować\";}}s:5:\"ta_IN\";a:8:{s:8:\"language\";s:5:\"ta_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:22:47\";s:12:\"english_name\";s:5:\"Tamil\";s:11:\"native_name\";s:15:\"தமிழ்\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ta_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ta\";i:2;s:3:\"tam\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"தொடரவும்\";}}s:5:\"ta_LK\";a:8:{s:8:\"language\";s:5:\"ta_LK\";s:7:\"version\";s:6:\"4.2.30\";s:7:\"updated\";s:19:\"2015-12-03 01:07:44\";s:12:\"english_name\";s:17:\"Tamil (Sri Lanka)\";s:11:\"native_name\";s:15:\"தமிழ்\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.2.30/ta_LK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ta\";i:2;s:3:\"tam\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"தொடர்க\";}}s:2:\"te\";a:8:{s:8:\"language\";s:2:\"te\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-26 15:47:39\";s:12:\"english_name\";s:6:\"Telugu\";s:11:\"native_name\";s:18:\"తెలుగు\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/te.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"te\";i:2;s:3:\"tel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"కొనసాగించు\";}}s:2:\"th\";a:8:{s:8:\"language\";s:2:\"th\";s:7:\"version\";s:5:\"5.5.5\";s:7:\"updated\";s:19:\"2021-04-22 18:43:36\";s:12:\"english_name\";s:4:\"Thai\";s:11:\"native_name\";s:9:\"ไทย\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.5.5/th.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"th\";i:2;s:3:\"tha\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"ต่อไป\";}}s:2:\"tl\";a:8:{s:8:\"language\";s:2:\"tl\";s:7:\"version\";s:6:\"4.8.17\";s:7:\"updated\";s:19:\"2017-09-30 09:04:29\";s:12:\"english_name\";s:7:\"Tagalog\";s:11:\"native_name\";s:7:\"Tagalog\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8.17/tl.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tl\";i:2;s:3:\"tgl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Magpatuloy\";}}s:5:\"tr_TR\";a:8:{s:8:\"language\";s:5:\"tr_TR\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-05-23 09:26:34\";s:12:\"english_name\";s:7:\"Turkish\";s:11:\"native_name\";s:8:\"Türkçe\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/tr_TR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tr\";i:2;s:3:\"tur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Devam\";}}s:5:\"tt_RU\";a:8:{s:8:\"language\";s:5:\"tt_RU\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-20 20:20:50\";s:12:\"english_name\";s:5:\"Tatar\";s:11:\"native_name\";s:19:\"Татар теле\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/tt_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tt\";i:2;s:3:\"tat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:17:\"дәвам итү\";}}s:3:\"tah\";a:8:{s:8:\"language\";s:3:\"tah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-06 18:39:39\";s:12:\"english_name\";s:8:\"Tahitian\";s:11:\"native_name\";s:10:\"Reo Tahiti\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/tah.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"ty\";i:2;s:3:\"tah\";i:3;s:3:\"tah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"ug_CN\";a:8:{s:8:\"language\";s:5:\"ug_CN\";s:7:\"version\";s:6:\"4.9.18\";s:7:\"updated\";s:19:\"2021-07-03 18:41:33\";s:12:\"english_name\";s:6:\"Uighur\";s:11:\"native_name\";s:16:\"ئۇيغۇرچە\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.18/ug_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ug\";i:2;s:3:\"uig\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:26:\"داۋاملاشتۇرۇش\";}}s:2:\"uk\";a:8:{s:8:\"language\";s:2:\"uk\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-17 19:11:00\";s:12:\"english_name\";s:9:\"Ukrainian\";s:11:\"native_name\";s:20:\"Українська\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.2/uk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uk\";i:2;s:3:\"ukr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продовжити\";}}s:2:\"ur\";a:8:{s:8:\"language\";s:2:\"ur\";s:7:\"version\";s:5:\"5.4.6\";s:7:\"updated\";s:19:\"2020-04-09 11:17:33\";s:12:\"english_name\";s:4:\"Urdu\";s:11:\"native_name\";s:8:\"اردو\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.4.6/ur.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ur\";i:2;s:3:\"urd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"جاری رکھیں\";}}s:5:\"uz_UZ\";a:8:{s:8:\"language\";s:5:\"uz_UZ\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-02-28 12:02:22\";s:12:\"english_name\";s:5:\"Uzbek\";s:11:\"native_name\";s:11:\"O‘zbekcha\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/uz_UZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uz\";i:2;s:3:\"uzb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продолжить\";}}s:2:\"vi\";a:8:{s:8:\"language\";s:2:\"vi\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-05-23 07:16:16\";s:12:\"english_name\";s:10:\"Vietnamese\";s:11:\"native_name\";s:14:\"Tiếng Việt\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/5.7.2/vi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"vi\";i:2;s:3:\"vie\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Tiếp tục\";}}s:5:\"zh_TW\";a:8:{s:8:\"language\";s:5:\"zh_TW\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-05-13 03:20:55\";s:12:\"english_name\";s:16:\"Chinese (Taiwan)\";s:11:\"native_name\";s:12:\"繁體中文\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/zh_TW.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_CN\";a:8:{s:8:\"language\";s:5:\"zh_CN\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-06-15 12:16:10\";s:12:\"english_name\";s:15:\"Chinese (China)\";s:11:\"native_name\";s:12:\"简体中文\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/zh_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"继续\";}}s:5:\"zh_HK\";a:8:{s:8:\"language\";s:5:\"zh_HK\";s:7:\"version\";s:5:\"5.7.2\";s:7:\"updated\";s:19:\"2021-07-03 09:42:47\";s:12:\"english_name\";s:19:\"Chinese (Hong Kong)\";s:11:\"native_name\";s:16:\"香港中文版	\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/5.7.2/zh_HK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}}', 'no');
INSERT INTO `fmn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1461, '_transient_timeout_wc_onboarding_product_data', '1626209488', 'no');
INSERT INTO `fmn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1462, '_transient_wc_onboarding_product_data', 'a:6:{s:7:\"headers\";O:42:\"Requests_Utility_CaseInsensitiveDictionary\":1:{s:7:\"\0*\0data\";a:18:{s:6:\"server\";s:5:\"nginx\";s:4:\"date\";s:29:\"Mon, 12 Jul 2021 20:51:28 GMT\";s:12:\"content-type\";s:31:\"application/json; charset=UTF-8\";s:14:\"content-length\";s:5:\"11651\";s:12:\"x-robots-tag\";s:7:\"noindex\";s:4:\"link\";s:60:\"<https://woocommerce.com/wp-json/>; rel=\"https://api.w.org/\"\";s:22:\"x-content-type-options\";s:7:\"nosniff\";s:29:\"access-control-expose-headers\";s:33:\"X-WP-Total, X-WP-TotalPages, Link\";s:28:\"access-control-allow-headers\";s:73:\"Authorization, X-WP-Nonce, Content-Disposition, Content-MD5, Content-Type\";s:13:\"x-wccom-cache\";s:3:\"HIT\";s:13:\"cache-control\";s:10:\"max-age=60\";s:5:\"allow\";s:3:\"GET\";s:16:\"content-encoding\";s:4:\"gzip\";s:4:\"x-rq\";s:13:\"vie2 0 4 9980\";s:3:\"age\";s:2:\"10\";s:7:\"x-cache\";s:3:\"hit\";s:4:\"vary\";s:23:\"Accept-Encoding, Origin\";s:13:\"accept-ranges\";s:5:\"bytes\";}}s:4:\"body\";s:48301:\"{\"products\":[{\"title\":\"WooCommerce Google Analytics\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/GA-Dark.png\",\"excerpt\":\"Understand your customers and increase revenue with world\\u2019s leading analytics platform - integrated with WooCommerce for free.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-google-analytics\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"2d21f7de14dfb8e9885a4622be701ddf\",\"slug\":\"woocommerce-google-analytics-integration\",\"id\":1442927},{\"title\":\"WooCommerce Tax\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Tax-Dark.png\",\"excerpt\":\"Automatically calculate how much sales tax should be collected for WooCommerce orders - by city, country, or state - at checkout.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/tax\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"f31b3b9273cce188cc2b27f7849d02dd\",\"slug\":\"woocommerce-services\",\"id\":3220291},{\"title\":\"Stripe\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Stripe-Dark-1.png\",\"excerpt\":\"Accept all major debit and credit cards as well as local payment methods with Stripe.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/stripe\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"50bb7a985c691bb943a9da4d2c8b5efd\",\"slug\":\"woocommerce-gateway-stripe\",\"id\":18627},{\"title\":\"Jetpack\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Jetpack-Dark.png\",\"excerpt\":\"Power up and protect your store with Jetpack\\r\\n\\r\\nFor free security, insights and monitoring, connect to Jetpack. It\'s everything you need for a strong, secure start.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/jetpack\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"d5bfef9700b62b2b132c74c74c3193eb\",\"slug\":\"jetpack\",\"id\":2725249},{\"title\":\"Facebook for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Facebook-Dark.png\",\"excerpt\":\"Get the Official Facebook for WooCommerce plugin for three powerful ways to help grow your business.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/facebook\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"0ea4fe4c2d7ca6338f8a322fb3e4e187\",\"slug\":\"facebook-for-woocommerce\",\"id\":2127297},{\"title\":\"Amazon Pay\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Amazon-Pay-Dark.png\",\"excerpt\":\"Amazon Pay is embedded in your WooCommerce store. Transactions take place via\\u00a0Amazon widgets, so the buyer never leaves your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/pay-with-amazon\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"9865e043bbbe4f8c9735af31cb509b53\",\"slug\":\"woocommerce-gateway-amazon-payments-advanced\",\"id\":238816},{\"title\":\"Square for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Square-Dark.png\",\"excerpt\":\"Accepting payments is easy with Square. Clear rates, fast deposits (1-2 business days). Sell online and in person, and sync all payments, items and inventory.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/square\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e907be8b86d7df0c8f8e0d0020b52638\",\"slug\":\"woocommerce-square\",\"id\":1770503},{\"title\":\"WooCommerce Shipping\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Ship-Dark-1.png\",\"excerpt\":\"Print USPS and DHL labels right from your WooCommerce dashboard and instantly save up to 90%. WooCommerce Shipping is free to use and saves you time and money.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipping\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"f31b3b9273cce188cc2b27f7849d02dd\",\"slug\":\"woocommerce-services\",\"id\":2165910},{\"title\":\"WooCommerce Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Pay-Dark.png\",\"excerpt\":\"Securely accept payments, track cash flow, and manage recurring revenue from your dashboard \\u2014 all without setup costs or monthly fees.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-payments\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"8c6319ca-8f41-4e69-be63-6b15ee37773b\",\"slug\":\"woocommerce-payments\",\"id\":5278104},{\"title\":\"Mailchimp for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/09\\/logo-mailchimp-dark-v2.png\",\"excerpt\":\"Increase traffic, drive repeat purchases, and personalize your marketing when you connect to Mailchimp.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/mailchimp-for-woocommerce\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"b4481616ebece8b1ff68fc59b90c1a91\",\"slug\":\"mailchimp-for-woocommerce\",\"id\":2545166},{\"title\":\"WooCommerce Subscriptions\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Subscriptions-Dark.png\",\"excerpt\":\"Let customers subscribe to your products or services and pay on a weekly, monthly or annual basis.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-subscriptions\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;199.00\",\"hash\":\"6115e6d7e297b623a169fdcf5728b224\",\"slug\":\"woocommerce-subscriptions\",\"id\":27147},{\"title\":\"ShipStation Integration\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Shipstation-Dark.png\",\"excerpt\":\"Fulfill all your Woo orders (and wherever else you sell) quickly and easily using ShipStation. Try it free for 30 days today!\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipstation-integration\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"9de8640767ba64237808ed7f245a49bb\",\"slug\":\"woocommerce-shipstation-integration\",\"id\":18734},{\"title\":\"Product Add-Ons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Product-Add-Ons-Dark.png\",\"excerpt\":\"Offer add-ons like gift wrapping, special messages or other special options for your products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"147d0077e591e16db9d0d67daeb8c484\",\"slug\":\"woocommerce-product-addons\",\"id\":18618},{\"title\":\"PayFast Payment Gateway\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Payfast-Dark-1.png\",\"excerpt\":\"Take payments on your WooCommerce store via PayFast (redirect method).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/payfast-payment-gateway\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"557bf07293ad916f20c207c6c9cd15ff\",\"slug\":\"woocommerce-payfast-gateway\",\"id\":18596},{\"title\":\"USPS Shipping Method\",\"image\":\"\",\"excerpt\":\"Get shipping rates from the USPS API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/usps-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"83d1524e8f5f1913e58889f83d442c32\",\"slug\":\"woocommerce-shipping-usps\",\"id\":18657},{\"title\":\"Google Ads &amp; Marketing by Kliken\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2019\\/02\\/GA-for-Woo-Logo-374x192px-qu3duk.png\",\"excerpt\":\"Get in front of shoppers and drive traffic to your store so you can grow your business with Smart Shopping Campaigns and free listings.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-ads-and-marketing\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"bf66e173-a220-4da7-9512-b5728c20fc16\",\"slug\":\"kliken-marketing-for-google\",\"id\":3866145},{\"title\":\"UPS Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/UPS-Shipping-Method-Dark.png\",\"excerpt\":\"Get shipping rates from the UPS API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/ups-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8dae58502913bac0fbcdcaba515ea998\",\"slug\":\"woocommerce-shipping-ups\",\"id\":18665},{\"title\":\"Shipment Tracking\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Ship-Tracking-Dark-1.png\",\"excerpt\":\"Add shipment tracking information to your orders.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipment-tracking\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"1968e199038a8a001c9f9966fd06bf88\",\"slug\":\"woocommerce-shipment-tracking\",\"id\":18693},{\"title\":\"Table Rate Shipping\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Product-Table-Rate-Shipping-Dark.png\",\"excerpt\":\"Advanced, flexible shipping. Define multiple shipping rates based on location, price, weight, shipping class or item count.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/table-rate-shipping\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"3034ed8aff427b0f635fe4c86bbf008a\",\"slug\":\"woocommerce-table-rate-shipping\",\"id\":18718},{\"title\":\"Checkout Field Editor\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Checkout-Field-Editor-Dark.png\",\"excerpt\":\"Optimize your checkout process by adding, removing or editing fields to suit your needs.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-checkout-field-editor\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"2b8029f0d7cdd1118f4d843eb3ab43ff\",\"slug\":\"woocommerce-checkout-field-editor\",\"id\":184594},{\"title\":\"Braintree for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/02\\/braintree-black-copy.png\",\"excerpt\":\"Accept PayPal, credit cards and debit cards with a single payment gateway solution \\u2014 PayPal Powered by Braintree.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gateway-paypal-powered-by-braintree\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"27f010c8e34ca65b205ddec88ad14536\",\"slug\":\"woocommerce-gateway-paypal-powered-by-braintree\",\"id\":1489837},{\"title\":\"WooCommerce Bookings\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Bookings-Dark.png\",\"excerpt\":\"Allow customers to book appointments, make reservations or rent equipment without leaving your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-bookings\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/themes.woocommerce.com\\/hotel\\/\",\"price\":\"&#36;249.00\",\"hash\":\"911c438934af094c2b38d5560b9f50f3\",\"slug\":\"WooCommerce Bookings\",\"id\":390890},{\"title\":\"WooCommerce Memberships\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2015\\/06\\/Thumbnail-Memberships-updated.png\",\"excerpt\":\"Give members access to restricted content or products, for a fee or for free.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-memberships\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;199.00\",\"hash\":\"9288e7609ad0b487b81ef6232efa5cfc\",\"slug\":\"woocommerce-memberships\",\"id\":958589},{\"title\":\"Product Bundles\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-PB.png?v=1\",\"excerpt\":\"Offer personalized product bundles, bulk discount packages, and assembled\\u00a0products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-bundles\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"aa2518b5-ab19-4b75-bde9-60ca51e20f28\",\"slug\":\"woocommerce-product-bundles\",\"id\":18716},{\"title\":\"Multichannel for WooCommerce: Google, Amazon, eBay &amp; Walmart Integration\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2018\\/10\\/Woo-Extension-Store-Logo-v2.png\",\"excerpt\":\"Get the official Google, Amazon, eBay and Walmart extension and create, sync and manage multichannel listings directly from WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/amazon-ebay-integration\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e4000666-9275-4c71-8619-be61fb41c9f9\",\"slug\":\"woocommerce-amazon-ebay-integration\",\"id\":3545890},{\"title\":\"Min\\/Max Quantities\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Min-Max-Qua-Dark.png\",\"excerpt\":\"Specify minimum and maximum allowed product quantities for orders to be completed.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/minmax-quantities\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"2b5188d90baecfb781a5aa2d6abb900a\",\"slug\":\"woocommerce-min-max-quantities\",\"id\":18616},{\"title\":\"LiveChat for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2015\\/11\\/LC_woo_regular-zmiaym.png\",\"excerpt\":\"Live Chat and messaging platform for sales and support -- increase average order value and overall sales through live conversations.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/livechat\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.livechat.com\\/livechat-for-ecommerce\\/?a=woocommerce&amp;utm_source=woocommerce.com&amp;utm_medium=integration&amp;utm_campaign=woocommerce.com\",\"price\":\"&#36;0.00\",\"hash\":\"5344cc1f-ed4a-4d00-beff-9d67f6d372f3\",\"slug\":\"livechat-woocommerce\",\"id\":1348888},{\"title\":\"FedEx Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/01\\/FedEx_Logo_Wallpaper.jpeg\",\"excerpt\":\"Get shipping rates from the FedEx API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/fedex-shipping-module\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1a48b598b47a81559baadef15e320f64\",\"slug\":\"woocommerce-shipping-fedex\",\"id\":18620},{\"title\":\"Product CSV Import Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Product-CSV-Import-Dark.png\",\"excerpt\":\"Import, merge, and export products and variations to and from WooCommerce using a CSV file.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-csv-import-suite\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"7ac9b00a1fe980fb61d28ab54d167d0d\",\"slug\":\"woocommerce-product-csv-import-suite\",\"id\":18680},{\"title\":\"Follow-Ups\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Follow-Ups-Dark.png\",\"excerpt\":\"Automatically contact customers after purchase - be it everyone, your most loyal or your biggest spenders - and keep your store top-of-mind.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/follow-up-emails\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"05ece68fe94558e65278fe54d9ec84d2\",\"slug\":\"woocommerce-follow-up-emails\",\"id\":18686},{\"title\":\"Authorize.Net\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2013\\/04\\/Thumbnail-Authorize.net-updated.png\",\"excerpt\":\"Authorize.Net gateway with support for pre-orders and subscriptions.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/authorize-net\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8b61524fe53add7fdd1a8d1b00b9327d\",\"slug\":\"woocommerce-gateway-authorize-net-cim\",\"id\":178481},{\"title\":\"Australia Post Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/australia-post.gif\",\"excerpt\":\"Get shipping rates for your WooCommerce store from the Australia Post API, which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/australia-post-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1dbd4dc6bd91a9cda1bd6b9e7a5e4f43\",\"slug\":\"woocommerce-shipping-australia-post\",\"id\":18622},{\"title\":\"Canada Post Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/canada-post.png\",\"excerpt\":\"Get shipping rates from the Canada Post Ratings API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/canada-post-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"ac029cdf3daba20b20c7b9be7dc00e0e\",\"slug\":\"woocommerce-shipping-canada-post\",\"id\":18623},{\"title\":\"Product Vendors\",\"image\":\"\",\"excerpt\":\"Turn your store into a multi-vendor marketplace\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-vendors\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"a97d99fccd651bbdd728f4d67d492c31\",\"slug\":\"woocommerce-product-vendors\",\"id\":219982},{\"title\":\"WooCommerce Customer \\/ Order \\/ Coupon Export\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/02\\/Thumbnail-Customer-Order-Coupon-Export-updated.png\",\"excerpt\":\"Export customers, orders, and coupons from WooCommerce manually or on an automated schedule.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/ordercustomer-csv-export\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"914de15813a903c767b55445608bf290\",\"slug\":\"woocommerce-customer-order-csv-export\",\"id\":18652},{\"title\":\"WooCommerce Accommodation Bookings\",\"image\":\"\",\"excerpt\":\"Book accommodation using WooCommerce and the WooCommerce Bookings extension.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-accommodation-bookings\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"99b2a7a4af90b6cefd2a733b3b1f78e7\",\"slug\":\"woocommerce-accommodation-bookings\",\"id\":1412069},{\"title\":\"Royal Mail\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/04\\/royalmail.png\",\"excerpt\":\"Offer Royal Mail shipping rates to your customers\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/royal-mail\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"03839cca1a16c4488fcb669aeb91a056\",\"slug\":\"woocommerce-shipping-royalmail\",\"id\":182719},{\"title\":\"WooCommerce Brands\",\"image\":\"\",\"excerpt\":\"Create, assign and list brands for products, and allow customers to view by brand.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/brands\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"8a88c7cbd2f1e73636c331c7a86f818c\",\"slug\":\"woocommerce-brands\",\"id\":18737},{\"title\":\"Xero\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2012\\/08\\/xero2.png\",\"excerpt\":\"Save time with automated sync between WooCommerce and your Xero account.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/xero\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"f0dd29d338d3c67cf6cee88eddf6869b\",\"slug\":\"woocommerce-xero\",\"id\":18733},{\"title\":\"Smart Coupons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/10\\/wc-product-smart-coupons.png\",\"excerpt\":\"Everything you need for discounts, coupons, credits, gift cards, product giveaways, offers, and promotions. Most popular and complete coupons plugin for WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/smart-coupons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/demo.storeapps.org\\/?demo=sc\",\"price\":\"&#36;99.00\",\"hash\":\"05c45f2aa466106a466de4402fff9dde\",\"slug\":\"woocommerce-smart-coupons\",\"id\":18729},{\"title\":\"AutomateWoo\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-AutomateWoo-Dark-1.png\",\"excerpt\":\"Powerful marketing automation for WooCommerce. AutomateWoo has the tools you need to grow your store and make more money.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/automatewoo\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"ba9299b8-1dba-4aa0-a313-28bc1755cb88\",\"slug\":\"automatewoo\",\"id\":4652610},{\"title\":\"Advanced Notifications\",\"image\":\"\",\"excerpt\":\"Easily setup \\\"new order\\\" and stock email notifications for multiple recipients of your choosing.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/advanced-notifications\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"112372c44b002fea2640bd6bfafbca27\",\"slug\":\"woocommerce-advanced-notifications\",\"id\":18740},{\"title\":\"WooCommerce Points and Rewards\",\"image\":\"\",\"excerpt\":\"Reward your customers for purchases and other actions with points which can be redeemed for discounts.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-points-and-rewards\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"1649b6cca5da8b923b01ca56b5cdd246\",\"slug\":\"woocommerce-points-and-rewards\",\"id\":210259},{\"title\":\"WooCommerce Zapier\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/woocommerce-zapier-logo.png\",\"excerpt\":\"Integrate with 3000+ cloud apps and services today.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-zapier\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;59.00\",\"hash\":\"0782bdbe932c00f4978850268c6cfe40\",\"slug\":\"woocommerce-zapier\",\"id\":243589},{\"title\":\"WooCommerce Pre-Orders\",\"image\":\"\",\"excerpt\":\"Allow customers to order products before they are available.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-pre-orders\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"b2dc75e7d55e6f5bbfaccb59830f66b7\",\"slug\":\"woocommerce-pre-orders\",\"id\":178477},{\"title\":\"WooCommerce Subscription Downloads\",\"image\":\"\",\"excerpt\":\"Offer additional downloads to your subscribers, via downloadable products listed in your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-subscription-downloads\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"5be9e21c13953253e4406d2a700382ec\",\"slug\":\"woocommerce-subscription-downloads\",\"id\":420458},{\"title\":\"Dynamic Pricing\",\"image\":\"\",\"excerpt\":\"Bulk discounts, role-based pricing and much more\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/dynamic-pricing\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"9a41775bb33843f52c93c922b0053986\",\"slug\":\"woocommerce-dynamic-pricing\",\"id\":18643},{\"title\":\"WooCommerce Additional Variation Images\",\"image\":\"\",\"excerpt\":\"Add gallery images per variation on variable products within WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-additional-variation-images\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/themes.woocommerce.com\\/storefront\\/product\\/woo-single-1\\/\",\"price\":\"&#36;49.00\",\"hash\":\"c61dd6de57dcecb32bd7358866de4539\",\"slug\":\"woocommerce-additional-variation-images\",\"id\":477384},{\"title\":\"Name Your Price\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2012\\/09\\/nyp-icon-dark-v83owf.png\",\"excerpt\":\"Allow customers to define the product price. Also useful for accepting user-set donations.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/name-your-price\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"31b4e11696cd99a3c0572975a84f1c08\",\"slug\":\"woocommerce-name-your-price\",\"id\":18738},{\"title\":\"WooCommerce Deposits\",\"image\":\"\",\"excerpt\":\"Enable customers to pay for products using a deposit or a payment plan.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-deposits\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;179.00\",\"hash\":\"de192a6cf12c4fd803248da5db700762\",\"slug\":\"woocommerce-deposits\",\"id\":977087},{\"title\":\"WooCommerce Print Invoices &amp; Packing lists\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/03\\/Thumbnail-Print-Invoices-Packing-lists-updated.png\",\"excerpt\":\"Generate invoices, packing slips, and pick lists for your WooCommerce orders.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/print-invoices-packing-lists\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"465de1126817cdfb42d97ebca7eea717\",\"slug\":\"woocommerce-pip\",\"id\":18666},{\"title\":\"Amazon S3 Storage\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/amazon.png\",\"excerpt\":\"Serve digital products via Amazon S3\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/amazon-s3-storage\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"473bf6f221b865eff165c97881b473bb\",\"slug\":\"woocommerce-amazon-s3-storage\",\"id\":18663},{\"title\":\"Cart Add-ons\",\"image\":\"\",\"excerpt\":\"A powerful tool for driving incremental and impulse purchases by customers once they are in the shopping cart\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/cart-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"3a8ef25334396206f5da4cf208adeda3\",\"slug\":\"woocommerce-cart-add-ons\",\"id\":18717},{\"title\":\"Shipping Multiple Addresses\",\"image\":\"\",\"excerpt\":\"Allow your customers to ship individual items in a single order to multiple addresses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipping-multiple-addresses\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"aa0eb6f777846d329952d5b891d6f8cc\",\"slug\":\"woocommerce-shipping-multiple-addresses\",\"id\":18741},{\"title\":\"WooCommerce AvaTax\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/01\\/Thumbnail-Avalara-updated.png\",\"excerpt\":\"Get 100% accurate sales tax calculations and on time tax return filing. No more tracking sales tax rates, rules, or jurisdictional boundaries.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-avatax\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"57077a4b28ba71cacf692bcf4a1a7f60\",\"slug\":\"woocommerce-avatax\",\"id\":1389326},{\"title\":\"Google Product Feed\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2011\\/11\\/logo-regular-lscryp.png\",\"excerpt\":\"Feed product data to Google Merchant Center for setting up Google product listings &amp; product ads.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-product-feed\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"d55b4f852872025741312839f142447e\",\"slug\":\"woocommerce-product-feeds\",\"id\":18619},{\"title\":\"TaxJar\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/10\\/taxjar-logotype.png\",\"excerpt\":\"Save hours every month by putting your sales tax on autopilot. Automated, multi-state sales tax calculation, reporting, and filing for your WooCommerce store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/taxjar\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"12072d8e-e933-4561-97b1-9db3c7eeed91\",\"slug\":\"taxjar-simplified-taxes-for-woocommerce\",\"id\":514914},{\"title\":\"Klarna Checkout\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2018\\/01\\/Partner_marketing_Klarna_Checkout_Black-1.png\",\"excerpt\":\"Klarna Checkout is a full checkout experience embedded on your site that includes all popular payment methods (Pay Now, Pay Later, Financing, Installments).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/klarna-checkout\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/demo.krokedil.se\\/klarnacheckout\\/\",\"price\":\"&#36;0.00\",\"hash\":\"90f8ce584e785fcd8c2d739fd4f40d78\",\"slug\":\"klarna-checkout-for-woocommerce\",\"id\":2754152},{\"title\":\"Bulk Stock Management\",\"image\":\"\",\"excerpt\":\"Edit product and variation stock levels in bulk via this handy interface\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/bulk-stock-management\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"02f4328d52f324ebe06a78eaaae7934f\",\"slug\":\"woocommerce-bulk-stock-management\",\"id\":18670},{\"title\":\"WooCommerce Email Customizer\",\"image\":\"\",\"excerpt\":\"Connect with your customers with each email you send by visually modifying your email templates via the WordPress Customizer.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-email-customizer\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"bd909fa97874d431f203b5336c7e8873\",\"slug\":\"woocommerce-email-customizer\",\"id\":853277},{\"title\":\"Force Sells\",\"image\":\"\",\"excerpt\":\"Force products to be added to the cart\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/force-sells\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"3ebddfc491ca168a4ea4800b893302b0\",\"slug\":\"woocommerce-force-sells\",\"id\":18678},{\"title\":\"WooCommerce Quick View\",\"image\":\"\",\"excerpt\":\"Show a quick-view button to view product details and add to cart via lightbox popup\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-quick-view\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"619c6e57ce72c49c4b57e15b06eddb65\",\"slug\":\"woocommerce-quick-view\",\"id\":187509},{\"title\":\"WooCommerce Purchase Order Gateway\",\"image\":\"\",\"excerpt\":\"Receive purchase orders via your WooCommerce-powered online store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gateway-purchase-order\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"573a92318244ece5facb449d63e74874\",\"slug\":\"woocommerce-gateway-purchase-order\",\"id\":478542},{\"title\":\"Returns and Warranty Requests\",\"image\":\"\",\"excerpt\":\"Manage the RMA process, add warranties to products &amp; let customers request &amp; manage returns \\/ exchanges from their account.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/warranty-requests\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"9b4c41102e6b61ea5f558e16f9b63e25\",\"slug\":\"woocommerce-warranty\",\"id\":228315},{\"title\":\"Product Enquiry Form\",\"image\":\"\",\"excerpt\":\"Allow visitors to contact you directly from the product details page via a reCAPTCHA protected form to enquire about a product.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-enquiry-form\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"5a0f5d72519a8ffcc86669f042296937\",\"slug\":\"woocommerce-product-enquiry-form\",\"id\":18601},{\"title\":\"WooCommerce Box Office\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-BO-Dark.png\",\"excerpt\":\"Sell tickets for your next event, concert, function, fundraiser or conference directly on your own site\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-box-office\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"e704c9160de318216a8fa657404b9131\",\"slug\":\"woocommerce-box-office\",\"id\":1628717},{\"title\":\"Gravity Forms Product Add-ons\",\"image\":\"\",\"excerpt\":\"Powerful product add-ons, Gravity style\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/gravity-forms-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.elementstark.com\\/woocommerce-extension-demos\\/product-category\\/gravity-forms\\/\",\"price\":\"&#36;99.00\",\"hash\":\"a6ac0ab1a1536e3a357ccf24c0650ed0\",\"slug\":\"woocommerce-gravityforms-product-addons\",\"id\":18633},{\"title\":\"WooCommerce Order Barcodes\",\"image\":\"\",\"excerpt\":\"Generates a unique barcode for each order on your site - perfect for e-tickets, packing slips, reservations and a variety of other uses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-barcodes\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"889835bb29ee3400923653e1e44a3779\",\"slug\":\"woocommerce-order-barcodes\",\"id\":391708},{\"title\":\"Composite Products\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-CP.png?v=1\",\"excerpt\":\"Create product kit builders and custom product configurators using existing products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/composite-products\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"0343e0115bbcb97ccd98442b8326a0af\",\"slug\":\"woocommerce-composite-products\",\"id\":216836},{\"title\":\"WooCommerce 360\\u00ba Image\",\"image\":\"\",\"excerpt\":\"An easy way to add a dynamic, controllable 360\\u00ba image rotation to your WooCommerce site, by adding a group of images to a product\\u2019s gallery.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-360-image\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"24eb2cfa3738a66bf3b2587876668cd2\",\"slug\":\"woocommerce-360-image\",\"id\":512186},{\"title\":\"WooCommerce Photography\",\"image\":\"\",\"excerpt\":\"Sell photos in the blink of an eye using this simple as dragging &amp; dropping interface.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-photography\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"ee76e8b9daf1d97ca4d3874cc9e35687\",\"slug\":\"woocommerce-photography\",\"id\":583602},{\"title\":\"WooCommerce Bookings Availability\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Bookings-Aval-Dark.png\",\"excerpt\":\"Sell more bookings by presenting a calendar or schedule of available slots in a page or post.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/bookings-availability\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"30770d2a-e392-4e82-baaa-76cfc7d02ae3\",\"slug\":\"woocommerce-bookings-availability\",\"id\":4228225},{\"title\":\"Software Add-on\",\"image\":\"\",\"excerpt\":\"Sell License Keys for Software\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/software-add-on\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"79f6dbfe1f1d3a56a86f0509b6d6b04b\",\"slug\":\"woocommerce-software-add-on\",\"id\":18683},{\"title\":\"WooCommerce Products Compare\",\"image\":\"\",\"excerpt\":\"WooCommerce Products Compare will allow your potential customers to easily compare products within your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-products-compare\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"c3ba0a4a3199a0cc7a6112eb24414548\",\"slug\":\"woocommerce-products-compare\",\"id\":853117},{\"title\":\"WooCommerce Store Catalog PDF Download\",\"image\":\"\",\"excerpt\":\"Offer your customers a PDF download of your product catalog, generated by WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-store-catalog-pdf-download\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"79ca7aadafe706364e2d738b7c1090c4\",\"slug\":\"woocommerce-store-catalog-pdf-download\",\"id\":675790},{\"title\":\"eWAY\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2011\\/10\\/eway-logo-3000-2000.jpg\",\"excerpt\":\"Take credit card payments securely via eWay (SG, MY, HK, AU, and NZ) keeping customers on your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/eway\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"2c497769d98d025e0d340cd0b5ea5da1\",\"slug\":\"woocommerce-gateway-eway\",\"id\":18604},{\"title\":\"WooCommerce Paid Courses\",\"image\":\"\",\"excerpt\":\"Sell your online courses using the most popular eCommerce platform on the web \\u2013 WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-paid-courses\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"bad2a02a063555b7e2bee59924690763\",\"slug\":\"woothemes-sensei\",\"id\":152116},{\"title\":\"PayPal\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2020\\/10\\/PPCP-Tile-PayPal-Logo-and-Cart-Art-2x-2-uozwz8.jpg\",\"excerpt\":\"PayPal\\u2019s latest, most complete payment processing solution. Accept PayPal exclusives, credit\\/debit cards and local payment methods. Turn on only PayPal options or process a full suite of payment methods. Enable global transactions with extensive currency and country coverage. Built and supported by WooCommerce and PayPal.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-paypal-payments\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"934115ab-e3f3-4435-9580-345b1ce21899\",\"slug\":\"woocommerce-paypal-payments\",\"id\":6410731},{\"title\":\"Catalog Visibility Options\",\"image\":\"\",\"excerpt\":\"Transform WooCommerce into an online catalog by removing eCommerce functionality\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/catalog-visibility-options\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"12e791110365fdbb5865c8658907967e\",\"slug\":\"woocommerce-catalog-visibility-options\",\"id\":18648},{\"title\":\"QuickBooks Sync for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2019\\/04\\/woocommerce-com-logo-1-hyhzbh.png\",\"excerpt\":\"Automatic two-way sync for orders, customers, products, inventory and more between WooCommerce and QuickBooks (Online, Desktop, or POS).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/quickbooks-sync-for-woocommerce\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"c5e32e20-7c1f-4585-8b15-d930c2d842ac\",\"slug\":\"myworks-woo-sync-for-quickbooks-online\",\"id\":4065824},{\"title\":\"WooCommerce Blocks\",\"image\":\"\",\"excerpt\":\"WooCommerce Blocks offers a range of Gutenberg blocks you can use to build and customise your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gutenberg-products-block\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"c2e9f13a-f90c-4ffe-a8a5-b432399ec263\",\"slug\":\"woo-gutenberg-products-block\",\"id\":3076677},{\"title\":\"Conditional Shipping and Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-CSP.png?v=1\",\"excerpt\":\"Use conditional logic to restrict the shipping and payment options available on your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/conditional-shipping-and-payments\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1f56ff002fa830b77017b0107505211a\",\"slug\":\"woocommerce-conditional-shipping-and-payments\",\"id\":680253},{\"title\":\"Sequential Order Numbers Pro\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/05\\/Thumbnail-Sequential-Order-Numbers-Pro-updated.png\",\"excerpt\":\"Tame your order numbers! Advanced &amp; sequential order numbers with optional prefixes \\/ suffixes\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/sequential-order-numbers-pro\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"0b18a2816e016ba9988b93b1cd8fe766\",\"slug\":\"woocommerce-sequential-order-numbers-pro\",\"id\":18688},{\"title\":\"WooCommerce Order Status Manager\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2015\\/02\\/Thumbnail-Order-Status-Manager-updated.png\",\"excerpt\":\"Create, edit, and delete completely custom order statuses and integrate them seamlessly into your order management flow.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-status-manager\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"51fd9ab45394b4cad5a0ebf58d012342\",\"slug\":\"woocommerce-order-status-manager\",\"id\":588398},{\"title\":\"WooCommerce Checkout Add-Ons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/07\\/Thumbnail-Checkout-Add-Ons-updated.png\",\"excerpt\":\"Highlight relevant products, offers like free shipping and other up-sells during checkout.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-checkout-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8fdca00b4000b7a8cc26371d0e470a8f\",\"slug\":\"woocommerce-checkout-add-ons\",\"id\":466854},{\"title\":\"WooCommerce Google Analytics Pro\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/01\\/Thumbnail-GAPro-updated.png\",\"excerpt\":\"Add advanced event tracking and enhanced eCommerce tracking to your WooCommerce site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-google-analytics-pro\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"d8aed8b7306b509eec1589e59abe319f\",\"slug\":\"woocommerce-google-analytics-pro\",\"id\":1312497},{\"title\":\"Coupon Shortcodes\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/09\\/woocommerce-coupon-shortcodes-product-image-1870x960-1-vc5gux.png\",\"excerpt\":\"Show coupon discount info using shortcodes. Allows to render coupon information and content conditionally, based on the validity of coupons.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/coupon-shortcodes\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"ac5d9d51-70b2-4d8f-8b89-24200eea1394\",\"slug\":\"woocommerce-coupon-shortcodes\",\"id\":244762},{\"title\":\"WooCommerce One Page Checkout\",\"image\":\"\",\"excerpt\":\"Create special pages where customers can choose products, checkout &amp; pay all on the one page.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-one-page-checkout\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"c9ba8f8352cd71b5508af5161268619a\",\"slug\":\"woocommerce-one-page-checkout\",\"id\":527886},{\"title\":\"Jilt\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2017\\/12\\/Thumbnail-Jilt-updated.png\",\"excerpt\":\"All-in-one email marketing platform built for WooCommerce stores. Send newsletters, abandoned cart reminders, win-backs, welcome automations, and more.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/jilt\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"b53aafb64dca33835e41ee06de7e9816\",\"slug\":\"jilt-for-woocommerce\",\"id\":2754876},{\"title\":\"WooCommerce Product Search\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2014\\/10\\/woocommerce-product-search-product-image-1870x960-1-jvsljj.png\",\"excerpt\":\"The perfect search engine helps customers to find and buy products quickly \\u2013 essential for every WooCommerce store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-product-search\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/demo.itthinx.com\\/wps\\/\",\"price\":\"&#36;49.00\",\"hash\":\"c84cc8ca16ddac3408e6b6c5871133a8\",\"slug\":\"woocommerce-product-search\",\"id\":512174},{\"title\":\"First Data\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/02\\/Thumbnail-FirstData-updated.png\",\"excerpt\":\"FirstData gateway for WooCommerce\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/firstdata\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"eb3e32663ec0810592eaf0d097796230\",\"slug\":\"woocommerce-gateway-firstdata\",\"id\":18645},{\"title\":\"WooSlider\",\"image\":\"\",\"excerpt\":\"WooSlider is the ultimate responsive slideshow WordPress slider plugin\\r\\n\\r\\n\\u00a0\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/wooslider\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/www.wooslider.com\\/\",\"price\":\"&#36;49.00\",\"hash\":\"209d98f3ccde6cc3de7e8732a2b20b6a\",\"slug\":\"wooslider\",\"id\":46506},{\"title\":\"WooCommerce Social Login\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/08\\/Thumbnail-Social-Login-updated.png\",\"excerpt\":\"Enable Social Login for seamless checkout and account creation.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-social-login\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/demos.skyverge.com\\/woocommerce-social-login\\/\",\"price\":\"&#36;79.00\",\"hash\":\"b231cd6367a79cc8a53b7d992d77525d\",\"slug\":\"woocommerce-social-login\",\"id\":473617},{\"title\":\"WooCommerce Order Status Control\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/06\\/Thumbnail-Order-Status-Control-updated.png\",\"excerpt\":\"Use this extension to automatically change the order status to \\\"completed\\\" after successful payment.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-status-control\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"32400e509c7c36dcc1cd368e8267d981\",\"slug\":\"woocommerce-order-status-control\",\"id\":439037},{\"title\":\"Variation Swatches and Photos\",\"image\":\"\",\"excerpt\":\"Show color and image swatches instead of dropdowns for variable products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/variation-swatches-and-photos\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.elementstark.com\\/woocommerce-extension-demos\\/product-category\\/swatches-and-photos\\/\",\"price\":\"&#36;99.00\",\"hash\":\"37bea8d549df279c8278878d081b062f\",\"slug\":\"woocommerce-variation-swatches-and-photos\",\"id\":18697},{\"title\":\"Opayo Payment Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2011\\/10\\/Opayo_logo_RGB.png\",\"excerpt\":\"Take payments on your WooCommerce store via Opayo (formally SagePay).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/sage-pay-form\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"6bc0cca47d0274d8ef9b164f6fbec1cc\",\"slug\":\"woocommerce-gateway-sagepay-form\",\"id\":18599},{\"title\":\"EU VAT Number\",\"image\":\"\",\"excerpt\":\"Collect VAT numbers at checkout and remove the VAT charge for eligible EU businesses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/eu-vat-number\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"d2720c4b4bb8d6908e530355b7a2d734\",\"slug\":\"woocommerce-eu-vat-number\",\"id\":18592},{\"title\":\"HubSpot for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2020\\/04\\/hubspotlogo-web-color-pxebeq.png\",\"excerpt\":\"Integrate HubSpot with WooCommerce to connect your store with HubSpot\'s CRM, abandoned cart tracking, marketing automation &amp; more.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/hubspot-for-woocommerce\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e50acec8-3a6c-454c-8562-2da4898fa6c1\",\"slug\":\"hubspot-for-woocommerce\",\"id\":5785079},{\"title\":\"Google Listings &amp; Ads\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2021\\/06\\/marketplace-card.png\",\"excerpt\":\"Reach millions of engaged shoppers across Google with free product listings and ads. Built in partnership with Google.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-listings-and-ads\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"118f4d86-f126-4c3a-8525-644e3170d161\",\"slug\":\"google-listings-and-ads\",\"id\":7623964},{\"title\":\"Customer\\/Order\\/Coupon CSV Import Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/07\\/Thumbnail-Customer-Order-Coupon-CSV-Import-Suite-updated.png\",\"excerpt\":\"Import both customers and orders into WooCommerce from a CSV file.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/customerorder-csv-import-suite\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"eb00ca8317a0f64dbe185c995e5ea3df\",\"slug\":\"woocommerce-customer-order-csv-import\",\"id\":18709}]}\";s:8:\"response\";a:2:{s:4:\"code\";i:200;s:7:\"message\";s:2:\"OK\";}s:7:\"cookies\";a:0:{}s:8:\"filename\";N;s:13:\"http_response\";O:25:\"WP_HTTP_Requests_Response\":5:{s:11:\"\0*\0response\";O:17:\"Requests_Response\":10:{s:4:\"body\";s:48301:\"{\"products\":[{\"title\":\"WooCommerce Google Analytics\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/GA-Dark.png\",\"excerpt\":\"Understand your customers and increase revenue with world\\u2019s leading analytics platform - integrated with WooCommerce for free.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-google-analytics\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"2d21f7de14dfb8e9885a4622be701ddf\",\"slug\":\"woocommerce-google-analytics-integration\",\"id\":1442927},{\"title\":\"WooCommerce Tax\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Tax-Dark.png\",\"excerpt\":\"Automatically calculate how much sales tax should be collected for WooCommerce orders - by city, country, or state - at checkout.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/tax\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"f31b3b9273cce188cc2b27f7849d02dd\",\"slug\":\"woocommerce-services\",\"id\":3220291},{\"title\":\"Stripe\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Stripe-Dark-1.png\",\"excerpt\":\"Accept all major debit and credit cards as well as local payment methods with Stripe.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/stripe\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"50bb7a985c691bb943a9da4d2c8b5efd\",\"slug\":\"woocommerce-gateway-stripe\",\"id\":18627},{\"title\":\"Jetpack\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Jetpack-Dark.png\",\"excerpt\":\"Power up and protect your store with Jetpack\\r\\n\\r\\nFor free security, insights and monitoring, connect to Jetpack. It\'s everything you need for a strong, secure start.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/jetpack\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"d5bfef9700b62b2b132c74c74c3193eb\",\"slug\":\"jetpack\",\"id\":2725249},{\"title\":\"Facebook for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Facebook-Dark.png\",\"excerpt\":\"Get the Official Facebook for WooCommerce plugin for three powerful ways to help grow your business.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/facebook\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"0ea4fe4c2d7ca6338f8a322fb3e4e187\",\"slug\":\"facebook-for-woocommerce\",\"id\":2127297},{\"title\":\"Amazon Pay\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Amazon-Pay-Dark.png\",\"excerpt\":\"Amazon Pay is embedded in your WooCommerce store. Transactions take place via\\u00a0Amazon widgets, so the buyer never leaves your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/pay-with-amazon\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"9865e043bbbe4f8c9735af31cb509b53\",\"slug\":\"woocommerce-gateway-amazon-payments-advanced\",\"id\":238816},{\"title\":\"Square for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Square-Dark.png\",\"excerpt\":\"Accepting payments is easy with Square. Clear rates, fast deposits (1-2 business days). Sell online and in person, and sync all payments, items and inventory.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/square\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e907be8b86d7df0c8f8e0d0020b52638\",\"slug\":\"woocommerce-square\",\"id\":1770503},{\"title\":\"WooCommerce Shipping\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Ship-Dark-1.png\",\"excerpt\":\"Print USPS and DHL labels right from your WooCommerce dashboard and instantly save up to 90%. WooCommerce Shipping is free to use and saves you time and money.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipping\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"f31b3b9273cce188cc2b27f7849d02dd\",\"slug\":\"woocommerce-services\",\"id\":2165910},{\"title\":\"WooCommerce Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Pay-Dark.png\",\"excerpt\":\"Securely accept payments, track cash flow, and manage recurring revenue from your dashboard \\u2014 all without setup costs or monthly fees.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-payments\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"8c6319ca-8f41-4e69-be63-6b15ee37773b\",\"slug\":\"woocommerce-payments\",\"id\":5278104},{\"title\":\"Mailchimp for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/09\\/logo-mailchimp-dark-v2.png\",\"excerpt\":\"Increase traffic, drive repeat purchases, and personalize your marketing when you connect to Mailchimp.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/mailchimp-for-woocommerce\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"b4481616ebece8b1ff68fc59b90c1a91\",\"slug\":\"mailchimp-for-woocommerce\",\"id\":2545166},{\"title\":\"WooCommerce Subscriptions\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Subscriptions-Dark.png\",\"excerpt\":\"Let customers subscribe to your products or services and pay on a weekly, monthly or annual basis.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-subscriptions\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;199.00\",\"hash\":\"6115e6d7e297b623a169fdcf5728b224\",\"slug\":\"woocommerce-subscriptions\",\"id\":27147},{\"title\":\"ShipStation Integration\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Shipstation-Dark.png\",\"excerpt\":\"Fulfill all your Woo orders (and wherever else you sell) quickly and easily using ShipStation. Try it free for 30 days today!\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipstation-integration\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"9de8640767ba64237808ed7f245a49bb\",\"slug\":\"woocommerce-shipstation-integration\",\"id\":18734},{\"title\":\"Product Add-Ons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Product-Add-Ons-Dark.png\",\"excerpt\":\"Offer add-ons like gift wrapping, special messages or other special options for your products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"147d0077e591e16db9d0d67daeb8c484\",\"slug\":\"woocommerce-product-addons\",\"id\":18618},{\"title\":\"PayFast Payment Gateway\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Payfast-Dark-1.png\",\"excerpt\":\"Take payments on your WooCommerce store via PayFast (redirect method).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/payfast-payment-gateway\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"557bf07293ad916f20c207c6c9cd15ff\",\"slug\":\"woocommerce-payfast-gateway\",\"id\":18596},{\"title\":\"USPS Shipping Method\",\"image\":\"\",\"excerpt\":\"Get shipping rates from the USPS API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/usps-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"83d1524e8f5f1913e58889f83d442c32\",\"slug\":\"woocommerce-shipping-usps\",\"id\":18657},{\"title\":\"Google Ads &amp; Marketing by Kliken\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2019\\/02\\/GA-for-Woo-Logo-374x192px-qu3duk.png\",\"excerpt\":\"Get in front of shoppers and drive traffic to your store so you can grow your business with Smart Shopping Campaigns and free listings.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-ads-and-marketing\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"bf66e173-a220-4da7-9512-b5728c20fc16\",\"slug\":\"kliken-marketing-for-google\",\"id\":3866145},{\"title\":\"UPS Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/UPS-Shipping-Method-Dark.png\",\"excerpt\":\"Get shipping rates from the UPS API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/ups-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8dae58502913bac0fbcdcaba515ea998\",\"slug\":\"woocommerce-shipping-ups\",\"id\":18665},{\"title\":\"Shipment Tracking\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Ship-Tracking-Dark-1.png\",\"excerpt\":\"Add shipment tracking information to your orders.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipment-tracking\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"1968e199038a8a001c9f9966fd06bf88\",\"slug\":\"woocommerce-shipment-tracking\",\"id\":18693},{\"title\":\"Table Rate Shipping\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Product-Table-Rate-Shipping-Dark.png\",\"excerpt\":\"Advanced, flexible shipping. Define multiple shipping rates based on location, price, weight, shipping class or item count.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/table-rate-shipping\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"3034ed8aff427b0f635fe4c86bbf008a\",\"slug\":\"woocommerce-table-rate-shipping\",\"id\":18718},{\"title\":\"Checkout Field Editor\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Checkout-Field-Editor-Dark.png\",\"excerpt\":\"Optimize your checkout process by adding, removing or editing fields to suit your needs.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-checkout-field-editor\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"2b8029f0d7cdd1118f4d843eb3ab43ff\",\"slug\":\"woocommerce-checkout-field-editor\",\"id\":184594},{\"title\":\"Braintree for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/02\\/braintree-black-copy.png\",\"excerpt\":\"Accept PayPal, credit cards and debit cards with a single payment gateway solution \\u2014 PayPal Powered by Braintree.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gateway-paypal-powered-by-braintree\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"27f010c8e34ca65b205ddec88ad14536\",\"slug\":\"woocommerce-gateway-paypal-powered-by-braintree\",\"id\":1489837},{\"title\":\"WooCommerce Bookings\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Bookings-Dark.png\",\"excerpt\":\"Allow customers to book appointments, make reservations or rent equipment without leaving your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-bookings\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/themes.woocommerce.com\\/hotel\\/\",\"price\":\"&#36;249.00\",\"hash\":\"911c438934af094c2b38d5560b9f50f3\",\"slug\":\"WooCommerce Bookings\",\"id\":390890},{\"title\":\"WooCommerce Memberships\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2015\\/06\\/Thumbnail-Memberships-updated.png\",\"excerpt\":\"Give members access to restricted content or products, for a fee or for free.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-memberships\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;199.00\",\"hash\":\"9288e7609ad0b487b81ef6232efa5cfc\",\"slug\":\"woocommerce-memberships\",\"id\":958589},{\"title\":\"Product Bundles\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-PB.png?v=1\",\"excerpt\":\"Offer personalized product bundles, bulk discount packages, and assembled\\u00a0products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-bundles\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"aa2518b5-ab19-4b75-bde9-60ca51e20f28\",\"slug\":\"woocommerce-product-bundles\",\"id\":18716},{\"title\":\"Multichannel for WooCommerce: Google, Amazon, eBay &amp; Walmart Integration\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2018\\/10\\/Woo-Extension-Store-Logo-v2.png\",\"excerpt\":\"Get the official Google, Amazon, eBay and Walmart extension and create, sync and manage multichannel listings directly from WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/amazon-ebay-integration\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e4000666-9275-4c71-8619-be61fb41c9f9\",\"slug\":\"woocommerce-amazon-ebay-integration\",\"id\":3545890},{\"title\":\"Min\\/Max Quantities\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Min-Max-Qua-Dark.png\",\"excerpt\":\"Specify minimum and maximum allowed product quantities for orders to be completed.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/minmax-quantities\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"2b5188d90baecfb781a5aa2d6abb900a\",\"slug\":\"woocommerce-min-max-quantities\",\"id\":18616},{\"title\":\"LiveChat for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2015\\/11\\/LC_woo_regular-zmiaym.png\",\"excerpt\":\"Live Chat and messaging platform for sales and support -- increase average order value and overall sales through live conversations.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/livechat\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.livechat.com\\/livechat-for-ecommerce\\/?a=woocommerce&amp;utm_source=woocommerce.com&amp;utm_medium=integration&amp;utm_campaign=woocommerce.com\",\"price\":\"&#36;0.00\",\"hash\":\"5344cc1f-ed4a-4d00-beff-9d67f6d372f3\",\"slug\":\"livechat-woocommerce\",\"id\":1348888},{\"title\":\"FedEx Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/01\\/FedEx_Logo_Wallpaper.jpeg\",\"excerpt\":\"Get shipping rates from the FedEx API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/fedex-shipping-module\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1a48b598b47a81559baadef15e320f64\",\"slug\":\"woocommerce-shipping-fedex\",\"id\":18620},{\"title\":\"Product CSV Import Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Product-CSV-Import-Dark.png\",\"excerpt\":\"Import, merge, and export products and variations to and from WooCommerce using a CSV file.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-csv-import-suite\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"7ac9b00a1fe980fb61d28ab54d167d0d\",\"slug\":\"woocommerce-product-csv-import-suite\",\"id\":18680},{\"title\":\"Follow-Ups\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Follow-Ups-Dark.png\",\"excerpt\":\"Automatically contact customers after purchase - be it everyone, your most loyal or your biggest spenders - and keep your store top-of-mind.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/follow-up-emails\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"05ece68fe94558e65278fe54d9ec84d2\",\"slug\":\"woocommerce-follow-up-emails\",\"id\":18686},{\"title\":\"Authorize.Net\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2013\\/04\\/Thumbnail-Authorize.net-updated.png\",\"excerpt\":\"Authorize.Net gateway with support for pre-orders and subscriptions.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/authorize-net\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8b61524fe53add7fdd1a8d1b00b9327d\",\"slug\":\"woocommerce-gateway-authorize-net-cim\",\"id\":178481},{\"title\":\"Australia Post Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/australia-post.gif\",\"excerpt\":\"Get shipping rates for your WooCommerce store from the Australia Post API, which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/australia-post-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1dbd4dc6bd91a9cda1bd6b9e7a5e4f43\",\"slug\":\"woocommerce-shipping-australia-post\",\"id\":18622},{\"title\":\"Canada Post Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/canada-post.png\",\"excerpt\":\"Get shipping rates from the Canada Post Ratings API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/canada-post-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"ac029cdf3daba20b20c7b9be7dc00e0e\",\"slug\":\"woocommerce-shipping-canada-post\",\"id\":18623},{\"title\":\"Product Vendors\",\"image\":\"\",\"excerpt\":\"Turn your store into a multi-vendor marketplace\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-vendors\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"a97d99fccd651bbdd728f4d67d492c31\",\"slug\":\"woocommerce-product-vendors\",\"id\":219982},{\"title\":\"WooCommerce Customer \\/ Order \\/ Coupon Export\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/02\\/Thumbnail-Customer-Order-Coupon-Export-updated.png\",\"excerpt\":\"Export customers, orders, and coupons from WooCommerce manually or on an automated schedule.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/ordercustomer-csv-export\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"914de15813a903c767b55445608bf290\",\"slug\":\"woocommerce-customer-order-csv-export\",\"id\":18652},{\"title\":\"WooCommerce Accommodation Bookings\",\"image\":\"\",\"excerpt\":\"Book accommodation using WooCommerce and the WooCommerce Bookings extension.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-accommodation-bookings\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"99b2a7a4af90b6cefd2a733b3b1f78e7\",\"slug\":\"woocommerce-accommodation-bookings\",\"id\":1412069},{\"title\":\"Royal Mail\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/04\\/royalmail.png\",\"excerpt\":\"Offer Royal Mail shipping rates to your customers\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/royal-mail\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"03839cca1a16c4488fcb669aeb91a056\",\"slug\":\"woocommerce-shipping-royalmail\",\"id\":182719},{\"title\":\"WooCommerce Brands\",\"image\":\"\",\"excerpt\":\"Create, assign and list brands for products, and allow customers to view by brand.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/brands\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"8a88c7cbd2f1e73636c331c7a86f818c\",\"slug\":\"woocommerce-brands\",\"id\":18737},{\"title\":\"Xero\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2012\\/08\\/xero2.png\",\"excerpt\":\"Save time with automated sync between WooCommerce and your Xero account.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/xero\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"f0dd29d338d3c67cf6cee88eddf6869b\",\"slug\":\"woocommerce-xero\",\"id\":18733},{\"title\":\"Smart Coupons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/10\\/wc-product-smart-coupons.png\",\"excerpt\":\"Everything you need for discounts, coupons, credits, gift cards, product giveaways, offers, and promotions. Most popular and complete coupons plugin for WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/smart-coupons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/demo.storeapps.org\\/?demo=sc\",\"price\":\"&#36;99.00\",\"hash\":\"05c45f2aa466106a466de4402fff9dde\",\"slug\":\"woocommerce-smart-coupons\",\"id\":18729},{\"title\":\"AutomateWoo\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-AutomateWoo-Dark-1.png\",\"excerpt\":\"Powerful marketing automation for WooCommerce. AutomateWoo has the tools you need to grow your store and make more money.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/automatewoo\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"ba9299b8-1dba-4aa0-a313-28bc1755cb88\",\"slug\":\"automatewoo\",\"id\":4652610},{\"title\":\"Advanced Notifications\",\"image\":\"\",\"excerpt\":\"Easily setup \\\"new order\\\" and stock email notifications for multiple recipients of your choosing.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/advanced-notifications\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"112372c44b002fea2640bd6bfafbca27\",\"slug\":\"woocommerce-advanced-notifications\",\"id\":18740},{\"title\":\"WooCommerce Points and Rewards\",\"image\":\"\",\"excerpt\":\"Reward your customers for purchases and other actions with points which can be redeemed for discounts.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-points-and-rewards\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"1649b6cca5da8b923b01ca56b5cdd246\",\"slug\":\"woocommerce-points-and-rewards\",\"id\":210259},{\"title\":\"WooCommerce Zapier\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/woocommerce-zapier-logo.png\",\"excerpt\":\"Integrate with 3000+ cloud apps and services today.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-zapier\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;59.00\",\"hash\":\"0782bdbe932c00f4978850268c6cfe40\",\"slug\":\"woocommerce-zapier\",\"id\":243589},{\"title\":\"WooCommerce Pre-Orders\",\"image\":\"\",\"excerpt\":\"Allow customers to order products before they are available.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-pre-orders\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"b2dc75e7d55e6f5bbfaccb59830f66b7\",\"slug\":\"woocommerce-pre-orders\",\"id\":178477},{\"title\":\"WooCommerce Subscription Downloads\",\"image\":\"\",\"excerpt\":\"Offer additional downloads to your subscribers, via downloadable products listed in your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-subscription-downloads\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"5be9e21c13953253e4406d2a700382ec\",\"slug\":\"woocommerce-subscription-downloads\",\"id\":420458},{\"title\":\"Dynamic Pricing\",\"image\":\"\",\"excerpt\":\"Bulk discounts, role-based pricing and much more\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/dynamic-pricing\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"9a41775bb33843f52c93c922b0053986\",\"slug\":\"woocommerce-dynamic-pricing\",\"id\":18643},{\"title\":\"WooCommerce Additional Variation Images\",\"image\":\"\",\"excerpt\":\"Add gallery images per variation on variable products within WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-additional-variation-images\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/themes.woocommerce.com\\/storefront\\/product\\/woo-single-1\\/\",\"price\":\"&#36;49.00\",\"hash\":\"c61dd6de57dcecb32bd7358866de4539\",\"slug\":\"woocommerce-additional-variation-images\",\"id\":477384},{\"title\":\"Name Your Price\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2012\\/09\\/nyp-icon-dark-v83owf.png\",\"excerpt\":\"Allow customers to define the product price. Also useful for accepting user-set donations.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/name-your-price\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"31b4e11696cd99a3c0572975a84f1c08\",\"slug\":\"woocommerce-name-your-price\",\"id\":18738},{\"title\":\"WooCommerce Deposits\",\"image\":\"\",\"excerpt\":\"Enable customers to pay for products using a deposit or a payment plan.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-deposits\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;179.00\",\"hash\":\"de192a6cf12c4fd803248da5db700762\",\"slug\":\"woocommerce-deposits\",\"id\":977087},{\"title\":\"WooCommerce Print Invoices &amp; Packing lists\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/03\\/Thumbnail-Print-Invoices-Packing-lists-updated.png\",\"excerpt\":\"Generate invoices, packing slips, and pick lists for your WooCommerce orders.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/print-invoices-packing-lists\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"465de1126817cdfb42d97ebca7eea717\",\"slug\":\"woocommerce-pip\",\"id\":18666},{\"title\":\"Amazon S3 Storage\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/amazon.png\",\"excerpt\":\"Serve digital products via Amazon S3\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/amazon-s3-storage\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"473bf6f221b865eff165c97881b473bb\",\"slug\":\"woocommerce-amazon-s3-storage\",\"id\":18663},{\"title\":\"Cart Add-ons\",\"image\":\"\",\"excerpt\":\"A powerful tool for driving incremental and impulse purchases by customers once they are in the shopping cart\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/cart-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"3a8ef25334396206f5da4cf208adeda3\",\"slug\":\"woocommerce-cart-add-ons\",\"id\":18717},{\"title\":\"Shipping Multiple Addresses\",\"image\":\"\",\"excerpt\":\"Allow your customers to ship individual items in a single order to multiple addresses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipping-multiple-addresses\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"aa0eb6f777846d329952d5b891d6f8cc\",\"slug\":\"woocommerce-shipping-multiple-addresses\",\"id\":18741},{\"title\":\"WooCommerce AvaTax\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/01\\/Thumbnail-Avalara-updated.png\",\"excerpt\":\"Get 100% accurate sales tax calculations and on time tax return filing. No more tracking sales tax rates, rules, or jurisdictional boundaries.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-avatax\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"57077a4b28ba71cacf692bcf4a1a7f60\",\"slug\":\"woocommerce-avatax\",\"id\":1389326},{\"title\":\"Google Product Feed\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2011\\/11\\/logo-regular-lscryp.png\",\"excerpt\":\"Feed product data to Google Merchant Center for setting up Google product listings &amp; product ads.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-product-feed\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"d55b4f852872025741312839f142447e\",\"slug\":\"woocommerce-product-feeds\",\"id\":18619},{\"title\":\"TaxJar\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/10\\/taxjar-logotype.png\",\"excerpt\":\"Save hours every month by putting your sales tax on autopilot. Automated, multi-state sales tax calculation, reporting, and filing for your WooCommerce store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/taxjar\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"12072d8e-e933-4561-97b1-9db3c7eeed91\",\"slug\":\"taxjar-simplified-taxes-for-woocommerce\",\"id\":514914},{\"title\":\"Klarna Checkout\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2018\\/01\\/Partner_marketing_Klarna_Checkout_Black-1.png\",\"excerpt\":\"Klarna Checkout is a full checkout experience embedded on your site that includes all popular payment methods (Pay Now, Pay Later, Financing, Installments).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/klarna-checkout\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/demo.krokedil.se\\/klarnacheckout\\/\",\"price\":\"&#36;0.00\",\"hash\":\"90f8ce584e785fcd8c2d739fd4f40d78\",\"slug\":\"klarna-checkout-for-woocommerce\",\"id\":2754152},{\"title\":\"Bulk Stock Management\",\"image\":\"\",\"excerpt\":\"Edit product and variation stock levels in bulk via this handy interface\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/bulk-stock-management\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"02f4328d52f324ebe06a78eaaae7934f\",\"slug\":\"woocommerce-bulk-stock-management\",\"id\":18670},{\"title\":\"WooCommerce Email Customizer\",\"image\":\"\",\"excerpt\":\"Connect with your customers with each email you send by visually modifying your email templates via the WordPress Customizer.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-email-customizer\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"bd909fa97874d431f203b5336c7e8873\",\"slug\":\"woocommerce-email-customizer\",\"id\":853277},{\"title\":\"Force Sells\",\"image\":\"\",\"excerpt\":\"Force products to be added to the cart\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/force-sells\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"3ebddfc491ca168a4ea4800b893302b0\",\"slug\":\"woocommerce-force-sells\",\"id\":18678},{\"title\":\"WooCommerce Quick View\",\"image\":\"\",\"excerpt\":\"Show a quick-view button to view product details and add to cart via lightbox popup\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-quick-view\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"619c6e57ce72c49c4b57e15b06eddb65\",\"slug\":\"woocommerce-quick-view\",\"id\":187509},{\"title\":\"WooCommerce Purchase Order Gateway\",\"image\":\"\",\"excerpt\":\"Receive purchase orders via your WooCommerce-powered online store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gateway-purchase-order\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"573a92318244ece5facb449d63e74874\",\"slug\":\"woocommerce-gateway-purchase-order\",\"id\":478542},{\"title\":\"Returns and Warranty Requests\",\"image\":\"\",\"excerpt\":\"Manage the RMA process, add warranties to products &amp; let customers request &amp; manage returns \\/ exchanges from their account.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/warranty-requests\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"9b4c41102e6b61ea5f558e16f9b63e25\",\"slug\":\"woocommerce-warranty\",\"id\":228315},{\"title\":\"Product Enquiry Form\",\"image\":\"\",\"excerpt\":\"Allow visitors to contact you directly from the product details page via a reCAPTCHA protected form to enquire about a product.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-enquiry-form\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"5a0f5d72519a8ffcc86669f042296937\",\"slug\":\"woocommerce-product-enquiry-form\",\"id\":18601},{\"title\":\"WooCommerce Box Office\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-BO-Dark.png\",\"excerpt\":\"Sell tickets for your next event, concert, function, fundraiser or conference directly on your own site\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-box-office\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"e704c9160de318216a8fa657404b9131\",\"slug\":\"woocommerce-box-office\",\"id\":1628717},{\"title\":\"Gravity Forms Product Add-ons\",\"image\":\"\",\"excerpt\":\"Powerful product add-ons, Gravity style\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/gravity-forms-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.elementstark.com\\/woocommerce-extension-demos\\/product-category\\/gravity-forms\\/\",\"price\":\"&#36;99.00\",\"hash\":\"a6ac0ab1a1536e3a357ccf24c0650ed0\",\"slug\":\"woocommerce-gravityforms-product-addons\",\"id\":18633},{\"title\":\"WooCommerce Order Barcodes\",\"image\":\"\",\"excerpt\":\"Generates a unique barcode for each order on your site - perfect for e-tickets, packing slips, reservations and a variety of other uses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-barcodes\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"889835bb29ee3400923653e1e44a3779\",\"slug\":\"woocommerce-order-barcodes\",\"id\":391708},{\"title\":\"Composite Products\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-CP.png?v=1\",\"excerpt\":\"Create product kit builders and custom product configurators using existing products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/composite-products\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"0343e0115bbcb97ccd98442b8326a0af\",\"slug\":\"woocommerce-composite-products\",\"id\":216836},{\"title\":\"WooCommerce 360\\u00ba Image\",\"image\":\"\",\"excerpt\":\"An easy way to add a dynamic, controllable 360\\u00ba image rotation to your WooCommerce site, by adding a group of images to a product\\u2019s gallery.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-360-image\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"24eb2cfa3738a66bf3b2587876668cd2\",\"slug\":\"woocommerce-360-image\",\"id\":512186},{\"title\":\"WooCommerce Photography\",\"image\":\"\",\"excerpt\":\"Sell photos in the blink of an eye using this simple as dragging &amp; dropping interface.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-photography\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"ee76e8b9daf1d97ca4d3874cc9e35687\",\"slug\":\"woocommerce-photography\",\"id\":583602},{\"title\":\"WooCommerce Bookings Availability\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Bookings-Aval-Dark.png\",\"excerpt\":\"Sell more bookings by presenting a calendar or schedule of available slots in a page or post.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/bookings-availability\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"30770d2a-e392-4e82-baaa-76cfc7d02ae3\",\"slug\":\"woocommerce-bookings-availability\",\"id\":4228225},{\"title\":\"Software Add-on\",\"image\":\"\",\"excerpt\":\"Sell License Keys for Software\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/software-add-on\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"79f6dbfe1f1d3a56a86f0509b6d6b04b\",\"slug\":\"woocommerce-software-add-on\",\"id\":18683},{\"title\":\"WooCommerce Products Compare\",\"image\":\"\",\"excerpt\":\"WooCommerce Products Compare will allow your potential customers to easily compare products within your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-products-compare\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"c3ba0a4a3199a0cc7a6112eb24414548\",\"slug\":\"woocommerce-products-compare\",\"id\":853117},{\"title\":\"WooCommerce Store Catalog PDF Download\",\"image\":\"\",\"excerpt\":\"Offer your customers a PDF download of your product catalog, generated by WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-store-catalog-pdf-download\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"79ca7aadafe706364e2d738b7c1090c4\",\"slug\":\"woocommerce-store-catalog-pdf-download\",\"id\":675790},{\"title\":\"eWAY\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2011\\/10\\/eway-logo-3000-2000.jpg\",\"excerpt\":\"Take credit card payments securely via eWay (SG, MY, HK, AU, and NZ) keeping customers on your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/eway\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"2c497769d98d025e0d340cd0b5ea5da1\",\"slug\":\"woocommerce-gateway-eway\",\"id\":18604},{\"title\":\"WooCommerce Paid Courses\",\"image\":\"\",\"excerpt\":\"Sell your online courses using the most popular eCommerce platform on the web \\u2013 WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-paid-courses\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"bad2a02a063555b7e2bee59924690763\",\"slug\":\"woothemes-sensei\",\"id\":152116},{\"title\":\"PayPal\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2020\\/10\\/PPCP-Tile-PayPal-Logo-and-Cart-Art-2x-2-uozwz8.jpg\",\"excerpt\":\"PayPal\\u2019s latest, most complete payment processing solution. Accept PayPal exclusives, credit\\/debit cards and local payment methods. Turn on only PayPal options or process a full suite of payment methods. Enable global transactions with extensive currency and country coverage. Built and supported by WooCommerce and PayPal.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-paypal-payments\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"934115ab-e3f3-4435-9580-345b1ce21899\",\"slug\":\"woocommerce-paypal-payments\",\"id\":6410731},{\"title\":\"Catalog Visibility Options\",\"image\":\"\",\"excerpt\":\"Transform WooCommerce into an online catalog by removing eCommerce functionality\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/catalog-visibility-options\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"12e791110365fdbb5865c8658907967e\",\"slug\":\"woocommerce-catalog-visibility-options\",\"id\":18648},{\"title\":\"QuickBooks Sync for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2019\\/04\\/woocommerce-com-logo-1-hyhzbh.png\",\"excerpt\":\"Automatic two-way sync for orders, customers, products, inventory and more between WooCommerce and QuickBooks (Online, Desktop, or POS).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/quickbooks-sync-for-woocommerce\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"c5e32e20-7c1f-4585-8b15-d930c2d842ac\",\"slug\":\"myworks-woo-sync-for-quickbooks-online\",\"id\":4065824},{\"title\":\"WooCommerce Blocks\",\"image\":\"\",\"excerpt\":\"WooCommerce Blocks offers a range of Gutenberg blocks you can use to build and customise your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gutenberg-products-block\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"c2e9f13a-f90c-4ffe-a8a5-b432399ec263\",\"slug\":\"woo-gutenberg-products-block\",\"id\":3076677},{\"title\":\"Conditional Shipping and Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-CSP.png?v=1\",\"excerpt\":\"Use conditional logic to restrict the shipping and payment options available on your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/conditional-shipping-and-payments\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1f56ff002fa830b77017b0107505211a\",\"slug\":\"woocommerce-conditional-shipping-and-payments\",\"id\":680253},{\"title\":\"Sequential Order Numbers Pro\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/05\\/Thumbnail-Sequential-Order-Numbers-Pro-updated.png\",\"excerpt\":\"Tame your order numbers! Advanced &amp; sequential order numbers with optional prefixes \\/ suffixes\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/sequential-order-numbers-pro\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"0b18a2816e016ba9988b93b1cd8fe766\",\"slug\":\"woocommerce-sequential-order-numbers-pro\",\"id\":18688},{\"title\":\"WooCommerce Order Status Manager\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2015\\/02\\/Thumbnail-Order-Status-Manager-updated.png\",\"excerpt\":\"Create, edit, and delete completely custom order statuses and integrate them seamlessly into your order management flow.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-status-manager\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"51fd9ab45394b4cad5a0ebf58d012342\",\"slug\":\"woocommerce-order-status-manager\",\"id\":588398},{\"title\":\"WooCommerce Checkout Add-Ons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/07\\/Thumbnail-Checkout-Add-Ons-updated.png\",\"excerpt\":\"Highlight relevant products, offers like free shipping and other up-sells during checkout.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-checkout-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8fdca00b4000b7a8cc26371d0e470a8f\",\"slug\":\"woocommerce-checkout-add-ons\",\"id\":466854},{\"title\":\"WooCommerce Google Analytics Pro\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/01\\/Thumbnail-GAPro-updated.png\",\"excerpt\":\"Add advanced event tracking and enhanced eCommerce tracking to your WooCommerce site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-google-analytics-pro\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"d8aed8b7306b509eec1589e59abe319f\",\"slug\":\"woocommerce-google-analytics-pro\",\"id\":1312497},{\"title\":\"Coupon Shortcodes\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/09\\/woocommerce-coupon-shortcodes-product-image-1870x960-1-vc5gux.png\",\"excerpt\":\"Show coupon discount info using shortcodes. Allows to render coupon information and content conditionally, based on the validity of coupons.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/coupon-shortcodes\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"ac5d9d51-70b2-4d8f-8b89-24200eea1394\",\"slug\":\"woocommerce-coupon-shortcodes\",\"id\":244762},{\"title\":\"WooCommerce One Page Checkout\",\"image\":\"\",\"excerpt\":\"Create special pages where customers can choose products, checkout &amp; pay all on the one page.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-one-page-checkout\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"c9ba8f8352cd71b5508af5161268619a\",\"slug\":\"woocommerce-one-page-checkout\",\"id\":527886},{\"title\":\"Jilt\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2017\\/12\\/Thumbnail-Jilt-updated.png\",\"excerpt\":\"All-in-one email marketing platform built for WooCommerce stores. Send newsletters, abandoned cart reminders, win-backs, welcome automations, and more.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/jilt\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"b53aafb64dca33835e41ee06de7e9816\",\"slug\":\"jilt-for-woocommerce\",\"id\":2754876},{\"title\":\"WooCommerce Product Search\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2014\\/10\\/woocommerce-product-search-product-image-1870x960-1-jvsljj.png\",\"excerpt\":\"The perfect search engine helps customers to find and buy products quickly \\u2013 essential for every WooCommerce store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-product-search\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/demo.itthinx.com\\/wps\\/\",\"price\":\"&#36;49.00\",\"hash\":\"c84cc8ca16ddac3408e6b6c5871133a8\",\"slug\":\"woocommerce-product-search\",\"id\":512174},{\"title\":\"First Data\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/02\\/Thumbnail-FirstData-updated.png\",\"excerpt\":\"FirstData gateway for WooCommerce\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/firstdata\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"eb3e32663ec0810592eaf0d097796230\",\"slug\":\"woocommerce-gateway-firstdata\",\"id\":18645},{\"title\":\"WooSlider\",\"image\":\"\",\"excerpt\":\"WooSlider is the ultimate responsive slideshow WordPress slider plugin\\r\\n\\r\\n\\u00a0\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/wooslider\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/www.wooslider.com\\/\",\"price\":\"&#36;49.00\",\"hash\":\"209d98f3ccde6cc3de7e8732a2b20b6a\",\"slug\":\"wooslider\",\"id\":46506},{\"title\":\"WooCommerce Social Login\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/08\\/Thumbnail-Social-Login-updated.png\",\"excerpt\":\"Enable Social Login for seamless checkout and account creation.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-social-login\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/demos.skyverge.com\\/woocommerce-social-login\\/\",\"price\":\"&#36;79.00\",\"hash\":\"b231cd6367a79cc8a53b7d992d77525d\",\"slug\":\"woocommerce-social-login\",\"id\":473617},{\"title\":\"WooCommerce Order Status Control\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/06\\/Thumbnail-Order-Status-Control-updated.png\",\"excerpt\":\"Use this extension to automatically change the order status to \\\"completed\\\" after successful payment.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-status-control\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"32400e509c7c36dcc1cd368e8267d981\",\"slug\":\"woocommerce-order-status-control\",\"id\":439037},{\"title\":\"Variation Swatches and Photos\",\"image\":\"\",\"excerpt\":\"Show color and image swatches instead of dropdowns for variable products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/variation-swatches-and-photos\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.elementstark.com\\/woocommerce-extension-demos\\/product-category\\/swatches-and-photos\\/\",\"price\":\"&#36;99.00\",\"hash\":\"37bea8d549df279c8278878d081b062f\",\"slug\":\"woocommerce-variation-swatches-and-photos\",\"id\":18697},{\"title\":\"Opayo Payment Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2011\\/10\\/Opayo_logo_RGB.png\",\"excerpt\":\"Take payments on your WooCommerce store via Opayo (formally SagePay).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/sage-pay-form\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"6bc0cca47d0274d8ef9b164f6fbec1cc\",\"slug\":\"woocommerce-gateway-sagepay-form\",\"id\":18599},{\"title\":\"EU VAT Number\",\"image\":\"\",\"excerpt\":\"Collect VAT numbers at checkout and remove the VAT charge for eligible EU businesses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/eu-vat-number\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"d2720c4b4bb8d6908e530355b7a2d734\",\"slug\":\"woocommerce-eu-vat-number\",\"id\":18592},{\"title\":\"HubSpot for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2020\\/04\\/hubspotlogo-web-color-pxebeq.png\",\"excerpt\":\"Integrate HubSpot with WooCommerce to connect your store with HubSpot\'s CRM, abandoned cart tracking, marketing automation &amp; more.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/hubspot-for-woocommerce\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e50acec8-3a6c-454c-8562-2da4898fa6c1\",\"slug\":\"hubspot-for-woocommerce\",\"id\":5785079},{\"title\":\"Google Listings &amp; Ads\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2021\\/06\\/marketplace-card.png\",\"excerpt\":\"Reach millions of engaged shoppers across Google with free product listings and ads. Built in partnership with Google.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-listings-and-ads\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"118f4d86-f126-4c3a-8525-644e3170d161\",\"slug\":\"google-listings-and-ads\",\"id\":7623964},{\"title\":\"Customer\\/Order\\/Coupon CSV Import Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/07\\/Thumbnail-Customer-Order-Coupon-CSV-Import-Suite-updated.png\",\"excerpt\":\"Import both customers and orders into WooCommerce from a CSV file.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/customerorder-csv-import-suite\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"eb00ca8317a0f64dbe185c995e5ea3df\",\"slug\":\"woocommerce-customer-order-csv-import\",\"id\":18709}]}\";s:3:\"raw\";s:48936:\"HTTP/1.1 200 OK\r\nServer: nginx\r\nDate: Mon, 12 Jul 2021 20:51:28 GMT\r\nContent-Type: application/json; charset=UTF-8\r\nContent-Length: 11651\r\nConnection: close\r\nX-Robots-Tag: noindex\r\nLink: <https://woocommerce.com/wp-json/>; rel=\"https://api.w.org/\"\r\nX-Content-Type-Options: nosniff\r\nAccess-Control-Expose-Headers: X-WP-Total, X-WP-TotalPages, Link\r\nAccess-Control-Allow-Headers: Authorization, X-WP-Nonce, Content-Disposition, Content-MD5, Content-Type\r\nX-WCCOM-Cache: HIT\r\nCache-Control: max-age=60\r\nAllow: GET\r\nContent-Encoding: gzip\r\nX-rq: vie2 0 4 9980\r\nAge: 10\r\nX-Cache: hit\r\nVary: Accept-Encoding, Origin\r\nAccept-Ranges: bytes\r\n\r\n{\"products\":[{\"title\":\"WooCommerce Google Analytics\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/GA-Dark.png\",\"excerpt\":\"Understand your customers and increase revenue with world\\u2019s leading analytics platform - integrated with WooCommerce for free.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-google-analytics\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"2d21f7de14dfb8e9885a4622be701ddf\",\"slug\":\"woocommerce-google-analytics-integration\",\"id\":1442927},{\"title\":\"WooCommerce Tax\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Tax-Dark.png\",\"excerpt\":\"Automatically calculate how much sales tax should be collected for WooCommerce orders - by city, country, or state - at checkout.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/tax\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"f31b3b9273cce188cc2b27f7849d02dd\",\"slug\":\"woocommerce-services\",\"id\":3220291},{\"title\":\"Stripe\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Stripe-Dark-1.png\",\"excerpt\":\"Accept all major debit and credit cards as well as local payment methods with Stripe.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/stripe\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"50bb7a985c691bb943a9da4d2c8b5efd\",\"slug\":\"woocommerce-gateway-stripe\",\"id\":18627},{\"title\":\"Jetpack\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Jetpack-Dark.png\",\"excerpt\":\"Power up and protect your store with Jetpack\\r\\n\\r\\nFor free security, insights and monitoring, connect to Jetpack. It\'s everything you need for a strong, secure start.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/jetpack\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"d5bfef9700b62b2b132c74c74c3193eb\",\"slug\":\"jetpack\",\"id\":2725249},{\"title\":\"Facebook for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Facebook-Dark.png\",\"excerpt\":\"Get the Official Facebook for WooCommerce plugin for three powerful ways to help grow your business.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/facebook\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"0ea4fe4c2d7ca6338f8a322fb3e4e187\",\"slug\":\"facebook-for-woocommerce\",\"id\":2127297},{\"title\":\"Amazon Pay\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Amazon-Pay-Dark.png\",\"excerpt\":\"Amazon Pay is embedded in your WooCommerce store. Transactions take place via\\u00a0Amazon widgets, so the buyer never leaves your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/pay-with-amazon\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"9865e043bbbe4f8c9735af31cb509b53\",\"slug\":\"woocommerce-gateway-amazon-payments-advanced\",\"id\":238816},{\"title\":\"Square for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Square-Dark.png\",\"excerpt\":\"Accepting payments is easy with Square. Clear rates, fast deposits (1-2 business days). Sell online and in person, and sync all payments, items and inventory.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/square\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e907be8b86d7df0c8f8e0d0020b52638\",\"slug\":\"woocommerce-square\",\"id\":1770503},{\"title\":\"WooCommerce Shipping\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Ship-Dark-1.png\",\"excerpt\":\"Print USPS and DHL labels right from your WooCommerce dashboard and instantly save up to 90%. WooCommerce Shipping is free to use and saves you time and money.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipping\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"f31b3b9273cce188cc2b27f7849d02dd\",\"slug\":\"woocommerce-services\",\"id\":2165910},{\"title\":\"WooCommerce Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Pay-Dark.png\",\"excerpt\":\"Securely accept payments, track cash flow, and manage recurring revenue from your dashboard \\u2014 all without setup costs or monthly fees.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-payments\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"8c6319ca-8f41-4e69-be63-6b15ee37773b\",\"slug\":\"woocommerce-payments\",\"id\":5278104},{\"title\":\"Mailchimp for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/09\\/logo-mailchimp-dark-v2.png\",\"excerpt\":\"Increase traffic, drive repeat purchases, and personalize your marketing when you connect to Mailchimp.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/mailchimp-for-woocommerce\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"b4481616ebece8b1ff68fc59b90c1a91\",\"slug\":\"mailchimp-for-woocommerce\",\"id\":2545166},{\"title\":\"WooCommerce Subscriptions\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Subscriptions-Dark.png\",\"excerpt\":\"Let customers subscribe to your products or services and pay on a weekly, monthly or annual basis.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-subscriptions\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;199.00\",\"hash\":\"6115e6d7e297b623a169fdcf5728b224\",\"slug\":\"woocommerce-subscriptions\",\"id\":27147},{\"title\":\"ShipStation Integration\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Shipstation-Dark.png\",\"excerpt\":\"Fulfill all your Woo orders (and wherever else you sell) quickly and easily using ShipStation. Try it free for 30 days today!\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipstation-integration\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"9de8640767ba64237808ed7f245a49bb\",\"slug\":\"woocommerce-shipstation-integration\",\"id\":18734},{\"title\":\"Product Add-Ons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Product-Add-Ons-Dark.png\",\"excerpt\":\"Offer add-ons like gift wrapping, special messages or other special options for your products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"147d0077e591e16db9d0d67daeb8c484\",\"slug\":\"woocommerce-product-addons\",\"id\":18618},{\"title\":\"PayFast Payment Gateway\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Payfast-Dark-1.png\",\"excerpt\":\"Take payments on your WooCommerce store via PayFast (redirect method).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/payfast-payment-gateway\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"557bf07293ad916f20c207c6c9cd15ff\",\"slug\":\"woocommerce-payfast-gateway\",\"id\":18596},{\"title\":\"USPS Shipping Method\",\"image\":\"\",\"excerpt\":\"Get shipping rates from the USPS API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/usps-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"83d1524e8f5f1913e58889f83d442c32\",\"slug\":\"woocommerce-shipping-usps\",\"id\":18657},{\"title\":\"Google Ads &amp; Marketing by Kliken\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2019\\/02\\/GA-for-Woo-Logo-374x192px-qu3duk.png\",\"excerpt\":\"Get in front of shoppers and drive traffic to your store so you can grow your business with Smart Shopping Campaigns and free listings.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-ads-and-marketing\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"bf66e173-a220-4da7-9512-b5728c20fc16\",\"slug\":\"kliken-marketing-for-google\",\"id\":3866145},{\"title\":\"UPS Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/UPS-Shipping-Method-Dark.png\",\"excerpt\":\"Get shipping rates from the UPS API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/ups-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8dae58502913bac0fbcdcaba515ea998\",\"slug\":\"woocommerce-shipping-ups\",\"id\":18665},{\"title\":\"Shipment Tracking\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Ship-Tracking-Dark-1.png\",\"excerpt\":\"Add shipment tracking information to your orders.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipment-tracking\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"1968e199038a8a001c9f9966fd06bf88\",\"slug\":\"woocommerce-shipment-tracking\",\"id\":18693},{\"title\":\"Table Rate Shipping\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Product-Table-Rate-Shipping-Dark.png\",\"excerpt\":\"Advanced, flexible shipping. Define multiple shipping rates based on location, price, weight, shipping class or item count.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/table-rate-shipping\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"3034ed8aff427b0f635fe4c86bbf008a\",\"slug\":\"woocommerce-table-rate-shipping\",\"id\":18718},{\"title\":\"Checkout Field Editor\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Checkout-Field-Editor-Dark.png\",\"excerpt\":\"Optimize your checkout process by adding, removing or editing fields to suit your needs.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-checkout-field-editor\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"2b8029f0d7cdd1118f4d843eb3ab43ff\",\"slug\":\"woocommerce-checkout-field-editor\",\"id\":184594},{\"title\":\"Braintree for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/02\\/braintree-black-copy.png\",\"excerpt\":\"Accept PayPal, credit cards and debit cards with a single payment gateway solution \\u2014 PayPal Powered by Braintree.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gateway-paypal-powered-by-braintree\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"27f010c8e34ca65b205ddec88ad14536\",\"slug\":\"woocommerce-gateway-paypal-powered-by-braintree\",\"id\":1489837},{\"title\":\"WooCommerce Bookings\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Bookings-Dark.png\",\"excerpt\":\"Allow customers to book appointments, make reservations or rent equipment without leaving your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-bookings\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/themes.woocommerce.com\\/hotel\\/\",\"price\":\"&#36;249.00\",\"hash\":\"911c438934af094c2b38d5560b9f50f3\",\"slug\":\"WooCommerce Bookings\",\"id\":390890},{\"title\":\"WooCommerce Memberships\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2015\\/06\\/Thumbnail-Memberships-updated.png\",\"excerpt\":\"Give members access to restricted content or products, for a fee or for free.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-memberships\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;199.00\",\"hash\":\"9288e7609ad0b487b81ef6232efa5cfc\",\"slug\":\"woocommerce-memberships\",\"id\":958589},{\"title\":\"Product Bundles\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-PB.png?v=1\",\"excerpt\":\"Offer personalized product bundles, bulk discount packages, and assembled\\u00a0products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-bundles\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"aa2518b5-ab19-4b75-bde9-60ca51e20f28\",\"slug\":\"woocommerce-product-bundles\",\"id\":18716},{\"title\":\"Multichannel for WooCommerce: Google, Amazon, eBay &amp; Walmart Integration\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2018\\/10\\/Woo-Extension-Store-Logo-v2.png\",\"excerpt\":\"Get the official Google, Amazon, eBay and Walmart extension and create, sync and manage multichannel listings directly from WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/amazon-ebay-integration\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e4000666-9275-4c71-8619-be61fb41c9f9\",\"slug\":\"woocommerce-amazon-ebay-integration\",\"id\":3545890},{\"title\":\"Min\\/Max Quantities\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Min-Max-Qua-Dark.png\",\"excerpt\":\"Specify minimum and maximum allowed product quantities for orders to be completed.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/minmax-quantities\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"2b5188d90baecfb781a5aa2d6abb900a\",\"slug\":\"woocommerce-min-max-quantities\",\"id\":18616},{\"title\":\"LiveChat for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2015\\/11\\/LC_woo_regular-zmiaym.png\",\"excerpt\":\"Live Chat and messaging platform for sales and support -- increase average order value and overall sales through live conversations.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/livechat\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.livechat.com\\/livechat-for-ecommerce\\/?a=woocommerce&amp;utm_source=woocommerce.com&amp;utm_medium=integration&amp;utm_campaign=woocommerce.com\",\"price\":\"&#36;0.00\",\"hash\":\"5344cc1f-ed4a-4d00-beff-9d67f6d372f3\",\"slug\":\"livechat-woocommerce\",\"id\":1348888},{\"title\":\"FedEx Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/01\\/FedEx_Logo_Wallpaper.jpeg\",\"excerpt\":\"Get shipping rates from the FedEx API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/fedex-shipping-module\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1a48b598b47a81559baadef15e320f64\",\"slug\":\"woocommerce-shipping-fedex\",\"id\":18620},{\"title\":\"Product CSV Import Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Product-CSV-Import-Dark.png\",\"excerpt\":\"Import, merge, and export products and variations to and from WooCommerce using a CSV file.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-csv-import-suite\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"7ac9b00a1fe980fb61d28ab54d167d0d\",\"slug\":\"woocommerce-product-csv-import-suite\",\"id\":18680},{\"title\":\"Follow-Ups\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Follow-Ups-Dark.png\",\"excerpt\":\"Automatically contact customers after purchase - be it everyone, your most loyal or your biggest spenders - and keep your store top-of-mind.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/follow-up-emails\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"05ece68fe94558e65278fe54d9ec84d2\",\"slug\":\"woocommerce-follow-up-emails\",\"id\":18686},{\"title\":\"Authorize.Net\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2013\\/04\\/Thumbnail-Authorize.net-updated.png\",\"excerpt\":\"Authorize.Net gateway with support for pre-orders and subscriptions.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/authorize-net\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8b61524fe53add7fdd1a8d1b00b9327d\",\"slug\":\"woocommerce-gateway-authorize-net-cim\",\"id\":178481},{\"title\":\"Australia Post Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/australia-post.gif\",\"excerpt\":\"Get shipping rates for your WooCommerce store from the Australia Post API, which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/australia-post-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1dbd4dc6bd91a9cda1bd6b9e7a5e4f43\",\"slug\":\"woocommerce-shipping-australia-post\",\"id\":18622},{\"title\":\"Canada Post Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/canada-post.png\",\"excerpt\":\"Get shipping rates from the Canada Post Ratings API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/canada-post-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"ac029cdf3daba20b20c7b9be7dc00e0e\",\"slug\":\"woocommerce-shipping-canada-post\",\"id\":18623},{\"title\":\"Product Vendors\",\"image\":\"\",\"excerpt\":\"Turn your store into a multi-vendor marketplace\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-vendors\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"a97d99fccd651bbdd728f4d67d492c31\",\"slug\":\"woocommerce-product-vendors\",\"id\":219982},{\"title\":\"WooCommerce Customer \\/ Order \\/ Coupon Export\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/02\\/Thumbnail-Customer-Order-Coupon-Export-updated.png\",\"excerpt\":\"Export customers, orders, and coupons from WooCommerce manually or on an automated schedule.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/ordercustomer-csv-export\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"914de15813a903c767b55445608bf290\",\"slug\":\"woocommerce-customer-order-csv-export\",\"id\":18652},{\"title\":\"WooCommerce Accommodation Bookings\",\"image\":\"\",\"excerpt\":\"Book accommodation using WooCommerce and the WooCommerce Bookings extension.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-accommodation-bookings\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"99b2a7a4af90b6cefd2a733b3b1f78e7\",\"slug\":\"woocommerce-accommodation-bookings\",\"id\":1412069},{\"title\":\"Royal Mail\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/04\\/royalmail.png\",\"excerpt\":\"Offer Royal Mail shipping rates to your customers\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/royal-mail\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"03839cca1a16c4488fcb669aeb91a056\",\"slug\":\"woocommerce-shipping-royalmail\",\"id\":182719},{\"title\":\"WooCommerce Brands\",\"image\":\"\",\"excerpt\":\"Create, assign and list brands for products, and allow customers to view by brand.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/brands\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"8a88c7cbd2f1e73636c331c7a86f818c\",\"slug\":\"woocommerce-brands\",\"id\":18737},{\"title\":\"Xero\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2012\\/08\\/xero2.png\",\"excerpt\":\"Save time with automated sync between WooCommerce and your Xero account.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/xero\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"f0dd29d338d3c67cf6cee88eddf6869b\",\"slug\":\"woocommerce-xero\",\"id\":18733},{\"title\":\"Smart Coupons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/10\\/wc-product-smart-coupons.png\",\"excerpt\":\"Everything you need for discounts, coupons, credits, gift cards, product giveaways, offers, and promotions. Most popular and complete coupons plugin for WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/smart-coupons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/demo.storeapps.org\\/?demo=sc\",\"price\":\"&#36;99.00\",\"hash\":\"05c45f2aa466106a466de4402fff9dde\",\"slug\":\"woocommerce-smart-coupons\",\"id\":18729},{\"title\":\"AutomateWoo\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-AutomateWoo-Dark-1.png\",\"excerpt\":\"Powerful marketing automation for WooCommerce. AutomateWoo has the tools you need to grow your store and make more money.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/automatewoo\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"ba9299b8-1dba-4aa0-a313-28bc1755cb88\",\"slug\":\"automatewoo\",\"id\":4652610},{\"title\":\"Advanced Notifications\",\"image\":\"\",\"excerpt\":\"Easily setup \\\"new order\\\" and stock email notifications for multiple recipients of your choosing.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/advanced-notifications\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"112372c44b002fea2640bd6bfafbca27\",\"slug\":\"woocommerce-advanced-notifications\",\"id\":18740},{\"title\":\"WooCommerce Points and Rewards\",\"image\":\"\",\"excerpt\":\"Reward your customers for purchases and other actions with points which can be redeemed for discounts.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-points-and-rewards\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"1649b6cca5da8b923b01ca56b5cdd246\",\"slug\":\"woocommerce-points-and-rewards\",\"id\":210259},{\"title\":\"WooCommerce Zapier\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/woocommerce-zapier-logo.png\",\"excerpt\":\"Integrate with 3000+ cloud apps and services today.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-zapier\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;59.00\",\"hash\":\"0782bdbe932c00f4978850268c6cfe40\",\"slug\":\"woocommerce-zapier\",\"id\":243589},{\"title\":\"WooCommerce Pre-Orders\",\"image\":\"\",\"excerpt\":\"Allow customers to order products before they are available.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-pre-orders\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"b2dc75e7d55e6f5bbfaccb59830f66b7\",\"slug\":\"woocommerce-pre-orders\",\"id\":178477},{\"title\":\"WooCommerce Subscription Downloads\",\"image\":\"\",\"excerpt\":\"Offer additional downloads to your subscribers, via downloadable products listed in your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-subscription-downloads\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"5be9e21c13953253e4406d2a700382ec\",\"slug\":\"woocommerce-subscription-downloads\",\"id\":420458},{\"title\":\"Dynamic Pricing\",\"image\":\"\",\"excerpt\":\"Bulk discounts, role-based pricing and much more\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/dynamic-pricing\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"9a41775bb33843f52c93c922b0053986\",\"slug\":\"woocommerce-dynamic-pricing\",\"id\":18643},{\"title\":\"WooCommerce Additional Variation Images\",\"image\":\"\",\"excerpt\":\"Add gallery images per variation on variable products within WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-additional-variation-images\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/themes.woocommerce.com\\/storefront\\/product\\/woo-single-1\\/\",\"price\":\"&#36;49.00\",\"hash\":\"c61dd6de57dcecb32bd7358866de4539\",\"slug\":\"woocommerce-additional-variation-images\",\"id\":477384},{\"title\":\"Name Your Price\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2012\\/09\\/nyp-icon-dark-v83owf.png\",\"excerpt\":\"Allow customers to define the product price. Also useful for accepting user-set donations.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/name-your-price\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"31b4e11696cd99a3c0572975a84f1c08\",\"slug\":\"woocommerce-name-your-price\",\"id\":18738},{\"title\":\"WooCommerce Deposits\",\"image\":\"\",\"excerpt\":\"Enable customers to pay for products using a deposit or a payment plan.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-deposits\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;179.00\",\"hash\":\"de192a6cf12c4fd803248da5db700762\",\"slug\":\"woocommerce-deposits\",\"id\":977087},{\"title\":\"WooCommerce Print Invoices &amp; Packing lists\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/03\\/Thumbnail-Print-Invoices-Packing-lists-updated.png\",\"excerpt\":\"Generate invoices, packing slips, and pick lists for your WooCommerce orders.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/print-invoices-packing-lists\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"465de1126817cdfb42d97ebca7eea717\",\"slug\":\"woocommerce-pip\",\"id\":18666},{\"title\":\"Amazon S3 Storage\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/amazon.png\",\"excerpt\":\"Serve digital products via Amazon S3\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/amazon-s3-storage\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"473bf6f221b865eff165c97881b473bb\",\"slug\":\"woocommerce-amazon-s3-storage\",\"id\":18663},{\"title\":\"Cart Add-ons\",\"image\":\"\",\"excerpt\":\"A powerful tool for driving incremental and impulse purchases by customers once they are in the shopping cart\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/cart-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"3a8ef25334396206f5da4cf208adeda3\",\"slug\":\"woocommerce-cart-add-ons\",\"id\":18717},{\"title\":\"Shipping Multiple Addresses\",\"image\":\"\",\"excerpt\":\"Allow your customers to ship individual items in a single order to multiple addresses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipping-multiple-addresses\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"aa0eb6f777846d329952d5b891d6f8cc\",\"slug\":\"woocommerce-shipping-multiple-addresses\",\"id\":18741},{\"title\":\"WooCommerce AvaTax\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/01\\/Thumbnail-Avalara-updated.png\",\"excerpt\":\"Get 100% accurate sales tax calculations and on time tax return filing. No more tracking sales tax rates, rules, or jurisdictional boundaries.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-avatax\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"57077a4b28ba71cacf692bcf4a1a7f60\",\"slug\":\"woocommerce-avatax\",\"id\":1389326},{\"title\":\"Google Product Feed\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2011\\/11\\/logo-regular-lscryp.png\",\"excerpt\":\"Feed product data to Google Merchant Center for setting up Google product listings &amp; product ads.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-product-feed\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"d55b4f852872025741312839f142447e\",\"slug\":\"woocommerce-product-feeds\",\"id\":18619},{\"title\":\"TaxJar\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/10\\/taxjar-logotype.png\",\"excerpt\":\"Save hours every month by putting your sales tax on autopilot. Automated, multi-state sales tax calculation, reporting, and filing for your WooCommerce store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/taxjar\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"12072d8e-e933-4561-97b1-9db3c7eeed91\",\"slug\":\"taxjar-simplified-taxes-for-woocommerce\",\"id\":514914},{\"title\":\"Klarna Checkout\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2018\\/01\\/Partner_marketing_Klarna_Checkout_Black-1.png\",\"excerpt\":\"Klarna Checkout is a full checkout experience embedded on your site that includes all popular payment methods (Pay Now, Pay Later, Financing, Installments).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/klarna-checkout\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/demo.krokedil.se\\/klarnacheckout\\/\",\"price\":\"&#36;0.00\",\"hash\":\"90f8ce584e785fcd8c2d739fd4f40d78\",\"slug\":\"klarna-checkout-for-woocommerce\",\"id\":2754152},{\"title\":\"Bulk Stock Management\",\"image\":\"\",\"excerpt\":\"Edit product and variation stock levels in bulk via this handy interface\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/bulk-stock-management\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"02f4328d52f324ebe06a78eaaae7934f\",\"slug\":\"woocommerce-bulk-stock-management\",\"id\":18670},{\"title\":\"WooCommerce Email Customizer\",\"image\":\"\",\"excerpt\":\"Connect with your customers with each email you send by visually modifying your email templates via the WordPress Customizer.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-email-customizer\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"bd909fa97874d431f203b5336c7e8873\",\"slug\":\"woocommerce-email-customizer\",\"id\":853277},{\"title\":\"Force Sells\",\"image\":\"\",\"excerpt\":\"Force products to be added to the cart\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/force-sells\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"3ebddfc491ca168a4ea4800b893302b0\",\"slug\":\"woocommerce-force-sells\",\"id\":18678},{\"title\":\"WooCommerce Quick View\",\"image\":\"\",\"excerpt\":\"Show a quick-view button to view product details and add to cart via lightbox popup\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-quick-view\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"619c6e57ce72c49c4b57e15b06eddb65\",\"slug\":\"woocommerce-quick-view\",\"id\":187509},{\"title\":\"WooCommerce Purchase Order Gateway\",\"image\":\"\",\"excerpt\":\"Receive purchase orders via your WooCommerce-powered online store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gateway-purchase-order\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"573a92318244ece5facb449d63e74874\",\"slug\":\"woocommerce-gateway-purchase-order\",\"id\":478542},{\"title\":\"Returns and Warranty Requests\",\"image\":\"\",\"excerpt\":\"Manage the RMA process, add warranties to products &amp; let customers request &amp; manage returns \\/ exchanges from their account.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/warranty-requests\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"9b4c41102e6b61ea5f558e16f9b63e25\",\"slug\":\"woocommerce-warranty\",\"id\":228315},{\"title\":\"Product Enquiry Form\",\"image\":\"\",\"excerpt\":\"Allow visitors to contact you directly from the product details page via a reCAPTCHA protected form to enquire about a product.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-enquiry-form\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"5a0f5d72519a8ffcc86669f042296937\",\"slug\":\"woocommerce-product-enquiry-form\",\"id\":18601},{\"title\":\"WooCommerce Box Office\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-BO-Dark.png\",\"excerpt\":\"Sell tickets for your next event, concert, function, fundraiser or conference directly on your own site\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-box-office\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"e704c9160de318216a8fa657404b9131\",\"slug\":\"woocommerce-box-office\",\"id\":1628717},{\"title\":\"Gravity Forms Product Add-ons\",\"image\":\"\",\"excerpt\":\"Powerful product add-ons, Gravity style\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/gravity-forms-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.elementstark.com\\/woocommerce-extension-demos\\/product-category\\/gravity-forms\\/\",\"price\":\"&#36;99.00\",\"hash\":\"a6ac0ab1a1536e3a357ccf24c0650ed0\",\"slug\":\"woocommerce-gravityforms-product-addons\",\"id\":18633},{\"title\":\"WooCommerce Order Barcodes\",\"image\":\"\",\"excerpt\":\"Generates a unique barcode for each order on your site - perfect for e-tickets, packing slips, reservations and a variety of other uses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-barcodes\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"889835bb29ee3400923653e1e44a3779\",\"slug\":\"woocommerce-order-barcodes\",\"id\":391708},{\"title\":\"Composite Products\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-CP.png?v=1\",\"excerpt\":\"Create product kit builders and custom product configurators using existing products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/composite-products\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"0343e0115bbcb97ccd98442b8326a0af\",\"slug\":\"woocommerce-composite-products\",\"id\":216836},{\"title\":\"WooCommerce 360\\u00ba Image\",\"image\":\"\",\"excerpt\":\"An easy way to add a dynamic, controllable 360\\u00ba image rotation to your WooCommerce site, by adding a group of images to a product\\u2019s gallery.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-360-image\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"24eb2cfa3738a66bf3b2587876668cd2\",\"slug\":\"woocommerce-360-image\",\"id\":512186},{\"title\":\"WooCommerce Photography\",\"image\":\"\",\"excerpt\":\"Sell photos in the blink of an eye using this simple as dragging &amp; dropping interface.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-photography\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"ee76e8b9daf1d97ca4d3874cc9e35687\",\"slug\":\"woocommerce-photography\",\"id\":583602},{\"title\":\"WooCommerce Bookings Availability\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Bookings-Aval-Dark.png\",\"excerpt\":\"Sell more bookings by presenting a calendar or schedule of available slots in a page or post.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/bookings-availability\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"30770d2a-e392-4e82-baaa-76cfc7d02ae3\",\"slug\":\"woocommerce-bookings-availability\",\"id\":4228225},{\"title\":\"Software Add-on\",\"image\":\"\",\"excerpt\":\"Sell License Keys for Software\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/software-add-on\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"79f6dbfe1f1d3a56a86f0509b6d6b04b\",\"slug\":\"woocommerce-software-add-on\",\"id\":18683},{\"title\":\"WooCommerce Products Compare\",\"image\":\"\",\"excerpt\":\"WooCommerce Products Compare will allow your potential customers to easily compare products within your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-products-compare\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"c3ba0a4a3199a0cc7a6112eb24414548\",\"slug\":\"woocommerce-products-compare\",\"id\":853117},{\"title\":\"WooCommerce Store Catalog PDF Download\",\"image\":\"\",\"excerpt\":\"Offer your customers a PDF download of your product catalog, generated by WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-store-catalog-pdf-download\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"79ca7aadafe706364e2d738b7c1090c4\",\"slug\":\"woocommerce-store-catalog-pdf-download\",\"id\":675790},{\"title\":\"eWAY\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2011\\/10\\/eway-logo-3000-2000.jpg\",\"excerpt\":\"Take credit card payments securely via eWay (SG, MY, HK, AU, and NZ) keeping customers on your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/eway\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"2c497769d98d025e0d340cd0b5ea5da1\",\"slug\":\"woocommerce-gateway-eway\",\"id\":18604},{\"title\":\"WooCommerce Paid Courses\",\"image\":\"\",\"excerpt\":\"Sell your online courses using the most popular eCommerce platform on the web \\u2013 WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-paid-courses\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"bad2a02a063555b7e2bee59924690763\",\"slug\":\"woothemes-sensei\",\"id\":152116},{\"title\":\"PayPal\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2020\\/10\\/PPCP-Tile-PayPal-Logo-and-Cart-Art-2x-2-uozwz8.jpg\",\"excerpt\":\"PayPal\\u2019s latest, most complete payment processing solution. Accept PayPal exclusives, credit\\/debit cards and local payment methods. Turn on only PayPal options or process a full suite of payment methods. Enable global transactions with extensive currency and country coverage. Built and supported by WooCommerce and PayPal.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-paypal-payments\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"934115ab-e3f3-4435-9580-345b1ce21899\",\"slug\":\"woocommerce-paypal-payments\",\"id\":6410731},{\"title\":\"Catalog Visibility Options\",\"image\":\"\",\"excerpt\":\"Transform WooCommerce into an online catalog by removing eCommerce functionality\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/catalog-visibility-options\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"12e791110365fdbb5865c8658907967e\",\"slug\":\"woocommerce-catalog-visibility-options\",\"id\":18648},{\"title\":\"QuickBooks Sync for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2019\\/04\\/woocommerce-com-logo-1-hyhzbh.png\",\"excerpt\":\"Automatic two-way sync for orders, customers, products, inventory and more between WooCommerce and QuickBooks (Online, Desktop, or POS).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/quickbooks-sync-for-woocommerce\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"c5e32e20-7c1f-4585-8b15-d930c2d842ac\",\"slug\":\"myworks-woo-sync-for-quickbooks-online\",\"id\":4065824},{\"title\":\"WooCommerce Blocks\",\"image\":\"\",\"excerpt\":\"WooCommerce Blocks offers a range of Gutenberg blocks you can use to build and customise your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gutenberg-products-block\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"c2e9f13a-f90c-4ffe-a8a5-b432399ec263\",\"slug\":\"woo-gutenberg-products-block\",\"id\":3076677},{\"title\":\"Conditional Shipping and Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-CSP.png?v=1\",\"excerpt\":\"Use conditional logic to restrict the shipping and payment options available on your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/conditional-shipping-and-payments\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1f56ff002fa830b77017b0107505211a\",\"slug\":\"woocommerce-conditional-shipping-and-payments\",\"id\":680253},{\"title\":\"Sequential Order Numbers Pro\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/05\\/Thumbnail-Sequential-Order-Numbers-Pro-updated.png\",\"excerpt\":\"Tame your order numbers! Advanced &amp; sequential order numbers with optional prefixes \\/ suffixes\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/sequential-order-numbers-pro\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"0b18a2816e016ba9988b93b1cd8fe766\",\"slug\":\"woocommerce-sequential-order-numbers-pro\",\"id\":18688},{\"title\":\"WooCommerce Order Status Manager\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2015\\/02\\/Thumbnail-Order-Status-Manager-updated.png\",\"excerpt\":\"Create, edit, and delete completely custom order statuses and integrate them seamlessly into your order management flow.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-status-manager\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"51fd9ab45394b4cad5a0ebf58d012342\",\"slug\":\"woocommerce-order-status-manager\",\"id\":588398},{\"title\":\"WooCommerce Checkout Add-Ons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/07\\/Thumbnail-Checkout-Add-Ons-updated.png\",\"excerpt\":\"Highlight relevant products, offers like free shipping and other up-sells during checkout.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-checkout-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8fdca00b4000b7a8cc26371d0e470a8f\",\"slug\":\"woocommerce-checkout-add-ons\",\"id\":466854},{\"title\":\"WooCommerce Google Analytics Pro\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/01\\/Thumbnail-GAPro-updated.png\",\"excerpt\":\"Add advanced event tracking and enhanced eCommerce tracking to your WooCommerce site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-google-analytics-pro\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"d8aed8b7306b509eec1589e59abe319f\",\"slug\":\"woocommerce-google-analytics-pro\",\"id\":1312497},{\"title\":\"Coupon Shortcodes\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/09\\/woocommerce-coupon-shortcodes-product-image-1870x960-1-vc5gux.png\",\"excerpt\":\"Show coupon discount info using shortcodes. Allows to render coupon information and content conditionally, based on the validity of coupons.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/coupon-shortcodes\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"ac5d9d51-70b2-4d8f-8b89-24200eea1394\",\"slug\":\"woocommerce-coupon-shortcodes\",\"id\":244762},{\"title\":\"WooCommerce One Page Checkout\",\"image\":\"\",\"excerpt\":\"Create special pages where customers can choose products, checkout &amp; pay all on the one page.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-one-page-checkout\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"c9ba8f8352cd71b5508af5161268619a\",\"slug\":\"woocommerce-one-page-checkout\",\"id\":527886},{\"title\":\"Jilt\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2017\\/12\\/Thumbnail-Jilt-updated.png\",\"excerpt\":\"All-in-one email marketing platform built for WooCommerce stores. Send newsletters, abandoned cart reminders, win-backs, welcome automations, and more.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/jilt\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"b53aafb64dca33835e41ee06de7e9816\",\"slug\":\"jilt-for-woocommerce\",\"id\":2754876},{\"title\":\"WooCommerce Product Search\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2014\\/10\\/woocommerce-product-search-product-image-1870x960-1-jvsljj.png\",\"excerpt\":\"The perfect search engine helps customers to find and buy products quickly \\u2013 essential for every WooCommerce store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-product-search\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/demo.itthinx.com\\/wps\\/\",\"price\":\"&#36;49.00\",\"hash\":\"c84cc8ca16ddac3408e6b6c5871133a8\",\"slug\":\"woocommerce-product-search\",\"id\":512174},{\"title\":\"First Data\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/02\\/Thumbnail-FirstData-updated.png\",\"excerpt\":\"FirstData gateway for WooCommerce\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/firstdata\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"eb3e32663ec0810592eaf0d097796230\",\"slug\":\"woocommerce-gateway-firstdata\",\"id\":18645},{\"title\":\"WooSlider\",\"image\":\"\",\"excerpt\":\"WooSlider is the ultimate responsive slideshow WordPress slider plugin\\r\\n\\r\\n\\u00a0\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/wooslider\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/www.wooslider.com\\/\",\"price\":\"&#36;49.00\",\"hash\":\"209d98f3ccde6cc3de7e8732a2b20b6a\",\"slug\":\"wooslider\",\"id\":46506},{\"title\":\"WooCommerce Social Login\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/08\\/Thumbnail-Social-Login-updated.png\",\"excerpt\":\"Enable Social Login for seamless checkout and account creation.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-social-login\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/demos.skyverge.com\\/woocommerce-social-login\\/\",\"price\":\"&#36;79.00\",\"hash\":\"b231cd6367a79cc8a53b7d992d77525d\",\"slug\":\"woocommerce-social-login\",\"id\":473617},{\"title\":\"WooCommerce Order Status Control\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/06\\/Thumbnail-Order-Status-Control-updated.png\",\"excerpt\":\"Use this extension to automatically change the order status to \\\"completed\\\" after successful payment.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-status-control\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"32400e509c7c36dcc1cd368e8267d981\",\"slug\":\"woocommerce-order-status-control\",\"id\":439037},{\"title\":\"Variation Swatches and Photos\",\"image\":\"\",\"excerpt\":\"Show color and image swatches instead of dropdowns for variable products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/variation-swatches-and-photos\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.elementstark.com\\/woocommerce-extension-demos\\/product-category\\/swatches-and-photos\\/\",\"price\":\"&#36;99.00\",\"hash\":\"37bea8d549df279c8278878d081b062f\",\"slug\":\"woocommerce-variation-swatches-and-photos\",\"id\":18697},{\"title\":\"Opayo Payment Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2011\\/10\\/Opayo_logo_RGB.png\",\"excerpt\":\"Take payments on your WooCommerce store via Opayo (formally SagePay).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/sage-pay-form\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"6bc0cca47d0274d8ef9b164f6fbec1cc\",\"slug\":\"woocommerce-gateway-sagepay-form\",\"id\":18599},{\"title\":\"EU VAT Number\",\"image\":\"\",\"excerpt\":\"Collect VAT numbers at checkout and remove the VAT charge for eligible EU businesses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/eu-vat-number\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"d2720c4b4bb8d6908e530355b7a2d734\",\"slug\":\"woocommerce-eu-vat-number\",\"id\":18592},{\"title\":\"HubSpot for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2020\\/04\\/hubspotlogo-web-color-pxebeq.png\",\"excerpt\":\"Integrate HubSpot with WooCommerce to connect your store with HubSpot\'s CRM, abandoned cart tracking, marketing automation &amp; more.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/hubspot-for-woocommerce\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e50acec8-3a6c-454c-8562-2da4898fa6c1\",\"slug\":\"hubspot-for-woocommerce\",\"id\":5785079},{\"title\":\"Google Listings &amp; Ads\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2021\\/06\\/marketplace-card.png\",\"excerpt\":\"Reach millions of engaged shoppers across Google with free product listings and ads. Built in partnership with Google.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-listings-and-ads\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"118f4d86-f126-4c3a-8525-644e3170d161\",\"slug\":\"google-listings-and-ads\",\"id\":7623964},{\"title\":\"Customer\\/Order\\/Coupon CSV Import Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/07\\/Thumbnail-Customer-Order-Coupon-CSV-Import-Suite-updated.png\",\"excerpt\":\"Import both customers and orders into WooCommerce from a CSV file.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/customerorder-csv-import-suite\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"eb00ca8317a0f64dbe185c995e5ea3df\",\"slug\":\"woocommerce-customer-order-csv-import\",\"id\":18709}]}\";s:7:\"headers\";O:25:\"Requests_Response_Headers\":1:{s:7:\"\0*\0data\";a:18:{s:6:\"server\";a:1:{i:0;s:5:\"nginx\";}s:4:\"date\";a:1:{i:0;s:29:\"Mon, 12 Jul 2021 20:51:28 GMT\";}s:12:\"content-type\";a:1:{i:0;s:31:\"application/json; charset=UTF-8\";}s:14:\"content-length\";a:1:{i:0;s:5:\"11651\";}s:12:\"x-robots-tag\";a:1:{i:0;s:7:\"noindex\";}s:4:\"link\";a:1:{i:0;s:60:\"<https://woocommerce.com/wp-json/>; rel=\"https://api.w.org/\"\";}s:22:\"x-content-type-options\";a:1:{i:0;s:7:\"nosniff\";}s:29:\"access-control-expose-headers\";a:1:{i:0;s:33:\"X-WP-Total, X-WP-TotalPages, Link\";}s:28:\"access-control-allow-headers\";a:1:{i:0;s:73:\"Authorization, X-WP-Nonce, Content-Disposition, Content-MD5, Content-Type\";}s:13:\"x-wccom-cache\";a:1:{i:0;s:3:\"HIT\";}s:13:\"cache-control\";a:1:{i:0;s:10:\"max-age=60\";}s:5:\"allow\";a:1:{i:0;s:3:\"GET\";}s:16:\"content-encoding\";a:1:{i:0;s:4:\"gzip\";}s:4:\"x-rq\";a:1:{i:0;s:13:\"vie2 0 4 9980\";}s:3:\"age\";a:1:{i:0;s:2:\"10\";}s:7:\"x-cache\";a:1:{i:0;s:3:\"hit\";}s:4:\"vary\";a:1:{i:0;s:23:\"Accept-Encoding, Origin\";}s:13:\"accept-ranges\";a:1:{i:0;s:5:\"bytes\";}}}s:11:\"status_code\";i:200;s:16:\"protocol_version\";d:1.1;s:7:\"success\";b:1;s:9:\"redirects\";i:0;s:3:\"url\";s:59:\"https://woocommerce.com/wp-json/wccom-extensions/1.0/search\";s:7:\"history\";a:0:{}s:7:\"cookies\";O:19:\"Requests_Cookie_Jar\":1:{s:10:\"\0*\0cookies\";a:0:{}}}s:11:\"\0*\0filename\";N;s:4:\"data\";N;s:7:\"headers\";N;s:6:\"status\";N;}}', 'no');
INSERT INTO `fmn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1464, '_transient_timeout_wpseo_total_unindexed_posts', '1626241668', 'no'),
(1465, '_transient_wpseo_total_unindexed_posts', '26', 'no'),
(1466, '_transient_timeout_wpseo_total_unindexed_terms', '1626241668', 'no'),
(1467, '_transient_wpseo_total_unindexed_terms', '4', 'no'),
(1468, '_transient_timeout_wpseo_total_unindexed_post_type_archives', '1626241668', 'no'),
(1469, '_transient_wpseo_total_unindexed_post_type_archives', '0', 'no'),
(1470, '_transient_timeout_wpseo_unindexed_post_link_count', '1626241668', 'no'),
(1471, '_transient_wpseo_unindexed_post_link_count', '18', 'no'),
(1472, '_transient_timeout_wpseo_unindexed_term_link_count', '1626241668', 'no'),
(1473, '_transient_wpseo_unindexed_term_link_count', '5', 'no'),
(1477, '_site_transient_timeout_theme_roots', '1626157069', 'no'),
(1478, '_site_transient_theme_roots', 'a:2:{s:6:\"gordon\";s:7:\"/themes\";s:15:\"twentytwentyone\";s:7:\"/themes\";}', 'no'),
(1479, '_transient_timeout__woocommerce_helper_updates', '1626198469', 'no'),
(1480, '_transient__woocommerce_helper_updates', 'a:4:{s:4:\"hash\";s:32:\"d751713988987e9331980363e24189ce\";s:7:\"updated\";i:1626155269;s:8:\"products\";a:0:{}s:6:\"errors\";a:1:{i:0;s:10:\"http-error\";}}', 'no'),
(1481, 'rewrite_rules', 'a:189:{s:24:\"^wc-auth/v([1]{1})/(.*)?\";s:63:\"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]\";s:22:\"^wc-api/v([1-3]{1})/?$\";s:51:\"index.php?wc-api-version=$matches[1]&wc-api-route=/\";s:24:\"^wc-api/v([1-3]{1})(.*)?\";s:61:\"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]\";s:47:\"(([^/]+/)*wishlist)(/(.*))?/page/([0-9]{1,})/?$\";s:76:\"index.php?pagename=$matches[1]&wishlist-action=$matches[4]&paged=$matches[5]\";s:30:\"(([^/]+/)*wishlist)(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&wishlist-action=$matches[4]\";s:19:\"sitemap_index\\.xml$\";s:19:\"index.php?sitemap=1\";s:31:\"([^/]+?)-sitemap([0-9]+)?\\.xml$\";s:51:\"index.php?sitemap=$matches[1]&sitemap_n=$matches[2]\";s:24:\"([a-z]+)?-?sitemap\\.xsl$\";s:39:\"index.php?yoast-sitemap-xsl=$matches[1]\";s:7:\"shop/?$\";s:27:\"index.php?post_type=product\";s:37:\"shop/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:32:\"shop/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:24:\"shop/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=product&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:32:\"category/(.+?)/wc-api(/(.*))?/?$\";s:54:\"index.php?category_name=$matches[1]&wc-api=$matches[3]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:29:\"tag/([^/]+)/wc-api(/(.*))?/?$\";s:44:\"index.php?tag=$matches[1]&wc-api=$matches[3]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:46:\"brand/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?brand=$matches[1]&feed=$matches[2]\";s:41:\"brand/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?brand=$matches[1]&feed=$matches[2]\";s:22:\"brand/([^/]+)/embed/?$\";s:38:\"index.php?brand=$matches[1]&embed=true\";s:34:\"brand/([^/]+)/page/?([0-9]{1,})/?$\";s:45:\"index.php?brand=$matches[1]&paged=$matches[2]\";s:16:\"brand/([^/]+)/?$\";s:27:\"index.php?brand=$matches[1]\";s:49:\"prod_age/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?prod_age=$matches[1]&feed=$matches[2]\";s:44:\"prod_age/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?prod_age=$matches[1]&feed=$matches[2]\";s:25:\"prod_age/([^/]+)/embed/?$\";s:41:\"index.php?prod_age=$matches[1]&embed=true\";s:37:\"prod_age/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?prod_age=$matches[1]&paged=$matches[2]\";s:19:\"prod_age/([^/]+)/?$\";s:30:\"index.php?prod_age=$matches[1]\";s:50:\"prod_tech/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?prod_tech=$matches[1]&feed=$matches[2]\";s:45:\"prod_tech/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?prod_tech=$matches[1]&feed=$matches[2]\";s:26:\"prod_tech/([^/]+)/embed/?$\";s:42:\"index.php?prod_tech=$matches[1]&embed=true\";s:38:\"prod_tech/([^/]+)/page/?([0-9]{1,})/?$\";s:49:\"index.php?prod_tech=$matches[1]&paged=$matches[2]\";s:20:\"prod_tech/([^/]+)/?$\";s:31:\"index.php?prod_tech=$matches[1]\";s:50:\"prod_area/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?prod_area=$matches[1]&feed=$matches[2]\";s:45:\"prod_area/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?prod_area=$matches[1]&feed=$matches[2]\";s:26:\"prod_area/([^/]+)/embed/?$\";s:42:\"index.php?prod_area=$matches[1]&embed=true\";s:38:\"prod_area/([^/]+)/page/?([0-9]{1,})/?$\";s:49:\"index.php?prod_area=$matches[1]&paged=$matches[2]\";s:20:\"prod_area/([^/]+)/?$\";s:31:\"index.php?prod_area=$matches[1]\";s:55:\"product-category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:50:\"product-category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:31:\"product-category/(.+?)/embed/?$\";s:44:\"index.php?product_cat=$matches[1]&embed=true\";s:43:\"product-category/(.+?)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:25:\"product-category/(.+?)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:52:\"product-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:47:\"product-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:28:\"product-tag/([^/]+)/embed/?$\";s:44:\"index.php?product_tag=$matches[1]&embed=true\";s:40:\"product-tag/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_tag=$matches[1]&paged=$matches[2]\";s:22:\"product-tag/([^/]+)/?$\";s:33:\"index.php?product_tag=$matches[1]\";s:35:\"product/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"product/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"product/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"product/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:24:\"product/([^/]+)/embed/?$\";s:40:\"index.php?product=$matches[1]&embed=true\";s:28:\"product/([^/]+)/trackback/?$\";s:34:\"index.php?product=$matches[1]&tb=1\";s:48:\"product/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:43:\"product/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:36:\"product/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&paged=$matches[2]\";s:43:\"product/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&cpage=$matches[2]\";s:33:\"product/([^/]+)/wc-api(/(.*))?/?$\";s:48:\"index.php?product=$matches[1]&wc-api=$matches[3]\";s:39:\"product/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:50:\"product/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:32:\"product/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?product=$matches[1]&page=$matches[2]\";s:24:\"product/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"product/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:54:\"product/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:30:\"product/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=67&cpage=$matches[1]\";s:17:\"wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:26:\"comments/wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:29:\"search/(.+)/wc-api(/(.*))?/?$\";s:42:\"index.php?s=$matches[1]&wc-api=$matches[3]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:32:\"author/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?author_name=$matches[1]&wc-api=$matches[3]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:54:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:82:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:41:\"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:66:\"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:28:\"([0-9]{4})/wc-api(/(.*))?/?$\";s:45:\"index.php?year=$matches[1]&wc-api=$matches[3]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:62:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/wc-api(/(.*))?/?$\";s:99:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&wc-api=$matches[6]\";s:62:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:73:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:25:\"(.?.+?)/wc-api(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&wc-api=$matches[3]\";s:28:\"(.?.+?)/order-pay(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&order-pay=$matches[3]\";s:33:\"(.?.+?)/order-received(/(.*))?/?$\";s:57:\"index.php?pagename=$matches[1]&order-received=$matches[3]\";s:25:\"(.?.+?)/orders(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&orders=$matches[3]\";s:29:\"(.?.+?)/view-order(/(.*))?/?$\";s:53:\"index.php?pagename=$matches[1]&view-order=$matches[3]\";s:28:\"(.?.+?)/downloads(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&downloads=$matches[3]\";s:31:\"(.?.+?)/edit-account(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-account=$matches[3]\";s:31:\"(.?.+?)/edit-address(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-address=$matches[3]\";s:34:\"(.?.+?)/payment-methods(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&payment-methods=$matches[3]\";s:32:\"(.?.+?)/lost-password(/(.*))?/?$\";s:56:\"index.php?pagename=$matches[1]&lost-password=$matches[3]\";s:34:\"(.?.+?)/customer-logout(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&customer-logout=$matches[3]\";s:37:\"(.?.+?)/add-payment-method(/(.*))?/?$\";s:61:\"index.php?pagename=$matches[1]&add-payment-method=$matches[3]\";s:40:\"(.?.+?)/delete-payment-method(/(.*))?/?$\";s:64:\"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]\";s:45:\"(.?.+?)/set-default-payment-method(/(.*))?/?$\";s:69:\"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]\";s:31:\".?.+?/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(1482, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/he_IL/wordpress-5.7.2.zip\";s:6:\"locale\";s:5:\"he_IL\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/he_IL/wordpress-5.7.2.zip\";s:10:\"no_content\";s:0:\"\";s:11:\"new_bundled\";s:0:\"\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.7.2\";s:7:\"version\";s:5:\"5.7.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1626155272;s:15:\"version_checked\";s:5:\"5.7.2\";s:12:\"translations\";a:0:{}}', 'no'),
(1483, '_site_transient_update_themes', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1626155272;s:7:\"checked\";a:2:{s:6:\"gordon\";s:3:\"1.0\";s:15:\"twentytwentyone\";s:3:\"1.3\";}s:8:\"response\";a:0:{}s:9:\"no_update\";a:1:{s:15:\"twentytwentyone\";a:6:{s:5:\"theme\";s:15:\"twentytwentyone\";s:11:\"new_version\";s:3:\"1.3\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentytwentyone/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentytwentyone.1.3.zip\";s:8:\"requires\";s:3:\"5.3\";s:12:\"requires_php\";s:3:\"5.6\";}}s:12:\"translations\";a:0:{}}', 'no'),
(1484, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1626155273;s:7:\"checked\";a:15:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.9.8\";s:33:\"classic-editor/classic-editor.php\";s:3:\"1.6\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.4.1\";s:42:\"contact-form-cfdb7/contact-form-cfdb-7.php\";s:7:\"1.2.5.9\";s:23:\"debug-bar/debug-bar.php\";s:5:\"1.1.2\";s:24:\"fancy-gallery/plugin.php\";s:6:\"1.6.56\";s:33:\"jquery-updater/jquery-updater.php\";s:7:\"3.6.0.1\";s:29:\"versionpress/versionpress.php\";s:9:\"4.0-beta2\";s:17:\"wesafe/wesafe.php\";s:5:\"1.7.2\";s:27:\"woocommerce/woocommerce.php\";s:5:\"5.4.1\";s:25:\"dummy-data/dummy-data.php\";s:3:\"1.0\";s:29:\"wp-sync-db-1.5/wp-sync-db.php\";s:3:\"1.5\";s:34:\"yith-woocommerce-wishlist/init.php\";s:6:\"3.0.23\";s:24:\"wordpress-seo/wp-seo.php\";s:6:\"16.6.1\";s:17:\"leosem/leosem.php\";s:3:\"2.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:3:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"contact-form-7\";s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:5:\"5.4.1\";s:7:\"updated\";s:19:\"2021-02-05 18:15:06\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/translation/plugin/contact-form-7/5.4.1/he_IL.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:11:\"woocommerce\";s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:5:\"5.4.1\";s:7:\"updated\";s:19:\"2021-06-10 19:42:09\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/translation/plugin/woocommerce/5.4.1/he_IL.zip\";s:10:\"autoupdate\";b:1;}i:2;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:13:\"wordpress-seo\";s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:6:\"16.6.1\";s:7:\"updated\";s:19:\"2021-03-22 15:34:46\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/translation/plugin/wordpress-seo/16.6.1/he_IL.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:9:{s:33:\"classic-editor/classic-editor.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/classic-editor\";s:4:\"slug\";s:14:\"classic-editor\";s:6:\"plugin\";s:33:\"classic-editor/classic-editor.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/classic-editor/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/classic-editor.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-256x256.png?rev=1998671\";s:2:\"1x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-128x128.png?rev=1998671\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/classic-editor/assets/banner-1544x500.png?rev=1998671\";s:2:\"1x\";s:69:\"https://ps.w.org/classic-editor/assets/banner-772x250.png?rev=1998676\";}s:11:\"banners_rtl\";a:0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.4.1\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.4.1.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=2279696\";s:2:\"1x\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";s:3:\"svg\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}}s:42:\"contact-form-cfdb7/contact-form-cfdb-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/contact-form-cfdb7\";s:4:\"slug\";s:18:\"contact-form-cfdb7\";s:6:\"plugin\";s:42:\"contact-form-cfdb7/contact-form-cfdb-7.php\";s:11:\"new_version\";s:7:\"1.2.5.9\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/contact-form-cfdb7/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/contact-form-cfdb7.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/contact-form-cfdb7/assets/icon-256x256.png?rev=1619878\";s:2:\"1x\";s:71:\"https://ps.w.org/contact-form-cfdb7/assets/icon-128x128.png?rev=1619878\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:73:\"https://ps.w.org/contact-form-cfdb7/assets/banner-772x250.png?rev=1619902\";}s:11:\"banners_rtl\";a:0:{}}s:23:\"debug-bar/debug-bar.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:23:\"w.org/plugins/debug-bar\";s:4:\"slug\";s:9:\"debug-bar\";s:6:\"plugin\";s:23:\"debug-bar/debug-bar.php\";s:11:\"new_version\";s:5:\"1.1.2\";s:3:\"url\";s:40:\"https://wordpress.org/plugins/debug-bar/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/debug-bar.1.1.2.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:62:\"https://ps.w.org/debug-bar/assets/icon-256x256.png?rev=1908362\";s:2:\"1x\";s:54:\"https://ps.w.org/debug-bar/assets/icon.svg?rev=1908362\";s:3:\"svg\";s:54:\"https://ps.w.org/debug-bar/assets/icon.svg?rev=1908362\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/debug-bar/assets/banner-1544x500.png?rev=1365496\";s:2:\"1x\";s:64:\"https://ps.w.org/debug-bar/assets/banner-772x250.png?rev=1365496\";}s:11:\"banners_rtl\";a:0:{}}s:24:\"fancy-gallery/plugin.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/fancy-gallery\";s:4:\"slug\";s:13:\"fancy-gallery\";s:6:\"plugin\";s:24:\"fancy-gallery/plugin.php\";s:11:\"new_version\";s:6:\"1.6.56\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/fancy-gallery/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/fancy-gallery.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/fancy-gallery/assets/icon-128x128.png?rev=1723946\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:68:\"https://ps.w.org/fancy-gallery/assets/banner-772x250.png?rev=1723946\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"jquery-updater/jquery-updater.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/jquery-updater\";s:4:\"slug\";s:14:\"jquery-updater\";s:6:\"plugin\";s:33:\"jquery-updater/jquery-updater.php\";s:11:\"new_version\";s:7:\"3.6.0.1\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/jquery-updater/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/jquery-updater.3.6.0.1.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:58:\"https://s.w.org/plugins/geopattern-icon/jquery-updater.svg\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:27:\"woocommerce/woocommerce.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:6:\"plugin\";s:27:\"woocommerce/woocommerce.php\";s:11:\"new_version\";s:5:\"5.4.1\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woocommerce/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce.5.4.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=2366418\";s:2:\"1x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=2366418\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=2366418\";s:2:\"1x\";s:66:\"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=2366418\";}s:11:\"banners_rtl\";a:0:{}}s:34:\"yith-woocommerce-wishlist/init.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:39:\"w.org/plugins/yith-woocommerce-wishlist\";s:4:\"slug\";s:25:\"yith-woocommerce-wishlist\";s:6:\"plugin\";s:34:\"yith-woocommerce-wishlist/init.php\";s:11:\"new_version\";s:6:\"3.0.23\";s:3:\"url\";s:56:\"https://wordpress.org/plugins/yith-woocommerce-wishlist/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/yith-woocommerce-wishlist.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:78:\"https://ps.w.org/yith-woocommerce-wishlist/assets/icon-128x128.jpg?rev=2215573\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:81:\"https://ps.w.org/yith-woocommerce-wishlist/assets/banner-1544x500.jpg?rev=2209192\";s:2:\"1x\";s:80:\"https://ps.w.org/yith-woocommerce-wishlist/assets/banner-772x250.jpg?rev=2209192\";}s:11:\"banners_rtl\";a:0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:6:\"16.6.1\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/wordpress-seo.16.6.1.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=2363699\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}}}}', 'no'),
(1586, 'options_shop_link', 'a:3:{s:5:\"title\";s:19:\"חזרה לחנות\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}', 'no'),
(1587, '_options_shop_link', 'field_60ed4859102f2', 'no'),
(1588, 'options_shop_img', '250', 'no'),
(1589, '_options_shop_img', 'field_60ed486f102f3', 'no'),
(1661, 'prod_area_children', 'a:0:{}', 'yes'),
(1665, 'prod_tech_children', 'a:0:{}', 'yes'),
(1669, 'prod_age_children', 'a:0:{}', 'yes'),
(1674, '_transient_timeout_wc_term_counts', '1628763039', 'no'),
(1675, '_transient_wc_term_counts', 'a:2:{i:15;s:1:\"5\";i:24;s:1:\"2\";}', 'no'),
(1752, 'wpseo_taxonomy_meta', 'a:1:{s:8:\"category\";a:1:{i:1;a:3:{s:13:\"wpseo_focuskw\";s:8:\"כללי\";s:13:\"wpseo_linkdex\";s:2:\"28\";s:19:\"wpseo_content_score\";s:1:\"0\";}}}', 'yes'),
(1753, 'category_children', 'a:0:{}', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_postmeta`
--

CREATE TABLE `fmn_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_postmeta`
--

INSERT INTO `fmn_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(9, 6, '_wp_attached_file', 'woocommerce-placeholder.png'),
(10, 6, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:1200;s:4:\"file\";s:27:\"woocommerce-placeholder.png\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:37:\"woocommerce-placeholder-1024x1024.png\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-768x768.png\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:33:\"woocommerce-placeholder-94x94.png\";s:5:\"width\";i:94;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(11, 11, '_form', '<div class=\"form-row d-flex justify-content-end align-items-stretch\">\n  <div class=\"col-xl-3 col-md-6 col-12\">\n		[text* text-779 placeholder \"שם:\"]\n  </div>\n    <div class=\"col-xl-3 col-md-6 col-12\">\n			[tel* tel-905 placeholder \"טלפון:\"]\n  </div>\n    <div class=\"col-xl-6 col-12\">\n			[submit \"כן אני צריך למידה חדשה ועדכנית\"]\n	</div>\n</div>'),
(12, 11, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:32:\"[_site_title] <wordpress@gordon>\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:4:\"body\";s:190:\"מאת: [your-name] <[your-email]>\nנושא: [your-subject]\n\nתוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(13, 11, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:32:\"[_site_title] <wordpress@gordon>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:129:\"תוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(14, 11, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:24:\"ההודעה נשלחה.\";s:12:\"mail_sent_ng\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:16:\"validation_error\";s:89:\"קיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\";s:4:\"spam\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:12:\"accept_terms\";s:68:\"עליך להסכים לתנאים לפני שליחת ההודעה.\";s:16:\"invalid_required\";s:23:\"זהו שדה חובה.\";s:16:\"invalid_too_long\";s:25:\"השדה ארוך מדי.\";s:17:\"invalid_too_short\";s:23:\"השדה קצר מדי.\";s:13:\"upload_failed\";s:51:\"שגיאה לא ידועה בהעלאת הקובץ.\";s:24:\"upload_file_type_invalid\";s:67:\"אין לך הרשאות להעלות קבצים בפורמט זה.\";s:21:\"upload_file_too_large\";s:27:\"הקובץ גדול מדי.\";s:23:\"upload_failed_php_error\";s:35:\"שגיאה בהעלאת הקובץ.\";s:12:\"invalid_date\";s:38:\"שדה התאריך אינו נכון.\";s:14:\"date_too_early\";s:50:\"התאריך מוקדם מהתאריך המותר.\";s:13:\"date_too_late\";s:50:\"התאריך מאוחר מהתאריך המותר.\";s:14:\"invalid_number\";s:40:\"פורמט המספר אינו תקין.\";s:16:\"number_too_small\";s:48:\"המספר קטן מהמינימום המותר.\";s:16:\"number_too_large\";s:50:\"המספר גדול מהמקסימום המותר.\";s:23:\"quiz_answer_not_correct\";s:59:\"התשובה לשאלת הביטחון אינה נכונה.\";s:13:\"invalid_email\";s:59:\"כתובת האימייל שהוזנה אינה תקינה.\";s:11:\"invalid_url\";s:31:\"הקישור אינו תקין.\";s:11:\"invalid_tel\";s:40:\"מספר הטלפון אינו תקין.\";}'),
(15, 11, '_additional_settings', ''),
(16, 11, '_locale', 'he_IL'),
(18, 12, '_form', '<div class=\"form-row d-flex justify-content-end align-items-stretch\">\n  <div class=\"col-xl-4 col-md-6 col-12\">\n		[text* text-383 placeholder \"שם:\"]\n  </div>\n    <div class=\"col-xl-4 col-md-6 col-12\">\n			[tel* tel-459 placeholder \"טלפון:\"]\n  </div>\n	    <div class=\"col-xl-4 col-md-6 col-12\">\n				[email* email-209 placeholder \"מייל:\"]\n  </div>\n	 <div class=\"col-xl-4 col-none\">\n  </div>\n		   <div class=\"col-xl-4 col-md-6 col-12\">\n				[select menu-576 \"סיבת הפניה\"]\n	</div>\n	 <div class=\"col-xl-4 col-12\">\n		 [file file-42 id:file class:custom-file]\n	</div>\n	    <div class=\"col-xl-8 col-12\">\n				[textarea textarea-674 placeholder \"במידה ותרצה להרחיב אז אפשר פשוט כאן:\"]\n	</div>\n    <div class=\"col-xl-8 col-12\">\n			[submit \"כן, חזרו אלי לגביי הפניה שלי\"]\n	</div>\n</div>'),
(19, 12, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:32:\"[_site_title] <wordpress@gordon>\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:4:\"body\";s:190:\"מאת: [your-name] <[your-email]>\nנושא: [your-subject]\n\nתוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(20, 12, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:32:\"[_site_title] <wordpress@gordon>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:129:\"תוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(21, 12, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:24:\"ההודעה נשלחה.\";s:12:\"mail_sent_ng\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:16:\"validation_error\";s:89:\"קיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\";s:4:\"spam\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:12:\"accept_terms\";s:68:\"עליך להסכים לתנאים לפני שליחת ההודעה.\";s:16:\"invalid_required\";s:23:\"זהו שדה חובה.\";s:16:\"invalid_too_long\";s:25:\"השדה ארוך מדי.\";s:17:\"invalid_too_short\";s:23:\"השדה קצר מדי.\";s:13:\"upload_failed\";s:51:\"שגיאה לא ידועה בהעלאת הקובץ.\";s:24:\"upload_file_type_invalid\";s:67:\"אין לך הרשאות להעלות קבצים בפורמט זה.\";s:21:\"upload_file_too_large\";s:27:\"הקובץ גדול מדי.\";s:23:\"upload_failed_php_error\";s:35:\"שגיאה בהעלאת הקובץ.\";s:12:\"invalid_date\";s:38:\"שדה התאריך אינו נכון.\";s:14:\"date_too_early\";s:50:\"התאריך מוקדם מהתאריך המותר.\";s:13:\"date_too_late\";s:50:\"התאריך מאוחר מהתאריך המותר.\";s:14:\"invalid_number\";s:40:\"פורמט המספר אינו תקין.\";s:16:\"number_too_small\";s:48:\"המספר קטן מהמינימום המותר.\";s:16:\"number_too_large\";s:50:\"המספר גדול מהמקסימום המותר.\";s:23:\"quiz_answer_not_correct\";s:59:\"התשובה לשאלת הביטחון אינה נכונה.\";s:13:\"invalid_email\";s:59:\"כתובת האימייל שהוזנה אינה תקינה.\";s:11:\"invalid_url\";s:31:\"הקישור אינו תקין.\";s:11:\"invalid_tel\";s:40:\"מספר הטלפון אינו תקין.\";}'),
(22, 12, '_additional_settings', ''),
(23, 12, '_locale', 'he_IL'),
(26, 21, '_edit_lock', '1626177027:1'),
(27, 21, '_edit_last', '1'),
(28, 13, '_edit_lock', '1625918137:1'),
(29, 13, '_edit_last', '1'),
(30, 32, '_edit_last', '1'),
(31, 32, '_edit_lock', '1626177003:1'),
(32, 33, '_wp_attached_file', '2021/07/course-1.png'),
(33, 33, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:352;s:6:\"height\";i:212;s:4:\"file\";s:20:\"2021/07/course-1.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"course-1-300x181.png\";s:5:\"width\";i:300;s:6:\"height\";i:181;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"course-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"course-1-134x81.png\";s:5:\"width\";i:134;s:6:\"height\";i:81;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"course-1-300x212.png\";s:5:\"width\";i:300;s:6:\"height\";i:212;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"course-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"course-1-300x212.png\";s:5:\"width\";i:300;s:6:\"height\";i:212;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"course-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(34, 34, '_wp_attached_file', '2021/07/course-2.png'),
(35, 34, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:351;s:6:\"height\";i:212;s:4:\"file\";s:20:\"2021/07/course-2.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"course-2-300x181.png\";s:5:\"width\";i:300;s:6:\"height\";i:181;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"course-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"course-2-134x81.png\";s:5:\"width\";i:134;s:6:\"height\";i:81;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"course-2-300x212.png\";s:5:\"width\";i:300;s:6:\"height\";i:212;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"course-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"course-2-300x212.png\";s:5:\"width\";i:300;s:6:\"height\";i:212;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"course-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(36, 35, '_wp_attached_file', '2021/07/course-3.png'),
(37, 35, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:351;s:6:\"height\";i:211;s:4:\"file\";s:20:\"2021/07/course-3.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"course-3-300x180.png\";s:5:\"width\";i:300;s:6:\"height\";i:180;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"course-3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"course-3-134x81.png\";s:5:\"width\";i:134;s:6:\"height\";i:81;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"course-3-300x211.png\";s:5:\"width\";i:300;s:6:\"height\";i:211;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"course-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"course-3-300x211.png\";s:5:\"width\";i:300;s:6:\"height\";i:211;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"course-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(38, 36, '_wp_attached_file', '2021/07/course-4.png'),
(39, 36, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:349;s:6:\"height\";i:216;s:4:\"file\";s:20:\"2021/07/course-4.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"course-4-300x186.png\";s:5:\"width\";i:300;s:6:\"height\";i:186;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"course-4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"course-4-134x83.png\";s:5:\"width\";i:134;s:6:\"height\";i:83;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"course-4-300x216.png\";s:5:\"width\";i:300;s:6:\"height\";i:216;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"course-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"course-4-300x216.png\";s:5:\"width\";i:300;s:6:\"height\";i:216;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"course-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(40, 37, '_wp_attached_file', '2021/07/post-1.png'),
(41, 37, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:398;s:6:\"height\";i:286;s:4:\"file\";s:18:\"2021/07/post-1.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"post-1-300x216.png\";s:5:\"width\";i:300;s:6:\"height\";i:216;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"post-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:17:\"post-1-131x94.png\";s:5:\"width\";i:131;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:18:\"post-1-300x286.png\";s:5:\"width\";i:300;s:6:\"height\";i:286;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"post-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:18:\"post-1-300x286.png\";s:5:\"width\";i:300;s:6:\"height\";i:286;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"post-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(42, 38, '_wp_attached_file', '2021/07/post-2.png'),
(43, 38, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:400;s:6:\"height\";i:288;s:4:\"file\";s:18:\"2021/07/post-2.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"post-2-300x216.png\";s:5:\"width\";i:300;s:6:\"height\";i:216;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"post-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:17:\"post-2-131x94.png\";s:5:\"width\";i:131;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:18:\"post-2-300x288.png\";s:5:\"width\";i:300;s:6:\"height\";i:288;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"post-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:18:\"post-2-300x288.png\";s:5:\"width\";i:300;s:6:\"height\";i:288;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"post-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(44, 39, '_wp_attached_file', '2021/07/post-3.png'),
(45, 39, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:295;s:6:\"height\";i:288;s:4:\"file\";s:18:\"2021/07/post-3.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"post-3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:16:\"post-3-96x94.png\";s:5:\"width\";i:96;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"post-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"post-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(46, 32, '_thumbnail_id', '39'),
(48, 32, 'title_tag', ''),
(49, 32, '_title_tag', 'field_5ddbe7577e0e7'),
(50, 32, 'single_slider_seo', ''),
(51, 32, '_single_slider_seo', 'field_5ddbde5499115'),
(52, 32, 'slider_img', ''),
(53, 32, '_slider_img', 'field_60e988d9254ba'),
(54, 40, 'title_tag', ''),
(55, 40, '_title_tag', 'field_5ddbe7577e0e7'),
(56, 40, 'single_slider_seo', ''),
(57, 40, '_single_slider_seo', 'field_5ddbde5499115'),
(58, 40, 'slider_img', ''),
(59, 40, '_slider_img', 'field_60e988d9254ba'),
(60, 41, '_edit_last', '1'),
(61, 41, '_edit_lock', '1626177002:1'),
(62, 41, '_thumbnail_id', '38'),
(64, 41, 'title_tag', ''),
(65, 41, '_title_tag', 'field_5ddbe7577e0e7'),
(66, 41, 'single_slider_seo', ''),
(67, 41, '_single_slider_seo', 'field_5ddbde5499115'),
(68, 41, 'slider_img', ''),
(69, 41, '_slider_img', 'field_60e988d9254ba'),
(70, 42, 'title_tag', ''),
(71, 42, '_title_tag', 'field_5ddbe7577e0e7'),
(72, 42, 'single_slider_seo', ''),
(73, 42, '_single_slider_seo', 'field_5ddbde5499115'),
(74, 42, 'slider_img', ''),
(75, 42, '_slider_img', 'field_60e988d9254ba'),
(76, 43, '_edit_last', '1'),
(77, 43, '_edit_lock', '1626177002:1'),
(78, 43, '_thumbnail_id', '35'),
(80, 43, 'title_tag', ''),
(81, 43, '_title_tag', 'field_5ddbe7577e0e7'),
(82, 43, 'single_slider_seo', ''),
(83, 43, '_single_slider_seo', 'field_5ddbde5499115'),
(84, 43, 'slider_img', ''),
(85, 43, '_slider_img', 'field_60e988d9254ba'),
(86, 44, 'title_tag', ''),
(87, 44, '_title_tag', 'field_5ddbe7577e0e7'),
(88, 44, 'single_slider_seo', ''),
(89, 44, '_single_slider_seo', 'field_5ddbde5499115'),
(90, 44, 'slider_img', ''),
(91, 44, '_slider_img', 'field_60e988d9254ba'),
(92, 45, '_edit_last', '1'),
(93, 45, '_edit_lock', '1626177002:1'),
(94, 45, '_thumbnail_id', '34'),
(96, 45, 'title_tag', ''),
(97, 45, '_title_tag', 'field_5ddbe7577e0e7'),
(98, 45, 'single_slider_seo', ''),
(99, 45, '_single_slider_seo', 'field_5ddbde5499115'),
(100, 45, 'slider_img', ''),
(101, 45, '_slider_img', 'field_60e988d9254ba'),
(102, 46, 'title_tag', ''),
(103, 46, '_title_tag', 'field_5ddbe7577e0e7'),
(104, 46, 'single_slider_seo', ''),
(105, 46, '_single_slider_seo', 'field_5ddbde5499115'),
(106, 46, 'slider_img', ''),
(107, 46, '_slider_img', 'field_60e988d9254ba'),
(108, 47, '_edit_last', '1'),
(109, 47, '_edit_lock', '1626177001:1'),
(110, 47, '_thumbnail_id', '33'),
(112, 47, 'title_tag', ''),
(113, 47, '_title_tag', 'field_5ddbe7577e0e7'),
(114, 47, 'single_slider_seo', ''),
(115, 47, '_single_slider_seo', 'field_5ddbde5499115'),
(116, 47, 'slider_img', ''),
(117, 47, '_slider_img', 'field_60e988d9254ba'),
(118, 48, 'title_tag', ''),
(119, 48, '_title_tag', 'field_5ddbe7577e0e7'),
(120, 48, 'single_slider_seo', ''),
(121, 48, '_single_slider_seo', 'field_5ddbde5499115'),
(122, 48, 'slider_img', ''),
(123, 48, '_slider_img', 'field_60e988d9254ba'),
(124, 49, '_edit_last', '1'),
(125, 49, '_edit_lock', '1626076088:1'),
(126, 49, '_wp_page_template', 'views/contact.php'),
(127, 49, 'title_tag', ''),
(128, 49, '_title_tag', 'field_5ddbe7577e0e7'),
(129, 49, 'single_slider_seo', ''),
(130, 49, '_single_slider_seo', 'field_5ddbde5499115'),
(131, 49, 'slider_img', ''),
(132, 49, '_slider_img', 'field_60e988d9254ba'),
(133, 50, 'title_tag', ''),
(134, 50, '_title_tag', 'field_5ddbe7577e0e7'),
(135, 50, 'single_slider_seo', ''),
(136, 50, '_single_slider_seo', 'field_5ddbde5499115'),
(137, 50, 'slider_img', ''),
(138, 50, '_slider_img', 'field_60e988d9254ba'),
(139, 52, '_edit_last', '1'),
(140, 52, '_edit_lock', '1626170834:1'),
(141, 53, '_wp_attached_file', '2021/07/product-1.png'),
(142, 53, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:267;s:6:\"height\";i:181;s:4:\"file\";s:21:\"2021/07/product-1.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"product-1-134x91.png\";s:5:\"width\";i:134;s:6:\"height\";i:91;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(143, 54, '_wp_attached_file', '2021/07/product-2.png'),
(144, 54, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:267;s:6:\"height\";i:185;s:4:\"file\";s:21:\"2021/07/product-2.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"product-2-134x94.png\";s:5:\"width\";i:134;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(145, 55, '_wp_attached_file', '2021/07/product-3.png'),
(146, 55, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:205;s:6:\"height\";i:198;s:4:\"file\";s:21:\"2021/07/product-3.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-3-97x94.png\";s:5:\"width\";i:97;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(147, 56, '_wp_attached_file', '2021/07/product-4.png'),
(148, 56, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:186;s:6:\"height\";i:203;s:4:\"file\";s:21:\"2021/07/product-4.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-4-86x94.png\";s:5:\"width\";i:86;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(149, 57, '_wp_attached_file', '2021/07/product-5.png'),
(150, 57, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:267;s:6:\"height\";i:181;s:4:\"file\";s:21:\"2021/07/product-5.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-5-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"product-5-134x91.png\";s:5:\"width\";i:134;s:6:\"height\";i:91;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(151, 58, '_wp_attached_file', '2021/07/product-6.png'),
(152, 58, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:267;s:6:\"height\";i:185;s:4:\"file\";s:21:\"2021/07/product-6.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-6-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"product-6-134x94.png\";s:5:\"width\";i:134;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(153, 59, '_wp_attached_file', '2021/07/product-7.png'),
(154, 59, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:205;s:6:\"height\";i:198;s:4:\"file\";s:21:\"2021/07/product-7.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-7-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-7-97x94.png\";s:5:\"width\";i:97;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-7-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-7-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(155, 60, '_wp_attached_file', '2021/07/product-8.png'),
(156, 60, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:186;s:6:\"height\";i:203;s:4:\"file\";s:21:\"2021/07/product-8.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-8-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-8-86x94.png\";s:5:\"width\";i:86;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-8-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-8-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(157, 52, '_thumbnail_id', '145'),
(158, 52, '_regular_price', '123'),
(159, 52, '_sale_price', '111'),
(160, 52, 'total_sales', '0'),
(161, 52, '_tax_status', 'taxable'),
(162, 52, '_tax_class', ''),
(163, 52, '_manage_stock', 'no'),
(164, 52, '_backorders', 'no'),
(165, 52, '_sold_individually', 'no'),
(166, 52, '_virtual', 'no'),
(167, 52, '_downloadable', 'no'),
(168, 52, '_download_limit', '-1'),
(169, 52, '_download_expiry', '-1'),
(170, 52, '_stock', NULL),
(171, 52, '_stock_status', 'instock'),
(172, 52, '_wc_average_rating', '0'),
(173, 52, '_wc_review_count', '0'),
(174, 52, '_product_version', '5.4.1'),
(175, 52, '_price', '111'),
(176, 61, '_edit_last', '1'),
(177, 61, '_edit_lock', '1626170884:1'),
(178, 61, '_thumbnail_id', '54'),
(179, 61, 'total_sales', '0'),
(180, 61, '_tax_status', 'taxable'),
(181, 61, '_tax_class', ''),
(182, 61, '_manage_stock', 'no'),
(183, 61, '_backorders', 'no'),
(184, 61, '_sold_individually', 'no'),
(185, 61, '_virtual', 'no'),
(186, 61, '_downloadable', 'no'),
(187, 61, '_download_limit', '-1'),
(188, 61, '_download_expiry', '-1'),
(189, 61, '_stock', NULL),
(190, 61, '_stock_status', 'instock'),
(191, 61, '_wc_average_rating', '0'),
(192, 61, '_wc_review_count', '0'),
(193, 61, '_product_version', '5.4.1'),
(194, 62, '_edit_last', '1'),
(195, 62, '_edit_lock', '1626170885:1'),
(196, 62, '_thumbnail_id', '55'),
(197, 62, 'total_sales', '0'),
(198, 62, '_tax_status', 'taxable'),
(199, 62, '_tax_class', ''),
(200, 62, '_manage_stock', 'no'),
(201, 62, '_backorders', 'no'),
(202, 62, '_sold_individually', 'no'),
(203, 62, '_virtual', 'no'),
(204, 62, '_downloadable', 'no'),
(205, 62, '_download_limit', '-1'),
(206, 62, '_download_expiry', '-1'),
(207, 62, '_stock', NULL),
(208, 62, '_stock_status', 'instock'),
(209, 62, '_wc_average_rating', '0'),
(210, 62, '_wc_review_count', '0'),
(211, 62, '_product_version', '5.4.1'),
(212, 63, '_edit_last', '1'),
(213, 63, '_edit_lock', '1626170885:1'),
(214, 63, '_thumbnail_id', '56'),
(215, 63, 'total_sales', '0'),
(216, 63, '_tax_status', 'taxable'),
(217, 63, '_tax_class', ''),
(218, 63, '_manage_stock', 'no'),
(219, 63, '_backorders', 'no'),
(220, 63, '_sold_individually', 'no'),
(221, 63, '_virtual', 'no'),
(222, 63, '_downloadable', 'no'),
(223, 63, '_download_limit', '-1'),
(224, 63, '_download_expiry', '-1'),
(225, 63, '_stock', NULL),
(226, 63, '_stock_status', 'instock'),
(227, 63, '_wc_average_rating', '0'),
(228, 63, '_wc_review_count', '0'),
(229, 63, '_product_version', '5.4.1'),
(230, 64, '_edit_last', '1'),
(231, 64, '_edit_lock', '1626170886:1'),
(232, 64, '_thumbnail_id', '57'),
(233, 64, 'total_sales', '0'),
(234, 64, '_tax_status', 'taxable'),
(235, 64, '_tax_class', ''),
(236, 64, '_manage_stock', 'no'),
(237, 64, '_backorders', 'no'),
(238, 64, '_sold_individually', 'no'),
(239, 64, '_virtual', 'no'),
(240, 64, '_downloadable', 'no'),
(241, 64, '_download_limit', '-1'),
(242, 64, '_download_expiry', '-1'),
(243, 64, '_stock', NULL),
(244, 64, '_stock_status', 'instock'),
(245, 64, '_wc_average_rating', '0'),
(246, 64, '_wc_review_count', '0'),
(247, 64, '_product_version', '5.4.1'),
(248, 65, '_edit_last', '1'),
(249, 65, '_edit_lock', '1626170887:1'),
(250, 65, '_thumbnail_id', '58'),
(251, 65, 'total_sales', '0'),
(252, 65, '_tax_status', 'taxable'),
(253, 65, '_tax_class', ''),
(254, 65, '_manage_stock', 'no'),
(255, 65, '_backorders', 'no'),
(256, 65, '_sold_individually', 'no'),
(257, 65, '_virtual', 'no'),
(258, 65, '_downloadable', 'no'),
(259, 65, '_download_limit', '-1'),
(260, 65, '_download_expiry', '-1'),
(261, 65, '_stock', NULL),
(262, 65, '_stock_status', 'instock'),
(263, 65, '_wc_average_rating', '0'),
(264, 65, '_wc_review_count', '0'),
(265, 65, '_product_version', '5.4.1'),
(266, 66, '_edit_last', '1'),
(267, 66, '_edit_lock', '1626171442:1'),
(268, 66, '_thumbnail_id', '60'),
(269, 66, '_regular_price', '234'),
(270, 66, '_sale_price', '180'),
(271, 66, 'total_sales', '0'),
(272, 66, '_tax_status', 'taxable'),
(273, 66, '_tax_class', ''),
(274, 66, '_manage_stock', 'no'),
(275, 66, '_backorders', 'no'),
(276, 66, '_sold_individually', 'no'),
(277, 66, '_virtual', 'no'),
(278, 66, '_downloadable', 'no'),
(279, 66, '_download_limit', '-1'),
(280, 66, '_download_expiry', '-1'),
(281, 66, '_stock', NULL),
(282, 66, '_stock_status', 'instock'),
(283, 66, '_wc_average_rating', '0'),
(284, 66, '_wc_review_count', '0'),
(285, 66, '_product_version', '5.4.1'),
(286, 66, '_price', '180'),
(287, 61, '_regular_price', '290'),
(288, 61, '_sale_price', '248'),
(289, 61, '_price', '248'),
(290, 62, '_regular_price', '98'),
(291, 62, '_sale_price', '77'),
(292, 62, '_price', '77'),
(293, 63, '_regular_price', '120'),
(294, 63, '_sale_price', '103'),
(295, 63, '_price', '103'),
(296, 64, '_regular_price', '220'),
(297, 64, '_sale_price', '200'),
(298, 64, '_price', '200'),
(299, 67, '_edit_last', '1'),
(300, 67, '_edit_lock', '1626158029:1'),
(301, 67, '_wp_page_template', 'views/home.php'),
(302, 67, 'title_tag', ''),
(303, 67, '_title_tag', 'field_5ddbe7577e0e7'),
(304, 67, 'single_slider_seo', '2'),
(305, 67, '_single_slider_seo', 'field_5ddbde5499115'),
(306, 67, 'slider_img', '161'),
(307, 67, '_slider_img', 'field_60e988d9254ba'),
(308, 68, 'title_tag', ''),
(309, 68, '_title_tag', 'field_5ddbe7577e0e7'),
(310, 68, 'single_slider_seo', ''),
(311, 68, '_single_slider_seo', 'field_5ddbde5499115'),
(312, 68, 'slider_img', ''),
(313, 68, '_slider_img', 'field_60e988d9254ba'),
(314, 69, '_menu_item_type', 'post_type'),
(315, 69, '_menu_item_menu_item_parent', '0'),
(316, 69, '_menu_item_object_id', '67'),
(317, 69, '_menu_item_object', 'page'),
(318, 69, '_menu_item_target', ''),
(319, 69, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(320, 69, '_menu_item_xfn', ''),
(321, 69, '_menu_item_url', ''),
(323, 70, '_menu_item_type', 'post_type'),
(324, 70, '_menu_item_menu_item_parent', '0'),
(325, 70, '_menu_item_object_id', '49'),
(326, 70, '_menu_item_object', 'page'),
(327, 70, '_menu_item_target', ''),
(328, 70, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(329, 70, '_menu_item_xfn', ''),
(330, 70, '_menu_item_url', ''),
(332, 71, '_menu_item_type', 'post_type'),
(333, 71, '_menu_item_menu_item_parent', '0'),
(334, 71, '_menu_item_object_id', '1'),
(335, 71, '_menu_item_object', 'post'),
(336, 71, '_menu_item_target', ''),
(337, 71, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(338, 71, '_menu_item_xfn', ''),
(339, 71, '_menu_item_url', ''),
(341, 72, '_menu_item_type', 'post_type'),
(342, 72, '_menu_item_menu_item_parent', '0'),
(343, 72, '_menu_item_object_id', '47'),
(344, 72, '_menu_item_object', 'post'),
(345, 72, '_menu_item_target', ''),
(346, 72, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(347, 72, '_menu_item_xfn', ''),
(348, 72, '_menu_item_url', ''),
(350, 73, '_menu_item_type', 'post_type'),
(351, 73, '_menu_item_menu_item_parent', '0'),
(352, 73, '_menu_item_object_id', '45'),
(353, 73, '_menu_item_object', 'post'),
(354, 73, '_menu_item_target', ''),
(355, 73, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(356, 73, '_menu_item_xfn', ''),
(357, 73, '_menu_item_url', ''),
(359, 74, '_menu_item_type', 'post_type'),
(360, 74, '_menu_item_menu_item_parent', '0'),
(361, 74, '_menu_item_object_id', '43'),
(362, 74, '_menu_item_object', 'post'),
(363, 74, '_menu_item_target', ''),
(364, 74, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(365, 74, '_menu_item_xfn', ''),
(366, 74, '_menu_item_url', ''),
(368, 75, '_menu_item_type', 'post_type'),
(369, 75, '_menu_item_menu_item_parent', '0'),
(370, 75, '_menu_item_object_id', '41'),
(371, 75, '_menu_item_object', 'post'),
(372, 75, '_menu_item_target', ''),
(373, 75, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(374, 75, '_menu_item_xfn', ''),
(375, 75, '_menu_item_url', ''),
(386, 77, '_menu_item_type', 'post_type'),
(387, 77, '_menu_item_menu_item_parent', '0'),
(388, 77, '_menu_item_object_id', '1'),
(389, 77, '_menu_item_object', 'post'),
(390, 77, '_menu_item_target', ''),
(391, 77, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(392, 77, '_menu_item_xfn', ''),
(393, 77, '_menu_item_url', ''),
(395, 78, '_menu_item_type', 'post_type'),
(396, 78, '_menu_item_menu_item_parent', '0'),
(397, 78, '_menu_item_object_id', '66'),
(398, 78, '_menu_item_object', 'product'),
(399, 78, '_menu_item_target', ''),
(400, 78, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(401, 78, '_menu_item_xfn', ''),
(402, 78, '_menu_item_url', ''),
(404, 79, '_menu_item_type', 'post_type'),
(405, 79, '_menu_item_menu_item_parent', '0'),
(406, 79, '_menu_item_object_id', '65'),
(407, 79, '_menu_item_object', 'product'),
(408, 79, '_menu_item_target', ''),
(409, 79, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(410, 79, '_menu_item_xfn', ''),
(411, 79, '_menu_item_url', ''),
(413, 80, '_menu_item_type', 'post_type'),
(414, 80, '_menu_item_menu_item_parent', '0'),
(415, 80, '_menu_item_object_id', '64'),
(416, 80, '_menu_item_object', 'product'),
(417, 80, '_menu_item_target', ''),
(418, 80, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(419, 80, '_menu_item_xfn', ''),
(420, 80, '_menu_item_url', ''),
(422, 81, '_menu_item_type', 'post_type'),
(423, 81, '_menu_item_menu_item_parent', '0'),
(424, 81, '_menu_item_object_id', '63'),
(425, 81, '_menu_item_object', 'product'),
(426, 81, '_menu_item_target', ''),
(427, 81, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(428, 81, '_menu_item_xfn', ''),
(429, 81, '_menu_item_url', ''),
(431, 82, '_menu_item_type', 'post_type'),
(432, 82, '_menu_item_menu_item_parent', '0'),
(433, 82, '_menu_item_object_id', '62'),
(434, 82, '_menu_item_object', 'product'),
(435, 82, '_menu_item_target', ''),
(436, 82, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(437, 82, '_menu_item_xfn', ''),
(438, 82, '_menu_item_url', ''),
(440, 83, '_menu_item_type', 'post_type'),
(441, 83, '_menu_item_menu_item_parent', '0'),
(442, 83, '_menu_item_object_id', '61'),
(443, 83, '_menu_item_object', 'product'),
(444, 83, '_menu_item_target', ''),
(445, 83, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(446, 83, '_menu_item_xfn', ''),
(447, 83, '_menu_item_url', ''),
(449, 84, '_menu_item_type', 'post_type'),
(450, 84, '_menu_item_menu_item_parent', '0'),
(451, 84, '_menu_item_object_id', '52'),
(452, 84, '_menu_item_object', 'product'),
(453, 84, '_menu_item_target', ''),
(454, 84, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(455, 84, '_menu_item_xfn', ''),
(456, 84, '_menu_item_url', ''),
(458, 85, '_menu_item_type', 'post_type'),
(459, 85, '_menu_item_menu_item_parent', '0'),
(460, 85, '_menu_item_object_id', '7'),
(461, 85, '_menu_item_object', 'page'),
(462, 85, '_menu_item_target', ''),
(463, 85, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(464, 85, '_menu_item_xfn', ''),
(465, 85, '_menu_item_url', ''),
(467, 86, '_menu_item_type', 'post_type'),
(468, 86, '_menu_item_menu_item_parent', '0'),
(469, 86, '_menu_item_object_id', '43'),
(470, 86, '_menu_item_object', 'post'),
(471, 86, '_menu_item_target', ''),
(472, 86, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(473, 86, '_menu_item_xfn', ''),
(474, 86, '_menu_item_url', ''),
(476, 87, '_menu_item_type', 'post_type'),
(477, 87, '_menu_item_menu_item_parent', '0'),
(478, 87, '_menu_item_object_id', '41'),
(479, 87, '_menu_item_object', 'post'),
(480, 87, '_menu_item_target', ''),
(481, 87, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(482, 87, '_menu_item_xfn', ''),
(483, 87, '_menu_item_url', ''),
(485, 88, '_menu_item_type', 'post_type'),
(486, 88, '_menu_item_menu_item_parent', '0'),
(487, 88, '_menu_item_object_id', '32'),
(488, 88, '_menu_item_object', 'post'),
(489, 88, '_menu_item_target', ''),
(490, 88, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(491, 88, '_menu_item_xfn', ''),
(492, 88, '_menu_item_url', ''),
(494, 89, '_menu_item_type', 'post_type'),
(495, 89, '_menu_item_menu_item_parent', '0'),
(496, 89, '_menu_item_object_id', '66'),
(497, 89, '_menu_item_object', 'product'),
(498, 89, '_menu_item_target', ''),
(499, 89, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(500, 89, '_menu_item_xfn', ''),
(501, 89, '_menu_item_url', ''),
(503, 90, '_menu_item_type', 'post_type'),
(504, 90, '_menu_item_menu_item_parent', '0'),
(505, 90, '_menu_item_object_id', '65'),
(506, 90, '_menu_item_object', 'product'),
(507, 90, '_menu_item_target', ''),
(508, 90, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(509, 90, '_menu_item_xfn', ''),
(510, 90, '_menu_item_url', ''),
(512, 91, '_menu_item_type', 'post_type'),
(513, 91, '_menu_item_menu_item_parent', '0'),
(514, 91, '_menu_item_object_id', '64'),
(515, 91, '_menu_item_object', 'product'),
(516, 91, '_menu_item_target', ''),
(517, 91, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(518, 91, '_menu_item_xfn', ''),
(519, 91, '_menu_item_url', ''),
(521, 92, '_menu_item_type', 'post_type'),
(522, 92, '_menu_item_menu_item_parent', '0'),
(523, 92, '_menu_item_object_id', '63'),
(524, 92, '_menu_item_object', 'product'),
(525, 92, '_menu_item_target', ''),
(526, 92, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(527, 92, '_menu_item_xfn', ''),
(528, 92, '_menu_item_url', ''),
(530, 93, '_menu_item_type', 'post_type'),
(531, 93, '_menu_item_menu_item_parent', '0'),
(532, 93, '_menu_item_object_id', '62'),
(533, 93, '_menu_item_object', 'product'),
(534, 93, '_menu_item_target', ''),
(535, 93, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(536, 93, '_menu_item_xfn', ''),
(537, 93, '_menu_item_url', ''),
(539, 94, '_menu_item_type', 'post_type'),
(540, 94, '_menu_item_menu_item_parent', '0'),
(541, 94, '_menu_item_object_id', '61'),
(542, 94, '_menu_item_object', 'product'),
(543, 94, '_menu_item_target', ''),
(544, 94, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(545, 94, '_menu_item_xfn', ''),
(546, 94, '_menu_item_url', ''),
(548, 95, '_menu_item_type', 'post_type'),
(549, 95, '_menu_item_menu_item_parent', '0'),
(550, 95, '_menu_item_object_id', '52'),
(551, 95, '_menu_item_object', 'product'),
(552, 95, '_menu_item_target', ''),
(553, 95, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(554, 95, '_menu_item_xfn', ''),
(555, 95, '_menu_item_url', ''),
(557, 96, '_menu_item_type', 'taxonomy'),
(558, 96, '_menu_item_menu_item_parent', '0'),
(559, 96, '_menu_item_object_id', '15'),
(560, 96, '_menu_item_object', 'product_cat'),
(561, 96, '_menu_item_target', ''),
(562, 96, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(563, 96, '_menu_item_xfn', ''),
(564, 96, '_menu_item_url', ''),
(566, 97, '_wp_attached_file', '2021/07/map.png'),
(567, 97, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:775;s:6:\"height\";i:286;s:4:\"file\";s:15:\"2021/07/map.png\";s:5:\"sizes\";a:10:{s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"map-300x111.png\";s:5:\"width\";i:300;s:6:\"height\";i:111;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"map-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"map-768x283.png\";s:5:\"width\";i:768;s:6:\"height\";i:283;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:14:\"map-134x49.png\";s:5:\"width\";i:134;s:6:\"height\";i:49;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:15:\"map-300x286.png\";s:5:\"width\";i:300;s:6:\"height\";i:286;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:15:\"map-600x221.png\";s:5:\"width\";i:600;s:6:\"height\";i:221;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:15:\"map-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:15:\"map-300x286.png\";s:5:\"width\";i:300;s:6:\"height\";i:286;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:15:\"map-600x221.png\";s:5:\"width\";i:600;s:6:\"height\";i:221;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:15:\"map-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(568, 11, '_config_errors', 'a:2:{s:11:\"mail.sender\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:0:\"\";s:6:\"params\";a:0:{}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:62:\"תחביר כתובת מייל בשדה %name% לא תקינה\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(569, 100, '_menu_item_type', 'taxonomy'),
(570, 100, '_menu_item_menu_item_parent', '0'),
(571, 100, '_menu_item_object_id', '15'),
(572, 100, '_menu_item_object', 'product_cat'),
(573, 100, '_menu_item_target', ''),
(574, 100, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(575, 100, '_menu_item_xfn', ''),
(576, 100, '_menu_item_url', ''),
(578, 101, '_menu_item_type', 'post_type'),
(579, 101, '_menu_item_menu_item_parent', '0'),
(580, 101, '_menu_item_object_id', '47'),
(581, 101, '_menu_item_object', 'post'),
(582, 101, '_menu_item_target', ''),
(583, 101, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(584, 101, '_menu_item_xfn', ''),
(585, 101, '_menu_item_url', ''),
(587, 102, '_menu_item_type', 'post_type'),
(588, 102, '_menu_item_menu_item_parent', '0'),
(589, 102, '_menu_item_object_id', '45'),
(590, 102, '_menu_item_object', 'post'),
(591, 102, '_menu_item_target', ''),
(592, 102, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(593, 102, '_menu_item_xfn', ''),
(594, 102, '_menu_item_url', ''),
(596, 103, '_menu_item_type', 'post_type'),
(597, 103, '_menu_item_menu_item_parent', '0'),
(598, 103, '_menu_item_object_id', '43'),
(599, 103, '_menu_item_object', 'post'),
(600, 103, '_menu_item_target', ''),
(601, 103, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(602, 103, '_menu_item_xfn', ''),
(603, 103, '_menu_item_url', ''),
(605, 104, '_menu_item_type', 'post_type'),
(606, 104, '_menu_item_menu_item_parent', '0'),
(607, 104, '_menu_item_object_id', '41'),
(608, 104, '_menu_item_object', 'post'),
(609, 104, '_menu_item_target', ''),
(610, 104, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(611, 104, '_menu_item_xfn', ''),
(612, 104, '_menu_item_url', ''),
(614, 105, '_menu_item_type', 'post_type'),
(615, 105, '_menu_item_menu_item_parent', '0'),
(616, 105, '_menu_item_object_id', '32'),
(617, 105, '_menu_item_object', 'post'),
(618, 105, '_menu_item_target', ''),
(619, 105, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(620, 105, '_menu_item_xfn', ''),
(621, 105, '_menu_item_url', ''),
(623, 106, '_menu_item_type', 'post_type'),
(624, 106, '_menu_item_menu_item_parent', '0'),
(625, 106, '_menu_item_object_id', '1'),
(626, 106, '_menu_item_object', 'post'),
(627, 106, '_menu_item_target', ''),
(628, 106, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(629, 106, '_menu_item_xfn', ''),
(630, 106, '_menu_item_url', ''),
(632, 107, '_menu_item_type', 'post_type'),
(633, 107, '_menu_item_menu_item_parent', '0'),
(634, 107, '_menu_item_object_id', '66');
INSERT INTO `fmn_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(635, 107, '_menu_item_object', 'product'),
(636, 107, '_menu_item_target', ''),
(637, 107, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(638, 107, '_menu_item_xfn', ''),
(639, 107, '_menu_item_url', ''),
(641, 108, '_menu_item_type', 'post_type'),
(642, 108, '_menu_item_menu_item_parent', '0'),
(643, 108, '_menu_item_object_id', '65'),
(644, 108, '_menu_item_object', 'product'),
(645, 108, '_menu_item_target', ''),
(646, 108, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(647, 108, '_menu_item_xfn', ''),
(648, 108, '_menu_item_url', ''),
(650, 109, '_menu_item_type', 'post_type'),
(651, 109, '_menu_item_menu_item_parent', '0'),
(652, 109, '_menu_item_object_id', '64'),
(653, 109, '_menu_item_object', 'product'),
(654, 109, '_menu_item_target', ''),
(655, 109, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(656, 109, '_menu_item_xfn', ''),
(657, 109, '_menu_item_url', ''),
(659, 110, '_menu_item_type', 'post_type'),
(660, 110, '_menu_item_menu_item_parent', '0'),
(661, 110, '_menu_item_object_id', '63'),
(662, 110, '_menu_item_object', 'product'),
(663, 110, '_menu_item_target', ''),
(664, 110, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(665, 110, '_menu_item_xfn', ''),
(666, 110, '_menu_item_url', ''),
(668, 111, '_menu_item_type', 'post_type'),
(669, 111, '_menu_item_menu_item_parent', '0'),
(670, 111, '_menu_item_object_id', '62'),
(671, 111, '_menu_item_object', 'product'),
(672, 111, '_menu_item_target', ''),
(673, 111, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(674, 111, '_menu_item_xfn', ''),
(675, 111, '_menu_item_url', ''),
(677, 112, '_menu_item_type', 'post_type'),
(678, 112, '_menu_item_menu_item_parent', '0'),
(679, 112, '_menu_item_object_id', '61'),
(680, 112, '_menu_item_object', 'product'),
(681, 112, '_menu_item_target', ''),
(682, 112, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(683, 112, '_menu_item_xfn', ''),
(684, 112, '_menu_item_url', ''),
(686, 113, '_menu_item_type', 'post_type'),
(687, 113, '_menu_item_menu_item_parent', '0'),
(688, 113, '_menu_item_object_id', '52'),
(689, 113, '_menu_item_object', 'product'),
(690, 113, '_menu_item_target', ''),
(691, 113, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(692, 113, '_menu_item_xfn', ''),
(693, 113, '_menu_item_url', ''),
(695, 114, '_wp_attached_file', '2021/07/logo.png'),
(696, 114, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:417;s:6:\"height\";i:113;s:4:\"file\";s:16:\"2021/07/logo.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"logo-300x81.png\";s:5:\"width\";i:300;s:6:\"height\";i:81;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-150x113.png\";s:5:\"width\";i:150;s:6:\"height\";i:113;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:15:\"logo-134x36.png\";s:5:\"width\";i:134;s:6:\"height\";i:36;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:16:\"logo-300x113.png\";s:5:\"width\";i:300;s:6:\"height\";i:113;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:16:\"logo-300x113.png\";s:5:\"width\";i:300;s:6:\"height\";i:113;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(697, 52, 'price_per_100', ''),
(698, 52, '_product_video', 'https://www.youtube.com/watch?v=5qap5aO4i9A'),
(699, 7, '_edit_lock', '1626177000:1'),
(700, 63, 'price_per_100', ''),
(701, 63, '_product_video', ''),
(702, 115, '_edit_last', '1'),
(703, 115, '_edit_lock', '1626163200:1'),
(704, 123, '_menu_item_type', 'taxonomy'),
(705, 123, '_menu_item_menu_item_parent', '0'),
(706, 123, '_menu_item_object_id', '24'),
(707, 123, '_menu_item_object', 'product_cat'),
(708, 123, '_menu_item_target', ''),
(709, 123, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(710, 123, '_menu_item_xfn', ''),
(711, 123, '_menu_item_url', ''),
(713, 100, '_wp_old_date', '2021-07-11'),
(714, 125, '_menu_item_type', 'post_type'),
(715, 125, '_menu_item_menu_item_parent', '0'),
(716, 125, '_menu_item_object_id', '49'),
(717, 125, '_menu_item_object', 'page'),
(718, 125, '_menu_item_target', ''),
(719, 125, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(720, 125, '_menu_item_xfn', ''),
(721, 125, '_menu_item_url', ''),
(723, 72, '_wp_old_date', '2021-07-11'),
(724, 73, '_wp_old_date', '2021-07-11'),
(725, 74, '_wp_old_date', '2021-07-11'),
(726, 75, '_wp_old_date', '2021-07-11'),
(728, 77, '_wp_old_date', '2021-07-11'),
(730, 126, '_wp_attached_file', '2021/07/contact-img.png'),
(731, 126, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:370;s:6:\"height\";i:483;s:4:\"file\";s:23:\"2021/07/contact-img.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"contact-img-230x300.png\";s:5:\"width\";i:230;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"contact-img-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:21:\"contact-img-72x94.png\";s:5:\"width\";i:72;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:23:\"contact-img-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"contact-img-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:23:\"contact-img-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"contact-img-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(732, 49, '_thumbnail_id', '126'),
(733, 49, '_yoast_wpseo_content_score', '90'),
(734, 49, '_yoast_wpseo_estimated-reading-time-minutes', '1'),
(736, 12, '_config_errors', 'a:2:{s:11:\"mail.sender\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:0:\"\";s:6:\"params\";a:0:{}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:62:\"תחביר כתובת מייל בשדה %name% לא תקינה\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(737, 127, 'title_tag', ''),
(738, 127, '_title_tag', 'field_5ddbe7577e0e7'),
(739, 127, 'single_slider_seo', ''),
(740, 127, '_single_slider_seo', 'field_5ddbde5499115'),
(741, 127, 'slider_img', ''),
(742, 127, '_slider_img', 'field_60e988d9254ba'),
(743, 128, '_edit_last', '1'),
(744, 128, '_edit_lock', '1626079033:1'),
(745, 132, '_wp_attached_file', '2021/07/לוגו.png'),
(746, 132, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:101;s:6:\"height\";i:86;s:4:\"file\";s:20:\"2021/07/לוגו.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(747, 133, '_wp_attached_file', '2021/07/לוגו-1.png'),
(748, 133, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:101;s:6:\"height\";i:86;s:4:\"file\";s:22:\"2021/07/לוגו-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(749, 134, '_wp_attached_file', '2021/07/לוגו-2.png'),
(750, 134, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:101;s:6:\"height\";i:86;s:4:\"file\";s:22:\"2021/07/לוגו-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(751, 135, '_wp_attached_file', '2021/07/לוגו-3.png'),
(752, 135, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:101;s:6:\"height\";i:86;s:4:\"file\";s:22:\"2021/07/לוגו-3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(753, 136, '_wp_attached_file', '2021/07/לוגו-4.png'),
(754, 136, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:101;s:6:\"height\";i:86;s:4:\"file\";s:22:\"2021/07/לוגו-4.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(755, 137, '_wp_attached_file', '2021/07/לוגו-5.png'),
(756, 137, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:101;s:6:\"height\";i:86;s:4:\"file\";s:22:\"2021/07/לוגו-5.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(757, 138, '_wp_attached_file', '2021/07/לוגו-6.png'),
(758, 138, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:101;s:6:\"height\";i:86;s:4:\"file\";s:22:\"2021/07/לוגו-6.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(759, 139, '_wp_attached_file', '2021/07/לוגו-7.png'),
(760, 139, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:101;s:6:\"height\";i:86;s:4:\"file\";s:22:\"2021/07/לוגו-7.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(761, 49, 'map_block_title', 'צרו איתנו קשר בכל דרך הנוחה לכם'),
(762, 49, '_map_block_title', 'field_60ebf240ab05f'),
(763, 49, 'clients_block_text', '<h2><span class=\"Y2IQFc\" lang=\"iw\">לקוחות אצלנו או מותגים מובילים </span></h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג'),
(764, 49, '_clients_block_text', 'field_60ebf2672dc08'),
(765, 49, 'clients', 'a:8:{i:0;s:3:\"132\";i:1;s:3:\"133\";i:2;s:3:\"134\";i:3;s:3:\"135\";i:4;s:3:\"136\";i:5;s:3:\"137\";i:6;s:3:\"138\";i:7;s:3:\"139\";}'),
(766, 49, '_clients', 'field_60ebf2902dc09'),
(767, 140, 'title_tag', ''),
(768, 140, '_title_tag', 'field_5ddbe7577e0e7'),
(769, 140, 'single_slider_seo', ''),
(770, 140, '_single_slider_seo', 'field_5ddbde5499115'),
(771, 140, 'slider_img', ''),
(772, 140, '_slider_img', 'field_60e988d9254ba'),
(773, 140, 'map_block_title', 'צרו איתנו קשר בכל דרך הנוחה לכם'),
(774, 140, '_map_block_title', 'field_60ebf240ab05f'),
(775, 140, 'clients_block_text', '<h2><span class=\"Y2IQFc\" lang=\"iw\">לקוחות אצלנו או מותגים מובילים </span></h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג'),
(776, 140, '_clients_block_text', 'field_60ebf2672dc08'),
(777, 140, 'clients', 'a:8:{i:0;s:3:\"132\";i:1;s:3:\"133\";i:2;s:3:\"134\";i:3;s:3:\"135\";i:4;s:3:\"136\";i:5;s:3:\"137\";i:6;s:3:\"138\";i:7;s:3:\"139\";}'),
(778, 140, '_clients', 'field_60ebf2902dc09'),
(779, 141, '_edit_last', '1'),
(780, 141, '_edit_lock', '1626085598:1'),
(781, 145, '_wp_attached_file', '2021/07/test-prod-1.png'),
(782, 145, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:533;s:6:\"height\";i:615;s:4:\"file\";s:23:\"2021/07/test-prod-1.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"test-prod-1-260x300.png\";s:5:\"width\";i:260;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:21:\"test-prod-1-81x94.png\";s:5:\"width\";i:81;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:23:\"test-prod-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:23:\"test-prod-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(783, 146, '_wp_attached_file', '2021/07/test-prod-2.png'),
(784, 146, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:173;s:6:\"height\";i:137;s:4:\"file\";s:23:\"2021/07/test-prod-2.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-2-150x137.png\";s:5:\"width\";i:150;s:6:\"height\";i:137;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:22:\"test-prod-2-119x94.png\";s:5:\"width\";i:119;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(785, 147, '_wp_attached_file', '2021/07/test-prod-3.png'),
(786, 147, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:173;s:6:\"height\";i:137;s:4:\"file\";s:23:\"2021/07/test-prod-3.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-3-150x137.png\";s:5:\"width\";i:150;s:6:\"height\";i:137;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:22:\"test-prod-3-119x94.png\";s:5:\"width\";i:119;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(787, 148, '_wp_attached_file', '2021/07/test-prod-4.png'),
(788, 148, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:173;s:6:\"height\";i:137;s:4:\"file\";s:23:\"2021/07/test-prod-4.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-4-150x137.png\";s:5:\"width\";i:150;s:6:\"height\";i:137;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:22:\"test-prod-4-119x94.png\";s:5:\"width\";i:119;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(789, 149, '_wp_attached_file', '2021/07/test-prod-5.png'),
(790, 149, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:173;s:6:\"height\";i:137;s:4:\"file\";s:23:\"2021/07/test-prod-5.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-5-150x137.png\";s:5:\"width\";i:150;s:6:\"height\";i:137;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:22:\"test-prod-5-119x94.png\";s:5:\"width\";i:119;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-prod-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(791, 52, '_product_image_gallery', '146,147,148'),
(792, 52, '_yoast_wpseo_content_score', '30'),
(793, 52, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(794, 52, 'info_tabs_0_title', 'פרטים'),
(795, 52, '_info_tabs_0_title', 'field_60ec00f97c9e0'),
(796, 52, 'info_tabs_0_content', 'נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(797, 52, '_info_tabs_0_content', 'field_60ec01007c9e1'),
(798, 52, 'info_tabs_1_title', 'על המותג'),
(799, 52, '_info_tabs_1_title', 'field_60ec00f97c9e0'),
(800, 52, 'info_tabs_1_content', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט'),
(801, 52, '_info_tabs_1_content', 'field_60ec01007c9e1'),
(802, 52, 'info_tabs_2_title', 'משלוחים והחזרות'),
(803, 52, '_info_tabs_2_title', 'field_60ec00f97c9e0'),
(804, 52, 'info_tabs_2_content', 'סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(805, 52, '_info_tabs_2_content', 'field_60ec01007c9e1'),
(806, 52, 'info_tabs_3_title', 'נתונים טכניים'),
(807, 52, '_info_tabs_3_title', 'field_60ec00f97c9e0'),
(808, 52, 'info_tabs_3_content', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nהועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(809, 52, '_info_tabs_3_content', 'field_60ec01007c9e1'),
(810, 52, 'info_tabs', '4'),
(811, 52, '_info_tabs', 'field_60ec00e97c9df'),
(812, 52, 'related_products', 'a:5:{i:0;s:2:\"63\";i:1;s:2:\"64\";i:2;s:2:\"65\";i:3;s:2:\"66\";i:4;s:2:\"62\";}'),
(813, 52, '_related_products', 'field_60ec05bfcd042'),
(814, 52, 'related_products_title', 'המוצרים אצלנו בגורדון לרכישה מיידית'),
(815, 52, '_related_products_title', 'field_60ec06ab6175e'),
(816, 52, 'related_products_text', 'ההצלחה שלך ההכנה שלנו בואו לבחור את המסלול המתאים לכם'),
(817, 52, '_related_products_text', 'field_60ec06bdda96b'),
(818, 154, '_edit_last', '1'),
(819, 154, '_edit_lock', '1626118288:1'),
(820, 161, '_wp_attached_file', '2021/07/slider-img.png'),
(821, 161, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:775;s:6:\"height\";i:536;s:4:\"file\";s:22:\"2021/07/slider-img.png\";s:5:\"sizes\";a:10:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"slider-img-300x207.png\";s:5:\"width\";i:300;s:6:\"height\";i:207;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"slider-img-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"slider-img-768x531.png\";s:5:\"width\";i:768;s:6:\"height\";i:531;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:21:\"slider-img-134x94.png\";s:5:\"width\";i:134;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"slider-img-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:22:\"slider-img-600x415.png\";s:5:\"width\";i:600;s:6:\"height\";i:415;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"slider-img-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"slider-img-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:22:\"slider-img-600x415.png\";s:5:\"width\";i:600;s:6:\"height\";i:415;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"slider-img-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(822, 67, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(823, 67, 'single_slider_seo_0_content', '<h2>מעניין לדעת 1</h2>\r\n<h4>כאן תופיע כותרת לורם אפסום 1</h4>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(824, 67, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(825, 67, 'single_slider_seo_1_content', '<h2>מעניין לדעת 2</h2>\r\n<h4>כאן תופיע כותרת לורם אפסום 2</h4>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(826, 67, '_single_slider_seo_1_content', 'field_5ddbde7399116'),
(827, 162, 'title_tag', ''),
(828, 162, '_title_tag', 'field_5ddbe7577e0e7'),
(829, 162, 'single_slider_seo', '2'),
(830, 162, '_single_slider_seo', 'field_5ddbde5499115'),
(831, 162, 'slider_img', '161'),
(832, 162, '_slider_img', 'field_60e988d9254ba'),
(833, 162, 'single_slider_seo_0_content', '<h2>מעניין לדעת 1</h2>\r\n<h4>כאן תופיע כותרת לורם אפסום 1</h4>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(834, 162, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(835, 162, 'single_slider_seo_1_content', '<h2>מעניין לדעת 2</h2>\r\n<h4>כאן תופיע כותרת לורם אפסום 2</h4>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(836, 162, '_single_slider_seo_1_content', 'field_5ddbde7399116'),
(837, 163, '_wp_attached_file', '2021/07/prod-slider-img.png'),
(838, 163, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:747;s:6:\"height\";i:496;s:4:\"file\";s:27:\"2021/07/prod-slider-img.png\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"prod-slider-img-300x199.png\";s:5:\"width\";i:300;s:6:\"height\";i:199;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"prod-slider-img-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:26:\"prod-slider-img-134x89.png\";s:5:\"width\";i:134;s:6:\"height\";i:89;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:27:\"prod-slider-img-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:27:\"prod-slider-img-600x398.png\";s:5:\"width\";i:600;s:6:\"height\";i:398;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:27:\"prod-slider-img-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:27:\"prod-slider-img-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:27:\"prod-slider-img-600x398.png\";s:5:\"width\";i:600;s:6:\"height\";i:398;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:27:\"prod-slider-img-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(839, 52, 'prod_slider_seo_0_content', '<h2>כאן תופיע כותרת לורם אפסום 1</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית ליבם סולגק. בראיט ולחת צורק מונחף, לתיג ישבעס..'),
(840, 52, '_prod_slider_seo_0_content', 'field_60ec1872365c7'),
(841, 52, 'prod_slider_seo_1_content', '<h2>כאן תופיע כותרת לורם אפסום 2</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית ליבם סולגק. בראיט ולחת צורק מונחף, לתיג ישבעס..'),
(842, 52, '_prod_slider_seo_1_content', 'field_60ec1872365c7'),
(843, 52, 'prod_slider_seo', '2'),
(844, 52, '_prod_slider_seo', 'field_60ec1872365c6'),
(845, 52, 'prod_slider_img', '163'),
(846, 52, '_prod_slider_img', 'field_60ec18857fa27'),
(847, 52, 'prod_slider_title', 'ערכות טכנולוגיות '),
(848, 52, '_prod_slider_title', 'field_60ec18943ddf7'),
(849, 52, 'prod_slider_text', 'מבחר מארזים מהודרים ויפים במיוחד'),
(850, 52, '_prod_slider_text', 'field_60ec18b23ddf8'),
(851, 52, 'prod_slider_link', 'a:3:{s:5:\"title\";s:8:\"SHOP NOW\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(852, 52, '_prod_slider_link', 'field_60ec18d93ddf9'),
(853, 165, '_edit_last', '1'),
(854, 165, '_edit_lock', '1626175421:1'),
(855, 207, '_wp_attached_file', '2021/07/course-1-1.png'),
(856, 207, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:352;s:6:\"height\";i:212;s:4:\"file\";s:22:\"2021/07/course-1-1.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"course-1-1-300x181.png\";s:5:\"width\";i:300;s:6:\"height\";i:181;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"course-1-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:21:\"course-1-1-134x81.png\";s:5:\"width\";i:134;s:6:\"height\";i:81;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"course-1-1-300x212.png\";s:5:\"width\";i:300;s:6:\"height\";i:212;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"course-1-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"course-1-1-300x212.png\";s:5:\"width\";i:300;s:6:\"height\";i:212;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"course-1-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(857, 208, '_wp_attached_file', '2021/07/course-2-1.png'),
(858, 208, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:351;s:6:\"height\";i:212;s:4:\"file\";s:22:\"2021/07/course-2-1.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"course-2-1-300x181.png\";s:5:\"width\";i:300;s:6:\"height\";i:181;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"course-2-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:21:\"course-2-1-134x81.png\";s:5:\"width\";i:134;s:6:\"height\";i:81;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"course-2-1-300x212.png\";s:5:\"width\";i:300;s:6:\"height\";i:212;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"course-2-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"course-2-1-300x212.png\";s:5:\"width\";i:300;s:6:\"height\";i:212;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"course-2-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(859, 209, '_wp_attached_file', '2021/07/course-3-1.png'),
(860, 209, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:351;s:6:\"height\";i:211;s:4:\"file\";s:22:\"2021/07/course-3-1.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"course-3-1-300x180.png\";s:5:\"width\";i:300;s:6:\"height\";i:180;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"course-3-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:21:\"course-3-1-134x81.png\";s:5:\"width\";i:134;s:6:\"height\";i:81;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"course-3-1-300x211.png\";s:5:\"width\";i:300;s:6:\"height\";i:211;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"course-3-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"course-3-1-300x211.png\";s:5:\"width\";i:300;s:6:\"height\";i:211;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"course-3-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(861, 210, '_wp_attached_file', '2021/07/course-4-1.png'),
(862, 210, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:349;s:6:\"height\";i:216;s:4:\"file\";s:22:\"2021/07/course-4-1.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"course-4-1-300x186.png\";s:5:\"width\";i:300;s:6:\"height\";i:186;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"course-4-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:21:\"course-4-1-134x83.png\";s:5:\"width\";i:134;s:6:\"height\";i:83;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"course-4-1-300x216.png\";s:5:\"width\";i:300;s:6:\"height\";i:216;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"course-4-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"course-4-1-300x216.png\";s:5:\"width\";i:300;s:6:\"height\";i:216;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"course-4-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(863, 211, '_wp_attached_file', '2021/07/courses-img.png'),
(864, 211, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:707;s:6:\"height\";i:969;s:4:\"file\";s:23:\"2021/07/courses-img.png\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"courses-img-219x300.png\";s:5:\"width\";i:219;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"courses-img-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:21:\"courses-img-69x94.png\";s:5:\"width\";i:69;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:23:\"courses-img-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:23:\"courses-img-600x822.png\";s:5:\"width\";i:600;s:6:\"height\";i:822;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"courses-img-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:23:\"courses-img-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:23:\"courses-img-600x822.png\";s:5:\"width\";i:600;s:6:\"height\";i:822;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"courses-img-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(865, 212, '_wp_attached_file', '2021/07/slider-benefits-img.png'),
(866, 212, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:413;s:6:\"height\";i:519;s:4:\"file\";s:31:\"2021/07/slider-benefits-img.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"slider-benefits-img-239x300.png\";s:5:\"width\";i:239;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"slider-benefits-img-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:29:\"slider-benefits-img-75x94.png\";s:5:\"width\";i:75;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:31:\"slider-benefits-img-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:31:\"slider-benefits-img-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:31:\"slider-benefits-img-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:31:\"slider-benefits-img-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(867, 213, '_wp_attached_file', '2021/07/main-img.png'),
(868, 213, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:982;s:4:\"file\";s:20:\"2021/07/main-img.png\";s:5:\"sizes\";a:12:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"main-img-300x153.png\";s:5:\"width\";i:300;s:6:\"height\";i:153;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:21:\"main-img-1024x524.png\";s:5:\"width\";i:1024;s:6:\"height\";i:524;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"main-img-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"main-img-768x393.png\";s:5:\"width\";i:768;s:6:\"height\";i:393;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:21:\"main-img-1536x786.png\";s:5:\"width\";i:1536;s:6:\"height\";i:786;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"main-img-134x69.png\";s:5:\"width\";i:134;s:6:\"height\";i:69;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"main-img-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:20:\"main-img-600x307.png\";s:5:\"width\";i:600;s:6:\"height\";i:307;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"main-img-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"main-img-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:20:\"main-img-600x307.png\";s:5:\"width\";i:600;s:6:\"height\";i:307;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"main-img-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(869, 214, '_wp_attached_file', '2021/07/home-part-img-1.png'),
(870, 214, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:524;s:6:\"height\";i:459;s:4:\"file\";s:27:\"2021/07/home-part-img-1.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"home-part-img-1-300x263.png\";s:5:\"width\";i:300;s:6:\"height\";i:263;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"home-part-img-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:26:\"home-part-img-1-107x94.png\";s:5:\"width\";i:107;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:27:\"home-part-img-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:27:\"home-part-img-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:27:\"home-part-img-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:27:\"home-part-img-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(871, 215, '_wp_attached_file', '2021/07/home-part-img-2.png'),
(872, 215, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:406;s:6:\"height\";i:390;s:4:\"file\";s:27:\"2021/07/home-part-img-2.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"home-part-img-2-300x288.png\";s:5:\"width\";i:300;s:6:\"height\";i:288;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"home-part-img-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:25:\"home-part-img-2-98x94.png\";s:5:\"width\";i:98;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:27:\"home-part-img-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:27:\"home-part-img-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:27:\"home-part-img-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:27:\"home-part-img-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(873, 216, '_wp_attached_file', '2021/07/baner-1.png'),
(874, 216, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:624;s:6:\"height\";i:511;s:4:\"file\";s:19:\"2021/07/baner-1.png\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"baner-1-300x246.png\";s:5:\"width\";i:300;s:6:\"height\";i:246;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"baner-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:18:\"baner-1-115x94.png\";s:5:\"width\";i:115;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:19:\"baner-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:19:\"baner-1-600x491.png\";s:5:\"width\";i:600;s:6:\"height\";i:491;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:19:\"baner-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:19:\"baner-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:19:\"baner-1-600x491.png\";s:5:\"width\";i:600;s:6:\"height\";i:491;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:19:\"baner-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(875, 217, '_wp_attached_file', '2021/07/baner-2.png');
INSERT INTO `fmn_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(876, 217, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:677;s:6:\"height\";i:258;s:4:\"file\";s:19:\"2021/07/baner-2.png\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"baner-2-300x114.png\";s:5:\"width\";i:300;s:6:\"height\";i:114;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"baner-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:18:\"baner-2-134x51.png\";s:5:\"width\";i:134;s:6:\"height\";i:51;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:19:\"baner-2-300x258.png\";s:5:\"width\";i:300;s:6:\"height\";i:258;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:19:\"baner-2-600x229.png\";s:5:\"width\";i:600;s:6:\"height\";i:229;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:19:\"baner-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:19:\"baner-2-300x258.png\";s:5:\"width\";i:300;s:6:\"height\";i:258;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:19:\"baner-2-600x229.png\";s:5:\"width\";i:600;s:6:\"height\";i:229;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:19:\"baner-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(877, 218, '_wp_attached_file', '2021/07/baner-3.png'),
(878, 218, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:674;s:6:\"height\";i:258;s:4:\"file\";s:19:\"2021/07/baner-3.png\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"baner-3-300x115.png\";s:5:\"width\";i:300;s:6:\"height\";i:115;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"baner-3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:18:\"baner-3-134x51.png\";s:5:\"width\";i:134;s:6:\"height\";i:51;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:19:\"baner-3-300x258.png\";s:5:\"width\";i:300;s:6:\"height\";i:258;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:19:\"baner-3-600x230.png\";s:5:\"width\";i:600;s:6:\"height\";i:230;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:19:\"baner-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:19:\"baner-3-300x258.png\";s:5:\"width\";i:300;s:6:\"height\";i:258;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:19:\"baner-3-600x230.png\";s:5:\"width\";i:600;s:6:\"height\";i:230;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:19:\"baner-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(879, 219, '_wp_attached_file', '2021/07/post-1-1.png'),
(880, 219, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:398;s:6:\"height\";i:286;s:4:\"file\";s:20:\"2021/07/post-1-1.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"post-1-1-300x216.png\";s:5:\"width\";i:300;s:6:\"height\";i:216;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"post-1-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"post-1-1-131x94.png\";s:5:\"width\";i:131;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"post-1-1-300x286.png\";s:5:\"width\";i:300;s:6:\"height\";i:286;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"post-1-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"post-1-1-300x286.png\";s:5:\"width\";i:300;s:6:\"height\";i:286;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"post-1-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(881, 220, '_wp_attached_file', '2021/07/post-2-1.png'),
(882, 220, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:400;s:6:\"height\";i:288;s:4:\"file\";s:20:\"2021/07/post-2-1.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"post-2-1-300x216.png\";s:5:\"width\";i:300;s:6:\"height\";i:216;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"post-2-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"post-2-1-131x94.png\";s:5:\"width\";i:131;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"post-2-1-300x288.png\";s:5:\"width\";i:300;s:6:\"height\";i:288;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"post-2-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"post-2-1-300x288.png\";s:5:\"width\";i:300;s:6:\"height\";i:288;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"post-2-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(883, 221, '_wp_attached_file', '2021/07/post-3-1.png'),
(884, 221, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:295;s:6:\"height\";i:288;s:4:\"file\";s:20:\"2021/07/post-3-1.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"post-3-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:18:\"post-3-1-96x94.png\";s:5:\"width\";i:96;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"post-3-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"post-3-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(885, 67, 'home_main_img', '213'),
(886, 67, '_home_main_img', 'field_60ec993a3abe0'),
(887, 67, 'home_small_title', 'מבחר טכנולוגיות מתקדמות במיוחד עבורכם'),
(888, 67, '_home_small_title', 'field_60ec99753abe1'),
(889, 67, 'home_big_title', 'קטגוריית מוצר  לורם אפרסום'),
(890, 67, '_home_big_title', 'field_60ec99953abe2'),
(891, 67, 'home_main_link', 'a:3:{s:5:\"title\";s:45:\"מעבר לקטגוריה לורם אפסום\";s:3:\"url\";s:55:\"/product-category/קטגורית-מוצרים-לטסט/\";s:6:\"target\";s:0:\"\";}'),
(892, 67, '_home_main_link', 'field_60ec99a43abe3'),
(893, 67, 'home_serv_prod_text', '<h2>גורדון טכנולוגיה מבחר שירותיים ומוצרים</h2>\r\nההצלחה שלך הטכנולוגיה שלנו בואו להתקדם לעתיד'),
(894, 67, '_home_serv_prod_text', 'field_60ec99ce781a9'),
(895, 67, 'home_serv_prod_item_0_block_item_img', '214'),
(896, 67, '_home_serv_prod_item_0_block_item_img', 'field_60ec99f5781ab'),
(897, 67, 'home_serv_prod_item_0_block_item_small_title', 'כל המוצרים לרכישה עכשיו'),
(898, 67, '_home_serv_prod_item_0_block_item_small_title', 'field_60ec9a10781ac'),
(899, 67, 'home_serv_prod_item_0_block_item_big_title', 'טכנולוגית מוצרים'),
(900, 67, '_home_serv_prod_item_0_block_item_big_title', 'field_60ec9a22781ad'),
(901, 67, 'home_serv_prod_item_0_block_item_link', 'a:3:{s:5:\"title\";s:23:\"מעבר למוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(902, 67, '_home_serv_prod_item_0_block_item_link', 'field_60ec9a33781ae'),
(903, 67, 'home_serv_prod_item_1_block_item_img', '215'),
(904, 67, '_home_serv_prod_item_1_block_item_img', 'field_60ec99f5781ab'),
(905, 67, 'home_serv_prod_item_1_block_item_small_title', 'דואגים לכם ללמוד גם מהבית'),
(906, 67, '_home_serv_prod_item_1_block_item_small_title', 'field_60ec9a10781ac'),
(907, 67, 'home_serv_prod_item_1_block_item_big_title', 'מרחבי למידה'),
(908, 67, '_home_serv_prod_item_1_block_item_big_title', 'field_60ec9a22781ad'),
(909, 67, 'home_serv_prod_item_1_block_item_link', 'a:3:{s:5:\"title\";s:32:\"מעבר למרחבי למידה\";s:3:\"url\";s:23:\"https://www.google.com/\";s:6:\"target\";s:0:\"\";}'),
(910, 67, '_home_serv_prod_item_1_block_item_link', 'field_60ec9a33781ae'),
(911, 67, 'home_serv_prod_item', '2'),
(912, 67, '_home_serv_prod_item', 'field_60ec99e5781aa'),
(913, 67, 'home_search_text', '<h2>כל הטכנולוגיות במקום אחד! פשוט רק לבחור</h2>\r\nלא יודעים מה אתם צריכים? גורדון תעשה לכם סדר'),
(914, 67, '_home_search_text', 'field_60ec9a6b8787f'),
(915, 67, 'slider_banner_0_slider_images', 'a:3:{i:0;s:3:\"216\";i:1;s:3:\"217\";i:2;s:3:\"218\";}'),
(916, 67, '_slider_banner_0_slider_images', 'field_60ec9adc87883'),
(917, 67, 'slider_banner_1_slider_images', 'a:3:{i:0;s:3:\"146\";i:1;s:3:\"147\";i:2;s:3:\"209\";}'),
(918, 67, '_slider_banner_1_slider_images', 'field_60ec9adc87883'),
(919, 67, 'slider_banner', '2'),
(920, 67, '_slider_banner', 'field_60ec9aae87881'),
(921, 67, 'h_block_prod_text', '<h2>המוצרים אצלנו</h2>\r\n<h3>טכנולוגית גורדון למידה מחודש</h3>\r\nאצלנו בגורדון תוכלו להכיר את המוצרים שיקדמו אתכם להתקדם, הגיע הזמן לבחור גורדון, אתם מוזמנים לצפות בקטלוג המוצרים שלנו:'),
(922, 67, '_h_block_prod_text', 'field_60ec9b18ac425'),
(923, 67, 'h_block_prod_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(924, 67, '_h_block_prod_link', 'field_60ec9b71ac426'),
(925, 67, 'h_block_prod_custom_0_prod_custom_img', '219'),
(926, 67, '_h_block_prod_custom_0_prod_custom_img', 'field_60ec9b9eac428'),
(927, 67, 'h_block_prod_custom_0_prod_custom_title', 'פיתוח מרחבי למידה אימרסיבי'),
(928, 67, '_h_block_prod_custom_0_prod_custom_title', 'field_60ec9bb6ac429'),
(929, 67, 'h_block_prod_custom_0_prod_custom_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:117:\"/product/%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-6/\";s:6:\"target\";s:0:\"\";}'),
(930, 67, '_h_block_prod_custom_0_prod_custom_link', 'field_60ec9bc0ac42a'),
(931, 67, 'h_block_prod_custom_1_prod_custom_img', '220'),
(932, 67, '_h_block_prod_custom_1_prod_custom_img', 'field_60ec9b9eac428'),
(933, 67, 'h_block_prod_custom_1_prod_custom_title', 'חדשנות וטכנולוגיות למידה'),
(934, 67, '_h_block_prod_custom_1_prod_custom_title', 'field_60ec9bb6ac429'),
(935, 67, 'h_block_prod_custom_1_prod_custom_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:117:\"/product/%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-5/\";s:6:\"target\";s:0:\"\";}'),
(936, 67, '_h_block_prod_custom_1_prod_custom_link', 'field_60ec9bc0ac42a'),
(937, 67, 'h_block_prod_custom_2_prod_custom_img', '221'),
(938, 67, '_h_block_prod_custom_2_prod_custom_img', 'field_60ec9b9eac428'),
(939, 67, 'h_block_prod_custom_2_prod_custom_title', ' חדשנות וטכנולוגיות למידה'),
(940, 67, '_h_block_prod_custom_2_prod_custom_title', 'field_60ec9bb6ac429'),
(941, 67, 'h_block_prod_custom_2_prod_custom_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:23:\"https://www.google.com/\";s:6:\"target\";s:0:\"\";}'),
(942, 67, '_h_block_prod_custom_2_prod_custom_link', 'field_60ec9bc0ac42a'),
(943, 67, 'h_block_prod_custom', '3'),
(944, 67, '_h_block_prod_custom', 'field_60ec9b8aac427'),
(945, 67, 'home_course_text', '<h2>קורסים אצלנו</h2>\r\n<h3>בגורדון שכדאי לכם להכיר</h3>\r\nאצלנו בגורדון תוכלו להכיר את המוצרים שיקדמו אתכם צעד לפני כולם לעולם טכנולוגי מרהיב מסוגו, בוא תוכלו להראות וללמד בצורה חדשנית ופורצת דרך, הגיע הזמן להתקדם, הגיע הזמן לבחור גורדון, אתם מוזמנים לצפות בקטלוג המוצרים שלנו:'),
(946, 67, '_home_course_text', 'field_60ec9d062bc44'),
(947, 67, 'home_course_img', '211'),
(948, 67, '_home_course_img', 'field_60ec9d252bc45'),
(949, 67, 'home_course_item_0_course_img', '207'),
(950, 67, '_home_course_item_0_course_img', 'field_60ec9d6e2bc47'),
(951, 67, 'home_course_item_0_course_title', 'כאן תופיע כותרת 1'),
(952, 67, '_home_course_item_0_course_title', 'field_60ec9d7a2bc48'),
(953, 67, 'home_course_item_0_course_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:138:\"http://gordon:8888/2021/07/11/%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-2/\";s:6:\"target\";s:0:\"\";}'),
(954, 67, '_home_course_item_0_course_link', 'field_60ec9d842bc49'),
(955, 67, 'home_course_item_1_course_img', '208'),
(956, 67, '_home_course_item_1_course_img', 'field_60ec9d6e2bc47'),
(957, 67, 'home_course_item_1_course_title', 'כאן תופיע כותרת 2'),
(958, 67, '_home_course_item_1_course_title', 'field_60ec9d7a2bc48'),
(959, 67, 'home_course_item_1_course_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:138:\"http://gordon:8888/2021/07/11/%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-1/\";s:6:\"target\";s:0:\"\";}'),
(960, 67, '_home_course_item_1_course_link', 'field_60ec9d842bc49'),
(961, 67, 'home_course_item_2_course_img', '209'),
(962, 67, '_home_course_item_2_course_img', 'field_60ec9d6e2bc47'),
(963, 67, 'home_course_item_2_course_title', 'כאן תופיע כותרת  3'),
(964, 67, '_home_course_item_2_course_title', 'field_60ec9d7a2bc48'),
(965, 67, 'home_course_item_2_course_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:23:\"https://www.google.com/\";s:6:\"target\";s:0:\"\";}'),
(966, 67, '_home_course_item_2_course_link', 'field_60ec9d842bc49'),
(967, 67, 'home_course_item_3_course_img', '210'),
(968, 67, '_home_course_item_3_course_img', 'field_60ec9d6e2bc47'),
(969, 67, 'home_course_item_3_course_title', 'כאן תופיע כותרת 4'),
(970, 67, '_home_course_item_3_course_title', 'field_60ec9d7a2bc48'),
(971, 67, 'home_course_item_3_course_link', 'a:3:{s:5:\"title\";s:18:\"שלום עולם!\";s:3:\"url\";s:80:\"http://gordon:8888/2021/07/08/%d7%a9%d7%9c%d7%95%d7%9d-%d7%a2%d7%95%d7%9c%d7%9d/\";s:6:\"target\";s:0:\"\";}'),
(972, 67, '_home_course_item_3_course_link', 'field_60ec9d842bc49'),
(973, 67, 'home_course_item', '4'),
(974, 67, '_home_course_item', 'field_60ec9d532bc46'),
(975, 67, 'home_course_link', 'a:3:{s:5:\"title\";s:21:\"לכל הקורסים\";s:3:\"url\";s:55:\"/product-category/קטגורית-מוצרים-לטסט/\";s:6:\"target\";s:0:\"\";}'),
(976, 67, '_home_course_link', 'field_60ec9d922bc4a'),
(977, 67, 'home_prod_slider_text', '<h2>המוצרים החדשים אצלנו בגורדון לרכישה מיידית</h2>\r\nההצלחה שלך ההכנה שלנו בואו לבחור את המסלול המתאים לכם'),
(978, 67, '_home_prod_slider_text', 'field_60ec9dc32bc4c'),
(979, 67, 'home_prod_slider_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(980, 67, '_home_prod_slider_link', 'field_60ec9dda2bc4d'),
(981, 67, 'home_prod_slider', 'a:5:{i:0;s:2:\"61\";i:1;s:2:\"62\";i:2;s:2:\"63\";i:3;s:2:\"64\";i:4;s:2:\"65\";}'),
(982, 67, '_home_prod_slider', 'field_60ec9df72bc4e'),
(983, 67, 'slider_benefits_0_benefit_content', '<h2>צוות מומחים</h2>\r\nבמרכז לחדשנות צוות מומחים מקצועי ובעל ניסיון עשיר בארץ ובחו״ל, בתחום אסטרטגיות הוראה ולמידה, פדגוגיה דיגיטלית, חדשנות, חקר עתידים וחשיבה יצירתית'),
(984, 67, '_slider_benefits_0_benefit_content', 'field_60ec9e4c2bc51'),
(985, 67, 'slider_benefits_1_benefit_content', '<h2>נולום ארווס סאפיאן - פוסיליס קוויס</h2>\r\nאקווזמן להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(986, 67, '_slider_benefits_1_benefit_content', 'field_60ec9e4c2bc51'),
(987, 67, 'slider_benefits_2_benefit_content', '<h2>קולורס מונפרד אדנדום סילקוף</h2>\r\nמרגשי ומרגשח. עמחליף להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט'),
(988, 67, '_slider_benefits_2_benefit_content', 'field_60ec9e4c2bc51'),
(989, 67, 'slider_benefits_3_benefit_content', '<h2>קוואזי במר מודוף</h2>\r\nאודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.'),
(990, 67, '_slider_benefits_3_benefit_content', 'field_60ec9e4c2bc51'),
(991, 67, 'slider_benefits_4_benefit_content', '<h2>לורם איפסום</h2>\r\nהועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(992, 67, '_slider_benefits_4_benefit_content', 'field_60ec9e4c2bc51'),
(993, 67, 'slider_benefits', '5'),
(994, 67, '_slider_benefits', 'field_60ec9e352bc50'),
(995, 67, 'slider_benefits_img', '212'),
(996, 67, '_slider_benefits_img', 'field_60ec9e5f2bc52'),
(997, 222, 'title_tag', ''),
(998, 222, '_title_tag', 'field_5ddbe7577e0e7'),
(999, 222, 'single_slider_seo', '2'),
(1000, 222, '_single_slider_seo', 'field_5ddbde5499115'),
(1001, 222, 'slider_img', '161'),
(1002, 222, '_slider_img', 'field_60e988d9254ba'),
(1003, 222, 'single_slider_seo_0_content', '<h2>מעניין לדעת 1</h2>\r\n<h4>כאן תופיע כותרת לורם אפסום 1</h4>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(1004, 222, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(1005, 222, 'single_slider_seo_1_content', '<h2>מעניין לדעת 2</h2>\r\n<h4>כאן תופיע כותרת לורם אפסום 2</h4>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(1006, 222, '_single_slider_seo_1_content', 'field_5ddbde7399116'),
(1007, 222, 'home_main_img', '213'),
(1008, 222, '_home_main_img', 'field_60ec993a3abe0'),
(1009, 222, 'home_small_title', 'מבחר טכנולוגיות מתקדמות במיוחד עבורכם'),
(1010, 222, '_home_small_title', 'field_60ec99753abe1'),
(1011, 222, 'home_big_title', 'קטגוריית מוצר  לורם אפרסום'),
(1012, 222, '_home_big_title', 'field_60ec99953abe2'),
(1013, 222, 'home_main_link', 'a:3:{s:5:\"title\";s:45:\"מעבר לקטגוריה לורם אפסום\";s:3:\"url\";s:55:\"/product-category/קטגורית-מוצרים-לטסט/\";s:6:\"target\";s:0:\"\";}'),
(1014, 222, '_home_main_link', 'field_60ec99a43abe3'),
(1015, 222, 'home_serv_prod_text', '<h2>גורדון טכנולוגיה מבחר שירותיים ומוצרים</h2>\r\nההצלחה שלך הטכנולוגיה שלנו בואו להתקדם לעתיד'),
(1016, 222, '_home_serv_prod_text', 'field_60ec99ce781a9'),
(1017, 222, 'home_serv_prod_item_0_block_item_img', '214'),
(1018, 222, '_home_serv_prod_item_0_block_item_img', 'field_60ec99f5781ab'),
(1019, 222, 'home_serv_prod_item_0_block_item_small_title', 'כל המוצרים לרכישה עכשיו'),
(1020, 222, '_home_serv_prod_item_0_block_item_small_title', 'field_60ec9a10781ac'),
(1021, 222, 'home_serv_prod_item_0_block_item_big_title', 'טכנולוגית מוצרים'),
(1022, 222, '_home_serv_prod_item_0_block_item_big_title', 'field_60ec9a22781ad'),
(1023, 222, 'home_serv_prod_item_0_block_item_link', 'a:3:{s:5:\"title\";s:23:\"מעבר למוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1024, 222, '_home_serv_prod_item_0_block_item_link', 'field_60ec9a33781ae'),
(1025, 222, 'home_serv_prod_item_1_block_item_img', '215'),
(1026, 222, '_home_serv_prod_item_1_block_item_img', 'field_60ec99f5781ab'),
(1027, 222, 'home_serv_prod_item_1_block_item_small_title', 'דואגים לכם ללמוד גם מהבית'),
(1028, 222, '_home_serv_prod_item_1_block_item_small_title', 'field_60ec9a10781ac'),
(1029, 222, 'home_serv_prod_item_1_block_item_big_title', 'מרחבי למידה'),
(1030, 222, '_home_serv_prod_item_1_block_item_big_title', 'field_60ec9a22781ad'),
(1031, 222, 'home_serv_prod_item_1_block_item_link', 'a:3:{s:5:\"title\";s:32:\"מעבר למרחבי למידה\";s:3:\"url\";s:23:\"https://www.google.com/\";s:6:\"target\";s:0:\"\";}'),
(1032, 222, '_home_serv_prod_item_1_block_item_link', 'field_60ec9a33781ae'),
(1033, 222, 'home_serv_prod_item', '2'),
(1034, 222, '_home_serv_prod_item', 'field_60ec99e5781aa'),
(1035, 222, 'home_search_text', '<h2>כל הטכנולוגיות במקום אחד! פשוט רק לבחור</h2>\r\nלא יודעים מה אתם צריכים? גורדון תעשה לכם סדר'),
(1036, 222, '_home_search_text', 'field_60ec9a6b8787f'),
(1037, 222, 'slider_banner_0_slider_images', 'a:3:{i:0;s:3:\"216\";i:1;s:3:\"217\";i:2;s:3:\"218\";}'),
(1038, 222, '_slider_banner_0_slider_images', 'field_60ec9adc87883'),
(1039, 222, 'slider_banner_1_slider_images', 'a:3:{i:0;s:3:\"146\";i:1;s:3:\"147\";i:2;s:3:\"209\";}'),
(1040, 222, '_slider_banner_1_slider_images', 'field_60ec9adc87883'),
(1041, 222, 'slider_banner', '2'),
(1042, 222, '_slider_banner', 'field_60ec9aae87881'),
(1043, 222, 'h_block_prod_text', '<h2>המוצרים אצלנו</h2>\r\n<h3>טכנולוגית גורדון למידה מחודש</h3>\r\nאצלנו בגורדון תוכלו להכיר את המוצרים שיקדמו אתכם להתקדם, הגיע הזמן לבחור גורדון, אתם מוזמנים לצפות בקטלוג המוצרים שלנו:'),
(1044, 222, '_h_block_prod_text', 'field_60ec9b18ac425'),
(1045, 222, 'h_block_prod_link', ''),
(1046, 222, '_h_block_prod_link', 'field_60ec9b71ac426'),
(1047, 222, 'h_block_prod_custom_0_prod_custom_img', '219'),
(1048, 222, '_h_block_prod_custom_0_prod_custom_img', 'field_60ec9b9eac428'),
(1049, 222, 'h_block_prod_custom_0_prod_custom_title', 'פיתוח מרחבי למידה אימרסיבי'),
(1050, 222, '_h_block_prod_custom_0_prod_custom_title', 'field_60ec9bb6ac429'),
(1051, 222, 'h_block_prod_custom_0_prod_custom_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:117:\"/product/%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-6/\";s:6:\"target\";s:0:\"\";}'),
(1052, 222, '_h_block_prod_custom_0_prod_custom_link', 'field_60ec9bc0ac42a'),
(1053, 222, 'h_block_prod_custom_1_prod_custom_img', '220'),
(1054, 222, '_h_block_prod_custom_1_prod_custom_img', 'field_60ec9b9eac428'),
(1055, 222, 'h_block_prod_custom_1_prod_custom_title', 'חדשנות וטכנולוגיות למידה'),
(1056, 222, '_h_block_prod_custom_1_prod_custom_title', 'field_60ec9bb6ac429'),
(1057, 222, 'h_block_prod_custom_1_prod_custom_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:117:\"/product/%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-5/\";s:6:\"target\";s:0:\"\";}'),
(1058, 222, '_h_block_prod_custom_1_prod_custom_link', 'field_60ec9bc0ac42a'),
(1059, 222, 'h_block_prod_custom_2_prod_custom_img', '221'),
(1060, 222, '_h_block_prod_custom_2_prod_custom_img', 'field_60ec9b9eac428'),
(1061, 222, 'h_block_prod_custom_2_prod_custom_title', ' חדשנות וטכנולוגיות למידה'),
(1062, 222, '_h_block_prod_custom_2_prod_custom_title', 'field_60ec9bb6ac429'),
(1063, 222, 'h_block_prod_custom_2_prod_custom_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:23:\"https://www.google.com/\";s:6:\"target\";s:0:\"\";}'),
(1064, 222, '_h_block_prod_custom_2_prod_custom_link', 'field_60ec9bc0ac42a'),
(1065, 222, 'h_block_prod_custom', '3'),
(1066, 222, '_h_block_prod_custom', 'field_60ec9b8aac427'),
(1067, 222, 'home_course_text', '<h2>קורסים אצלנו</h2>\r\n<h3>בגורדון שכדאי לכם להכיר</h3>\r\nאצלנו בגורדון תוכלו להכיר את המוצרים שיקדמו אתכם צעד לפני כולם לעולם טכנולוגי מרהיב מסוגו, בוא תוכלו להראות וללמד בצורה חדשנית ופורצת דרך, הגיע הזמן להתקדם, הגיע הזמן לבחור גורדון, אתם מוזמנים לצפות בקטלוג המוצרים שלנו:'),
(1068, 222, '_home_course_text', 'field_60ec9d062bc44'),
(1069, 222, 'home_course_img', '211'),
(1070, 222, '_home_course_img', 'field_60ec9d252bc45'),
(1071, 222, 'home_course_item_0_course_img', '207'),
(1072, 222, '_home_course_item_0_course_img', 'field_60ec9d6e2bc47'),
(1073, 222, 'home_course_item_0_course_title', 'כאן תופיע כותרת 1'),
(1074, 222, '_home_course_item_0_course_title', 'field_60ec9d7a2bc48'),
(1075, 222, 'home_course_item_0_course_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:138:\"http://gordon:8888/2021/07/11/%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-2/\";s:6:\"target\";s:0:\"\";}'),
(1076, 222, '_home_course_item_0_course_link', 'field_60ec9d842bc49'),
(1077, 222, 'home_course_item_1_course_img', '208'),
(1078, 222, '_home_course_item_1_course_img', 'field_60ec9d6e2bc47'),
(1079, 222, 'home_course_item_1_course_title', 'כאן תופיע כותרת 2'),
(1080, 222, '_home_course_item_1_course_title', 'field_60ec9d7a2bc48'),
(1081, 222, 'home_course_item_1_course_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:138:\"http://gordon:8888/2021/07/11/%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-1/\";s:6:\"target\";s:0:\"\";}'),
(1082, 222, '_home_course_item_1_course_link', 'field_60ec9d842bc49'),
(1083, 222, 'home_course_item_2_course_img', '209'),
(1084, 222, '_home_course_item_2_course_img', 'field_60ec9d6e2bc47'),
(1085, 222, 'home_course_item_2_course_title', 'כאן תופיע כותרת  3'),
(1086, 222, '_home_course_item_2_course_title', 'field_60ec9d7a2bc48'),
(1087, 222, 'home_course_item_2_course_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:23:\"https://www.google.com/\";s:6:\"target\";s:0:\"\";}'),
(1088, 222, '_home_course_item_2_course_link', 'field_60ec9d842bc49'),
(1089, 222, 'home_course_item_3_course_img', '210'),
(1090, 222, '_home_course_item_3_course_img', 'field_60ec9d6e2bc47'),
(1091, 222, 'home_course_item_3_course_title', 'כאן תופיע כותרת 4'),
(1092, 222, '_home_course_item_3_course_title', 'field_60ec9d7a2bc48'),
(1093, 222, 'home_course_item_3_course_link', 'a:3:{s:5:\"title\";s:18:\"שלום עולם!\";s:3:\"url\";s:80:\"http://gordon:8888/2021/07/08/%d7%a9%d7%9c%d7%95%d7%9d-%d7%a2%d7%95%d7%9c%d7%9d/\";s:6:\"target\";s:0:\"\";}'),
(1094, 222, '_home_course_item_3_course_link', 'field_60ec9d842bc49'),
(1095, 222, 'home_course_item', '4'),
(1096, 222, '_home_course_item', 'field_60ec9d532bc46'),
(1097, 222, 'home_course_link', 'a:3:{s:5:\"title\";s:21:\"לכל הקורסים\";s:3:\"url\";s:55:\"/product-category/קטגורית-מוצרים-לטסט/\";s:6:\"target\";s:0:\"\";}'),
(1098, 222, '_home_course_link', 'field_60ec9d922bc4a'),
(1099, 222, 'home_prod_slider_text', '<h2>המוצרים החדשים אצלנו בגורדון לרכישה מיידית</h2>\r\nההצלחה שלך ההכנה שלנו בואו לבחור את המסלול המתאים לכם'),
(1100, 222, '_home_prod_slider_text', 'field_60ec9dc32bc4c'),
(1101, 222, 'home_prod_slider_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1102, 222, '_home_prod_slider_link', 'field_60ec9dda2bc4d'),
(1103, 222, 'home_prod_slider', 'a:5:{i:0;s:2:\"61\";i:1;s:2:\"62\";i:2;s:2:\"63\";i:3;s:2:\"64\";i:4;s:2:\"65\";}'),
(1104, 222, '_home_prod_slider', 'field_60ec9df72bc4e'),
(1105, 222, 'slider_benefits_0_benefit_content', '<h2>צוות מומחים</h2>\r\nבמרכז לחדשנות צוות מומחים מקצועי ובעל ניסיון עשיר בארץ ובחו״ל, בתחום אסטרטגיות הוראה ולמידה, פדגוגיה דיגיטלית, חדשנות, חקר עתידים וחשיבה יצירתית'),
(1106, 222, '_slider_benefits_0_benefit_content', 'field_60ec9e4c2bc51'),
(1107, 222, 'slider_benefits_1_benefit_content', '<h2>נולום ארווס סאפיאן - פוסיליס קוויס</h2>\r\nאקווזמן להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(1108, 222, '_slider_benefits_1_benefit_content', 'field_60ec9e4c2bc51'),
(1109, 222, 'slider_benefits_2_benefit_content', '<h2>קולורס מונפרד אדנדום סילקוף</h2>\r\nמרגשי ומרגשח. עמחליף להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט'),
(1110, 222, '_slider_benefits_2_benefit_content', 'field_60ec9e4c2bc51'),
(1111, 222, 'slider_benefits_3_benefit_content', '<h2>קוואזי במר מודוף</h2>\r\nאודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.'),
(1112, 222, '_slider_benefits_3_benefit_content', 'field_60ec9e4c2bc51'),
(1113, 222, 'slider_benefits_4_benefit_content', '<h2>לורם איפסום</h2>\r\nהועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(1114, 222, '_slider_benefits_4_benefit_content', 'field_60ec9e4c2bc51'),
(1115, 222, 'slider_benefits', '5'),
(1116, 222, '_slider_benefits', 'field_60ec9e352bc50'),
(1117, 222, 'slider_benefits_img', '212'),
(1118, 222, '_slider_benefits_img', 'field_60ec9e5f2bc52'),
(1119, 67, 'slider_benefits_title', 'למה לבחור בגורדון ללמידה מחדש'),
(1120, 67, '_slider_benefits_title', 'field_60ecab1140ae5'),
(1121, 224, 'title_tag', ''),
(1122, 224, '_title_tag', 'field_5ddbe7577e0e7'),
(1123, 224, 'single_slider_seo', '2'),
(1124, 224, '_single_slider_seo', 'field_5ddbde5499115'),
(1125, 224, 'slider_img', '161'),
(1126, 224, '_slider_img', 'field_60e988d9254ba'),
(1127, 224, 'single_slider_seo_0_content', '<h2>מעניין לדעת 1</h2>\r\n<h4>כאן תופיע כותרת לורם אפסום 1</h4>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(1128, 224, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(1129, 224, 'single_slider_seo_1_content', '<h2>מעניין לדעת 2</h2>\r\n<h4>כאן תופיע כותרת לורם אפסום 2</h4>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(1130, 224, '_single_slider_seo_1_content', 'field_5ddbde7399116'),
(1131, 224, 'home_main_img', '213'),
(1132, 224, '_home_main_img', 'field_60ec993a3abe0'),
(1133, 224, 'home_small_title', 'מבחר טכנולוגיות מתקדמות במיוחד עבורכם'),
(1134, 224, '_home_small_title', 'field_60ec99753abe1'),
(1135, 224, 'home_big_title', 'קטגוריית מוצר  לורם אפרסום'),
(1136, 224, '_home_big_title', 'field_60ec99953abe2'),
(1137, 224, 'home_main_link', 'a:3:{s:5:\"title\";s:45:\"מעבר לקטגוריה לורם אפסום\";s:3:\"url\";s:55:\"/product-category/קטגורית-מוצרים-לטסט/\";s:6:\"target\";s:0:\"\";}'),
(1138, 224, '_home_main_link', 'field_60ec99a43abe3'),
(1139, 224, 'home_serv_prod_text', '<h2>גורדון טכנולוגיה מבחר שירותיים ומוצרים</h2>\r\nההצלחה שלך הטכנולוגיה שלנו בואו להתקדם לעתיד'),
(1140, 224, '_home_serv_prod_text', 'field_60ec99ce781a9'),
(1141, 224, 'home_serv_prod_item_0_block_item_img', '214'),
(1142, 224, '_home_serv_prod_item_0_block_item_img', 'field_60ec99f5781ab'),
(1143, 224, 'home_serv_prod_item_0_block_item_small_title', 'כל המוצרים לרכישה עכשיו'),
(1144, 224, '_home_serv_prod_item_0_block_item_small_title', 'field_60ec9a10781ac'),
(1145, 224, 'home_serv_prod_item_0_block_item_big_title', 'טכנולוגית מוצרים'),
(1146, 224, '_home_serv_prod_item_0_block_item_big_title', 'field_60ec9a22781ad'),
(1147, 224, 'home_serv_prod_item_0_block_item_link', 'a:3:{s:5:\"title\";s:23:\"מעבר למוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1148, 224, '_home_serv_prod_item_0_block_item_link', 'field_60ec9a33781ae'),
(1149, 224, 'home_serv_prod_item_1_block_item_img', '215'),
(1150, 224, '_home_serv_prod_item_1_block_item_img', 'field_60ec99f5781ab'),
(1151, 224, 'home_serv_prod_item_1_block_item_small_title', 'דואגים לכם ללמוד גם מהבית'),
(1152, 224, '_home_serv_prod_item_1_block_item_small_title', 'field_60ec9a10781ac'),
(1153, 224, 'home_serv_prod_item_1_block_item_big_title', 'מרחבי למידה'),
(1154, 224, '_home_serv_prod_item_1_block_item_big_title', 'field_60ec9a22781ad'),
(1155, 224, 'home_serv_prod_item_1_block_item_link', 'a:3:{s:5:\"title\";s:32:\"מעבר למרחבי למידה\";s:3:\"url\";s:23:\"https://www.google.com/\";s:6:\"target\";s:0:\"\";}'),
(1156, 224, '_home_serv_prod_item_1_block_item_link', 'field_60ec9a33781ae'),
(1157, 224, 'home_serv_prod_item', '2'),
(1158, 224, '_home_serv_prod_item', 'field_60ec99e5781aa'),
(1159, 224, 'home_search_text', '<h2>כל הטכנולוגיות במקום אחד! פשוט רק לבחור</h2>\r\nלא יודעים מה אתם צריכים? גורדון תעשה לכם סדר'),
(1160, 224, '_home_search_text', 'field_60ec9a6b8787f'),
(1161, 224, 'slider_banner_0_slider_images', 'a:3:{i:0;s:3:\"216\";i:1;s:3:\"217\";i:2;s:3:\"218\";}'),
(1162, 224, '_slider_banner_0_slider_images', 'field_60ec9adc87883'),
(1163, 224, 'slider_banner_1_slider_images', 'a:3:{i:0;s:3:\"146\";i:1;s:3:\"147\";i:2;s:3:\"209\";}'),
(1164, 224, '_slider_banner_1_slider_images', 'field_60ec9adc87883'),
(1165, 224, 'slider_banner', '2'),
(1166, 224, '_slider_banner', 'field_60ec9aae87881'),
(1167, 224, 'h_block_prod_text', '<h2>המוצרים אצלנו</h2>\r\n<h3>טכנולוגית גורדון למידה מחודש</h3>\r\nאצלנו בגורדון תוכלו להכיר את המוצרים שיקדמו אתכם להתקדם, הגיע הזמן לבחור גורדון, אתם מוזמנים לצפות בקטלוג המוצרים שלנו:'),
(1168, 224, '_h_block_prod_text', 'field_60ec9b18ac425'),
(1169, 224, 'h_block_prod_link', ''),
(1170, 224, '_h_block_prod_link', 'field_60ec9b71ac426'),
(1171, 224, 'h_block_prod_custom_0_prod_custom_img', '219'),
(1172, 224, '_h_block_prod_custom_0_prod_custom_img', 'field_60ec9b9eac428'),
(1173, 224, 'h_block_prod_custom_0_prod_custom_title', 'פיתוח מרחבי למידה אימרסיבי'),
(1174, 224, '_h_block_prod_custom_0_prod_custom_title', 'field_60ec9bb6ac429'),
(1175, 224, 'h_block_prod_custom_0_prod_custom_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:117:\"/product/%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-6/\";s:6:\"target\";s:0:\"\";}'),
(1176, 224, '_h_block_prod_custom_0_prod_custom_link', 'field_60ec9bc0ac42a'),
(1177, 224, 'h_block_prod_custom_1_prod_custom_img', '220'),
(1178, 224, '_h_block_prod_custom_1_prod_custom_img', 'field_60ec9b9eac428'),
(1179, 224, 'h_block_prod_custom_1_prod_custom_title', 'חדשנות וטכנולוגיות למידה'),
(1180, 224, '_h_block_prod_custom_1_prod_custom_title', 'field_60ec9bb6ac429'),
(1181, 224, 'h_block_prod_custom_1_prod_custom_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:117:\"/product/%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-5/\";s:6:\"target\";s:0:\"\";}'),
(1182, 224, '_h_block_prod_custom_1_prod_custom_link', 'field_60ec9bc0ac42a'),
(1183, 224, 'h_block_prod_custom_2_prod_custom_img', '221'),
(1184, 224, '_h_block_prod_custom_2_prod_custom_img', 'field_60ec9b9eac428'),
(1185, 224, 'h_block_prod_custom_2_prod_custom_title', ' חדשנות וטכנולוגיות למידה'),
(1186, 224, '_h_block_prod_custom_2_prod_custom_title', 'field_60ec9bb6ac429'),
(1187, 224, 'h_block_prod_custom_2_prod_custom_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:23:\"https://www.google.com/\";s:6:\"target\";s:0:\"\";}'),
(1188, 224, '_h_block_prod_custom_2_prod_custom_link', 'field_60ec9bc0ac42a'),
(1189, 224, 'h_block_prod_custom', '3'),
(1190, 224, '_h_block_prod_custom', 'field_60ec9b8aac427'),
(1191, 224, 'home_course_text', '<h2>קורסים אצלנו</h2>\r\n<h3>בגורדון שכדאי לכם להכיר</h3>\r\nאצלנו בגורדון תוכלו להכיר את המוצרים שיקדמו אתכם צעד לפני כולם לעולם טכנולוגי מרהיב מסוגו, בוא תוכלו להראות וללמד בצורה חדשנית ופורצת דרך, הגיע הזמן להתקדם, הגיע הזמן לבחור גורדון, אתם מוזמנים לצפות בקטלוג המוצרים שלנו:'),
(1192, 224, '_home_course_text', 'field_60ec9d062bc44'),
(1193, 224, 'home_course_img', '211'),
(1194, 224, '_home_course_img', 'field_60ec9d252bc45'),
(1195, 224, 'home_course_item_0_course_img', '207'),
(1196, 224, '_home_course_item_0_course_img', 'field_60ec9d6e2bc47'),
(1197, 224, 'home_course_item_0_course_title', 'כאן תופיע כותרת 1'),
(1198, 224, '_home_course_item_0_course_title', 'field_60ec9d7a2bc48'),
(1199, 224, 'home_course_item_0_course_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:138:\"http://gordon:8888/2021/07/11/%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-2/\";s:6:\"target\";s:0:\"\";}'),
(1200, 224, '_home_course_item_0_course_link', 'field_60ec9d842bc49'),
(1201, 224, 'home_course_item_1_course_img', '208'),
(1202, 224, '_home_course_item_1_course_img', 'field_60ec9d6e2bc47'),
(1203, 224, 'home_course_item_1_course_title', 'כאן תופיע כותרת 2'),
(1204, 224, '_home_course_item_1_course_title', 'field_60ec9d7a2bc48'),
(1205, 224, 'home_course_item_1_course_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:138:\"http://gordon:8888/2021/07/11/%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-1/\";s:6:\"target\";s:0:\"\";}'),
(1206, 224, '_home_course_item_1_course_link', 'field_60ec9d842bc49'),
(1207, 224, 'home_course_item_2_course_img', '209'),
(1208, 224, '_home_course_item_2_course_img', 'field_60ec9d6e2bc47'),
(1209, 224, 'home_course_item_2_course_title', 'כאן תופיע כותרת  3'),
(1210, 224, '_home_course_item_2_course_title', 'field_60ec9d7a2bc48'),
(1211, 224, 'home_course_item_2_course_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:23:\"https://www.google.com/\";s:6:\"target\";s:0:\"\";}'),
(1212, 224, '_home_course_item_2_course_link', 'field_60ec9d842bc49'),
(1213, 224, 'home_course_item_3_course_img', '210'),
(1214, 224, '_home_course_item_3_course_img', 'field_60ec9d6e2bc47'),
(1215, 224, 'home_course_item_3_course_title', 'כאן תופיע כותרת 4'),
(1216, 224, '_home_course_item_3_course_title', 'field_60ec9d7a2bc48'),
(1217, 224, 'home_course_item_3_course_link', 'a:3:{s:5:\"title\";s:18:\"שלום עולם!\";s:3:\"url\";s:80:\"http://gordon:8888/2021/07/08/%d7%a9%d7%9c%d7%95%d7%9d-%d7%a2%d7%95%d7%9c%d7%9d/\";s:6:\"target\";s:0:\"\";}'),
(1218, 224, '_home_course_item_3_course_link', 'field_60ec9d842bc49'),
(1219, 224, 'home_course_item', '4'),
(1220, 224, '_home_course_item', 'field_60ec9d532bc46'),
(1221, 224, 'home_course_link', 'a:3:{s:5:\"title\";s:21:\"לכל הקורסים\";s:3:\"url\";s:55:\"/product-category/קטגורית-מוצרים-לטסט/\";s:6:\"target\";s:0:\"\";}'),
(1222, 224, '_home_course_link', 'field_60ec9d922bc4a'),
(1223, 224, 'home_prod_slider_text', '<h2>המוצרים החדשים אצלנו בגורדון לרכישה מיידית</h2>\r\nההצלחה שלך ההכנה שלנו בואו לבחור את המסלול המתאים לכם'),
(1224, 224, '_home_prod_slider_text', 'field_60ec9dc32bc4c'),
(1225, 224, 'home_prod_slider_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1226, 224, '_home_prod_slider_link', 'field_60ec9dda2bc4d'),
(1227, 224, 'home_prod_slider', 'a:5:{i:0;s:2:\"61\";i:1;s:2:\"62\";i:2;s:2:\"63\";i:3;s:2:\"64\";i:4;s:2:\"65\";}'),
(1228, 224, '_home_prod_slider', 'field_60ec9df72bc4e'),
(1229, 224, 'slider_benefits_0_benefit_content', '<h2>צוות מומחים</h2>\r\nבמרכז לחדשנות צוות מומחים מקצועי ובעל ניסיון עשיר בארץ ובחו״ל, בתחום אסטרטגיות הוראה ולמידה, פדגוגיה דיגיטלית, חדשנות, חקר עתידים וחשיבה יצירתית'),
(1230, 224, '_slider_benefits_0_benefit_content', 'field_60ec9e4c2bc51'),
(1231, 224, 'slider_benefits_1_benefit_content', '<h2>נולום ארווס סאפיאן - פוסיליס קוויס</h2>\r\nאקווזמן להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(1232, 224, '_slider_benefits_1_benefit_content', 'field_60ec9e4c2bc51'),
(1233, 224, 'slider_benefits_2_benefit_content', '<h2>קולורס מונפרד אדנדום סילקוף</h2>\r\nמרגשי ומרגשח. עמחליף להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט'),
(1234, 224, '_slider_benefits_2_benefit_content', 'field_60ec9e4c2bc51'),
(1235, 224, 'slider_benefits_3_benefit_content', '<h2>קוואזי במר מודוף</h2>\r\nאודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.'),
(1236, 224, '_slider_benefits_3_benefit_content', 'field_60ec9e4c2bc51'),
(1237, 224, 'slider_benefits_4_benefit_content', '<h2>לורם איפסום</h2>\r\nהועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(1238, 224, '_slider_benefits_4_benefit_content', 'field_60ec9e4c2bc51'),
(1239, 224, 'slider_benefits', '5'),
(1240, 224, '_slider_benefits', 'field_60ec9e352bc50'),
(1241, 224, 'slider_benefits_img', '212'),
(1242, 224, '_slider_benefits_img', 'field_60ec9e5f2bc52'),
(1243, 224, 'slider_benefits_title', 'למה לבחור בגורדון ללמידה מחדש'),
(1244, 224, '_slider_benefits_title', 'field_60ecab1140ae5'),
(1245, 225, 'title_tag', ''),
(1246, 225, '_title_tag', 'field_5ddbe7577e0e7'),
(1247, 225, 'single_slider_seo', '2'),
(1248, 225, '_single_slider_seo', 'field_5ddbde5499115'),
(1249, 225, 'slider_img', '161'),
(1250, 225, '_slider_img', 'field_60e988d9254ba'),
(1251, 225, 'single_slider_seo_0_content', '<h2>מעניין לדעת 1</h2>\r\n<h4>כאן תופיע כותרת לורם אפסום 1</h4>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(1252, 225, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(1253, 225, 'single_slider_seo_1_content', '<h2>מעניין לדעת 2</h2>\r\n<h4>כאן תופיע כותרת לורם אפסום 2</h4>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(1254, 225, '_single_slider_seo_1_content', 'field_5ddbde7399116'),
(1255, 225, 'home_main_img', '213'),
(1256, 225, '_home_main_img', 'field_60ec993a3abe0'),
(1257, 225, 'home_small_title', 'מבחר טכנולוגיות מתקדמות במיוחד עבורכם'),
(1258, 225, '_home_small_title', 'field_60ec99753abe1'),
(1259, 225, 'home_big_title', 'קטגוריית מוצר  לורם אפרסום'),
(1260, 225, '_home_big_title', 'field_60ec99953abe2'),
(1261, 225, 'home_main_link', 'a:3:{s:5:\"title\";s:45:\"מעבר לקטגוריה לורם אפסום\";s:3:\"url\";s:55:\"/product-category/קטגורית-מוצרים-לטסט/\";s:6:\"target\";s:0:\"\";}'),
(1262, 225, '_home_main_link', 'field_60ec99a43abe3'),
(1263, 225, 'home_serv_prod_text', '<h2>גורדון טכנולוגיה מבחר שירותיים ומוצרים</h2>\r\nההצלחה שלך הטכנולוגיה שלנו בואו להתקדם לעתיד'),
(1264, 225, '_home_serv_prod_text', 'field_60ec99ce781a9'),
(1265, 225, 'home_serv_prod_item_0_block_item_img', '214'),
(1266, 225, '_home_serv_prod_item_0_block_item_img', 'field_60ec99f5781ab'),
(1267, 225, 'home_serv_prod_item_0_block_item_small_title', 'כל המוצרים לרכישה עכשיו'),
(1268, 225, '_home_serv_prod_item_0_block_item_small_title', 'field_60ec9a10781ac'),
(1269, 225, 'home_serv_prod_item_0_block_item_big_title', 'טכנולוגית מוצרים'),
(1270, 225, '_home_serv_prod_item_0_block_item_big_title', 'field_60ec9a22781ad'),
(1271, 225, 'home_serv_prod_item_0_block_item_link', 'a:3:{s:5:\"title\";s:23:\"מעבר למוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1272, 225, '_home_serv_prod_item_0_block_item_link', 'field_60ec9a33781ae'),
(1273, 225, 'home_serv_prod_item_1_block_item_img', '215'),
(1274, 225, '_home_serv_prod_item_1_block_item_img', 'field_60ec99f5781ab'),
(1275, 225, 'home_serv_prod_item_1_block_item_small_title', 'דואגים לכם ללמוד גם מהבית'),
(1276, 225, '_home_serv_prod_item_1_block_item_small_title', 'field_60ec9a10781ac'),
(1277, 225, 'home_serv_prod_item_1_block_item_big_title', 'מרחבי למידה'),
(1278, 225, '_home_serv_prod_item_1_block_item_big_title', 'field_60ec9a22781ad'),
(1279, 225, 'home_serv_prod_item_1_block_item_link', 'a:3:{s:5:\"title\";s:32:\"מעבר למרחבי למידה\";s:3:\"url\";s:23:\"https://www.google.com/\";s:6:\"target\";s:0:\"\";}'),
(1280, 225, '_home_serv_prod_item_1_block_item_link', 'field_60ec9a33781ae'),
(1281, 225, 'home_serv_prod_item', '2'),
(1282, 225, '_home_serv_prod_item', 'field_60ec99e5781aa'),
(1283, 225, 'home_search_text', '<h2>כל הטכנולוגיות במקום אחד! פשוט רק לבחור</h2>\r\nלא יודעים מה אתם צריכים? גורדון תעשה לכם סדר'),
(1284, 225, '_home_search_text', 'field_60ec9a6b8787f'),
(1285, 225, 'slider_banner_0_slider_images', 'a:3:{i:0;s:3:\"216\";i:1;s:3:\"217\";i:2;s:3:\"218\";}'),
(1286, 225, '_slider_banner_0_slider_images', 'field_60ec9adc87883'),
(1287, 225, 'slider_banner_1_slider_images', 'a:3:{i:0;s:3:\"146\";i:1;s:3:\"147\";i:2;s:3:\"209\";}'),
(1288, 225, '_slider_banner_1_slider_images', 'field_60ec9adc87883'),
(1289, 225, 'slider_banner', '2'),
(1290, 225, '_slider_banner', 'field_60ec9aae87881'),
(1291, 225, 'h_block_prod_text', '<h2>המוצרים אצלנו</h2>\r\n<h3>טכנולוגית גורדון למידה מחודש</h3>\r\nאצלנו בגורדון תוכלו להכיר את המוצרים שיקדמו אתכם להתקדם, הגיע הזמן לבחור גורדון, אתם מוזמנים לצפות בקטלוג המוצרים שלנו:'),
(1292, 225, '_h_block_prod_text', 'field_60ec9b18ac425'),
(1293, 225, 'h_block_prod_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1294, 225, '_h_block_prod_link', 'field_60ec9b71ac426'),
(1295, 225, 'h_block_prod_custom_0_prod_custom_img', '219'),
(1296, 225, '_h_block_prod_custom_0_prod_custom_img', 'field_60ec9b9eac428'),
(1297, 225, 'h_block_prod_custom_0_prod_custom_title', 'פיתוח מרחבי למידה אימרסיבי'),
(1298, 225, '_h_block_prod_custom_0_prod_custom_title', 'field_60ec9bb6ac429'),
(1299, 225, 'h_block_prod_custom_0_prod_custom_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:117:\"/product/%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-6/\";s:6:\"target\";s:0:\"\";}'),
(1300, 225, '_h_block_prod_custom_0_prod_custom_link', 'field_60ec9bc0ac42a'),
(1301, 225, 'h_block_prod_custom_1_prod_custom_img', '220'),
(1302, 225, '_h_block_prod_custom_1_prod_custom_img', 'field_60ec9b9eac428'),
(1303, 225, 'h_block_prod_custom_1_prod_custom_title', 'חדשנות וטכנולוגיות למידה'),
(1304, 225, '_h_block_prod_custom_1_prod_custom_title', 'field_60ec9bb6ac429'),
(1305, 225, 'h_block_prod_custom_1_prod_custom_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:117:\"/product/%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-5/\";s:6:\"target\";s:0:\"\";}'),
(1306, 225, '_h_block_prod_custom_1_prod_custom_link', 'field_60ec9bc0ac42a'),
(1307, 225, 'h_block_prod_custom_2_prod_custom_img', '221'),
(1308, 225, '_h_block_prod_custom_2_prod_custom_img', 'field_60ec9b9eac428'),
(1309, 225, 'h_block_prod_custom_2_prod_custom_title', ' חדשנות וטכנולוגיות למידה'),
(1310, 225, '_h_block_prod_custom_2_prod_custom_title', 'field_60ec9bb6ac429'),
(1311, 225, 'h_block_prod_custom_2_prod_custom_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:23:\"https://www.google.com/\";s:6:\"target\";s:0:\"\";}'),
(1312, 225, '_h_block_prod_custom_2_prod_custom_link', 'field_60ec9bc0ac42a'),
(1313, 225, 'h_block_prod_custom', '3'),
(1314, 225, '_h_block_prod_custom', 'field_60ec9b8aac427'),
(1315, 225, 'home_course_text', '<h2>קורסים אצלנו</h2>\r\n<h3>בגורדון שכדאי לכם להכיר</h3>\r\nאצלנו בגורדון תוכלו להכיר את המוצרים שיקדמו אתכם צעד לפני כולם לעולם טכנולוגי מרהיב מסוגו, בוא תוכלו להראות וללמד בצורה חדשנית ופורצת דרך, הגיע הזמן להתקדם, הגיע הזמן לבחור גורדון, אתם מוזמנים לצפות בקטלוג המוצרים שלנו:'),
(1316, 225, '_home_course_text', 'field_60ec9d062bc44'),
(1317, 225, 'home_course_img', '211');
INSERT INTO `fmn_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1318, 225, '_home_course_img', 'field_60ec9d252bc45'),
(1319, 225, 'home_course_item_0_course_img', '207'),
(1320, 225, '_home_course_item_0_course_img', 'field_60ec9d6e2bc47'),
(1321, 225, 'home_course_item_0_course_title', 'כאן תופיע כותרת 1'),
(1322, 225, '_home_course_item_0_course_title', 'field_60ec9d7a2bc48'),
(1323, 225, 'home_course_item_0_course_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:138:\"http://gordon:8888/2021/07/11/%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-2/\";s:6:\"target\";s:0:\"\";}'),
(1324, 225, '_home_course_item_0_course_link', 'field_60ec9d842bc49'),
(1325, 225, 'home_course_item_1_course_img', '208'),
(1326, 225, '_home_course_item_1_course_img', 'field_60ec9d6e2bc47'),
(1327, 225, 'home_course_item_1_course_title', 'כאן תופיע כותרת 2'),
(1328, 225, '_home_course_item_1_course_title', 'field_60ec9d7a2bc48'),
(1329, 225, 'home_course_item_1_course_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:138:\"http://gordon:8888/2021/07/11/%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-1/\";s:6:\"target\";s:0:\"\";}'),
(1330, 225, '_home_course_item_1_course_link', 'field_60ec9d842bc49'),
(1331, 225, 'home_course_item_2_course_img', '209'),
(1332, 225, '_home_course_item_2_course_img', 'field_60ec9d6e2bc47'),
(1333, 225, 'home_course_item_2_course_title', 'כאן תופיע כותרת  3'),
(1334, 225, '_home_course_item_2_course_title', 'field_60ec9d7a2bc48'),
(1335, 225, 'home_course_item_2_course_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:23:\"https://www.google.com/\";s:6:\"target\";s:0:\"\";}'),
(1336, 225, '_home_course_item_2_course_link', 'field_60ec9d842bc49'),
(1337, 225, 'home_course_item_3_course_img', '210'),
(1338, 225, '_home_course_item_3_course_img', 'field_60ec9d6e2bc47'),
(1339, 225, 'home_course_item_3_course_title', 'כאן תופיע כותרת 4'),
(1340, 225, '_home_course_item_3_course_title', 'field_60ec9d7a2bc48'),
(1341, 225, 'home_course_item_3_course_link', 'a:3:{s:5:\"title\";s:18:\"שלום עולם!\";s:3:\"url\";s:80:\"http://gordon:8888/2021/07/08/%d7%a9%d7%9c%d7%95%d7%9d-%d7%a2%d7%95%d7%9c%d7%9d/\";s:6:\"target\";s:0:\"\";}'),
(1342, 225, '_home_course_item_3_course_link', 'field_60ec9d842bc49'),
(1343, 225, 'home_course_item', '4'),
(1344, 225, '_home_course_item', 'field_60ec9d532bc46'),
(1345, 225, 'home_course_link', 'a:3:{s:5:\"title\";s:21:\"לכל הקורסים\";s:3:\"url\";s:55:\"/product-category/קטגורית-מוצרים-לטסט/\";s:6:\"target\";s:0:\"\";}'),
(1346, 225, '_home_course_link', 'field_60ec9d922bc4a'),
(1347, 225, 'home_prod_slider_text', '<h2>המוצרים החדשים אצלנו בגורדון לרכישה מיידית</h2>\r\nההצלחה שלך ההכנה שלנו בואו לבחור את המסלול המתאים לכם'),
(1348, 225, '_home_prod_slider_text', 'field_60ec9dc32bc4c'),
(1349, 225, 'home_prod_slider_link', 'a:3:{s:5:\"title\";s:21:\"לכל המוצרים\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1350, 225, '_home_prod_slider_link', 'field_60ec9dda2bc4d'),
(1351, 225, 'home_prod_slider', 'a:5:{i:0;s:2:\"61\";i:1;s:2:\"62\";i:2;s:2:\"63\";i:3;s:2:\"64\";i:4;s:2:\"65\";}'),
(1352, 225, '_home_prod_slider', 'field_60ec9df72bc4e'),
(1353, 225, 'slider_benefits_0_benefit_content', '<h2>צוות מומחים</h2>\r\nבמרכז לחדשנות צוות מומחים מקצועי ובעל ניסיון עשיר בארץ ובחו״ל, בתחום אסטרטגיות הוראה ולמידה, פדגוגיה דיגיטלית, חדשנות, חקר עתידים וחשיבה יצירתית'),
(1354, 225, '_slider_benefits_0_benefit_content', 'field_60ec9e4c2bc51'),
(1355, 225, 'slider_benefits_1_benefit_content', '<h2>נולום ארווס סאפיאן - פוסיליס קוויס</h2>\r\nאקווזמן להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(1356, 225, '_slider_benefits_1_benefit_content', 'field_60ec9e4c2bc51'),
(1357, 225, 'slider_benefits_2_benefit_content', '<h2>קולורס מונפרד אדנדום סילקוף</h2>\r\nמרגשי ומרגשח. עמחליף להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט'),
(1358, 225, '_slider_benefits_2_benefit_content', 'field_60ec9e4c2bc51'),
(1359, 225, 'slider_benefits_3_benefit_content', '<h2>קוואזי במר מודוף</h2>\r\nאודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.'),
(1360, 225, '_slider_benefits_3_benefit_content', 'field_60ec9e4c2bc51'),
(1361, 225, 'slider_benefits_4_benefit_content', '<h2>לורם איפסום</h2>\r\nהועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.'),
(1362, 225, '_slider_benefits_4_benefit_content', 'field_60ec9e4c2bc51'),
(1363, 225, 'slider_benefits', '5'),
(1364, 225, '_slider_benefits', 'field_60ec9e352bc50'),
(1365, 225, 'slider_benefits_img', '212'),
(1366, 225, '_slider_benefits_img', 'field_60ec9e5f2bc52'),
(1367, 225, 'slider_benefits_title', 'למה לבחור בגורדון ללמידה מחדש'),
(1368, 225, '_slider_benefits_title', 'field_60ecab1140ae5'),
(1369, 226, '_edit_last', '1'),
(1370, 226, '_edit_lock', '1626158106:1'),
(1371, 228, '_edit_last', '1'),
(1372, 228, '_edit_lock', '1626158160:1'),
(1373, 228, '_wp_page_template', 'views/gallery.php'),
(1374, 228, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(1375, 228, 'gallery_main', 'a:12:{i:0;s:3:\"219\";i:1;s:3:\"220\";i:2;s:3:\"221\";i:3;s:3:\"210\";i:4;s:3:\"209\";i:5;s:3:\"208\";i:6;s:3:\"213\";i:7;s:3:\"207\";i:8;s:3:\"163\";i:9;s:3:\"147\";i:10;s:2:\"35\";i:11;s:2:\"37\";}'),
(1376, 228, '_gallery_main', 'field_60ed346d48181'),
(1377, 228, 'title_tag', ''),
(1378, 228, '_title_tag', 'field_5ddbe7577e0e7'),
(1379, 228, 'single_slider_seo', ''),
(1380, 228, '_single_slider_seo', 'field_5ddbde5499115'),
(1381, 228, 'slider_img', ''),
(1382, 228, '_slider_img', 'field_60e988d9254ba'),
(1383, 229, 'gallery_main', ''),
(1384, 229, '_gallery_main', 'field_60ed346d48181'),
(1385, 229, 'title_tag', ''),
(1386, 229, '_title_tag', 'field_5ddbe7577e0e7'),
(1387, 229, 'single_slider_seo', ''),
(1388, 229, '_single_slider_seo', 'field_5ddbde5499115'),
(1389, 229, 'slider_img', ''),
(1390, 229, '_slider_img', 'field_60e988d9254ba'),
(1391, 230, 'gallery_main', 'a:12:{i:0;s:3:\"219\";i:1;s:3:\"220\";i:2;s:3:\"221\";i:3;s:3:\"210\";i:4;s:3:\"209\";i:5;s:3:\"208\";i:6;s:3:\"213\";i:7;s:3:\"207\";i:8;s:3:\"163\";i:9;s:3:\"147\";i:10;s:2:\"35\";i:11;s:2:\"37\";}'),
(1392, 230, '_gallery_main', 'field_60ed346d48181'),
(1393, 230, 'title_tag', ''),
(1394, 230, '_title_tag', 'field_5ddbe7577e0e7'),
(1395, 230, 'single_slider_seo', ''),
(1396, 230, '_single_slider_seo', 'field_5ddbde5499115'),
(1397, 230, 'slider_img', ''),
(1398, 230, '_slider_img', 'field_60e988d9254ba'),
(1399, 231, '_edit_last', '1'),
(1400, 231, '_wp_page_template', 'views/blog.php'),
(1401, 231, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(1402, 231, 'title_tag', ''),
(1403, 231, '_title_tag', 'field_5ddbe7577e0e7'),
(1404, 231, 'single_slider_seo', '2'),
(1405, 231, '_single_slider_seo', 'field_5ddbde5499115'),
(1406, 231, 'slider_img', '161'),
(1407, 231, '_slider_img', 'field_60e988d9254ba'),
(1408, 232, 'title_tag', ''),
(1409, 232, '_title_tag', 'field_5ddbe7577e0e7'),
(1410, 232, 'single_slider_seo', ''),
(1411, 232, '_single_slider_seo', 'field_5ddbde5499115'),
(1412, 232, 'slider_img', ''),
(1413, 232, '_slider_img', 'field_60e988d9254ba'),
(1414, 231, '_edit_lock', '1626175830:1'),
(1415, 233, '_menu_item_type', 'post_type'),
(1416, 233, '_menu_item_menu_item_parent', '0'),
(1417, 233, '_menu_item_object_id', '231'),
(1418, 233, '_menu_item_object', 'page'),
(1419, 233, '_menu_item_target', ''),
(1420, 233, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1421, 233, '_menu_item_xfn', ''),
(1422, 233, '_menu_item_url', ''),
(1424, 234, '_menu_item_type', 'post_type'),
(1425, 234, '_menu_item_menu_item_parent', '0'),
(1426, 234, '_menu_item_object_id', '228'),
(1427, 234, '_menu_item_object', 'page'),
(1428, 234, '_menu_item_target', ''),
(1429, 234, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1430, 234, '_menu_item_xfn', ''),
(1431, 234, '_menu_item_url', ''),
(1433, 125, '_wp_old_date', '2021-07-12'),
(1434, 72, '_wp_old_date', '2021-07-12'),
(1435, 73, '_wp_old_date', '2021-07-12'),
(1436, 74, '_wp_old_date', '2021-07-12'),
(1437, 75, '_wp_old_date', '2021-07-12'),
(1438, 77, '_wp_old_date', '2021-07-12'),
(1439, 235, '_edit_last', '1'),
(1440, 235, '_edit_lock', '1626162809:1'),
(1441, 1, '_edit_lock', '1626177007:1'),
(1442, 1, '_edit_last', '1'),
(1443, 1, '_thumbnail_id', '213'),
(1445, 1, '_yoast_wpseo_content_score', '60'),
(1446, 1, '_yoast_wpseo_estimated-reading-time-minutes', '1'),
(1447, 1, 'title_tag', ''),
(1448, 1, '_title_tag', 'field_5ddbe7577e0e7'),
(1449, 1, 'post_gallery', 'a:3:{i:0;s:3:\"210\";i:1;s:3:\"209\";i:2;s:3:\"147\";}'),
(1450, 1, '_post_gallery', 'field_60ed3c0bf26b4'),
(1451, 1, 'post_links_title', 'קישורים בנושא:'),
(1452, 1, '_post_links_title', 'field_60ed3dcef26b6'),
(1453, 1, 'post_link_item_0_link_title', 'כאן תופיע כותרת כאשר יש צורך בקישור חיצוני או פנימי לאתר 1'),
(1454, 1, '_post_link_item_0_link_title', 'field_60ed3decf26b8'),
(1455, 1, 'post_link_item_0_link_url', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:117:\"/product/%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-2/\";s:6:\"target\";s:0:\"\";}'),
(1456, 1, '_post_link_item_0_link_url', 'field_60ed3df3f26b9'),
(1457, 1, 'post_link_item_1_link_title', 'כאן תופיע כותרת כאשר יש צורך בקישור חיצוני או פנימי לאתר 2'),
(1458, 1, '_post_link_item_1_link_title', 'field_60ed3decf26b8'),
(1459, 1, 'post_link_item_1_link_url', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:120:\"/2021/07/11/%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-3/\";s:6:\"target\";s:0:\"\";}'),
(1460, 1, '_post_link_item_1_link_url', 'field_60ed3df3f26b9'),
(1461, 1, 'post_link_item', '2'),
(1462, 1, '_post_link_item', 'field_60ed3ddef26b7'),
(1463, 1, 'single_slider_seo', ''),
(1464, 1, '_single_slider_seo', 'field_5ddbde5499115'),
(1465, 1, 'slider_img', ''),
(1466, 1, '_slider_img', 'field_60e988d9254ba'),
(1467, 243, 'title_tag', ''),
(1468, 243, '_title_tag', 'field_5ddbe7577e0e7'),
(1469, 243, 'post_gallery', 'a:3:{i:0;s:3:\"210\";i:1;s:3:\"209\";i:2;s:3:\"147\";}'),
(1470, 243, '_post_gallery', 'field_60ed3c0bf26b4'),
(1471, 243, 'post_links_title', 'קישורים בנושא:'),
(1472, 243, '_post_links_title', 'field_60ed3dcef26b6'),
(1473, 243, 'post_link_item_0_link_title', 'כאן תופיע כותרת כאשר יש צורך בקישור חיצוני או פנימי לאתר 1'),
(1474, 243, '_post_link_item_0_link_title', 'field_60ed3decf26b8'),
(1475, 243, 'post_link_item_0_link_url', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:117:\"/product/%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-2/\";s:6:\"target\";s:0:\"\";}'),
(1476, 243, '_post_link_item_0_link_url', 'field_60ed3df3f26b9'),
(1477, 243, 'post_link_item_1_link_title', 'כאן תופיע כותרת כאשר יש צורך בקישור חיצוני או פנימי לאתר 2'),
(1478, 243, '_post_link_item_1_link_title', 'field_60ed3decf26b8'),
(1479, 243, 'post_link_item_1_link_url', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:120:\"/2021/07/11/%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-3/\";s:6:\"target\";s:0:\"\";}'),
(1480, 243, '_post_link_item_1_link_url', 'field_60ed3df3f26b9'),
(1481, 243, 'post_link_item', '2'),
(1482, 243, '_post_link_item', 'field_60ed3ddef26b7'),
(1483, 243, 'single_slider_seo', ''),
(1484, 243, '_single_slider_seo', 'field_5ddbde5499115'),
(1485, 243, 'slider_img', ''),
(1486, 243, '_slider_img', 'field_60e988d9254ba'),
(1488, 1, 'same_title', ''),
(1489, 1, '_same_title', 'field_60ed46e077c78'),
(1490, 1, 'same_posts', ''),
(1491, 1, '_same_posts', 'field_60ed46ea77c79'),
(1492, 243, 'same_title', ''),
(1493, 243, '_same_title', 'field_60ed46e077c78'),
(1494, 243, 'same_posts', ''),
(1495, 243, '_same_posts', 'field_60ed46ea77c79'),
(1496, 250, '_wp_attached_file', '2021/07/girls.png'),
(1497, 250, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:395;s:6:\"height\";i:531;s:4:\"file\";s:17:\"2021/07/girls.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"girls-223x300.png\";s:5:\"width\";i:223;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"girls-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:15:\"girls-70x94.png\";s:5:\"width\";i:70;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"girls-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"girls-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"girls-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"girls-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1498, 52, '_yoast_wpseo_primary_prod_age', '33'),
(1499, 52, '_yoast_wpseo_primary_prod_tech', '30'),
(1500, 52, '_yoast_wpseo_primary_prod_area', '27'),
(1501, 61, 'price_per_100', ''),
(1502, 61, 'prod_slider_seo', ''),
(1503, 61, '_prod_slider_seo', 'field_60ec1872365c6'),
(1504, 61, 'prod_slider_img', ''),
(1505, 61, '_prod_slider_img', 'field_60ec18857fa27'),
(1506, 61, 'prod_slider_title', ''),
(1507, 61, '_prod_slider_title', 'field_60ec18943ddf7'),
(1508, 61, 'prod_slider_text', ''),
(1509, 61, '_prod_slider_text', 'field_60ec18b23ddf8'),
(1510, 61, 'prod_slider_link', ''),
(1511, 61, '_prod_slider_link', 'field_60ec18d93ddf9'),
(1512, 61, 'info_tabs', ''),
(1513, 61, '_info_tabs', 'field_60ec00e97c9df'),
(1514, 61, 'related_products_title', ''),
(1515, 61, '_related_products_title', 'field_60ec06ab6175e'),
(1516, 61, 'related_products_text', ''),
(1517, 61, '_related_products_text', 'field_60ec06bdda96b'),
(1518, 61, 'related_products', ''),
(1519, 61, '_related_products', 'field_60ec05bfcd042'),
(1520, 61, '_product_video', ''),
(1521, 61, '_yoast_wpseo_primary_brand', '26'),
(1522, 61, '_yoast_wpseo_primary_prod_age', '34'),
(1523, 61, '_yoast_wpseo_primary_prod_tech', '31'),
(1524, 61, '_yoast_wpseo_primary_prod_area', '28'),
(1525, 61, '_yoast_wpseo_content_score', '30'),
(1526, 61, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(1527, 62, 'price_per_100', ''),
(1528, 62, 'prod_slider_seo', ''),
(1529, 62, '_prod_slider_seo', 'field_60ec1872365c6'),
(1530, 62, 'prod_slider_img', ''),
(1531, 62, '_prod_slider_img', 'field_60ec18857fa27'),
(1532, 62, 'prod_slider_title', ''),
(1533, 62, '_prod_slider_title', 'field_60ec18943ddf7'),
(1534, 62, 'prod_slider_text', ''),
(1535, 62, '_prod_slider_text', 'field_60ec18b23ddf8'),
(1536, 62, 'prod_slider_link', ''),
(1537, 62, '_prod_slider_link', 'field_60ec18d93ddf9'),
(1538, 62, 'info_tabs', ''),
(1539, 62, '_info_tabs', 'field_60ec00e97c9df'),
(1540, 62, 'related_products_title', ''),
(1541, 62, '_related_products_title', 'field_60ec06ab6175e'),
(1542, 62, 'related_products_text', ''),
(1543, 62, '_related_products_text', 'field_60ec06bdda96b'),
(1544, 62, 'related_products', ''),
(1545, 62, '_related_products', 'field_60ec05bfcd042'),
(1546, 62, '_product_video', ''),
(1547, 62, '_yoast_wpseo_primary_brand', '25'),
(1548, 62, '_yoast_wpseo_primary_prod_age', '35'),
(1549, 62, '_yoast_wpseo_primary_prod_tech', '32'),
(1550, 62, '_yoast_wpseo_primary_prod_area', '29'),
(1551, 62, '_yoast_wpseo_primary_product_cat', '24'),
(1552, 62, '_yoast_wpseo_content_score', '30'),
(1553, 62, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(1554, 63, 'prod_slider_seo', ''),
(1555, 63, '_prod_slider_seo', 'field_60ec1872365c6'),
(1556, 63, 'prod_slider_img', ''),
(1557, 63, '_prod_slider_img', 'field_60ec18857fa27'),
(1558, 63, 'prod_slider_title', ''),
(1559, 63, '_prod_slider_title', 'field_60ec18943ddf7'),
(1560, 63, 'prod_slider_text', ''),
(1561, 63, '_prod_slider_text', 'field_60ec18b23ddf8'),
(1562, 63, 'prod_slider_link', ''),
(1563, 63, '_prod_slider_link', 'field_60ec18d93ddf9'),
(1564, 63, 'info_tabs', ''),
(1565, 63, '_info_tabs', 'field_60ec00e97c9df'),
(1566, 63, 'related_products_title', ''),
(1567, 63, '_related_products_title', 'field_60ec06ab6175e'),
(1568, 63, 'related_products_text', ''),
(1569, 63, '_related_products_text', 'field_60ec06bdda96b'),
(1570, 63, 'related_products', ''),
(1571, 63, '_related_products', 'field_60ec05bfcd042'),
(1572, 63, '_yoast_wpseo_primary_prod_age', '36'),
(1573, 63, '_yoast_wpseo_primary_prod_tech', '30'),
(1574, 63, '_yoast_wpseo_primary_prod_area', '28'),
(1575, 63, '_yoast_wpseo_content_score', '30'),
(1576, 63, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(1577, 64, 'price_per_100', ''),
(1578, 64, 'prod_slider_seo', ''),
(1579, 64, '_prod_slider_seo', 'field_60ec1872365c6'),
(1580, 64, 'prod_slider_img', ''),
(1581, 64, '_prod_slider_img', 'field_60ec18857fa27'),
(1582, 64, 'prod_slider_title', ''),
(1583, 64, '_prod_slider_title', 'field_60ec18943ddf7'),
(1584, 64, 'prod_slider_text', ''),
(1585, 64, '_prod_slider_text', 'field_60ec18b23ddf8'),
(1586, 64, 'prod_slider_link', ''),
(1587, 64, '_prod_slider_link', 'field_60ec18d93ddf9'),
(1588, 64, 'info_tabs', ''),
(1589, 64, '_info_tabs', 'field_60ec00e97c9df'),
(1590, 64, 'related_products_title', ''),
(1591, 64, '_related_products_title', 'field_60ec06ab6175e'),
(1592, 64, 'related_products_text', ''),
(1593, 64, '_related_products_text', 'field_60ec06bdda96b'),
(1594, 64, 'related_products', ''),
(1595, 64, '_related_products', 'field_60ec05bfcd042'),
(1596, 64, '_product_video', ''),
(1597, 64, '_yoast_wpseo_primary_prod_age', '36'),
(1598, 64, '_yoast_wpseo_primary_prod_tech', '31'),
(1599, 64, '_yoast_wpseo_primary_prod_area', '27'),
(1600, 64, '_yoast_wpseo_content_score', '30'),
(1601, 64, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(1602, 65, 'price_per_100', ''),
(1603, 65, 'prod_slider_seo', ''),
(1604, 65, '_prod_slider_seo', 'field_60ec1872365c6'),
(1605, 65, 'prod_slider_img', ''),
(1606, 65, '_prod_slider_img', 'field_60ec18857fa27'),
(1607, 65, 'prod_slider_title', ''),
(1608, 65, '_prod_slider_title', 'field_60ec18943ddf7'),
(1609, 65, 'prod_slider_text', ''),
(1610, 65, '_prod_slider_text', 'field_60ec18b23ddf8'),
(1611, 65, 'prod_slider_link', ''),
(1612, 65, '_prod_slider_link', 'field_60ec18d93ddf9'),
(1613, 65, 'info_tabs', ''),
(1614, 65, '_info_tabs', 'field_60ec00e97c9df'),
(1615, 65, 'related_products_title', ''),
(1616, 65, '_related_products_title', 'field_60ec06ab6175e'),
(1617, 65, 'related_products_text', ''),
(1618, 65, '_related_products_text', 'field_60ec06bdda96b'),
(1619, 65, 'related_products', ''),
(1620, 65, '_related_products', 'field_60ec05bfcd042'),
(1621, 65, '_product_video', ''),
(1622, 65, '_yoast_wpseo_primary_prod_age', '34'),
(1623, 65, '_yoast_wpseo_primary_prod_tech', '31'),
(1624, 65, '_yoast_wpseo_primary_prod_area', '27'),
(1625, 65, '_yoast_wpseo_content_score', '30'),
(1626, 65, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(1627, 66, 'price_per_100', ''),
(1628, 66, 'prod_slider_seo', ''),
(1629, 66, '_prod_slider_seo', 'field_60ec1872365c6'),
(1630, 66, 'prod_slider_img', ''),
(1631, 66, '_prod_slider_img', 'field_60ec18857fa27'),
(1632, 66, 'prod_slider_title', ''),
(1633, 66, '_prod_slider_title', 'field_60ec18943ddf7'),
(1634, 66, 'prod_slider_text', ''),
(1635, 66, '_prod_slider_text', 'field_60ec18b23ddf8'),
(1636, 66, 'prod_slider_link', ''),
(1637, 66, '_prod_slider_link', 'field_60ec18d93ddf9'),
(1638, 66, 'info_tabs', ''),
(1639, 66, '_info_tabs', 'field_60ec00e97c9df'),
(1640, 66, 'related_products_title', ''),
(1641, 66, '_related_products_title', 'field_60ec06ab6175e'),
(1642, 66, 'related_products_text', ''),
(1643, 66, '_related_products_text', 'field_60ec06bdda96b'),
(1644, 66, 'related_products', ''),
(1645, 66, '_related_products', 'field_60ec05bfcd042'),
(1646, 66, '_product_video', ''),
(1647, 66, '_yoast_wpseo_primary_prod_age', '33'),
(1648, 66, '_yoast_wpseo_primary_prod_tech', '31'),
(1649, 66, '_yoast_wpseo_primary_prod_area', '28'),
(1650, 66, '_yoast_wpseo_content_score', '30'),
(1651, 66, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(1652, 231, 'single_slider_seo_0_content', '<h2>לורם איפסום 111</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1653, 231, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(1654, 231, 'single_slider_seo_1_content', '<h2>לורם איפסום 222</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1655, 231, '_single_slider_seo_1_content', 'field_5ddbde7399116'),
(1656, 252, 'title_tag', ''),
(1657, 252, '_title_tag', 'field_5ddbe7577e0e7'),
(1658, 252, 'single_slider_seo', '2'),
(1659, 252, '_single_slider_seo', 'field_5ddbde5499115'),
(1660, 252, 'slider_img', '161'),
(1661, 252, '_slider_img', 'field_60e988d9254ba'),
(1662, 252, 'single_slider_seo_0_content', '<h2>לורם איפסום 111</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1663, 252, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(1664, 252, 'single_slider_seo_1_content', '<h2>לורם איפסום 222</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1665, 252, '_single_slider_seo_1_content', 'field_5ddbde7399116'),
(1666, 7, '_edit_last', '1'),
(1667, 7, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(1668, 7, 'title_tag', ''),
(1669, 7, '_title_tag', 'field_5ddbe7577e0e7'),
(1670, 7, 'single_slider_seo', ''),
(1671, 7, '_single_slider_seo', 'field_5ddbde5499115'),
(1672, 7, 'slider_img', ''),
(1673, 7, '_slider_img', 'field_60e988d9254ba'),
(1674, 253, 'title_tag', ''),
(1675, 253, '_title_tag', 'field_5ddbe7577e0e7'),
(1676, 253, 'single_slider_seo', ''),
(1677, 253, '_single_slider_seo', 'field_5ddbde5499115'),
(1678, 253, 'slider_img', ''),
(1679, 253, '_slider_img', 'field_60e988d9254ba'),
(1681, 47, 'post_gallery', ''),
(1682, 47, '_post_gallery', 'field_60ed3c0bf26b4'),
(1683, 47, 'post_links_title', ''),
(1684, 47, '_post_links_title', 'field_60ed3dcef26b6'),
(1685, 47, 'post_link_item', ''),
(1686, 47, '_post_link_item', 'field_60ed3ddef26b7'),
(1687, 47, 'same_title', ''),
(1688, 47, '_same_title', 'field_60ed46e077c78'),
(1689, 47, 'same_posts', ''),
(1690, 47, '_same_posts', 'field_60ed46ea77c79'),
(1691, 48, 'post_gallery', ''),
(1692, 48, '_post_gallery', 'field_60ed3c0bf26b4'),
(1693, 48, 'post_links_title', ''),
(1694, 48, '_post_links_title', 'field_60ed3dcef26b6'),
(1695, 48, 'post_link_item', ''),
(1696, 48, '_post_link_item', 'field_60ed3ddef26b7'),
(1697, 48, 'same_title', ''),
(1698, 48, '_same_title', 'field_60ed46e077c78'),
(1699, 48, 'same_posts', ''),
(1700, 48, '_same_posts', 'field_60ed46ea77c79'),
(1701, 47, '_yoast_wpseo_content_score', '60'),
(1702, 47, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(1704, 45, 'post_gallery', ''),
(1705, 45, '_post_gallery', 'field_60ed3c0bf26b4'),
(1706, 45, 'post_links_title', ''),
(1707, 45, '_post_links_title', 'field_60ed3dcef26b6'),
(1708, 45, 'post_link_item', ''),
(1709, 45, '_post_link_item', 'field_60ed3ddef26b7'),
(1710, 45, 'same_title', ''),
(1711, 45, '_same_title', 'field_60ed46e077c78'),
(1712, 45, 'same_posts', ''),
(1713, 45, '_same_posts', 'field_60ed46ea77c79'),
(1714, 46, 'post_gallery', ''),
(1715, 46, '_post_gallery', 'field_60ed3c0bf26b4'),
(1716, 46, 'post_links_title', ''),
(1717, 46, '_post_links_title', 'field_60ed3dcef26b6'),
(1718, 46, 'post_link_item', ''),
(1719, 46, '_post_link_item', 'field_60ed3ddef26b7'),
(1720, 46, 'same_title', ''),
(1721, 46, '_same_title', 'field_60ed46e077c78'),
(1722, 46, 'same_posts', ''),
(1723, 46, '_same_posts', 'field_60ed46ea77c79'),
(1724, 45, '_yoast_wpseo_content_score', '60'),
(1725, 45, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(1727, 43, 'post_gallery', ''),
(1728, 43, '_post_gallery', 'field_60ed3c0bf26b4'),
(1729, 43, 'post_links_title', ''),
(1730, 43, '_post_links_title', 'field_60ed3dcef26b6'),
(1731, 43, 'post_link_item', ''),
(1732, 43, '_post_link_item', 'field_60ed3ddef26b7'),
(1733, 43, 'same_title', ''),
(1734, 43, '_same_title', 'field_60ed46e077c78'),
(1735, 43, 'same_posts', ''),
(1736, 43, '_same_posts', 'field_60ed46ea77c79'),
(1737, 44, 'post_gallery', ''),
(1738, 44, '_post_gallery', 'field_60ed3c0bf26b4'),
(1739, 44, 'post_links_title', ''),
(1740, 44, '_post_links_title', 'field_60ed3dcef26b6'),
(1741, 44, 'post_link_item', ''),
(1742, 44, '_post_link_item', 'field_60ed3ddef26b7'),
(1743, 44, 'same_title', ''),
(1744, 44, '_same_title', 'field_60ed46e077c78'),
(1745, 44, 'same_posts', ''),
(1746, 44, '_same_posts', 'field_60ed46ea77c79'),
(1747, 43, '_yoast_wpseo_content_score', '60'),
(1748, 43, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(1750, 41, 'post_gallery', ''),
(1751, 41, '_post_gallery', 'field_60ed3c0bf26b4'),
(1752, 41, 'post_links_title', ''),
(1753, 41, '_post_links_title', 'field_60ed3dcef26b6'),
(1754, 41, 'post_link_item', ''),
(1755, 41, '_post_link_item', 'field_60ed3ddef26b7'),
(1756, 41, 'same_title', ''),
(1757, 41, '_same_title', 'field_60ed46e077c78'),
(1758, 41, 'same_posts', ''),
(1759, 41, '_same_posts', 'field_60ed46ea77c79'),
(1760, 42, 'post_gallery', ''),
(1761, 42, '_post_gallery', 'field_60ed3c0bf26b4'),
(1762, 42, 'post_links_title', ''),
(1763, 42, '_post_links_title', 'field_60ed3dcef26b6'),
(1764, 42, 'post_link_item', ''),
(1765, 42, '_post_link_item', 'field_60ed3ddef26b7'),
(1766, 42, 'same_title', ''),
(1767, 42, '_same_title', 'field_60ed46e077c78'),
(1768, 42, 'same_posts', ''),
(1769, 42, '_same_posts', 'field_60ed46ea77c79'),
(1770, 41, '_yoast_wpseo_content_score', '60'),
(1771, 41, '_yoast_wpseo_estimated-reading-time-minutes', ''),
(1773, 32, 'post_gallery', ''),
(1774, 32, '_post_gallery', 'field_60ed3c0bf26b4'),
(1775, 32, 'post_links_title', ''),
(1776, 32, '_post_links_title', 'field_60ed3dcef26b6'),
(1777, 32, 'post_link_item', ''),
(1778, 32, '_post_link_item', 'field_60ed3ddef26b7'),
(1779, 32, 'same_title', ''),
(1780, 32, '_same_title', 'field_60ed46e077c78'),
(1781, 32, 'same_posts', ''),
(1782, 32, '_same_posts', 'field_60ed46ea77c79'),
(1783, 40, 'post_gallery', ''),
(1784, 40, '_post_gallery', 'field_60ed3c0bf26b4'),
(1785, 40, 'post_links_title', ''),
(1786, 40, '_post_links_title', 'field_60ed3dcef26b6'),
(1787, 40, 'post_link_item', ''),
(1788, 40, '_post_link_item', 'field_60ed3ddef26b7'),
(1789, 40, 'same_title', ''),
(1790, 40, '_same_title', 'field_60ed46e077c78'),
(1791, 40, 'same_posts', ''),
(1792, 40, '_same_posts', 'field_60ed46ea77c79'),
(1793, 32, '_yoast_wpseo_content_score', '30'),
(1794, 32, '_yoast_wpseo_estimated-reading-time-minutes', '');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_posts`
--

CREATE TABLE `fmn_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_posts`
--

INSERT INTO `fmn_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2021-07-08 20:04:37', '2021-07-08 17:04:37', '<!-- wp:paragraph -->\r\n<p>לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה.</p>\r\n<h2>כאן תופיע כותרת משנה או תת כותרת נוספת</h2>\r\n<p>לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה.</p>\r\n<!-- /wp:paragraph -->', 'שלום עולם! TEST', '', 'publish', 'open', 'open', '', '%d7%a9%d7%9c%d7%95%d7%9d-%d7%a2%d7%95%d7%9c%d7%9d', '', '', '2021-07-13 14:50:24', '2021-07-13 11:50:24', '', 0, 'http://gordon:8888/?p=1', 0, 'post', '', 1),
(2, 1, '2021-07-08 20:04:37', '2021-07-08 17:04:37', '<!-- wp:paragraph -->\n<p>זהו עמוד לדוגמה. הוא שונה מפוסט רגיל בבלוג מכיוון שהוא יישאר במקום אחד ויופיע בתפריט הניווט באתר (ברוב התבניות). רוב האנשים מתחילים עם עמוד אודות המציג אותם למבקרים חדשים באתר. הנה תוכן לדוגמה:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>אהלן! אני סטודנט לכלכלה ביום, אך מבלה את הלילות בהגשמת החלום שלי להיות שחקן תאטרון. אני גר ביפו, יש לי כלב נהדר בשם שוקו, אני אוהב ערק אשכוליות בשישי בצהריים (במיוחד תוך כדי משחק שש-בש על חוף הים). ברוכים הבאים לאתר שלי!</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>... או משהו כזה:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>החברה א.א. שומדברים נוסדה בשנת 1971, והיא מספקת שומדברים איכותיים לציבור מאז. א.א. שומדברים ממוקמת בעיר תקוות-ים, מעסיקה מעל 2,000 אנשים ועושה כל מיני דברים מדהים עבור הקהילה התקוות-ימית.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>כמשתמש וורדפרס חדש, כדאי לבקר <a href=\"http://gordon:8888/wp-admin/\">בלוח הבקרה שלך</a> כדי למחוק את העמוד הזה, וליצור עמודים חדשים עבור התוכן שלך. שיהיה בכיף!</p>\n<!-- /wp:paragraph -->', 'עמוד לדוגמא', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2021-07-08 20:04:37', '2021-07-08 17:04:37', '', 0, 'http://gordon:8888/?page_id=2', 0, 'page', '', 0),
(3, 1, '2021-07-08 20:04:37', '2021-07-08 17:04:37', '<!-- wp:heading --><h2>מי אנחנו</h2><!-- /wp:heading --><!-- wp:paragraph --><p>כתובת האתר שלנו היא: http://gordon:8888.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>מה המידע האישי שאנו אוספים ומדוע אנחנו אוספים אותו</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>תגובות</h3><!-- /wp:heading --><!-- wp:paragraph --><p>כאשר המבקרים משאירים תגובות באתר אנו אוספים את הנתונים המוצגים בטופס התגובה, ובנוסף גם את כתובת ה-IP של המבקר, ואת מחרוזת ה-user agent של הדפדפן שלו כדי לסייע בזיהוי תגובות זבל.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>יתכן ונעביר מחרוזת אנונימית שנוצרה מכתובת הדואר האלקטרוני שלך (הנקראת גם hash) לשירות Gravatar כדי לראות אם הנך חבר/ה בשירות. מדיניות הפרטיות של שירות Gravatar זמינה כאן: https://automattic.com/privacy/. לאחר אישור התגובה שלך, תמונת הפרופיל שלך גלויה לציבור בהקשר של התגובה שלך.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>מדיה</h3><!-- /wp:heading --><!-- wp:paragraph --><p>בהעלאה של תמונות לאתר, מומלץ להימנע מהעלאת תמונות עם נתוני מיקום מוטבעים (EXIF GPS). המבקרים באתר יכולים להוריד ולחלץ את כל נתוני מיקום מהתמונות באתר.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>טפסי יצירת קשר</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>עוגיות</h3><!-- /wp:heading --><!-- wp:paragraph --><p>בכתיבת תגובה באתר שלנו, באפשרותך להחליט אם לאפשר לנו לשמור את השם שלך, כתובת האימייל שלך וכתובת האתר שלך בקבצי עוגיות (cookies). השמירה תתבצע לנוחיותך, על מנת שלא יהיה צורך למלא את הפרטים שלך שוב בכתיבת תגובה נוספת. קבצי העוגיות ישמרו לשנה.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>אם אתה מבקר בעמוד ההתחברות של האתר, נגדיר קובץ עוגיה זמני על מנת לקבוע האם הדפדפן שלך מקבל קבצי עוגיות. קובץ עוגיה זה אינו מכיל נתונים אישיים והוא נמחק בעת סגירת הדפדפן.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>כאשר תתחבר, אנחנו גם נגדיר מספר \'עוגיות\' על מנת לשמור את פרטי ההתחברות שלך ואת בחירות התצוגה שלך. עוגיות התחברות תקפות ליומיים, ועוגיות אפשרויות מסך תקפות לשנה. אם תבחר באפשרות &quot;זכור אותי&quot;, פרטי ההתחברות שלך יהיו תקפים למשך שבועיים. אם תתנתק מהחשבון שלך, עוגיות ההתחברות יימחקו.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>אם אתה עורך או מפרסם מאמר, קובץ \'עוגיה\' נוסף יישמר בדפדפן שלך. קובץ \'עוגיה\' זה אינו כולל נתונים אישיים ופשוט מציין את מזהה הפוסט של המאמר. הוא יפוג לאחר יום אחד.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>תוכן מוטמע מאתרים אחרים</h3><!-- /wp:heading --><!-- wp:paragraph --><p>כתבות או פוסטים באתר זה עשויים לכלול תוכן מוטבע (לדוגמה, קטעי וידאו, תמונות, מאמרים, וכו\'). תוכן מוטבע מאתרי אינטרנט אחרים דינו כביקור הקורא באתרי האינטרנט מהם מוטבע התוכן.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>אתרים אלו עשויים לאסוף נתונים אודותיך, להשתמש בקבצי \'עוגיות\', להטמיע מעקב של צד שלישי נוסף, ולנטר את האינטראקציה שלך עם תוכן מוטמע זה, לרבות מעקב אחר האינטראקציה שלך עם התוכן המוטמע, אם יש לך חשבון ואתה מחובר לאתר זה.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>אנליטיקה</h3><!-- /wp:heading --><!-- wp:heading --><h2>עם מי אנו חולקים את המידע שלך</h2><!-- /wp:heading --><!-- wp:heading --><h2>משך הזמן בו נשמור את המידע שלך</h2><!-- /wp:heading --><!-- wp:paragraph --><p>במידה ותגיב/י על תוכן באתר, התגובה והנתונים אודותיה יישמרו ללא הגבלת זמן, כדי שנוכל לזהות ולאשר את כל התגובות העוקבות באופן אוטומטי.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>עבור משתמשים רשומים באתר (במידה ויש כאלה) אנו מאחסנים גם את המידע האישי שהם מספקים בפרופיל המשתמש שלהם. כל המשתמשים יכולים לראות, לערוך או למחוק את המידע האישי שלהם בכל עת (פרט לשם המשתמש אותו לא ניתן לשנות). גם מנהלי האתר יכולים לראות ולערוך מידע זה.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>אילו זכויות יש לך על המידע שלך</h2><!-- /wp:heading --><!-- wp:paragraph --><p>אם יש לך חשבון באתר זה, או שהשארת תגובות באתר, באפשרותך לבקש לקבל קובץ של הנתונים האישיים שאנו מחזיקים לגביך, כולל כל הנתונים שסיפקת לנו. באפשרותך גם לבקש שנמחק כל מידע אישי שאנו מחזיקים לגביך. הדבר אינו כולל נתונים שאנו מחויבים לשמור למטרות מנהליות, משפטיות או ביטחוניות.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>להיכן אנו שולחים את המידע שלך</h2><!-- /wp:heading --><!-- wp:paragraph --><p>תגובות מבקרים עלולות להיבדק על ידי שירות אוטומטי למניעת תגובות זבל.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>פרטי ההתקשרות שלך</h2><!-- /wp:heading --><!-- wp:heading --><h2>מידע נוסף</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>כיצד אנו מגינים על המידע שלך</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>אילו הליכי פרצת נתונים יש לנו</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>מאילו גופי צד-שלישי אנו מקבלים מידע</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>קבלת החלטות אוטומטיות ו/או יצירת פרופילים שאנו עושים עם נתוני המשתמשים שלנו</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>דרישות גילוי רגולטוריות של התעשייה </h3><!-- /wp:heading -->', 'מדיניות פרטיות', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2021-07-08 20:04:37', '2021-07-08 17:04:37', '', 0, 'http://gordon:8888/?page_id=3', 0, 'page', '', 0),
(4, 1, '2021-07-08 20:04:48', '0000-00-00 00:00:00', '', 'טיוטה משמירה אוטומטית', '', 'auto-draft', 'open', 'open', '', '', '', '', '2021-07-08 20:04:48', '0000-00-00 00:00:00', '', 0, 'http://gordon:8888/?p=4', 0, 'post', '', 0),
(6, 1, '2021-07-10 13:43:35', '2021-07-10 10:43:35', '', 'woocommerce-placeholder', '', 'inherit', 'open', 'closed', '', 'woocommerce-placeholder', '', '', '2021-07-10 13:43:35', '2021-07-10 10:43:35', '', 0, 'http://gordon:8888/wp-content/uploads/2021/07/woocommerce-placeholder.png', 0, 'attachment', 'image/png', 0),
(7, 1, '2021-07-10 13:43:36', '2021-07-10 10:43:36', '', 'חנות', '', 'publish', 'closed', 'closed', '', 'shop', '', '', '2021-07-13 14:50:11', '2021-07-13 11:50:11', '', 0, 'http://gordon:8888/shop/', 0, 'page', '', 0),
(8, 1, '2021-07-10 13:43:36', '2021-07-10 10:43:36', '<!-- wp:shortcode -->[woocommerce_cart]<!-- /wp:shortcode -->', 'Cart', '', 'publish', 'closed', 'closed', '', 'cart', '', '', '2021-07-10 13:43:36', '2021-07-10 10:43:36', '', 0, 'http://gordon:8888/cart/', 0, 'page', '', 0),
(9, 1, '2021-07-10 13:43:36', '2021-07-10 10:43:36', '<!-- wp:shortcode -->[woocommerce_checkout]<!-- /wp:shortcode -->', 'Checkout', '', 'publish', 'closed', 'closed', '', 'checkout', '', '', '2021-07-10 13:43:36', '2021-07-10 10:43:36', '', 0, 'http://gordon:8888/checkout/', 0, 'page', '', 0),
(10, 1, '2021-07-10 13:43:36', '2021-07-10 10:43:36', '<!-- wp:shortcode -->[woocommerce_my_account]<!-- /wp:shortcode -->', 'My account', '', 'publish', 'closed', 'closed', '', 'my-account', '', '', '2021-07-10 13:43:36', '2021-07-10 10:43:36', '', 0, 'http://gordon:8888/my-account/', 0, 'page', '', 0),
(11, 1, '2021-07-10 13:50:23', '2021-07-10 10:50:23', '<div class=\"form-row d-flex justify-content-end align-items-stretch\">\r\n  <div class=\"col-xl-3 col-md-6 col-12\">\r\n		[text* text-779 placeholder \"שם:\"]\r\n  </div>\r\n    <div class=\"col-xl-3 col-md-6 col-12\">\r\n			[tel* tel-905 placeholder \"טלפון:\"]\r\n  </div>\r\n    <div class=\"col-xl-6 col-12\">\r\n			[submit \"כן אני צריך למידה חדשה ועדכנית\"]\r\n	</div>\r\n</div>\n1\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@gordon>\n[_site_admin_email]\nמאת: [your-name] <[your-email]>\r\nנושא: [your-subject]\r\n\r\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [your-email]\n\n\n\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@gordon>\n[your-email]\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [_site_admin_email]\n\n\n\nההודעה נשלחה.\nארעה שגיאה בשליחת ההודעה.\nקיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\nארעה שגיאה בשליחת ההודעה.\nעליך להסכים לתנאים לפני שליחת ההודעה.\nזהו שדה חובה.\nהשדה ארוך מדי.\nהשדה קצר מדי.\nשגיאה לא ידועה בהעלאת הקובץ.\nאין לך הרשאות להעלות קבצים בפורמט זה.\nהקובץ גדול מדי.\nשגיאה בהעלאת הקובץ.\nשדה התאריך אינו נכון.\nהתאריך מוקדם מהתאריך המותר.\nהתאריך מאוחר מהתאריך המותר.\nפורמט המספר אינו תקין.\nהמספר קטן מהמינימום המותר.\nהמספר גדול מהמקסימום המותר.\nהתשובה לשאלת הביטחון אינה נכונה.\nכתובת האימייל שהוזנה אינה תקינה.\nהקישור אינו תקין.\nמספר הטלפון אינו תקין.', 'פוטר', '', 'publish', 'closed', 'closed', '', '%d7%a4%d7%95%d7%98%d7%a8', '', '', '2021-07-11 14:03:36', '2021-07-11 11:03:36', '', 0, 'http://gordon:8888/?post_type=wpcf7_contact_form&#038;p=11', 0, 'wpcf7_contact_form', '', 0),
(12, 1, '2021-07-10 13:54:01', '2021-07-10 10:54:01', '<div class=\"form-row d-flex justify-content-end align-items-stretch\">\r\n  <div class=\"col-xl-4 col-md-6 col-12\">\r\n		[text* text-383 placeholder \"שם:\"]\r\n  </div>\r\n    <div class=\"col-xl-4 col-md-6 col-12\">\r\n			[tel* tel-459 placeholder \"טלפון:\"]\r\n  </div>\r\n	    <div class=\"col-xl-4 col-md-6 col-12\">\r\n				[email* email-209 placeholder \"מייל:\"]\r\n  </div>\r\n	 <div class=\"col-xl-4 col-none\">\r\n  </div>\r\n		   <div class=\"col-xl-4 col-md-6 col-12\">\r\n				[select menu-576 \"סיבת הפניה\"]\r\n	</div>\r\n	 <div class=\"col-xl-4 col-12\">\r\n		 [file file-42 id:file class:custom-file]\r\n	</div>\r\n	    <div class=\"col-xl-8 col-12\">\r\n				[textarea textarea-674 placeholder \"במידה ותרצה להרחיב אז אפשר פשוט כאן:\"]\r\n	</div>\r\n    <div class=\"col-xl-8 col-12\">\r\n			[submit \"כן, חזרו אלי לגביי הפניה שלי\"]\r\n	</div>\r\n</div>\n1\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@gordon>\n[_site_admin_email]\nמאת: [your-name] <[your-email]>\r\nנושא: [your-subject]\r\n\r\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [your-email]\n\n\n\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@gordon>\n[your-email]\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [_site_admin_email]\n\n\n\nההודעה נשלחה.\nארעה שגיאה בשליחת ההודעה.\nקיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\nארעה שגיאה בשליחת ההודעה.\nעליך להסכים לתנאים לפני שליחת ההודעה.\nזהו שדה חובה.\nהשדה ארוך מדי.\nהשדה קצר מדי.\nשגיאה לא ידועה בהעלאת הקובץ.\nאין לך הרשאות להעלות קבצים בפורמט זה.\nהקובץ גדול מדי.\nשגיאה בהעלאת הקובץ.\nשדה התאריך אינו נכון.\nהתאריך מוקדם מהתאריך המותר.\nהתאריך מאוחר מהתאריך המותר.\nפורמט המספר אינו תקין.\nהמספר קטן מהמינימום המותר.\nהמספר גדול מהמקסימום המותר.\nהתשובה לשאלת הביטחון אינה נכונה.\nכתובת האימייל שהוזנה אינה תקינה.\nהקישור אינו תקין.\nמספר הטלפון אינו תקין.', 'צור קשר', '', 'publish', 'closed', 'closed', '', '%d7%a6%d7%95%d7%a8-%d7%a7%d7%a9%d7%a8', '', '', '2021-07-12 01:54:13', '2021-07-11 22:54:13', '', 0, 'http://gordon:8888/?post_type=wpcf7_contact_form&#038;p=12', 0, 'wpcf7_contact_form', '', 0),
(13, 1, '2021-07-10 14:47:16', '2021-07-10 11:47:16', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:22:\"theme-general-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'פרטי קשר', '%d7%a4%d7%a8%d7%98%d7%99-%d7%a7%d7%a9%d7%a8', 'publish', 'closed', 'closed', '', 'group_5dd3a32b38e68', '', '', '2021-07-10 14:49:48', '2021-07-10 11:49:48', '', 0, 'http://gordon:8888/?p=13', 0, 'acf-field-group', '', 0),
(14, 1, '2021-07-10 14:47:15', '2021-07-10 11:47:15', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'טלפון', 'tel', 'publish', 'closed', 'closed', '', 'field_5dd3a33f42e0c', '', '', '2021-07-10 14:47:15', '2021-07-10 11:47:15', '', 13, 'http://gordon:8888/?post_type=acf-field&p=14', 0, 'acf-field', '', 0),
(15, 1, '2021-07-10 14:47:15', '2021-07-10 11:47:15', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'מייל', 'mail', 'publish', 'closed', 'closed', '', 'field_5dd3a35642e0d', '', '', '2021-07-10 14:47:15', '2021-07-10 11:47:15', '', 13, 'http://gordon:8888/?post_type=acf-field&p=15', 1, 'acf-field', '', 0),
(16, 1, '2021-07-10 14:47:15', '2021-07-10 11:47:15', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'פקס', 'fax', 'publish', 'closed', 'closed', '', 'field_5dd3a36942e0e', '', '', '2021-07-10 14:47:15', '2021-07-10 11:47:15', '', 13, 'http://gordon:8888/?post_type=acf-field&p=16', 2, 'acf-field', '', 0),
(17, 1, '2021-07-10 14:47:15', '2021-07-10 11:47:15', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כתובת', 'address', 'publish', 'closed', 'closed', '', 'field_5dd3a4d142e0f', '', '', '2021-07-10 14:47:15', '2021-07-10 11:47:15', '', 13, 'http://gordon:8888/?post_type=acf-field&p=17', 3, 'acf-field', '', 0),
(18, 1, '2021-07-10 14:47:15', '2021-07-10 11:47:15', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'מפה (תמונה)', 'map_image', 'publish', 'closed', 'closed', '', 'field_5ddbd67734310', '', '', '2021-07-10 14:47:15', '2021-07-10 11:47:15', '', 13, 'http://gordon:8888/?post_type=acf-field&p=18', 4, 'acf-field', '', 0),
(19, 1, '2021-07-10 14:47:15', '2021-07-10 11:47:15', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'פייסבוק', 'facebook', 'publish', 'closed', 'closed', '', 'field_5ddbd6b3cc0cd', '', '', '2021-07-10 14:47:15', '2021-07-10 11:47:15', '', 13, 'http://gordon:8888/?post_type=acf-field&p=19', 5, 'acf-field', '', 0),
(20, 1, '2021-07-10 14:47:16', '2021-07-10 11:47:16', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Whatsapp', 'whatsapp', 'publish', 'closed', 'closed', '', 'field_5ddbd6cdcc0ce', '', '', '2021-07-10 14:47:16', '2021-07-10 11:47:16', '', 13, 'http://gordon:8888/?post_type=acf-field&p=20', 6, 'acf-field', '', 0),
(21, 1, '2021-07-10 14:47:27', '2021-07-10 11:47:27', 'a:7:{s:8:\"location\";a:3:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"post\";}}i:1;a:2:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"page\";}i:1;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"!=\";s:5:\"value\";s:17:\"views/contact.php\";}}i:2;a:1:{i:0;a:3:{s:5:\"param\";s:8:\"taxonomy\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:8:\"category\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'תוספות תוכן', '%d7%aa%d7%95%d7%a1%d7%a4%d7%95%d7%aa-%d7%aa%d7%95%d7%9b%d7%9f', 'publish', 'closed', 'closed', '', 'group_5ddbde4d59412', '', '', '2021-07-13 14:52:47', '2021-07-13 11:52:47', '', 0, 'http://gordon:8888/?p=21', 0, 'acf-field-group', '', 0),
(22, 1, '2021-07-10 14:47:26', '2021-07-10 11:47:26', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"70\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'סליידר SEO', 'single_slider_seo', 'publish', 'closed', 'closed', '', 'field_5ddbde5499115', '', '', '2021-07-10 14:48:05', '2021-07-10 11:48:05', '', 21, 'http://gordon:8888/?post_type=acf-field&#038;p=22', 0, 'acf-field', '', 0),
(23, 1, '2021-07-10 14:47:26', '2021-07-10 11:47:26', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'תוכן', 'content', 'publish', 'closed', 'closed', '', 'field_5ddbde7399116', '', '', '2021-07-10 14:47:26', '2021-07-10 11:47:26', '', 22, 'http://gordon:8888/?post_type=acf-field&p=23', 0, 'acf-field', '', 0),
(27, 1, '2021-07-10 14:48:05', '2021-07-10 11:48:05', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"30\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'slider_img', 'publish', 'closed', 'closed', '', 'field_60e988d9254ba', '', '', '2021-07-12 13:27:42', '2021-07-12 10:27:42', '', 21, 'http://gordon:8888/?post_type=acf-field&#038;p=27', 1, 'acf-field', '', 0),
(28, 1, '2021-07-10 14:49:48', '2021-07-10 11:49:48', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'Instagram', 'instagram', 'publish', 'closed', 'closed', '', 'field_60e988ff7e1ad', '', '', '2021-07-10 14:49:48', '2021-07-10 11:49:48', '', 13, 'http://gordon:8888/?post_type=acf-field&p=28', 7, 'acf-field', '', 0),
(29, 1, '2021-07-10 14:49:48', '2021-07-10 11:49:48', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'Youtube', 'youtube', 'publish', 'closed', 'closed', '', 'field_60e9890b7e1ae', '', '', '2021-07-10 14:49:48', '2021-07-10 11:49:48', '', 13, 'http://gordon:8888/?post_type=acf-field&p=29', 8, 'acf-field', '', 0),
(30, 1, '2021-07-10 14:49:48', '2021-07-10 11:49:48', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'Linkedin', 'linkedin', 'publish', 'closed', 'closed', '', 'field_60e989197e1af', '', '', '2021-07-10 14:49:48', '2021-07-10 11:49:48', '', 13, 'http://gordon:8888/?post_type=acf-field&p=30', 9, 'acf-field', '', 0),
(31, 1, '2021-07-10 14:49:48', '2021-07-10 11:49:48', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'שעות עבודה', 'open_hours', 'publish', 'closed', 'closed', '', 'field_60e9892d7e1b0', '', '', '2021-07-10 14:49:48', '2021-07-10 11:49:48', '', 13, 'http://gordon:8888/?post_type=acf-field&p=31', 10, 'acf-field', '', 0),
(32, 1, '2021-07-11 13:15:10', '2021-07-11 10:15:10', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nהועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.', 'שם המאמר לורם איפסום 1', '', 'publish', 'open', 'open', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-1', '', '', '2021-07-13 14:50:22', '2021-07-13 11:50:22', '', 0, 'http://gordon:8888/?p=32', 0, 'post', '', 0),
(33, 1, '2021-07-11 13:14:38', '2021-07-11 10:14:38', '', 'course-1', '', 'inherit', 'open', 'closed', '', 'course-1', '', '', '2021-07-11 13:14:38', '2021-07-11 10:14:38', '', 32, 'http://gordon:8888/wp-content/uploads/2021/07/course-1.png', 0, 'attachment', 'image/png', 0),
(34, 1, '2021-07-11 13:14:40', '2021-07-11 10:14:40', '', 'course-2', '', 'inherit', 'open', 'closed', '', 'course-2', '', '', '2021-07-11 13:14:40', '2021-07-11 10:14:40', '', 32, 'http://gordon:8888/wp-content/uploads/2021/07/course-2.png', 0, 'attachment', 'image/png', 0),
(35, 1, '2021-07-11 13:14:42', '2021-07-11 10:14:42', '', 'course-3', '', 'inherit', 'open', 'closed', '', 'course-3', '', '', '2021-07-11 13:14:42', '2021-07-11 10:14:42', '', 32, 'http://gordon:8888/wp-content/uploads/2021/07/course-3.png', 0, 'attachment', 'image/png', 0),
(36, 1, '2021-07-11 13:14:44', '2021-07-11 10:14:44', '', 'course-4', '', 'inherit', 'open', 'closed', '', 'course-4', '', '', '2021-07-11 13:14:44', '2021-07-11 10:14:44', '', 32, 'http://gordon:8888/wp-content/uploads/2021/07/course-4.png', 0, 'attachment', 'image/png', 0),
(37, 1, '2021-07-11 13:14:46', '2021-07-11 10:14:46', '', 'post-1', '', 'inherit', 'open', 'closed', '', 'post-1', '', '', '2021-07-11 13:14:46', '2021-07-11 10:14:46', '', 32, 'http://gordon:8888/wp-content/uploads/2021/07/post-1.png', 0, 'attachment', 'image/png', 0),
(38, 1, '2021-07-11 13:14:48', '2021-07-11 10:14:48', '', 'post-2', '', 'inherit', 'open', 'closed', '', 'post-2', '', '', '2021-07-11 13:14:48', '2021-07-11 10:14:48', '', 32, 'http://gordon:8888/wp-content/uploads/2021/07/post-2.png', 0, 'attachment', 'image/png', 0),
(39, 1, '2021-07-11 13:14:50', '2021-07-11 10:14:50', '', 'post-3', '', 'inherit', 'open', 'closed', '', 'post-3', '', '', '2021-07-11 13:14:50', '2021-07-11 10:14:50', '', 32, 'http://gordon:8888/wp-content/uploads/2021/07/post-3.png', 0, 'attachment', 'image/png', 0),
(40, 1, '2021-07-11 13:15:10', '2021-07-11 10:15:10', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nהועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.', 'שם המאמר לורם איפסום 1', '', 'inherit', 'closed', 'closed', '', '32-revision-v1', '', '', '2021-07-11 13:15:10', '2021-07-11 10:15:10', '', 32, 'http://gordon:8888/?p=40', 0, 'revision', '', 0),
(41, 1, '2021-07-11 13:15:36', '2021-07-11 10:15:36', 'נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט', 'שם המאמר לורם איפסום 2', '', 'publish', 'open', 'open', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-2', '', '', '2021-07-13 14:50:21', '2021-07-13 11:50:21', '', 0, 'http://gordon:8888/?p=41', 0, 'post', '', 0),
(42, 1, '2021-07-11 13:15:36', '2021-07-11 10:15:36', 'נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט', 'שם המאמר לורם איפסום 2', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2021-07-11 13:15:36', '2021-07-11 10:15:36', '', 41, 'http://gordon:8888/?p=42', 0, 'revision', '', 0),
(43, 1, '2021-07-11 13:16:01', '2021-07-11 10:16:01', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט\r\n\r\nסחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'שם המאמר לורם איפסום 3', '', 'publish', 'open', 'open', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-3', '', '', '2021-07-13 14:50:18', '2021-07-13 11:50:18', '', 0, 'http://gordon:8888/?p=43', 0, 'post', '', 0),
(44, 1, '2021-07-11 13:16:01', '2021-07-11 10:16:01', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט\r\n\r\nסחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'שם המאמר לורם איפסום 3', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2021-07-11 13:16:01', '2021-07-11 10:16:01', '', 43, 'http://gordon:8888/?p=44', 0, 'revision', '', 0),
(45, 1, '2021-07-11 13:16:22', '2021-07-11 10:16:22', 'הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nנולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט', 'שם המאמר לורם איפסום 4', '', 'publish', 'open', 'open', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-4', '', '', '2021-07-13 14:50:15', '2021-07-13 11:50:15', '', 0, 'http://gordon:8888/?p=45', 0, 'post', '', 0),
(46, 1, '2021-07-11 13:16:22', '2021-07-11 10:16:22', 'הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nנולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט', 'שם המאמר לורם איפסום 4', '', 'inherit', 'closed', 'closed', '', '45-revision-v1', '', '', '2021-07-11 13:16:22', '2021-07-11 10:16:22', '', 45, 'http://gordon:8888/?p=46', 0, 'revision', '', 0),
(47, 1, '2021-07-11 13:16:46', '2021-07-11 10:16:46', 'סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nקוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.', 'שם המאמר לורם איפסום 5', '', 'publish', 'open', 'open', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-5', '', '', '2021-07-13 14:50:13', '2021-07-13 11:50:13', '', 0, 'http://gordon:8888/?p=47', 0, 'post', '', 0),
(48, 1, '2021-07-11 13:16:46', '2021-07-11 10:16:46', 'סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nקוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.', 'שם המאמר לורם איפסום 5', '', 'inherit', 'closed', 'closed', '', '47-revision-v1', '', '', '2021-07-11 13:16:46', '2021-07-11 10:16:46', '', 47, 'http://gordon:8888/?p=48', 0, 'revision', '', 0),
(49, 1, '2021-07-11 13:17:39', '2021-07-11 10:17:39', 'יצירת קשר עם הנהלת האתר, נשמח לעמוד לשירותכם בכל נושא או פניה\r\n<h2>השאירו לנו פרטים בכל נושא ואנחנו נצור עמכם קשר בהקדם</h2>', 'צור קשר', '', 'publish', 'closed', 'closed', '', '%d7%a6%d7%95%d7%a8-%d7%a7%d7%a9%d7%a8', '', '', '2021-07-12 10:48:25', '2021-07-12 07:48:25', '', 0, 'http://gordon:8888/?page_id=49', 0, 'page', '', 0),
(50, 1, '2021-07-11 13:17:39', '2021-07-11 10:17:39', '', 'צור קשר', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2021-07-11 13:17:39', '2021-07-11 10:17:39', '', 49, 'http://gordon:8888/?p=50', 0, 'revision', '', 0),
(51, 1, '2021-07-11 13:20:08', '0000-00-00 00:00:00', '', 'AUTO-DRAFT', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2021-07-11 13:20:08', '0000-00-00 00:00:00', '', 0, 'http://gordon:8888/?post_type=product&p=51', 0, 'product', '', 0),
(52, 1, '2021-07-11 13:21:50', '2021-07-11 10:21:50', '', 'שם המוצר לורם איפסום TEST', '', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-test', '', '', '2021-07-13 13:09:28', '2021-07-13 10:09:28', '', 0, 'http://gordon:8888/?post_type=product&#038;p=52', 0, 'product', '', 0),
(53, 1, '2021-07-11 13:21:21', '2021-07-11 10:21:21', '', 'product-1', '', 'inherit', 'open', 'closed', '', 'product-1', '', '', '2021-07-11 13:21:21', '2021-07-11 10:21:21', '', 52, 'http://gordon:8888/wp-content/uploads/2021/07/product-1.png', 0, 'attachment', 'image/png', 0),
(54, 1, '2021-07-11 13:21:22', '2021-07-11 10:21:22', '', 'product-2', '', 'inherit', 'open', 'closed', '', 'product-2', '', '', '2021-07-11 13:21:22', '2021-07-11 10:21:22', '', 52, 'http://gordon:8888/wp-content/uploads/2021/07/product-2.png', 0, 'attachment', 'image/png', 0),
(55, 1, '2021-07-11 13:21:23', '2021-07-11 10:21:23', '', 'product-3', '', 'inherit', 'open', 'closed', '', 'product-3', '', '', '2021-07-11 13:21:23', '2021-07-11 10:21:23', '', 52, 'http://gordon:8888/wp-content/uploads/2021/07/product-3.png', 0, 'attachment', 'image/png', 0),
(56, 1, '2021-07-11 13:21:24', '2021-07-11 10:21:24', '', 'product-4', '', 'inherit', 'open', 'closed', '', 'product-4', '', '', '2021-07-11 13:21:24', '2021-07-11 10:21:24', '', 52, 'http://gordon:8888/wp-content/uploads/2021/07/product-4.png', 0, 'attachment', 'image/png', 0),
(57, 1, '2021-07-11 13:21:25', '2021-07-11 10:21:25', '', 'product-5', '', 'inherit', 'open', 'closed', '', 'product-5', '', '', '2021-07-11 13:21:25', '2021-07-11 10:21:25', '', 52, 'http://gordon:8888/wp-content/uploads/2021/07/product-5.png', 0, 'attachment', 'image/png', 0),
(58, 1, '2021-07-11 13:21:26', '2021-07-11 10:21:26', '', 'product-6', '', 'inherit', 'open', 'closed', '', 'product-6', '', '', '2021-07-11 13:21:26', '2021-07-11 10:21:26', '', 52, 'http://gordon:8888/wp-content/uploads/2021/07/product-6.png', 0, 'attachment', 'image/png', 0),
(59, 1, '2021-07-11 13:21:26', '2021-07-11 10:21:26', '', 'product-7', '', 'inherit', 'open', 'closed', '', 'product-7', '', '', '2021-07-11 13:21:26', '2021-07-11 10:21:26', '', 52, 'http://gordon:8888/wp-content/uploads/2021/07/product-7.png', 0, 'attachment', 'image/png', 0),
(60, 1, '2021-07-11 13:21:27', '2021-07-11 10:21:27', '', 'product-8', '', 'inherit', 'open', 'closed', '', 'product-8', '', '', '2021-07-11 13:21:27', '2021-07-11 10:21:27', '', 52, 'http://gordon:8888/wp-content/uploads/2021/07/product-8.png', 0, 'attachment', 'image/png', 0),
(61, 1, '2021-07-11 13:22:05', '2021-07-11 10:22:05', '', 'שם המוצר לורם איפסום 1', '', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-1', '', '', '2021-07-13 13:09:39', '2021-07-13 10:09:39', '', 0, 'http://gordon:8888/?post_type=product&#038;p=61', 0, 'product', '', 0),
(62, 1, '2021-07-11 13:22:22', '2021-07-11 10:22:22', '', 'שם המוצר לורם איפסום 2', '', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-2', '', '', '2021-07-13 13:09:55', '2021-07-13 10:09:55', '', 0, 'http://gordon:8888/?post_type=product&#038;p=62', 0, 'product', '', 0),
(63, 1, '2021-07-11 13:22:37', '2021-07-11 10:22:37', '', 'שם המוצר לורם איפסום 3', '', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-3', '', '', '2021-07-13 13:10:03', '2021-07-13 10:10:03', '', 0, 'http://gordon:8888/?post_type=product&#038;p=63', 0, 'product', '', 0),
(64, 1, '2021-07-11 13:22:55', '2021-07-11 10:22:55', '', 'שם המוצר לורם איפסום 4', '', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-4', '', '', '2021-07-13 13:10:13', '2021-07-13 10:10:13', '', 0, 'http://gordon:8888/?post_type=product&#038;p=64', 0, 'product', '', 0),
(65, 1, '2021-07-11 13:23:12', '2021-07-11 10:23:12', '', 'שם המוצר לורם איפסום 5', '', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-5', '', '', '2021-07-13 13:10:21', '2021-07-13 10:10:21', '', 0, 'http://gordon:8888/?post_type=product&#038;p=65', 0, 'product', '', 0),
(66, 1, '2021-07-11 13:24:16', '2021-07-11 10:24:16', '', 'שם המוצר לורם איפסום 6', '', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-6', '', '', '2021-07-13 13:10:29', '2021-07-13 10:10:29', '', 0, 'http://gordon:8888/?post_type=product&#038;p=66', 0, 'product', '', 0),
(67, 1, '2021-07-11 13:27:10', '2021-07-11 10:27:10', '', 'דף הבית', '', 'publish', 'closed', 'closed', '', '%d7%93%d7%a3-%d7%94%d7%91%d7%99%d7%aa', '', '', '2021-07-13 09:17:00', '2021-07-13 06:17:00', '', 0, 'http://gordon:8888/?page_id=67', 0, 'page', '', 0),
(68, 1, '2021-07-11 13:27:10', '2021-07-11 10:27:10', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '67-revision-v1', '', '', '2021-07-11 13:27:10', '2021-07-11 10:27:10', '', 67, 'http://gordon:8888/?p=68', 0, 'revision', '', 0),
(69, 1, '2021-07-11 13:27:31', '2021-07-11 10:27:31', ' ', '', '', 'publish', 'closed', 'closed', '', '69', '', '', '2021-07-11 13:27:31', '2021-07-11 10:27:31', '', 0, 'http://gordon:8888/?p=69', 1, 'nav_menu_item', '', 0),
(70, 1, '2021-07-11 13:27:31', '2021-07-11 10:27:31', ' ', '', '', 'publish', 'closed', 'closed', '', '70', '', '', '2021-07-11 13:27:31', '2021-07-11 10:27:31', '', 0, 'http://gordon:8888/?p=70', 2, 'nav_menu_item', '', 0),
(71, 1, '2021-07-11 13:27:31', '2021-07-11 10:27:31', ' ', '', '', 'publish', 'closed', 'closed', '', '71', '', '', '2021-07-11 13:27:31', '2021-07-11 10:27:31', '', 0, 'http://gordon:8888/?p=71', 3, 'nav_menu_item', '', 0),
(72, 1, '2021-07-13 09:40:07', '2021-07-11 10:28:23', ' ', '', '', 'publish', 'closed', 'closed', '', '72', '', '', '2021-07-13 09:40:07', '2021-07-13 06:40:07', '', 0, 'http://gordon:8888/?p=72', 4, 'nav_menu_item', '', 0),
(73, 1, '2021-07-13 09:40:07', '2021-07-11 10:28:23', ' ', '', '', 'publish', 'closed', 'closed', '', '73', '', '', '2021-07-13 09:40:07', '2021-07-13 06:40:07', '', 0, 'http://gordon:8888/?p=73', 5, 'nav_menu_item', '', 0),
(74, 1, '2021-07-13 09:40:07', '2021-07-11 10:28:23', ' ', '', '', 'publish', 'closed', 'closed', '', '74', '', '', '2021-07-13 09:40:07', '2021-07-13 06:40:07', '', 0, 'http://gordon:8888/?p=74', 6, 'nav_menu_item', '', 0),
(75, 1, '2021-07-13 09:40:07', '2021-07-11 10:28:23', ' ', '', '', 'publish', 'closed', 'closed', '', '75', '', '', '2021-07-13 09:40:07', '2021-07-13 06:40:07', '', 0, 'http://gordon:8888/?p=75', 7, 'nav_menu_item', '', 0),
(77, 1, '2021-07-13 09:40:07', '2021-07-11 10:28:23', ' ', '', '', 'publish', 'closed', 'closed', '', '77', '', '', '2021-07-13 09:40:07', '2021-07-13 06:40:07', '', 0, 'http://gordon:8888/?p=77', 8, 'nav_menu_item', '', 0),
(78, 1, '2021-07-11 13:29:12', '2021-07-11 10:29:12', ' ', '', '', 'publish', 'closed', 'closed', '', '78', '', '', '2021-07-11 13:29:12', '2021-07-11 10:29:12', '', 0, 'http://gordon:8888/?p=78', 1, 'nav_menu_item', '', 0),
(79, 1, '2021-07-11 13:29:12', '2021-07-11 10:29:12', ' ', '', '', 'publish', 'closed', 'closed', '', '79', '', '', '2021-07-11 13:29:12', '2021-07-11 10:29:12', '', 0, 'http://gordon:8888/?p=79', 2, 'nav_menu_item', '', 0),
(80, 1, '2021-07-11 13:29:12', '2021-07-11 10:29:12', ' ', '', '', 'publish', 'closed', 'closed', '', '80', '', '', '2021-07-11 13:29:12', '2021-07-11 10:29:12', '', 0, 'http://gordon:8888/?p=80', 3, 'nav_menu_item', '', 0),
(81, 1, '2021-07-11 13:29:12', '2021-07-11 10:29:12', ' ', '', '', 'publish', 'closed', 'closed', '', '81', '', '', '2021-07-11 13:29:12', '2021-07-11 10:29:12', '', 0, 'http://gordon:8888/?p=81', 4, 'nav_menu_item', '', 0),
(82, 1, '2021-07-11 13:29:12', '2021-07-11 10:29:12', ' ', '', '', 'publish', 'closed', 'closed', '', '82', '', '', '2021-07-11 13:29:12', '2021-07-11 10:29:12', '', 0, 'http://gordon:8888/?p=82', 5, 'nav_menu_item', '', 0),
(83, 1, '2021-07-11 13:29:12', '2021-07-11 10:29:12', ' ', '', '', 'publish', 'closed', 'closed', '', '83', '', '', '2021-07-11 13:29:12', '2021-07-11 10:29:12', '', 0, 'http://gordon:8888/?p=83', 6, 'nav_menu_item', '', 0),
(84, 1, '2021-07-11 13:29:12', '2021-07-11 10:29:12', ' ', '', '', 'publish', 'closed', 'closed', '', '84', '', '', '2021-07-11 13:29:12', '2021-07-11 10:29:12', '', 0, 'http://gordon:8888/?p=84', 7, 'nav_menu_item', '', 0),
(85, 1, '2021-07-11 13:29:48', '2021-07-11 10:29:48', ' ', '', '', 'publish', 'closed', 'closed', '', '85', '', '', '2021-07-11 13:29:48', '2021-07-11 10:29:48', '', 0, 'http://gordon:8888/?p=85', 1, 'nav_menu_item', '', 0),
(86, 1, '2021-07-11 13:29:48', '2021-07-11 10:29:48', ' ', '', '', 'publish', 'closed', 'closed', '', '86', '', '', '2021-07-11 13:29:48', '2021-07-11 10:29:48', '', 0, 'http://gordon:8888/?p=86', 2, 'nav_menu_item', '', 0),
(87, 1, '2021-07-11 13:29:48', '2021-07-11 10:29:48', ' ', '', '', 'publish', 'closed', 'closed', '', '87', '', '', '2021-07-11 13:29:48', '2021-07-11 10:29:48', '', 0, 'http://gordon:8888/?p=87', 3, 'nav_menu_item', '', 0),
(88, 1, '2021-07-11 13:29:48', '2021-07-11 10:29:48', ' ', '', '', 'publish', 'closed', 'closed', '', '88', '', '', '2021-07-11 13:29:48', '2021-07-11 10:29:48', '', 0, 'http://gordon:8888/?p=88', 4, 'nav_menu_item', '', 0),
(89, 1, '2021-07-11 13:30:31', '2021-07-11 10:30:31', ' ', '', '', 'publish', 'closed', 'closed', '', '89', '', '', '2021-07-11 13:30:31', '2021-07-11 10:30:31', '', 0, 'http://gordon:8888/?p=89', 1, 'nav_menu_item', '', 0),
(90, 1, '2021-07-11 13:30:31', '2021-07-11 10:30:31', ' ', '', '', 'publish', 'closed', 'closed', '', '90', '', '', '2021-07-11 13:30:31', '2021-07-11 10:30:31', '', 0, 'http://gordon:8888/?p=90', 2, 'nav_menu_item', '', 0),
(91, 1, '2021-07-11 13:30:31', '2021-07-11 10:30:31', ' ', '', '', 'publish', 'closed', 'closed', '', '91', '', '', '2021-07-11 13:30:31', '2021-07-11 10:30:31', '', 0, 'http://gordon:8888/?p=91', 3, 'nav_menu_item', '', 0),
(92, 1, '2021-07-11 13:30:31', '2021-07-11 10:30:31', ' ', '', '', 'publish', 'closed', 'closed', '', '92', '', '', '2021-07-11 13:30:31', '2021-07-11 10:30:31', '', 0, 'http://gordon:8888/?p=92', 4, 'nav_menu_item', '', 0),
(93, 1, '2021-07-11 13:30:31', '2021-07-11 10:30:31', ' ', '', '', 'publish', 'closed', 'closed', '', '93', '', '', '2021-07-11 13:30:31', '2021-07-11 10:30:31', '', 0, 'http://gordon:8888/?p=93', 5, 'nav_menu_item', '', 0),
(94, 1, '2021-07-11 13:30:31', '2021-07-11 10:30:31', ' ', '', '', 'publish', 'closed', 'closed', '', '94', '', '', '2021-07-11 13:30:31', '2021-07-11 10:30:31', '', 0, 'http://gordon:8888/?p=94', 6, 'nav_menu_item', '', 0),
(95, 1, '2021-07-11 13:30:31', '2021-07-11 10:30:31', ' ', '', '', 'publish', 'closed', 'closed', '', '95', '', '', '2021-07-11 13:30:31', '2021-07-11 10:30:31', '', 0, 'http://gordon:8888/?p=95', 7, 'nav_menu_item', '', 0),
(96, 1, '2021-07-11 13:30:31', '2021-07-11 10:30:31', ' ', '', '', 'publish', 'closed', 'closed', '', '96', '', '', '2021-07-11 13:30:31', '2021-07-11 10:30:31', '', 0, 'http://gordon:8888/?p=96', 8, 'nav_menu_item', '', 0),
(97, 1, '2021-07-11 13:55:15', '2021-07-11 10:55:15', '', 'map', '', 'inherit', 'open', 'closed', '', 'map', '', '', '2021-07-11 13:55:15', '2021-07-11 10:55:15', '', 0, 'http://gordon:8888/wp-content/uploads/2021/07/map.png', 0, 'attachment', 'image/png', 0),
(98, 1, '2021-07-11 15:41:34', '2021-07-11 12:41:34', '<!-- wp:shortcode -->[yith_wcwl_wishlist]<!-- /wp:shortcode -->', 'רשימת משאלות', '', 'publish', 'closed', 'closed', '', 'wishlist', '', '', '2021-07-11 15:41:34', '2021-07-11 12:41:34', '', 0, 'http://gordon:8888/wishlist/', 0, 'page', '', 0),
(99, 1, '2021-07-11 16:01:10', '0000-00-00 00:00:00', '', 'טיוטה אוטומטית', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2021-07-11 16:01:10', '0000-00-00 00:00:00', '', 0, 'http://gordon:8888/?post_type=acf-field-group&p=99', 0, 'acf-field-group', '', 0),
(100, 1, '2021-07-12 00:25:21', '2021-07-11 13:04:32', ' ', '', '', 'publish', 'closed', 'closed', '', '100', '', '', '2021-07-12 00:25:21', '2021-07-11 21:25:21', '', 0, 'http://gordon:8888/?p=100', 1, 'nav_menu_item', '', 0),
(101, 1, '2021-07-11 16:05:09', '2021-07-11 13:05:09', ' ', '', '', 'publish', 'closed', 'closed', '', '101', '', '', '2021-07-11 16:05:09', '2021-07-11 13:05:09', '', 0, 'http://gordon:8888/?p=101', 1, 'nav_menu_item', '', 0),
(102, 1, '2021-07-11 16:05:09', '2021-07-11 13:05:09', ' ', '', '', 'publish', 'closed', 'closed', '', '102', '', '', '2021-07-11 16:05:09', '2021-07-11 13:05:09', '', 0, 'http://gordon:8888/?p=102', 2, 'nav_menu_item', '', 0),
(103, 1, '2021-07-11 16:05:09', '2021-07-11 13:05:09', ' ', '', '', 'publish', 'closed', 'closed', '', '103', '', '', '2021-07-11 16:05:09', '2021-07-11 13:05:09', '', 0, 'http://gordon:8888/?p=103', 3, 'nav_menu_item', '', 0),
(104, 1, '2021-07-11 16:05:09', '2021-07-11 13:05:09', ' ', '', '', 'publish', 'closed', 'closed', '', '104', '', '', '2021-07-11 16:05:09', '2021-07-11 13:05:09', '', 0, 'http://gordon:8888/?p=104', 4, 'nav_menu_item', '', 0),
(105, 1, '2021-07-11 16:05:09', '2021-07-11 13:05:09', ' ', '', '', 'publish', 'closed', 'closed', '', '105', '', '', '2021-07-11 16:05:09', '2021-07-11 13:05:09', '', 0, 'http://gordon:8888/?p=105', 5, 'nav_menu_item', '', 0);
INSERT INTO `fmn_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(106, 1, '2021-07-11 16:05:09', '2021-07-11 13:05:09', ' ', '', '', 'publish', 'closed', 'closed', '', '106', '', '', '2021-07-11 16:05:09', '2021-07-11 13:05:09', '', 0, 'http://gordon:8888/?p=106', 6, 'nav_menu_item', '', 0),
(107, 1, '2021-07-11 16:05:48', '2021-07-11 13:05:48', ' ', '', '', 'publish', 'closed', 'closed', '', '107', '', '', '2021-07-11 16:05:48', '2021-07-11 13:05:48', '', 0, 'http://gordon:8888/?p=107', 1, 'nav_menu_item', '', 0),
(108, 1, '2021-07-11 16:05:48', '2021-07-11 13:05:48', ' ', '', '', 'publish', 'closed', 'closed', '', '108', '', '', '2021-07-11 16:05:48', '2021-07-11 13:05:48', '', 0, 'http://gordon:8888/?p=108', 2, 'nav_menu_item', '', 0),
(109, 1, '2021-07-11 16:05:48', '2021-07-11 13:05:48', ' ', '', '', 'publish', 'closed', 'closed', '', '109', '', '', '2021-07-11 16:05:48', '2021-07-11 13:05:48', '', 0, 'http://gordon:8888/?p=109', 3, 'nav_menu_item', '', 0),
(110, 1, '2021-07-11 16:05:48', '2021-07-11 13:05:48', ' ', '', '', 'publish', 'closed', 'closed', '', '110', '', '', '2021-07-11 16:05:48', '2021-07-11 13:05:48', '', 0, 'http://gordon:8888/?p=110', 4, 'nav_menu_item', '', 0),
(111, 1, '2021-07-11 16:05:48', '2021-07-11 13:05:48', ' ', '', '', 'publish', 'closed', 'closed', '', '111', '', '', '2021-07-11 16:05:48', '2021-07-11 13:05:48', '', 0, 'http://gordon:8888/?p=111', 5, 'nav_menu_item', '', 0),
(112, 1, '2021-07-11 16:05:48', '2021-07-11 13:05:48', ' ', '', '', 'publish', 'closed', 'closed', '', '112', '', '', '2021-07-11 16:05:48', '2021-07-11 13:05:48', '', 0, 'http://gordon:8888/?p=112', 6, 'nav_menu_item', '', 0),
(113, 1, '2021-07-11 16:05:48', '2021-07-11 13:05:48', ' ', '', '', 'publish', 'closed', 'closed', '', '113', '', '', '2021-07-11 16:05:48', '2021-07-11 13:05:48', '', 0, 'http://gordon:8888/?p=113', 7, 'nav_menu_item', '', 0),
(114, 1, '2021-07-11 16:53:55', '2021-07-11 13:53:55', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2021-07-11 16:53:55', '2021-07-11 13:53:55', '', 0, 'http://gordon:8888/wp-content/uploads/2021/07/logo.png', 0, 'attachment', 'image/png', 0),
(115, 1, '2021-07-11 18:18:37', '2021-07-11 15:18:37', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:22:\"theme-general-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'תוספות', '%d7%aa%d7%95%d7%a1%d7%a4%d7%95%d7%aa', 'publish', 'closed', 'closed', '', 'group_60eb0b8815e8f', '', '', '2021-07-13 11:02:14', '2021-07-13 08:02:14', '', 0, 'http://gordon:8888/?post_type=acf-field-group&#038;p=115', 0, 'acf-field-group', '', 0),
(116, 1, '2021-07-11 18:18:37', '2021-07-11 15:18:37', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'חדשות', 'חדשות', 'publish', 'closed', 'closed', '', 'field_60eb0b96b9692', '', '', '2021-07-11 18:18:37', '2021-07-11 15:18:37', '', 115, 'http://gordon:8888/?post_type=acf-field&p=116', 0, 'acf-field', '', 0),
(117, 1, '2021-07-11 18:18:37', '2021-07-11 15:18:37', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'חדשות', 'flash_msg', 'publish', 'closed', 'closed', '', 'field_60eb0ba1b9693', '', '', '2021-07-11 18:18:37', '2021-07-11 15:18:37', '', 115, 'http://gordon:8888/?post_type=acf-field&p=117', 1, 'acf-field', '', 0),
(118, 1, '2021-07-11 18:18:37', '2021-07-11 15:18:37', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'קישור', 'link', 'publish', 'closed', 'closed', '', 'field_60eb0bbab9694', '', '', '2021-07-11 18:18:37', '2021-07-11 15:18:37', '', 117, 'http://gordon:8888/?post_type=acf-field&p=118', 0, 'acf-field', '', 0),
(119, 1, '2021-07-11 18:18:37', '2021-07-11 15:18:37', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'title', 'publish', 'closed', 'closed', '', 'field_60eb0bc4b9695', '', '', '2021-07-11 18:18:37', '2021-07-11 15:18:37', '', 117, 'http://gordon:8888/?post_type=acf-field&p=119', 1, 'acf-field', '', 0),
(120, 1, '2021-07-11 18:29:01', '2021-07-11 15:29:01', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'פוטר', 'פוטר', 'publish', 'closed', 'closed', '', 'field_60eb0e101c29d', '', '', '2021-07-11 18:29:01', '2021-07-11 15:29:01', '', 115, 'http://gordon:8888/?post_type=acf-field&p=120', 2, 'acf-field', '', 0),
(121, 1, '2021-07-11 18:29:01', '2021-07-11 15:29:01', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'כותרות של תפריטים', 'menu_titles', 'publish', 'closed', 'closed', '', 'field_60eb0e1a1c29e', '', '', '2021-07-12 01:33:44', '2021-07-11 22:33:44', '', 115, 'http://gordon:8888/?post_type=acf-field&#038;p=121', 4, 'acf-field', '', 0),
(122, 1, '2021-07-11 18:29:01', '2021-07-11 15:29:01', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'foo_menu_title', 'publish', 'closed', 'closed', '', 'field_60eb0e321c29f', '', '', '2021-07-11 18:29:01', '2021-07-11 15:29:01', '', 121, 'http://gordon:8888/?post_type=acf-field&p=122', 0, 'acf-field', '', 0),
(123, 1, '2021-07-12 00:25:21', '2021-07-11 21:25:12', ' ', '', '', 'publish', 'closed', 'closed', '', '123', '', '', '2021-07-12 00:25:21', '2021-07-11 21:25:21', '', 0, 'http://gordon:8888/?p=123', 2, 'nav_menu_item', '', 0),
(124, 1, '2021-07-12 01:33:44', '2021-07-11 22:33:44', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת טופס', 'foo_form_title', 'publish', 'closed', 'closed', '', 'field_60eb71af9d079', '', '', '2021-07-12 01:33:44', '2021-07-11 22:33:44', '', 115, 'http://gordon:8888/?post_type=acf-field&p=124', 3, 'acf-field', '', 0),
(125, 1, '2021-07-13 09:40:07', '2021-07-11 22:43:22', ' ', '', '', 'publish', 'closed', 'closed', '', '125', '', '', '2021-07-13 09:40:07', '2021-07-13 06:40:07', '', 0, 'http://gordon:8888/?p=125', 2, 'nav_menu_item', '', 0),
(126, 1, '2021-07-12 01:46:40', '2021-07-11 22:46:40', '', 'contact-img', '', 'inherit', 'open', 'closed', '', 'contact-img', '', '', '2021-07-12 01:46:40', '2021-07-11 22:46:40', '', 49, 'http://gordon:8888/wp-content/uploads/2021/07/contact-img.png', 0, 'attachment', 'image/png', 0),
(127, 1, '2021-07-12 01:57:39', '2021-07-11 22:57:39', 'יצירת קשר עם הנהלת האתר, נשמח לעמוד לשירותכם בכל נושא או פניה\r\n<h2>השאירו לנו פרטים בכל נושא ואנחנו נצור עמכם קשר בהקדם</h2>', 'צור קשר', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2021-07-12 01:57:39', '2021-07-11 22:57:39', '', 49, 'http://gordon:8888/?p=127', 0, 'revision', '', 0),
(128, 1, '2021-07-12 10:42:19', '2021-07-12 07:42:19', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:17:\"views/contact.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'צור קשר', '%d7%a6%d7%95%d7%a8-%d7%a7%d7%a9%d7%a8', 'publish', 'closed', 'closed', '', 'group_60ebf2290b8ac', '', '', '2021-07-12 10:43:30', '2021-07-12 07:43:30', '', 0, 'http://gordon:8888/?post_type=acf-field-group&#038;p=128', 0, 'acf-field-group', '', 0),
(129, 1, '2021-07-12 10:42:19', '2021-07-12 07:42:19', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת של בלוק עם מפה', 'map_block_title', 'publish', 'closed', 'closed', '', 'field_60ebf240ab05f', '', '', '2021-07-12 10:42:19', '2021-07-12 07:42:19', '', 128, 'http://gordon:8888/?post_type=acf-field&p=129', 0, 'acf-field', '', 0),
(130, 1, '2021-07-12 10:43:30', '2021-07-12 07:43:30', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'טקסט בבלוק עם לקוחות', 'clients_block_text', 'publish', 'closed', 'closed', '', 'field_60ebf2672dc08', '', '', '2021-07-12 10:43:30', '2021-07-12 07:43:30', '', 128, 'http://gordon:8888/?post_type=acf-field&p=130', 1, 'acf-field', '', 0),
(131, 1, '2021-07-12 10:43:30', '2021-07-12 07:43:30', 'a:18:{s:4:\"type\";s:7:\"gallery\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:9:\"thumbnail\";s:6:\"insert\";s:6:\"append\";s:7:\"library\";s:3:\"all\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'לקוחות', 'clients', 'publish', 'closed', 'closed', '', 'field_60ebf2902dc09', '', '', '2021-07-12 10:43:30', '2021-07-12 07:43:30', '', 128, 'http://gordon:8888/?post_type=acf-field&p=131', 2, 'acf-field', '', 0),
(132, 1, '2021-07-12 10:46:58', '2021-07-12 07:46:58', '', 'לוגו', '', 'inherit', 'open', 'closed', '', '%d7%9c%d7%95%d7%92%d7%95', '', '', '2021-07-12 10:46:58', '2021-07-12 07:46:58', '', 49, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו.png', 0, 'attachment', 'image/png', 0),
(133, 1, '2021-07-12 10:47:08', '2021-07-12 07:47:08', '', 'לוגו', '', 'inherit', 'open', 'closed', '', '%d7%9c%d7%95%d7%92%d7%95-2', '', '', '2021-07-12 10:47:08', '2021-07-12 07:47:08', '', 49, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו-1.png', 0, 'attachment', 'image/png', 0),
(134, 1, '2021-07-12 10:47:23', '2021-07-12 07:47:23', '', 'לוגו', '', 'inherit', 'open', 'closed', '', '%d7%9c%d7%95%d7%92%d7%95-3', '', '', '2021-07-12 10:47:23', '2021-07-12 07:47:23', '', 49, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו-2.png', 0, 'attachment', 'image/png', 0),
(135, 1, '2021-07-12 10:47:40', '2021-07-12 07:47:40', '', 'לוגו', '', 'inherit', 'open', 'closed', '', '%d7%9c%d7%95%d7%92%d7%95-4', '', '', '2021-07-12 10:47:40', '2021-07-12 07:47:40', '', 49, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו-3.png', 0, 'attachment', 'image/png', 0),
(136, 1, '2021-07-12 10:47:48', '2021-07-12 07:47:48', '', 'לוגו', '', 'inherit', 'open', 'closed', '', '%d7%9c%d7%95%d7%92%d7%95-5', '', '', '2021-07-12 10:47:48', '2021-07-12 07:47:48', '', 49, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו-4.png', 0, 'attachment', 'image/png', 0),
(137, 1, '2021-07-12 10:47:58', '2021-07-12 07:47:58', '', 'לוגו', '', 'inherit', 'open', 'closed', '', '%d7%9c%d7%95%d7%92%d7%95-6', '', '', '2021-07-12 10:47:58', '2021-07-12 07:47:58', '', 49, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו-5.png', 0, 'attachment', 'image/png', 0),
(138, 1, '2021-07-12 10:48:06', '2021-07-12 07:48:06', '', 'לוגו', '', 'inherit', 'open', 'closed', '', '%d7%9c%d7%95%d7%92%d7%95-7', '', '', '2021-07-12 10:48:06', '2021-07-12 07:48:06', '', 49, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו-6.png', 0, 'attachment', 'image/png', 0),
(139, 1, '2021-07-12 10:48:15', '2021-07-12 07:48:15', '', 'לוגו', '', 'inherit', 'open', 'closed', '', '%d7%9c%d7%95%d7%92%d7%95-8', '', '', '2021-07-12 10:48:15', '2021-07-12 07:48:15', '', 49, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו-7.png', 0, 'attachment', 'image/png', 0),
(140, 1, '2021-07-12 10:48:25', '2021-07-12 07:48:25', 'יצירת קשר עם הנהלת האתר, נשמח לעמוד לשירותכם בכל נושא או פניה\r\n<h2>השאירו לנו פרטים בכל נושא ואנחנו נצור עמכם קשר בהקדם</h2>', 'צור קשר', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2021-07-12 10:48:25', '2021-07-12 07:48:25', '', 49, 'http://gordon:8888/?p=140', 0, 'revision', '', 0),
(141, 1, '2021-07-12 11:45:20', '2021-07-12 08:45:20', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:7:\"product\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'מוצר', '%d7%9e%d7%95%d7%a6%d7%a8', 'publish', 'closed', 'closed', '', 'group_60ec00da2b99b', '', '', '2021-07-12 13:28:54', '2021-07-12 10:28:54', '', 0, 'http://gordon:8888/?post_type=acf-field-group&#038;p=141', 0, 'acf-field-group', '', 0),
(142, 1, '2021-07-12 11:45:20', '2021-07-12 08:45:20', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'מידע', 'info_tabs', 'publish', 'closed', 'closed', '', 'field_60ec00e97c9df', '', '', '2021-07-12 11:45:20', '2021-07-12 08:45:20', '', 141, 'http://gordon:8888/?post_type=acf-field&p=142', 0, 'acf-field', '', 0),
(143, 1, '2021-07-12 11:45:20', '2021-07-12 08:45:20', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'title', 'publish', 'closed', 'closed', '', 'field_60ec00f97c9e0', '', '', '2021-07-12 11:45:20', '2021-07-12 08:45:20', '', 142, 'http://gordon:8888/?post_type=acf-field&p=143', 0, 'acf-field', '', 0),
(144, 1, '2021-07-12 11:45:20', '2021-07-12 08:45:20', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'תוכן', 'content', 'publish', 'closed', 'closed', '', 'field_60ec01007c9e1', '', '', '2021-07-12 11:45:20', '2021-07-12 08:45:20', '', 142, 'http://gordon:8888/?post_type=acf-field&p=144', 1, 'acf-field', '', 0),
(145, 1, '2021-07-12 12:00:22', '2021-07-12 09:00:22', '', 'test-prod-1', '', 'inherit', 'open', 'closed', '', 'test-prod-1', '', '', '2021-07-12 12:00:22', '2021-07-12 09:00:22', '', 52, 'http://gordon:8888/wp-content/uploads/2021/07/test-prod-1.png', 0, 'attachment', 'image/png', 0),
(146, 1, '2021-07-12 12:00:25', '2021-07-12 09:00:25', '', 'test-prod-2', '', 'inherit', 'open', 'closed', '', 'test-prod-2', '', '', '2021-07-12 12:00:25', '2021-07-12 09:00:25', '', 52, 'http://gordon:8888/wp-content/uploads/2021/07/test-prod-2.png', 0, 'attachment', 'image/png', 0),
(147, 1, '2021-07-12 12:00:27', '2021-07-12 09:00:27', '', 'test-prod-3', '', 'inherit', 'open', 'closed', '', 'test-prod-3', '', '', '2021-07-12 12:00:27', '2021-07-12 09:00:27', '', 52, 'http://gordon:8888/wp-content/uploads/2021/07/test-prod-3.png', 0, 'attachment', 'image/png', 0),
(148, 1, '2021-07-12 12:00:28', '2021-07-12 09:00:28', '', 'test-prod-4', '', 'inherit', 'open', 'closed', '', 'test-prod-4', '', '', '2021-07-12 12:00:28', '2021-07-12 09:00:28', '', 52, 'http://gordon:8888/wp-content/uploads/2021/07/test-prod-4.png', 0, 'attachment', 'image/png', 0),
(149, 1, '2021-07-12 12:00:30', '2021-07-12 09:00:30', '', 'test-prod-5', '', 'inherit', 'open', 'closed', '', 'test-prod-5', '', '', '2021-07-12 12:00:30', '2021-07-12 09:00:30', '', 52, 'http://gordon:8888/wp-content/uploads/2021/07/test-prod-5.png', 0, 'attachment', 'image/png', 0),
(150, 1, '2021-07-12 12:05:46', '2021-07-12 09:05:46', 'a:12:{s:4:\"type\";s:12:\"relationship\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:7:\"product\";}s:8:\"taxonomy\";s:0:\"\";s:7:\"filters\";a:3:{i:0;s:6:\"search\";i:1;s:9:\"post_type\";i:2;s:8:\"taxonomy\";}s:8:\"elements\";a:1:{i:0;s:14:\"featured_image\";}s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:13:\"return_format\";s:6:\"object\";}', 'סליידר עם מוצרים נוספים', 'related_products', 'publish', 'closed', 'closed', '', 'field_60ec05bfcd042', '', '', '2021-07-12 13:28:54', '2021-07-12 10:28:54', '', 141, 'http://gordon:8888/?post_type=acf-field&#038;p=150', 3, 'acf-field', '', 0),
(151, 1, '2021-07-12 12:09:14', '2021-07-12 09:09:14', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'related_products_title', 'publish', 'closed', 'closed', '', 'field_60ec06ab6175e', '', '', '2021-07-12 12:09:14', '2021-07-12 09:09:14', '', 141, 'http://gordon:8888/?post_type=acf-field&p=151', 1, 'acf-field', '', 0),
(152, 1, '2021-07-12 12:09:26', '2021-07-12 09:09:26', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'טקסט', 'related_products_text', 'publish', 'closed', 'closed', '', 'field_60ec06bdda96b', '', '', '2021-07-12 13:28:54', '2021-07-12 10:28:54', '', 141, 'http://gordon:8888/?post_type=acf-field&#038;p=152', 2, 'acf-field', '', 0),
(154, 1, '2021-07-12 13:24:31', '2021-07-12 10:24:31', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:7:\"product\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'תוספות תוכן בעמוד מוצר', '%d7%aa%d7%95%d7%a1%d7%a4%d7%95%d7%aa-%d7%aa%d7%95%d7%9b%d7%9f-%d7%91%d7%a2%d7%9e%d7%95%d7%93-%d7%9e%d7%95%d7%a6%d7%a8', 'publish', 'closed', 'closed', '', 'group_60ec18528e2af', '', '', '2021-07-12 13:29:10', '2021-07-12 10:29:10', '', 0, 'http://gordon:8888/?post_type=acf-field-group&#038;p=154', 0, 'acf-field-group', '', 0),
(155, 1, '2021-07-12 13:25:06', '2021-07-12 10:25:06', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"70\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";i:0;s:3:\"max\";i:0;s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'סליידר SEO', 'prod_slider_seo', 'publish', 'closed', 'closed', '', 'field_60ec1872365c6', '', '', '2021-07-12 13:28:08', '2021-07-12 10:28:08', '', 154, 'http://gordon:8888/?post_type=acf-field&#038;p=155', 1, 'acf-field', '', 0),
(156, 1, '2021-07-12 13:25:06', '2021-07-12 10:25:06', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'תוכן', 'content', 'publish', 'closed', 'closed', '', 'field_60ec1872365c7', '', '', '2021-07-12 13:25:06', '2021-07-12 10:25:06', '', 155, 'http://gordon:8888/?post_type=acf-field&p=156', 0, 'acf-field', '', 0),
(157, 1, '2021-07-12 13:25:21', '2021-07-12 10:25:21', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"30\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'prod_slider_img', 'publish', 'closed', 'closed', '', 'field_60ec18857fa27', '', '', '2021-07-12 13:28:19', '2021-07-12 10:28:19', '', 154, 'http://gordon:8888/?post_type=acf-field&#038;p=157', 3, 'acf-field', '', 0),
(158, 1, '2021-07-12 13:27:07', '2021-07-12 10:27:07', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת של תמונה', 'prod_slider_title', 'publish', 'closed', 'closed', '', 'field_60ec18943ddf7', '', '', '2021-07-12 13:28:27', '2021-07-12 10:28:27', '', 154, 'http://gordon:8888/?post_type=acf-field&#038;p=158', 4, 'acf-field', '', 0),
(159, 1, '2021-07-12 13:27:07', '2021-07-12 10:27:07', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'טקסט בתמונה', 'prod_slider_text', 'publish', 'closed', 'closed', '', 'field_60ec18b23ddf8', '', '', '2021-07-12 13:28:41', '2021-07-12 10:28:41', '', 154, 'http://gordon:8888/?post_type=acf-field&#038;p=159', 5, 'acf-field', '', 0),
(160, 1, '2021-07-12 13:27:07', '2021-07-12 10:27:07', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור', 'prod_slider_link', 'publish', 'closed', 'closed', '', 'field_60ec18d93ddf9', '', '', '2021-07-12 13:28:49', '2021-07-12 10:28:49', '', 154, 'http://gordon:8888/?post_type=acf-field&#038;p=160', 6, 'acf-field', '', 0),
(161, 1, '2021-07-12 14:00:04', '2021-07-12 11:00:04', '', 'slider-img', '', 'inherit', 'open', 'closed', '', 'slider-img', '', '', '2021-07-12 14:00:04', '2021-07-12 11:00:04', '', 67, 'http://gordon:8888/wp-content/uploads/2021/07/slider-img.png', 0, 'attachment', 'image/png', 0),
(162, 1, '2021-07-12 14:00:13', '2021-07-12 11:00:13', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '67-revision-v1', '', '', '2021-07-12 14:00:13', '2021-07-12 11:00:13', '', 67, 'http://gordon:8888/?p=162', 0, 'revision', '', 0),
(163, 1, '2021-07-12 14:18:30', '2021-07-12 11:18:30', '', 'prod-slider-img', '', 'inherit', 'open', 'closed', '', 'prod-slider-img', '', '', '2021-07-12 14:18:30', '2021-07-12 11:18:30', '', 52, 'http://gordon:8888/wp-content/uploads/2021/07/prod-slider-img.png', 0, 'attachment', 'image/png', 0),
(165, 1, '2021-07-12 22:36:23', '2021-07-12 19:36:23', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:14:\"views/home.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'דף הבית', '%d7%93%d7%a3-%d7%94%d7%91%d7%99%d7%aa', 'publish', 'closed', 'closed', '', 'group_60ec9923a16cb', '', '', '2021-07-12 23:51:05', '2021-07-12 20:51:05', '', 0, 'http://gordon:8888/?post_type=acf-field-group&#038;p=165', 0, 'acf-field-group', '', 0),
(166, 1, '2021-07-12 22:36:23', '2021-07-12 19:36:23', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'בלוק ראשי', 'סליידר_ראשי', 'publish', 'closed', 'closed', '', 'field_60ec99303abdf', '', '', '2021-07-12 22:36:23', '2021-07-12 19:36:23', '', 165, 'http://gordon:8888/?post_type=acf-field&p=166', 0, 'acf-field', '', 0),
(167, 1, '2021-07-12 22:36:23', '2021-07-12 19:36:23', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:9:\"2048x2048\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'home_main_img', 'publish', 'closed', 'closed', '', 'field_60ec993a3abe0', '', '', '2021-07-12 22:36:23', '2021-07-12 19:36:23', '', 165, 'http://gordon:8888/?post_type=acf-field&p=167', 1, 'acf-field', '', 0),
(168, 1, '2021-07-12 22:36:23', '2021-07-12 19:36:23', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת קטנה', 'home_small_title', 'publish', 'closed', 'closed', '', 'field_60ec99753abe1', '', '', '2021-07-12 22:36:23', '2021-07-12 19:36:23', '', 165, 'http://gordon:8888/?post_type=acf-field&p=168', 2, 'acf-field', '', 0),
(169, 1, '2021-07-12 22:36:23', '2021-07-12 19:36:23', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת גדולה', 'home_big_title', 'publish', 'closed', 'closed', '', 'field_60ec99953abe2', '', '', '2021-07-12 22:36:23', '2021-07-12 19:36:23', '', 165, 'http://gordon:8888/?post_type=acf-field&p=169', 3, 'acf-field', '', 0),
(170, 1, '2021-07-12 22:36:23', '2021-07-12 19:36:23', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור', 'home_main_link', 'publish', 'closed', 'closed', '', 'field_60ec99a43abe3', '', '', '2021-07-12 22:36:23', '2021-07-12 19:36:23', '', 165, 'http://gordon:8888/?post_type=acf-field&p=170', 4, 'acf-field', '', 0),
(171, 1, '2021-07-12 22:38:50', '2021-07-12 19:38:50', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'מבחר שירותיים ומוצרים', 'מבחר_שירותיים_ומוצרים', 'publish', 'closed', 'closed', '', 'field_60ec99c3781a8', '', '', '2021-07-12 22:38:50', '2021-07-12 19:38:50', '', 165, 'http://gordon:8888/?post_type=acf-field&p=171', 5, 'acf-field', '', 0),
(172, 1, '2021-07-12 22:38:50', '2021-07-12 19:38:50', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'טקסט', 'home_serv_prod_text', 'publish', 'closed', 'closed', '', 'field_60ec99ce781a9', '', '', '2021-07-12 22:38:50', '2021-07-12 19:38:50', '', 165, 'http://gordon:8888/?post_type=acf-field&p=172', 6, 'acf-field', '', 0),
(173, 1, '2021-07-12 22:38:50', '2021-07-12 19:38:50', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'פריט', 'home_serv_prod_item', 'publish', 'closed', 'closed', '', 'field_60ec99e5781aa', '', '', '2021-07-12 22:38:50', '2021-07-12 19:38:50', '', 165, 'http://gordon:8888/?post_type=acf-field&p=173', 7, 'acf-field', '', 0),
(174, 1, '2021-07-12 22:38:50', '2021-07-12 19:38:50', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'block_item_img', 'publish', 'closed', 'closed', '', 'field_60ec99f5781ab', '', '', '2021-07-12 22:38:50', '2021-07-12 19:38:50', '', 173, 'http://gordon:8888/?post_type=acf-field&p=174', 0, 'acf-field', '', 0),
(175, 1, '2021-07-12 22:38:50', '2021-07-12 19:38:50', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת קטנה', 'block_item_small_title', 'publish', 'closed', 'closed', '', 'field_60ec9a10781ac', '', '', '2021-07-12 22:38:50', '2021-07-12 19:38:50', '', 173, 'http://gordon:8888/?post_type=acf-field&p=175', 1, 'acf-field', '', 0),
(176, 1, '2021-07-12 22:38:51', '2021-07-12 19:38:51', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת גדולה', 'block_item_big_title', 'publish', 'closed', 'closed', '', 'field_60ec9a22781ad', '', '', '2021-07-12 22:38:51', '2021-07-12 19:38:51', '', 173, 'http://gordon:8888/?post_type=acf-field&p=176', 2, 'acf-field', '', 0),
(177, 1, '2021-07-12 22:38:51', '2021-07-12 19:38:51', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור', 'block_item_link', 'publish', 'closed', 'closed', '', 'field_60ec9a33781ae', '', '', '2021-07-12 22:38:51', '2021-07-12 19:38:51', '', 173, 'http://gordon:8888/?post_type=acf-field&p=177', 3, 'acf-field', '', 0),
(178, 1, '2021-07-12 22:41:33', '2021-07-12 19:41:33', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'בלוק עם חיפוש', 'בלוק_עם_חיפוש', 'publish', 'closed', 'closed', '', 'field_60ec9a518787e', '', '', '2021-07-12 22:41:33', '2021-07-12 19:41:33', '', 165, 'http://gordon:8888/?post_type=acf-field&p=178', 8, 'acf-field', '', 0),
(179, 1, '2021-07-12 22:41:33', '2021-07-12 19:41:33', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'טקסט', 'home_search_text', 'publish', 'closed', 'closed', '', 'field_60ec9a6b8787f', '', '', '2021-07-12 22:41:33', '2021-07-12 19:41:33', '', 165, 'http://gordon:8888/?post_type=acf-field&p=179', 9, 'acf-field', '', 0),
(180, 1, '2021-07-12 22:41:33', '2021-07-12 19:41:33', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'סליידר עם באנרים', 'סליידר_עם_באנרים', 'publish', 'closed', 'closed', '', 'field_60ec9a7c87880', '', '', '2021-07-12 22:41:33', '2021-07-12 19:41:33', '', 165, 'http://gordon:8888/?post_type=acf-field&p=180', 10, 'acf-field', '', 0),
(181, 1, '2021-07-12 22:41:33', '2021-07-12 19:41:33', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'סליידר עם באנרים', 'slider_banner', 'publish', 'closed', 'closed', '', 'field_60ec9aae87881', '', '', '2021-07-12 22:41:33', '2021-07-12 19:41:33', '', 165, 'http://gordon:8888/?post_type=acf-field&p=181', 11, 'acf-field', '', 0),
(182, 1, '2021-07-12 22:41:33', '2021-07-12 19:41:33', 'a:18:{s:4:\"type\";s:7:\"gallery\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:6:\"insert\";s:6:\"append\";s:7:\"library\";s:3:\"all\";s:3:\"min\";i:0;s:3:\"max\";i:0;s:9:\"min_width\";i:0;s:10:\"min_height\";i:0;s:8:\"min_size\";i:0;s:9:\"max_width\";i:0;s:10:\"max_height\";i:0;s:8:\"max_size\";i:0;s:10:\"mime_types\";s:0:\"\";}', 'תמונות', 'slider_images', 'publish', 'closed', 'closed', '', 'field_60ec9adc87883', '', '', '2021-07-12 22:41:33', '2021-07-12 19:41:33', '', 181, 'http://gordon:8888/?post_type=acf-field&p=182', 0, 'acf-field', '', 0),
(184, 1, '2021-07-12 22:45:20', '2021-07-12 19:45:20', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'בלוק עם מוצרים', 'בלוק_עם_מוצרים', 'publish', 'closed', 'closed', '', 'field_60ec9b0bac424', '', '', '2021-07-12 22:45:20', '2021-07-12 19:45:20', '', 165, 'http://gordon:8888/?post_type=acf-field&p=184', 12, 'acf-field', '', 0),
(185, 1, '2021-07-12 22:45:20', '2021-07-12 19:45:20', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"70\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'טקסט', 'h_block_prod_text', 'publish', 'closed', 'closed', '', 'field_60ec9b18ac425', '', '', '2021-07-12 22:45:20', '2021-07-12 19:45:20', '', 165, 'http://gordon:8888/?post_type=acf-field&p=185', 13, 'acf-field', '', 0),
(186, 1, '2021-07-12 22:45:20', '2021-07-12 19:45:20', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"30\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור לכל המוצרים', 'h_block_prod_link', 'publish', 'closed', 'closed', '', 'field_60ec9b71ac426', '', '', '2021-07-12 22:45:20', '2021-07-12 19:45:20', '', 165, 'http://gordon:8888/?post_type=acf-field&p=186', 14, 'acf-field', '', 0),
(187, 1, '2021-07-12 22:45:20', '2021-07-12 19:45:20', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'מוצרים custom', 'h_block_prod_custom', 'publish', 'closed', 'closed', '', 'field_60ec9b8aac427', '', '', '2021-07-12 22:45:20', '2021-07-12 19:45:20', '', 165, 'http://gordon:8888/?post_type=acf-field&p=187', 15, 'acf-field', '', 0),
(188, 1, '2021-07-12 22:45:20', '2021-07-12 19:45:20', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'prod_custom_img', 'publish', 'closed', 'closed', '', 'field_60ec9b9eac428', '', '', '2021-07-12 22:45:20', '2021-07-12 19:45:20', '', 187, 'http://gordon:8888/?post_type=acf-field&p=188', 0, 'acf-field', '', 0),
(189, 1, '2021-07-12 22:45:20', '2021-07-12 19:45:20', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'prod_custom_title', 'publish', 'closed', 'closed', '', 'field_60ec9bb6ac429', '', '', '2021-07-12 22:45:20', '2021-07-12 19:45:20', '', 187, 'http://gordon:8888/?post_type=acf-field&p=189', 1, 'acf-field', '', 0),
(190, 1, '2021-07-12 22:45:20', '2021-07-12 19:45:20', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור', 'prod_custom_link', 'publish', 'closed', 'closed', '', 'field_60ec9bc0ac42a', '', '', '2021-07-12 22:45:20', '2021-07-12 19:45:20', '', 187, 'http://gordon:8888/?post_type=acf-field&p=190', 2, 'acf-field', '', 0),
(191, 1, '2021-07-12 22:56:44', '2021-07-12 19:56:44', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'קורסים', 'קורסים', 'publish', 'closed', 'closed', '', 'field_60ec9cfc2bc43', '', '', '2021-07-12 22:56:44', '2021-07-12 19:56:44', '', 165, 'http://gordon:8888/?post_type=acf-field&p=191', 16, 'acf-field', '', 0),
(192, 1, '2021-07-12 22:56:44', '2021-07-12 19:56:44', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"60\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'טקסט', 'home_course_text', 'publish', 'closed', 'closed', '', 'field_60ec9d062bc44', '', '', '2021-07-12 22:56:44', '2021-07-12 19:56:44', '', 165, 'http://gordon:8888/?post_type=acf-field&p=192', 17, 'acf-field', '', 0),
(193, 1, '2021-07-12 22:56:44', '2021-07-12 19:56:44', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"40\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'home_course_img', 'publish', 'closed', 'closed', '', 'field_60ec9d252bc45', '', '', '2021-07-12 22:56:44', '2021-07-12 19:56:44', '', 165, 'http://gordon:8888/?post_type=acf-field&p=193', 18, 'acf-field', '', 0),
(194, 1, '2021-07-12 22:56:44', '2021-07-12 19:56:44', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'קורסים', 'home_course_item', 'publish', 'closed', 'closed', '', 'field_60ec9d532bc46', '', '', '2021-07-12 22:56:44', '2021-07-12 19:56:44', '', 165, 'http://gordon:8888/?post_type=acf-field&p=194', 19, 'acf-field', '', 0),
(195, 1, '2021-07-12 22:56:44', '2021-07-12 19:56:44', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'course_img', 'publish', 'closed', 'closed', '', 'field_60ec9d6e2bc47', '', '', '2021-07-12 22:56:44', '2021-07-12 19:56:44', '', 194, 'http://gordon:8888/?post_type=acf-field&p=195', 0, 'acf-field', '', 0),
(196, 1, '2021-07-12 22:56:44', '2021-07-12 19:56:44', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'course_title', 'publish', 'closed', 'closed', '', 'field_60ec9d7a2bc48', '', '', '2021-07-12 22:56:44', '2021-07-12 19:56:44', '', 194, 'http://gordon:8888/?post_type=acf-field&p=196', 1, 'acf-field', '', 0),
(197, 1, '2021-07-12 22:56:44', '2021-07-12 19:56:44', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור', 'course_link', 'publish', 'closed', 'closed', '', 'field_60ec9d842bc49', '', '', '2021-07-12 22:56:44', '2021-07-12 19:56:44', '', 194, 'http://gordon:8888/?post_type=acf-field&p=197', 2, 'acf-field', '', 0),
(198, 1, '2021-07-12 22:56:44', '2021-07-12 19:56:44', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור לכל הקורסים', 'home_course_link', 'publish', 'closed', 'closed', '', 'field_60ec9d922bc4a', '', '', '2021-07-12 22:56:44', '2021-07-12 19:56:44', '', 165, 'http://gordon:8888/?post_type=acf-field&p=198', 20, 'acf-field', '', 0),
(199, 1, '2021-07-12 22:56:44', '2021-07-12 19:56:44', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'סליידר עם מוצרים', 'home_prod_slider', 'publish', 'closed', 'closed', '', 'field_60ec9dab2bc4b', '', '', '2021-07-12 22:56:44', '2021-07-12 19:56:44', '', 165, 'http://gordon:8888/?post_type=acf-field&p=199', 21, 'acf-field', '', 0),
(200, 1, '2021-07-12 22:56:44', '2021-07-12 19:56:44', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"70\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'טקסט', 'home_prod_slider_text', 'publish', 'closed', 'closed', '', 'field_60ec9dc32bc4c', '', '', '2021-07-12 22:56:44', '2021-07-12 19:56:44', '', 165, 'http://gordon:8888/?post_type=acf-field&p=200', 22, 'acf-field', '', 0),
(201, 1, '2021-07-12 22:56:44', '2021-07-12 19:56:44', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"30\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור לכל המוצרים', 'home_prod_slider_link', 'publish', 'closed', 'closed', '', 'field_60ec9dda2bc4d', '', '', '2021-07-12 22:56:44', '2021-07-12 19:56:44', '', 165, 'http://gordon:8888/?post_type=acf-field&p=201', 23, 'acf-field', '', 0),
(202, 1, '2021-07-12 22:56:44', '2021-07-12 19:56:44', 'a:12:{s:4:\"type\";s:12:\"relationship\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:7:\"product\";}s:8:\"taxonomy\";s:0:\"\";s:7:\"filters\";a:3:{i:0;s:6:\"search\";i:1;s:9:\"post_type\";i:2;s:8:\"taxonomy\";}s:8:\"elements\";a:1:{i:0;s:14:\"featured_image\";}s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:13:\"return_format\";s:6:\"object\";}', 'סליידר עם מוצרים', 'home_prod_slider', 'publish', 'closed', 'closed', '', 'field_60ec9df72bc4e', '', '', '2021-07-12 22:56:44', '2021-07-12 19:56:44', '', 165, 'http://gordon:8888/?post_type=acf-field&p=202', 24, 'acf-field', '', 0),
(203, 1, '2021-07-12 22:56:44', '2021-07-12 19:56:44', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'יתרונות', 'יתרונות', 'publish', 'closed', 'closed', '', 'field_60ec9e092bc4f', '', '', '2021-07-12 22:56:44', '2021-07-12 19:56:44', '', 165, 'http://gordon:8888/?post_type=acf-field&p=203', 25, 'acf-field', '', 0),
(204, 1, '2021-07-12 22:56:44', '2021-07-12 19:56:44', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"75\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'סליידר עם יתרונות', 'slider_benefits', 'publish', 'closed', 'closed', '', 'field_60ec9e352bc50', '', '', '2021-07-12 23:51:05', '2021-07-12 20:51:05', '', 165, 'http://gordon:8888/?post_type=acf-field&#038;p=204', 27, 'acf-field', '', 0);
INSERT INTO `fmn_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(205, 1, '2021-07-12 22:56:44', '2021-07-12 19:56:44', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'טקסט', 'benefit_content', 'publish', 'closed', 'closed', '', 'field_60ec9e4c2bc51', '', '', '2021-07-12 22:56:44', '2021-07-12 19:56:44', '', 204, 'http://gordon:8888/?post_type=acf-field&p=205', 0, 'acf-field', '', 0),
(206, 1, '2021-07-12 22:56:44', '2021-07-12 19:56:44', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"25\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'slider_benefits_img', 'publish', 'closed', 'closed', '', 'field_60ec9e5f2bc52', '', '', '2021-07-12 23:51:05', '2021-07-12 20:51:05', '', 165, 'http://gordon:8888/?post_type=acf-field&#038;p=206', 28, 'acf-field', '', 0),
(207, 1, '2021-07-12 22:59:07', '2021-07-12 19:59:07', '', 'course-1', '', 'inherit', 'open', 'closed', '', 'course-1-2', '', '', '2021-07-12 22:59:07', '2021-07-12 19:59:07', '', 67, 'http://gordon:8888/wp-content/uploads/2021/07/course-1-1.png', 0, 'attachment', 'image/png', 0),
(208, 1, '2021-07-12 22:59:10', '2021-07-12 19:59:10', '', 'course-2', '', 'inherit', 'open', 'closed', '', 'course-2-2', '', '', '2021-07-12 22:59:10', '2021-07-12 19:59:10', '', 67, 'http://gordon:8888/wp-content/uploads/2021/07/course-2-1.png', 0, 'attachment', 'image/png', 0),
(209, 1, '2021-07-12 22:59:13', '2021-07-12 19:59:13', '', 'course-3', '', 'inherit', 'open', 'closed', '', 'course-3-2', '', '', '2021-07-12 23:11:09', '2021-07-12 20:11:09', '', 67, 'http://gordon:8888/wp-content/uploads/2021/07/course-3-1.png', 0, 'attachment', 'image/png', 0),
(210, 1, '2021-07-12 22:59:15', '2021-07-12 19:59:15', '', 'course-4', '', 'inherit', 'open', 'closed', '', 'course-4-2', '', '', '2021-07-12 22:59:15', '2021-07-12 19:59:15', '', 67, 'http://gordon:8888/wp-content/uploads/2021/07/course-4-1.png', 0, 'attachment', 'image/png', 0),
(211, 1, '2021-07-12 23:00:00', '2021-07-12 20:00:00', '', 'courses-img', '', 'inherit', 'open', 'closed', '', 'courses-img', '', '', '2021-07-12 23:00:00', '2021-07-12 20:00:00', '', 67, 'http://gordon:8888/wp-content/uploads/2021/07/courses-img.png', 0, 'attachment', 'image/png', 0),
(212, 1, '2021-07-12 23:03:46', '2021-07-12 20:03:46', '', 'slider-benefits-img', '', 'inherit', 'open', 'closed', '', 'slider-benefits-img', '', '', '2021-07-12 23:03:46', '2021-07-12 20:03:46', '', 67, 'http://gordon:8888/wp-content/uploads/2021/07/slider-benefits-img.png', 0, 'attachment', 'image/png', 0),
(213, 1, '2021-07-12 23:06:07', '2021-07-12 20:06:07', '', 'main-img', '', 'inherit', 'open', 'closed', '', 'main-img', '', '', '2021-07-12 23:06:07', '2021-07-12 20:06:07', '', 67, 'http://gordon:8888/wp-content/uploads/2021/07/main-img.png', 0, 'attachment', 'image/png', 0),
(214, 1, '2021-07-12 23:07:10', '2021-07-12 20:07:10', '', 'home-part-img-1', '', 'inherit', 'open', 'closed', '', 'home-part-img-1', '', '', '2021-07-12 23:07:10', '2021-07-12 20:07:10', '', 67, 'http://gordon:8888/wp-content/uploads/2021/07/home-part-img-1.png', 0, 'attachment', 'image/png', 0),
(215, 1, '2021-07-12 23:07:12', '2021-07-12 20:07:12', '', 'home-part-img-2', '', 'inherit', 'open', 'closed', '', 'home-part-img-2', '', '', '2021-07-12 23:07:12', '2021-07-12 20:07:12', '', 67, 'http://gordon:8888/wp-content/uploads/2021/07/home-part-img-2.png', 0, 'attachment', 'image/png', 0),
(216, 1, '2021-07-12 23:10:42', '2021-07-12 20:10:42', '', 'baner-1', '', 'inherit', 'open', 'closed', '', 'baner-1', '', '', '2021-07-12 23:10:42', '2021-07-12 20:10:42', '', 67, 'http://gordon:8888/wp-content/uploads/2021/07/baner-1.png', 0, 'attachment', 'image/png', 0),
(217, 1, '2021-07-12 23:10:44', '2021-07-12 20:10:44', '', 'baner-2', '', 'inherit', 'open', 'closed', '', 'baner-2', '', '', '2021-07-12 23:10:44', '2021-07-12 20:10:44', '', 67, 'http://gordon:8888/wp-content/uploads/2021/07/baner-2.png', 0, 'attachment', 'image/png', 0),
(218, 1, '2021-07-12 23:10:46', '2021-07-12 20:10:46', '', 'baner-3', '', 'inherit', 'open', 'closed', '', 'baner-3', '', '', '2021-07-12 23:10:46', '2021-07-12 20:10:46', '', 67, 'http://gordon:8888/wp-content/uploads/2021/07/baner-3.png', 0, 'attachment', 'image/png', 0),
(219, 1, '2021-07-12 23:11:47', '2021-07-12 20:11:47', '', 'post-1', '', 'inherit', 'open', 'closed', '', 'post-1-2', '', '', '2021-07-12 23:11:47', '2021-07-12 20:11:47', '', 67, 'http://gordon:8888/wp-content/uploads/2021/07/post-1-1.png', 0, 'attachment', 'image/png', 0),
(220, 1, '2021-07-12 23:11:49', '2021-07-12 20:11:49', '', 'post-2', '', 'inherit', 'open', 'closed', '', 'post-2-2', '', '', '2021-07-12 23:11:49', '2021-07-12 20:11:49', '', 67, 'http://gordon:8888/wp-content/uploads/2021/07/post-2-1.png', 0, 'attachment', 'image/png', 0),
(221, 1, '2021-07-12 23:11:52', '2021-07-12 20:11:52', '', 'post-3', '', 'inherit', 'open', 'closed', '', 'post-3-2', '', '', '2021-07-12 23:11:52', '2021-07-12 20:11:52', '', 67, 'http://gordon:8888/wp-content/uploads/2021/07/post-3-1.png', 0, 'attachment', 'image/png', 0),
(222, 1, '2021-07-12 23:13:07', '2021-07-12 20:13:07', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '67-revision-v1', '', '', '2021-07-12 23:13:07', '2021-07-12 20:13:07', '', 67, 'http://gordon:8888/?p=222', 0, 'revision', '', 0),
(223, 1, '2021-07-12 23:51:04', '2021-07-12 20:51:04', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת של סליידר עם יתרונות', 'slider_benefits_title', 'publish', 'closed', 'closed', '', 'field_60ecab1140ae5', '', '', '2021-07-12 23:51:04', '2021-07-12 20:51:04', '', 165, 'http://gordon:8888/?post_type=acf-field&p=223', 26, 'acf-field', '', 0),
(224, 1, '2021-07-12 23:51:41', '2021-07-12 20:51:41', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '67-revision-v1', '', '', '2021-07-12 23:51:41', '2021-07-12 20:51:41', '', 67, 'http://gordon:8888/?p=224', 0, 'revision', '', 0),
(225, 1, '2021-07-13 09:17:00', '2021-07-13 06:17:00', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '67-revision-v1', '', '', '2021-07-13 09:17:00', '2021-07-13 06:17:00', '', 67, 'http://gordon:8888/?p=225', 0, 'revision', '', 0),
(226, 1, '2021-07-13 09:37:23', '2021-07-13 06:37:23', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:17:\"views/gallery.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'גלריה', '%d7%92%d7%9c%d7%a8%d7%99%d7%94', 'publish', 'closed', 'closed', '', 'group_60ed3463adf14', '', '', '2021-07-13 09:37:23', '2021-07-13 06:37:23', '', 0, 'http://gordon:8888/?post_type=acf-field-group&#038;p=226', 0, 'acf-field-group', '', 0),
(227, 1, '2021-07-13 09:37:23', '2021-07-13 06:37:23', 'a:18:{s:4:\"type\";s:7:\"gallery\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:6:\"insert\";s:6:\"append\";s:7:\"library\";s:3:\"all\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונות', 'gallery_main', 'publish', 'closed', 'closed', '', 'field_60ed346d48181', '', '', '2021-07-13 09:37:23', '2021-07-13 06:37:23', '', 226, 'http://gordon:8888/?post_type=acf-field&p=227', 0, 'acf-field', '', 0),
(228, 1, '2021-07-13 09:37:49', '2021-07-13 06:37:49', '', 'גלריה', '', 'publish', 'closed', 'closed', '', '%d7%92%d7%9c%d7%a8%d7%99%d7%94', '', '', '2021-07-13 09:38:18', '2021-07-13 06:38:18', '', 0, 'http://gordon:8888/?page_id=228', 0, 'page', '', 0),
(229, 1, '2021-07-13 09:37:49', '2021-07-13 06:37:49', '', 'גלריה', '', 'inherit', 'closed', 'closed', '', '228-revision-v1', '', '', '2021-07-13 09:37:49', '2021-07-13 06:37:49', '', 228, 'http://gordon:8888/?p=229', 0, 'revision', '', 0),
(230, 1, '2021-07-13 09:38:18', '2021-07-13 06:38:18', '', 'גלריה', '', 'inherit', 'closed', 'closed', '', '228-revision-v1', '', '', '2021-07-13 09:38:18', '2021-07-13 06:38:18', '', 228, 'http://gordon:8888/?p=230', 0, 'revision', '', 0),
(231, 1, '2021-07-13 09:39:35', '2021-07-13 06:39:35', '', 'מאמרים', '', 'publish', 'closed', 'closed', '', '%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d', '', '', '2021-07-13 14:26:56', '2021-07-13 11:26:56', '', 0, 'http://gordon:8888/?page_id=231', 0, 'page', '', 0),
(232, 1, '2021-07-13 09:39:35', '2021-07-13 06:39:35', '', 'מאמרים', '', 'inherit', 'closed', 'closed', '', '231-revision-v1', '', '', '2021-07-13 09:39:35', '2021-07-13 06:39:35', '', 231, 'http://gordon:8888/?p=232', 0, 'revision', '', 0),
(233, 1, '2021-07-13 09:40:07', '2021-07-13 06:40:07', ' ', '', '', 'publish', 'closed', 'closed', '', '233', '', '', '2021-07-13 09:40:07', '2021-07-13 06:40:07', '', 0, 'http://gordon:8888/?p=233', 1, 'nav_menu_item', '', 0),
(234, 1, '2021-07-13 09:40:07', '2021-07-13 06:40:07', ' ', '', '', 'publish', 'closed', 'closed', '', '234', '', '', '2021-07-13 09:40:07', '2021-07-13 06:40:07', '', 0, 'http://gordon:8888/?p=234', 3, 'nav_menu_item', '', 0),
(235, 1, '2021-07-13 10:17:21', '2021-07-13 07:17:21', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"post\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'מאמר', '%d7%9e%d7%90%d7%9e%d7%a8', 'publish', 'closed', 'closed', '', 'group_60ed3c061ffe4', '', '', '2021-07-13 10:55:47', '2021-07-13 07:55:47', '', 0, 'http://gordon:8888/?post_type=acf-field-group&#038;p=235', 0, 'acf-field-group', '', 0),
(236, 1, '2021-07-13 10:17:21', '2021-07-13 07:17:21', 'a:18:{s:4:\"type\";s:7:\"gallery\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:6:\"insert\";s:6:\"append\";s:7:\"library\";s:3:\"all\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונות אחרי תוכן', 'post_gallery', 'publish', 'closed', 'closed', '', 'field_60ed3c0bf26b4', '', '', '2021-07-13 10:17:21', '2021-07-13 07:17:21', '', 235, 'http://gordon:8888/?post_type=acf-field&p=236', 0, 'acf-field', '', 0),
(237, 1, '2021-07-13 10:17:21', '2021-07-13 07:17:21', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'קישורים בנושא', 'pos', 'publish', 'closed', 'closed', '', 'field_60ed3da7f26b5', '', '', '2021-07-13 10:17:21', '2021-07-13 07:17:21', '', 235, 'http://gordon:8888/?post_type=acf-field&p=237', 1, 'acf-field', '', 0),
(238, 1, '2021-07-13 10:17:21', '2021-07-13 07:17:21', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'post_links_title', 'publish', 'closed', 'closed', '', 'field_60ed3dcef26b6', '', '', '2021-07-13 10:17:21', '2021-07-13 07:17:21', '', 235, 'http://gordon:8888/?post_type=acf-field&p=238', 2, 'acf-field', '', 0),
(239, 1, '2021-07-13 10:17:21', '2021-07-13 07:17:21', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'קישורים בנושא', 'post_link_item', 'publish', 'closed', 'closed', '', 'field_60ed3ddef26b7', '', '', '2021-07-13 10:17:21', '2021-07-13 07:17:21', '', 235, 'http://gordon:8888/?post_type=acf-field&p=239', 3, 'acf-field', '', 0),
(240, 1, '2021-07-13 10:17:21', '2021-07-13 07:17:21', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'link_title', 'publish', 'closed', 'closed', '', 'field_60ed3decf26b8', '', '', '2021-07-13 10:17:21', '2021-07-13 07:17:21', '', 239, 'http://gordon:8888/?post_type=acf-field&p=240', 0, 'acf-field', '', 0),
(241, 1, '2021-07-13 10:17:21', '2021-07-13 07:17:21', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור', 'link_url', 'publish', 'closed', 'closed', '', 'field_60ed3df3f26b9', '', '', '2021-07-13 10:17:21', '2021-07-13 07:17:21', '', 239, 'http://gordon:8888/?post_type=acf-field&p=241', 1, 'acf-field', '', 0),
(242, 1, '2021-07-13 10:53:07', '2021-07-13 07:53:07', '<!-- wp:paragraph -->\n<p>לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה.</p>\n<h2>כאן תופיע כותרת משנה או תת כותרת נוספת</h2>\n<p>לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה.</p>\n<!-- /wp:paragraph -->', 'שלום עולם! TEST', '', 'inherit', 'closed', 'closed', '', '1-autosave-v1', '', '', '2021-07-13 10:53:07', '2021-07-13 07:53:07', '', 1, 'http://gordon:8888/?p=242', 0, 'revision', '', 0),
(243, 1, '2021-07-13 10:53:38', '2021-07-13 07:53:38', '<!-- wp:paragraph -->\r\n<p>לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה.</p>\r\n<h2>כאן תופיע כותרת משנה או תת כותרת נוספת</h2>\r\n<p>לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה.</p>\r\n<!-- /wp:paragraph -->', 'שלום עולם! TEST', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2021-07-13 10:53:38', '2021-07-13 07:53:38', '', 1, 'http://gordon:8888/?p=243', 0, 'revision', '', 0),
(244, 1, '2021-07-13 10:55:47', '2021-07-13 07:55:47', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'מאמרים נוספים', 'מאמרים_נוספים', 'publish', 'closed', 'closed', '', 'field_60ed46d077c77', '', '', '2021-07-13 10:55:47', '2021-07-13 07:55:47', '', 235, 'http://gordon:8888/?post_type=acf-field&p=244', 4, 'acf-field', '', 0),
(245, 1, '2021-07-13 10:55:47', '2021-07-13 07:55:47', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'same_title', 'publish', 'closed', 'closed', '', 'field_60ed46e077c78', '', '', '2021-07-13 10:55:47', '2021-07-13 07:55:47', '', 235, 'http://gordon:8888/?post_type=acf-field&p=245', 5, 'acf-field', '', 0),
(246, 1, '2021-07-13 10:55:47', '2021-07-13 07:55:47', 'a:12:{s:4:\"type\";s:12:\"relationship\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:4:\"post\";}s:8:\"taxonomy\";s:0:\"\";s:7:\"filters\";a:3:{i:0;s:6:\"search\";i:1;s:9:\"post_type\";i:2;s:8:\"taxonomy\";}s:8:\"elements\";a:1:{i:0;s:14:\"featured_image\";}s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:13:\"return_format\";s:6:\"object\";}', 'מאמרים נוספים', 'same_posts', 'publish', 'closed', 'closed', '', 'field_60ed46ea77c79', '', '', '2021-07-13 10:55:47', '2021-07-13 07:55:47', '', 235, 'http://gordon:8888/?post_type=acf-field&p=246', 6, 'acf-field', '', 0),
(247, 1, '2021-07-13 11:02:13', '2021-07-13 08:02:13', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'פוסט', 'פוסט', 'publish', 'closed', 'closed', '', 'field_60ed4848102f1', '', '', '2021-07-13 11:02:13', '2021-07-13 08:02:13', '', 115, 'http://gordon:8888/?post_type=acf-field&p=247', 5, 'acf-field', '', 0),
(248, 1, '2021-07-13 11:02:13', '2021-07-13 08:02:13', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור לחנות', 'shop_link', 'publish', 'closed', 'closed', '', 'field_60ed4859102f2', '', '', '2021-07-13 11:02:13', '2021-07-13 08:02:13', '', 115, 'http://gordon:8888/?post_type=acf-field&p=248', 6, 'acf-field', '', 0),
(249, 1, '2021-07-13 11:02:14', '2021-07-13 08:02:14', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'shop_img', 'publish', 'closed', 'closed', '', 'field_60ed486f102f3', '', '', '2021-07-13 11:02:14', '2021-07-13 08:02:14', '', 115, 'http://gordon:8888/?post_type=acf-field&p=249', 7, 'acf-field', '', 0),
(250, 1, '2021-07-13 11:03:01', '2021-07-13 08:03:01', '', 'girls', '', 'inherit', 'open', 'closed', '', 'girls', '', '', '2021-07-13 11:03:01', '2021-07-13 08:03:01', '', 0, 'http://gordon:8888/wp-content/uploads/2021/07/girls.png', 0, 'attachment', 'image/png', 0),
(251, 1, '2021-07-13 13:19:45', '2021-07-13 10:19:45', '', 'שם המוצר לורם איפסום 6', '<p><br data-mce-bogus=\"1\"></p>', 'inherit', 'closed', 'closed', '', '66-autosave-v1', '', '', '2021-07-13 13:19:45', '2021-07-13 10:19:45', '', 66, 'http://gordon:8888/?p=251', 0, 'revision', '', 0),
(252, 1, '2021-07-13 14:26:56', '2021-07-13 11:26:56', '', 'מאמרים', '', 'inherit', 'closed', 'closed', '', '231-revision-v1', '', '', '2021-07-13 14:26:56', '2021-07-13 11:26:56', '', 231, 'http://gordon:8888/?p=252', 0, 'revision', '', 0),
(253, 1, '2021-07-13 14:50:11', '2021-07-13 11:50:11', '', 'חנות', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-07-13 14:50:11', '2021-07-13 11:50:11', '', 7, 'http://gordon:8888/?p=253', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_termmeta`
--

CREATE TABLE `fmn_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_termmeta`
--

INSERT INTO `fmn_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(1, 15, 'product_count_product_cat', '5'),
(2, 15, 'display_type', ''),
(3, 15, 'thumbnail_id', '0'),
(4, 24, 'order', '0'),
(5, 24, 'display_type', ''),
(6, 24, 'thumbnail_id', '0'),
(7, 24, 'product_count_product_cat', '2'),
(8, 1, 'single_slider_seo', ''),
(9, 1, '_single_slider_seo', 'field_5ddbde5499115'),
(10, 1, 'slider_img', ''),
(11, 1, '_slider_img', 'field_60e988d9254ba');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_terms`
--

CREATE TABLE `fmn_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_terms`
--

INSERT INTO `fmn_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'כללי', '%d7%9b%d7%9c%d7%9c%d7%99', 0),
(2, 'simple', 'simple', 0),
(3, 'grouped', 'grouped', 0),
(4, 'variable', 'variable', 0),
(5, 'external', 'external', 0),
(6, 'exclude-from-search', 'exclude-from-search', 0),
(7, 'exclude-from-catalog', 'exclude-from-catalog', 0),
(8, 'featured', 'featured', 0),
(9, 'outofstock', 'outofstock', 0),
(10, 'rated-1', 'rated-1', 0),
(11, 'rated-2', 'rated-2', 0),
(12, 'rated-3', 'rated-3', 0),
(13, 'rated-4', 'rated-4', 0),
(14, 'rated-5', 'rated-5', 0),
(15, 'כללי', 'uncategorized', 0),
(16, 'Header menu', 'header-menu', 0),
(17, 'Footer menu main', 'footer-menu-main', 0),
(18, 'Footer menu second', 'footer-menu-second', 0),
(19, 'Footer menu third', 'footer-menu-third', 0),
(20, 'Footer menu fourth', 'footer-menu-fourth', 0),
(21, 'Mega menu top', 'mega-menu-top', 0),
(22, 'Mega menu right', 'mega-menu-right', 0),
(23, 'Mega menu left', 'mega-menu-left', 0),
(24, 'קטגורית מוצרים לטסט', '%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%aa-%d7%9e%d7%95%d7%a6%d7%a8%d7%99%d7%9d-%d7%9c%d7%98%d7%a1%d7%98', 0),
(25, 'מותג לורם איפסום 1', '%d7%9e%d7%95%d7%aa%d7%92-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-1', 0),
(26, 'מותג לורם איפסום 2', '%d7%9e%d7%95%d7%aa%d7%92-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-2', 0),
(27, 'תחום דעת 1', '%d7%aa%d7%97%d7%95%d7%9d-%d7%93%d7%a2%d7%aa-1', 0),
(28, 'תחום דעת 2', '%d7%aa%d7%97%d7%95%d7%9d-%d7%93%d7%a2%d7%aa-2', 0),
(29, 'תחום דעת 3', '%d7%aa%d7%97%d7%95%d7%9d-%d7%93%d7%a2%d7%aa-3', 0),
(30, 'טכנולוגיה לורם איפסום 1', '%d7%98%d7%9b%d7%a0%d7%95%d7%9c%d7%95%d7%92%d7%99%d7%94-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-1', 0),
(31, 'טכנולוגיה לורם איפסום 2', '%d7%98%d7%9b%d7%a0%d7%95%d7%9c%d7%95%d7%92%d7%99%d7%94-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-2', 0),
(32, 'טכנולוגיה לורם איפסום 3', '%d7%98%d7%9b%d7%a0%d7%95%d7%9c%d7%95%d7%92%d7%99%d7%94-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-3', 0),
(33, 'גיל לורם איפסום 1', '%d7%92%d7%99%d7%9c-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-1', 0),
(34, 'גיל לורם איפסום 2', '%d7%92%d7%99%d7%9c-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-2', 0),
(35, 'גיל לורם איפסום 3', '%d7%92%d7%99%d7%9c-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-3', 0),
(36, 'גיל לורם איפסום 4', '%d7%92%d7%99%d7%9c-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-4', 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_term_relationships`
--

CREATE TABLE `fmn_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_term_relationships`
--

INSERT INTO `fmn_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(13, 1, 0),
(21, 1, 0),
(32, 1, 0),
(41, 1, 0),
(43, 1, 0),
(45, 1, 0),
(47, 1, 0),
(52, 2, 0),
(52, 24, 0),
(52, 25, 0),
(52, 27, 0),
(52, 30, 0),
(52, 33, 0),
(61, 2, 0),
(61, 15, 0),
(61, 26, 0),
(61, 28, 0),
(61, 31, 0),
(61, 34, 0),
(62, 2, 0),
(62, 24, 0),
(62, 25, 0),
(62, 29, 0),
(62, 32, 0),
(62, 35, 0),
(63, 2, 0),
(63, 15, 0),
(63, 26, 0),
(63, 28, 0),
(63, 30, 0),
(63, 36, 0),
(64, 2, 0),
(64, 15, 0),
(64, 27, 0),
(64, 31, 0),
(64, 36, 0),
(65, 2, 0),
(65, 15, 0),
(65, 27, 0),
(65, 31, 0),
(65, 34, 0),
(66, 2, 0),
(66, 15, 0),
(66, 28, 0),
(66, 31, 0),
(66, 33, 0),
(69, 16, 0),
(70, 16, 0),
(71, 16, 0),
(72, 17, 0),
(73, 17, 0),
(74, 17, 0),
(75, 17, 0),
(77, 17, 0),
(78, 18, 0),
(79, 18, 0),
(80, 18, 0),
(81, 18, 0),
(82, 18, 0),
(83, 18, 0),
(84, 18, 0),
(85, 19, 0),
(86, 19, 0),
(87, 19, 0),
(88, 19, 0),
(89, 20, 0),
(90, 20, 0),
(91, 20, 0),
(92, 20, 0),
(93, 20, 0),
(94, 20, 0),
(95, 20, 0),
(96, 20, 0),
(100, 21, 0),
(101, 22, 0),
(102, 22, 0),
(103, 22, 0),
(104, 22, 0),
(105, 22, 0),
(106, 22, 0),
(107, 23, 0),
(108, 23, 0),
(109, 23, 0),
(110, 23, 0),
(111, 23, 0),
(112, 23, 0),
(113, 23, 0),
(123, 21, 0),
(125, 17, 0),
(233, 17, 0),
(234, 17, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_term_taxonomy`
--

CREATE TABLE `fmn_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_term_taxonomy`
--

INSERT INTO `fmn_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 6),
(2, 2, 'product_type', '', 0, 7),
(3, 3, 'product_type', '', 0, 0),
(4, 4, 'product_type', '', 0, 0),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_visibility', '', 0, 0),
(7, 7, 'product_visibility', '', 0, 0),
(8, 8, 'product_visibility', '', 0, 0),
(9, 9, 'product_visibility', '', 0, 0),
(10, 10, 'product_visibility', '', 0, 0),
(11, 11, 'product_visibility', '', 0, 0),
(12, 12, 'product_visibility', '', 0, 0),
(13, 13, 'product_visibility', '', 0, 0),
(14, 14, 'product_visibility', '', 0, 0),
(15, 15, 'product_cat', '', 0, 5),
(16, 16, 'nav_menu', '', 0, 3),
(17, 17, 'nav_menu', '', 0, 8),
(18, 18, 'nav_menu', '', 0, 7),
(19, 19, 'nav_menu', '', 0, 4),
(20, 20, 'nav_menu', '', 0, 8),
(21, 21, 'nav_menu', '', 0, 2),
(22, 22, 'nav_menu', '', 0, 6),
(23, 23, 'nav_menu', '', 0, 7),
(24, 24, 'product_cat', '', 0, 2),
(25, 25, 'brand', '', 0, 2),
(26, 26, 'brand', '', 0, 2),
(27, 27, 'prod_area', '', 0, 3),
(28, 28, 'prod_area', '', 0, 3),
(29, 29, 'prod_area', '', 0, 1),
(30, 30, 'prod_tech', '', 0, 2),
(31, 31, 'prod_tech', '', 0, 4),
(32, 32, 'prod_tech', '', 0, 1),
(33, 33, 'prod_age', '', 0, 2),
(34, 34, 'prod_age', '', 0, 2),
(35, 35, 'prod_age', '', 0, 1),
(36, 36, 'prod_age', '', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_usermeta`
--

CREATE TABLE `fmn_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_usermeta`
--

INSERT INTO `fmn_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'dev_admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'fmn_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'fmn_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"c3a0637f8006e01b64e544db1faa0cea5b8aa0ad8ae58a77a4984137d7545c0c\";a:4:{s:10:\"expiration\";i:1626973486;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.101 Safari/537.36\";s:5:\"login\";i:1625763886;}}'),
(17, 1, 'fmn_dashboard_quick_press_last_post_id', '4'),
(18, 1, '_woocommerce_tracks_anon_id', 'woo:axKIPDDeboADdkKF64aH7fiz'),
(19, 1, 'last_update', '1626016136'),
(20, 1, 'woocommerce_admin_activity_panel_inbox_last_read', '1626016135006'),
(21, 1, 'wc_last_active', '1626134400'),
(22, 1, '_order_count', '0'),
(23, 1, 'fmn_user-settings', 'libraryContent=browse'),
(24, 1, 'fmn_user-settings-time', '1625998507'),
(25, 1, 'dismissed_no_secure_connection_notice', '1'),
(26, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(27, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:12:\"add-post_tag\";i:1;s:15:\"add-product_tag\";}'),
(28, 1, 'nav_menu_recently_edited', '17'),
(29, 1, 'closedpostboxes_nav-menus', 'a:0:{}'),
(30, 1, 'wpcf7_hide_welcome_panel_on', 'a:1:{i:0;s:3:\"5.4\";}'),
(31, 1, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(33, 1, 'meta-box-order_product', 'a:4:{s:15:\"acf_after_title\";s:0:\"\";s:4:\"side\";s:107:\"submitdiv,branddiv,product_catdiv,tagsdiv-product_tag,postimagediv,product_video,woocommerce-product-images\";s:6:\"normal\";s:91:\"acf-group_60ec00da2b99b,wpseo_meta,woocommerce-product-data,slugdiv,postexcerpt,commentsdiv\";s:8:\"advanced\";s:0:\"\";}'),
(34, 1, 'screen_layout_product', '2'),
(35, 1, '_yoast_wpseo_profile_updated', '1626088947'),
(36, 1, 'fmn_yoast_notifications', 'a:2:{i:0;a:2:{s:7:\"message\";s:288:\"<p>Because of a change in your permalink structure, some of your SEO data needs to be reprocessed.</p><p>We estimate this will take less than a minute.</p><a class=\"button\" href=\"http://gordon:8888/wp-admin/admin.php?page=wpseo_tools&start-indexation=true\">Start SEO data optimization</a>\";s:7:\"options\";a:10:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:13:\"wpseo-reindex\";s:4:\"user\";O:7:\"WP_User\":8:{s:4:\"data\";O:8:\"stdClass\":10:{s:2:\"ID\";s:1:\"1\";s:10:\"user_login\";s:9:\"dev_admin\";s:9:\"user_pass\";s:34:\"$P$B1xO7euiqHi4PQdHn5iQH2JyhoMcXr0\";s:13:\"user_nicename\";s:9:\"dev_admin\";s:10:\"user_email\";s:15:\"maxf@leos.co.il\";s:8:\"user_url\";s:0:\"\";s:15:\"user_registered\";s:19:\"2021-07-08 17:04:37\";s:19:\"user_activation_key\";s:0:\"\";s:11:\"user_status\";s:1:\"0\";s:12:\"display_name\";s:9:\"dev_admin\";}s:2:\"ID\";i:1;s:4:\"caps\";a:1:{s:13:\"administrator\";b:1;}s:7:\"cap_key\";s:16:\"fmn_capabilities\";s:5:\"roles\";a:1:{i:0;s:13:\"administrator\";}s:7:\"allcaps\";a:116:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;s:20:\"wpseo_manage_options\";b:1;s:13:\"administrator\";b:1;}s:6:\"filter\";N;s:16:\"\0WP_User\0site_id\";i:1;}s:5:\"nonce\";N;s:8:\"priority\";d:0.8;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:20:\"wpseo_manage_options\";s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}i:1;a:2:{s:7:\"message\";s:481:\"Yoast SEO וגם WooCommerce יכולים לפעול הרבה יות טוב ביחד באמצעות תוסף עזר. יש להתקין את Yoast WooCommerce SEO לניהול פשוט וקל. <a href=\"https://yoa.st/1o0?php_version=7.4&platform=wordpress&platform_version=5.7.2&software=free&software_version=16.6.1&days_active=2-5&user_language=he_IL\" aria-label=\"מידע נוסף אודות Yoast WooCommerce SEO\" target=\"_blank\" rel=\"noopener noreferrer\">מידע נוסף</a>.\";s:7:\"options\";a:10:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:44:\"wpseo-suggested-plugin-yoast-woocommerce-seo\";s:4:\"user\";O:7:\"WP_User\":8:{s:4:\"data\";O:8:\"stdClass\":10:{s:2:\"ID\";s:1:\"1\";s:10:\"user_login\";s:9:\"dev_admin\";s:9:\"user_pass\";s:34:\"$P$B1xO7euiqHi4PQdHn5iQH2JyhoMcXr0\";s:13:\"user_nicename\";s:9:\"dev_admin\";s:10:\"user_email\";s:15:\"maxf@leos.co.il\";s:8:\"user_url\";s:0:\"\";s:15:\"user_registered\";s:19:\"2021-07-08 17:04:37\";s:19:\"user_activation_key\";s:0:\"\";s:11:\"user_status\";s:1:\"0\";s:12:\"display_name\";s:9:\"dev_admin\";}s:2:\"ID\";i:1;s:4:\"caps\";a:1:{s:13:\"administrator\";b:1;}s:7:\"cap_key\";s:16:\"fmn_capabilities\";s:5:\"roles\";a:1:{i:0;s:13:\"administrator\";}s:7:\"allcaps\";a:116:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;s:20:\"wpseo_manage_options\";b:1;s:13:\"administrator\";b:1;}s:6:\"filter\";N;s:16:\"\0WP_User\0site_id\";i:1;}s:5:\"nonce\";N;s:8:\"priority\";d:0.5;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";a:1:{i:0;s:15:\"install_plugins\";}s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}}');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_users`
--

CREATE TABLE `fmn_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_users`
--

INSERT INTO `fmn_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'dev_admin', '$P$B1xO7euiqHi4PQdHn5iQH2JyhoMcXr0', 'dev_admin', 'maxf@leos.co.il', '', '2021-07-08 17:04:37', '', 0, 'dev_admin');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_admin_notes`
--

CREATE TABLE `fmn_wc_admin_notes` (
  `note_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `locale` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content_data` longtext COLLATE utf8mb4_unicode_520_ci,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_reminder` datetime DEFAULT NULL,
  `is_snoozable` tinyint(1) NOT NULL DEFAULT '0',
  `layout` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `icon` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'info'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_wc_admin_notes`
--

INSERT INTO `fmn_wc_admin_notes` (`note_id`, `name`, `type`, `locale`, `title`, `content`, `content_data`, `status`, `source`, `date_created`, `date_reminder`, `is_snoozable`, `layout`, `image`, `is_deleted`, `icon`) VALUES
(1, 'eu_vat_changes_2021', 'marketing', 'en_US', 'Get your business ready for the new EU tax regulations', 'On July 1, 2021, new taxation rules will come into play when the <a href=\"https://ec.europa.eu/taxation_customs/business/vat/modernising-vat-cross-border-ecommerce_en\">European Union (EU) Value-Added Tax (VAT) eCommerce package</a> takes effect.<br /><br />The new regulations will impact virtually every B2C business involved in cross-border eCommerce trade with the EU.<br /><br />We therefore recommend <a href=\"https://woocommerce.com/posts/new-eu-vat-regulations\">familiarizing yourself with the new updates</a>, and consult with a tax professional to ensure your business is following regulations and best practice.', '{}', 'unactioned', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(2, 'paypal_ppcp_gtm_2021', 'marketing', 'en_US', 'Offer more options with the new PayPal', 'Get the latest PayPal extension for a full suite of payment methods with extensive currency and country coverage.', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(3, 'facebook_pixel_api_2021', 'marketing', 'en_US', 'Improve the performance of your Facebook ads', 'Enable Facebook Pixel and Conversions API through the latest version of Facebook for WooCommerce for improved measurement and ad targeting capabilities.', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(4, 'facebook_ec_2021', 'marketing', 'en_US', 'Sync your product catalog with Facebook to help boost sales', 'A single click adds all products to your Facebook Business Page shop. Product changes are automatically synced, with the flexibility to control which products are listed.', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(5, 'ecomm-need-help-setting-up-your-store', 'info', 'en_US', 'Need help setting up your Store?', 'Schedule a free 30-min <a href=\"https://wordpress.com/support/concierge-support/\">quick start session</a> and get help from our specialists. We’re happy to walk through setup steps, show you around the WordPress.com dashboard, troubleshoot any issues you may have, and help you the find the features you need to accomplish your goals for your site.', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(6, 'woocommerce-services', 'info', 'en_US', 'WooCommerce Shipping & Tax', 'WooCommerce Shipping &amp; Tax helps get your store “ready to sell” as quickly as possible. You create your products. We take care of tax calculation, payment processing, and shipping label printing! Learn more about the extension that you just installed.', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(7, 'ecomm-unique-shopping-experience', 'info', 'en_US', 'For a shopping experience as unique as your customers', 'Product Add-Ons allow your customers to personalize products while they’re shopping on your online store. No more follow-up email requests—customers get what they want, before they’re done checking out. Learn more about this extension that comes included in your plan.', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(8, 'wc-admin-getting-started-in-ecommerce', 'info', 'en_US', 'Getting Started in eCommerce - webinar', 'We want to make eCommerce and this process of getting started as easy as possible for you. Watch this webinar to get tips on how to have our store up and running in a breeze.', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(9, 'your-first-product', 'info', 'en_US', 'Your first product', 'That\'s huge! You\'re well on your way to building a successful online store — now it’s time to think about how you\'ll fulfill your orders.<br /><br />Read our shipping guide to learn best practices and options for putting together your shipping strategy. And for WooCommerce stores in the United States, you can print discounted shipping labels via USPS with <a href=\"https://href.li/?https://woocommerce.com/shipping\" target=\"_blank\">WooCommerce Shipping</a>.', '{}', 'unactioned', 'woocommerce.com', '2021-07-11 10:21:50', NULL, 0, 'plain', '', 0, 'info'),
(10, 'wc-square-apple-pay-boost-sales', 'marketing', 'en_US', 'Boost sales with Apple Pay', 'Now that you accept Apple Pay® with Square you can increase conversion rates by letting your customers know that Apple Pay® is available. Here’s a marketing guide to help you get started.', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(11, 'wc-square-apple-pay-grow-your-business', 'marketing', 'en_US', 'Grow your business with Square and Apple Pay ', 'Now more than ever, shoppers want a fast, simple, and secure online checkout experience. Increase conversion rates by letting your customers know that you now accept Apple Pay®.', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(12, 'wcpay-apple-pay-is-now-available', 'marketing', 'en_US', 'Apple Pay is now available with WooCommerce Payments!', 'Increase your conversion rate by offering a fast and secure checkout with <a href=\"https://woocommerce.com/apple-pay/?utm_source=inbox&amp;utm_medium=product&amp;utm_campaign=wcpay_applepay\" target=\"_blank\">Apple Pay</a>®. It’s free to get started with <a href=\"https://woocommerce.com/payments/?utm_source=inbox&amp;utm_medium=product&amp;utm_campaign=wcpay_applepay\" target=\"_blank\">WooCommerce Payments</a>.', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(13, 'wcpay-apple-pay-boost-sales', 'marketing', 'en_US', 'Boost sales with Apple Pay', 'Now that you accept Apple Pay® with WooCommerce Payments you can increase conversion rates by letting your customers know that Apple Pay® is available. Here’s a marketing guide to help you get started.', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(14, 'wcpay-apple-pay-grow-your-business', 'marketing', 'en_US', 'Grow your business with WooCommerce Payments and Apple Pay', 'Now more than ever, shoppers want a fast, simple, and secure online checkout experience. Increase conversion rates by letting your customers know that you now accept Apple Pay®.', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(15, 'wc-admin-optimizing-the-checkout-flow', 'info', 'en_US', 'Optimizing the checkout flow', 'It\'s crucial to get your store\'s checkout as smooth as possible to avoid losing sales. Let\'s take a look at how you can optimize the checkout experience for your shoppers.', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(16, 'wc-admin-first-five-things-to-customize', 'info', 'en_US', 'The first 5 things to customize in your store', 'Deciding what to start with first is tricky. To help you properly prioritize, we\'ve put together this short list of the first few things you should customize in WooCommerce.', '{}', 'unactioned', 'woocommerce.com', '2021-07-12 10:43:54', NULL, 0, 'plain', '', 0, 'info'),
(17, 'wc-payments-qualitative-feedback', 'info', 'en_US', 'WooCommerce Payments setup - let us know what you think', 'Congrats on enabling WooCommerce Payments for your store. Please share your feedback in this 2 minute survey to help us improve the setup process.', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(18, 'share-your-feedback-on-paypal', 'info', 'en_US', 'Share your feedback on PayPal', 'Share your feedback in this 2 minute survey about how we can make the process of accepting payments more useful for your store.', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(19, 'wcpay_instant_deposits_gtm_2021', 'marketing', 'en_US', 'Get paid within minutes – Instant Deposits for WooCommerce Payments', 'Stay flexible with immediate access to your funds when you need them – including nights, weekends, and holidays. With <a href=\"https://woocommerce.com/products/woocommerce-payments/?utm_source=inbox&amp;utm_medium=product&amp;utm_campaign=wcpay_instant_deposits\">WooCommerce Payments\'</a> new Instant Deposits feature, you’re able to transfer your earnings to a debit card within minutes.', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(20, 'google_listings_and_ads_install', 'marketing', 'en_US', 'Drive traffic and sales with Google', 'Reach online shoppers to drive traffic and sales for your store by showcasing products across Google, for free or with ads.', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(21, 'wc-subscriptions-security-update-3-0-15', 'info', 'en_US', 'WooCommerce Subscriptions security update!', 'We recently released an important security update to WooCommerce Subscriptions. To ensure your site\'s data is protected, please upgrade <strong>WooCommerce Subscriptions to version 3.0.15</strong> or later.<br /><br />Click the button below to view and update to the latest Subscriptions version, or log in to <a href=\"https://woocommerce.com/my-dashboard\">WooCommerce.com Dashboard</a> and navigate to your <strong>Downloads</strong> page.<br /><br />We recommend always using the latest version of WooCommerce Subscriptions, and other software running on your site, to ensure maximum security.<br /><br />If you have any questions we are here to help — just <a href=\"https://woocommerce.com/my-account/create-a-ticket/\">open a ticket</a>.', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(22, 'woocommerce-core-update-5-4-0', 'info', 'en_US', 'Update to WooCommerce 5.4.1 now', 'WooCommerce 5.4.1 addresses a checkout issue discovered in WooCommerce 5.4. We recommend upgrading to WooCommerce 5.4.1 as soon as possible.', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(23, 'wcpay-promo-2020-11', 'marketing', 'en_US', 'wcpay-promo-2020-11', 'wcpay-promo-2020-11', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(24, 'wcpay-promo-2020-12', 'marketing', 'en_US', 'wcpay-promo-2020-12', 'wcpay-promo-2020-12', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(25, 'wcpay-promo-2021-6-incentive-1', 'marketing', 'en_US', 'Special offer: Save 50% on transaction fees for up to $125,000 in payments', 'Save big when you add <a href=\"https://woocommerce.com/payments/?utm_medium=notification&amp;utm_source=product&amp;utm_campaign=wcpay_exp222\">WooCommerce Payments</a> to your store today.\n                Get a discounted rate of 1.5% + $0.15 on all transactions – that’s 50% off the standard fee on up to $125,000 in processed payments (or six months, whichever comes first). Act now – this offer is available for a limited time only.\n                <br /><br />By clicking \"Get WooCommerce Payments,\" you agree to the promotional <a href=\"https://woocommerce.com/terms-conditions/woocommerce-payments-half-off-six-promotion/?utm_medium=notification&amp;utm_source=product&amp;utm_campaign=wcpay_exp222\">Terms of Service</a>.', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(26, 'wcpay-promo-2021-6-incentive-2', 'marketing', 'en_US', 'Special offer: No transaction fees* for up to three months', 'Save big when you add <a href=\"https://woocommerce.com/payments/?utm_medium=notification&amp;utm_source=product&amp;utm_campaign=wcpay_exp233\">WooCommerce Payments</a> to your store today. Pay zero transaction fees* on up to $25,000 in processed payments (or three months, whichever comes first). Act now – this offer is available for a limited time only. *Currency conversion fees excluded.\n                <br /><br />By clicking \"Get WooCommerce Payments,\" you agree to the promotional <a href=\"https://woocommerce.com/terms-conditions/woocommerce-payments-no-transaction-fees-for-three-promotion/?utm_medium=notification&amp;utm_source=product&amp;utm_campaign=wcpay_exp233\">Terms of Service</a>.', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(27, 'ppxo-pps-upgrade-paypal-payments-1', 'info', 'en_US', 'Get the latest PayPal extension for WooCommerce', 'Heads up! There\'s a new PayPal on the block!<br /><br />Now is a great time to upgrade to our latest <a href=\"https://woocommerce.com/products/woocommerce-paypal-payments/\" target=\"_blank\">PayPal extension</a> to continue to receive support and updates with PayPal.<br /><br />Get access to a full suite of PayPal payment methods, extensive currency and country coverage, and pay later options with the all-new PayPal extension for WooCommerce.', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(28, 'ppxo-pps-upgrade-paypal-payments-2', 'info', 'en_US', 'Upgrade your PayPal experience!', 'We\'ve developed a whole new <a href=\"https://woocommerce.com/products/woocommerce-paypal-payments/\" target=\"_blank\">PayPal extension for WooCommerce</a> that combines the best features of our many PayPal extensions into just one extension.<br /><br />Get access to a full suite of PayPal payment methods, extensive currency and country coverage, offer subscription and recurring payments, and the new PayPal pay later options.<br /><br />Start using our latest PayPal today to continue to receive support and updates.', '{}', 'pending', 'woocommerce.com', '2021-07-10 10:43:37', NULL, 0, 'plain', '', 0, 'info'),
(29, 'wc-admin-onboarding-email-marketing', 'info', 'en_US', 'Sign up for tips, product updates, and inspiration', 'We\'re here for you - get tips, product updates and inspiration straight to your email box', '{}', 'unactioned', 'woocommerce-admin', '2021-07-10 10:43:38', NULL, 0, 'plain', '', 0, 'info'),
(30, 'wc-admin-wc-helper-connection', 'info', 'en_US', 'Connect to WooCommerce.com', 'Connect to get important product notifications and updates.', '{}', 'unactioned', 'woocommerce-admin', '2021-07-10 10:43:39', NULL, 0, 'plain', '', 0, 'info'),
(31, 'wc-admin-selling-online-courses', 'marketing', 'en_US', 'Do you want to sell online courses?', 'Online courses are a great solution for any business that can teach a new skill. Since courses don’t require physical product development or shipping, they’re affordable, fast to create, and can generate passive income for years to come. In this article, we provide you more information about selling courses using WooCommerce.', '{}', 'unactioned', 'woocommerce-admin', '2021-07-10 10:45:28', NULL, 0, 'plain', '', 0, 'info'),
(32, 'wc-admin-learn-more-about-variable-products', 'info', 'en_US', 'למידע נוסף בנושא מוצרים עם סוגים', 'מוצרים עם סוגים הם מוצרים עוצמתיים שמאפשרים לך להציע ללקוחות מגוון של סוגים בכל מוצר ולשלוט במחיר, במלאי, בתמונה ובמאפיינים נוספים של כל סוג. אפשר להשתמש באפשרות זאת במוצרים כגון חולצות, ולהציע ללקוחות מידה גדולה, בינונית או קטנה וצבעים שונים.', '{}', 'unactioned', 'woocommerce-admin', '2021-07-11 10:21:50', NULL, 0, 'plain', '', 0, 'info'),
(33, 'wc-admin-choosing-a-theme', 'marketing', 'en_US', 'רוצה לבחור ערכת עיצוב?', 'כדאי לעיין בערכות העיצוב שתואמות ל-WooCommerce ולבחור ערכת עיצוב שמתאימה לצרכים של המותג והעסק שלך.', '{}', 'unactioned', 'woocommerce-admin', '2021-07-11 10:44:43', NULL, 0, 'plain', '', 0, 'info'),
(34, 'wc-admin-insight-first-product-and-payment', 'survey', 'en_US', 'תובנות', 'More than 80% of new merchants add the first product and have at least one payment method set up during the first week.<br><br>Do you find this type of insight useful?', '{}', 'unactioned', 'woocommerce-admin', '2021-07-11 10:44:43', NULL, 0, 'plain', '', 0, 'info'),
(35, 'wc-admin-mobile-app', 'info', 'en_US', 'להתקין את האפליקציה של Woo לנייד', 'כדאי להתקין את האפליקציה של WooCommerce לנייד כדי לנהל הזמנות, לקבל הודעות על מכירות ולהציג מדדים חשובים – בכל מקום.', '{}', 'unactioned', 'woocommerce-admin', '2021-07-12 10:43:53', NULL, 0, 'plain', '', 0, 'info'),
(36, 'wc-admin-customizing-product-catalog', 'info', 'en_US', 'איך להתאים אישית את קטלוג המוצרים שלך', 'כדאי ליצור קטלוג מוצרים ותמונות מוצר שנראים נהדר ומתאימים למותג שלך. המדריך הזה מספק את כל העצות שחשוב לדעת כדי להציג את המוצרים שלך בצורה מושכת בחנות.', '{}', 'unactioned', 'woocommerce-admin', '2021-07-12 10:43:53', NULL, 0, 'plain', '', 0, 'info');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_admin_note_actions`
--

CREATE TABLE `fmn_wc_admin_note_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `note_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `query` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_primary` tinyint(1) NOT NULL DEFAULT '0',
  `actioned_text` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nonce_action` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `nonce_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_wc_admin_note_actions`
--

INSERT INTO `fmn_wc_admin_note_actions` (`action_id`, `note_id`, `name`, `label`, `query`, `status`, `is_primary`, `actioned_text`, `nonce_action`, `nonce_name`) VALUES
(28, 29, 'yes-please', 'Yes please!', 'https://woocommerce.us8.list-manage.com/subscribe/post?u=2c1434dc56f9506bf3c3ecd21&amp;id=13860df971&amp;SIGNUPPAGE=plugin', 'actioned', 0, '', NULL, NULL),
(56, 30, 'connect', 'Connect', '?page=wc-addons&section=helper', 'unactioned', 0, '', NULL, NULL),
(84, 31, 'learn-more', 'Learn more', 'https://woocommerce.com/posts/how-to-sell-online-courses-wordpress/?utm_source=inbox', 'actioned', 1, '', NULL, NULL),
(112, 32, 'learn-more', 'מידע נוסף', 'https://docs.woocommerce.com/document/variable-product/?utm_source=inbox', 'actioned', 0, '', NULL, NULL),
(410, 33, 'visit-the-theme-marketplace', 'מעבר לחנות של ערכות העיצוב', 'https://woocommerce.com/product-category/themes/?utm_source=inbox', 'actioned', 0, '', NULL, NULL),
(411, 34, 'affirm-insight-first-product-and-payment', 'כן', '', 'actioned', 0, 'תודה על המשוב', NULL, NULL),
(412, 34, 'affirm-insight-first-product-and-payment', 'לא', '', 'actioned', 0, 'תודה על המשוב', NULL, NULL),
(683, 35, 'learn-more', 'מידע נוסף', 'https://woocommerce.com/mobile/', 'actioned', 0, '', NULL, NULL),
(684, 36, 'day-after-first-product', 'מידע נוסף', 'https://docs.woocommerce.com/document/woocommerce-customizer/?utm_source=inbox', 'actioned', 0, '', NULL, NULL),
(982, 1, 'eu_vat_changes_2021', 'Learn more about the EU tax regulations', 'https://woocommerce.com/posts/new-eu-vat-regulations', 'actioned', 1, '', NULL, NULL),
(983, 2, 'open_wc_paypal_payments_product_page', 'Learn more', 'https://woocommerce.com/products/woocommerce-paypal-payments/', 'actioned', 1, '', NULL, NULL),
(984, 3, 'upgrade_now_facebook_pixel_api', 'Upgrade now', 'plugin-install.php?tab=plugin-information&plugin=&section=changelog', 'actioned', 1, '', NULL, NULL),
(985, 4, 'learn_more_facebook_ec', 'Learn more', 'https://woocommerce.com/products/facebook/', 'unactioned', 1, '', NULL, NULL),
(986, 5, 'set-up-concierge', 'Schedule free session', 'https://wordpress.com/me/concierge', 'actioned', 1, '', NULL, NULL),
(987, 6, 'learn-more', 'Learn more', 'https://docs.woocommerce.com/document/woocommerce-shipping-and-tax/?utm_source=inbox', 'unactioned', 1, '', NULL, NULL),
(988, 7, 'learn-more-ecomm-unique-shopping-experience', 'Learn more', 'https://docs.woocommerce.com/document/product-add-ons/?utm_source=inbox', 'actioned', 1, '', NULL, NULL),
(989, 8, 'watch-the-webinar', 'Watch the webinar', 'https://youtu.be/V_2XtCOyZ7o', 'actioned', 1, '', NULL, NULL),
(990, 9, 'learn-more', 'Learn more', 'https://woocommerce.com/posts/ecommerce-shipping-solutions-guide/?utm_source=inbox', 'actioned', 1, '', NULL, NULL),
(991, 10, 'boost-sales-marketing-guide', 'See marketing guide', 'https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=square-boost-sales', 'actioned', 1, '', NULL, NULL),
(992, 11, 'grow-your-business-marketing-guide', 'See marketing guide', 'https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=square-grow-your-business', 'actioned', 1, '', NULL, NULL),
(993, 12, 'add-apple-pay', 'Add Apple Pay', '/admin.php?page=wc-settings&tab=checkout&section=woocommerce_payments', 'actioned', 1, '', NULL, NULL),
(994, 12, 'learn-more', 'Learn more', 'https://docs.woocommerce.com/document/payments/apple-pay/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_applepay', 'actioned', 1, '', NULL, NULL),
(995, 13, 'boost-sales-marketing-guide', 'See marketing guide', 'https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=wcpay-boost-sales', 'actioned', 1, '', NULL, NULL),
(996, 14, 'grow-your-business-marketing-guide', 'See marketing guide', 'https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=wcpay-grow-your-business', 'actioned', 1, '', NULL, NULL),
(997, 15, 'optimizing-the-checkout-flow', 'Learn more', 'https://woocommerce.com/posts/optimizing-woocommerce-checkout?utm_source=inbox', 'actioned', 1, '', NULL, NULL),
(998, 16, 'learn-more', 'Learn more', 'https://woocommerce.com/posts/first-things-customize-woocommerce/?utm_source=inbox', 'unactioned', 1, '', NULL, NULL),
(999, 17, 'qualitative-feedback-from-new-users', 'Share feedback', 'https://automattic.survey.fm/wc-pay-new', 'actioned', 1, '', NULL, NULL),
(1000, 18, 'share-feedback', 'Share feedback', 'http://automattic.survey.fm/paypal-feedback', 'unactioned', 1, '', NULL, NULL),
(1001, 19, 'learn-more', 'Learn about Instant Deposits eligibility', 'https://docs.woocommerce.com/document/payments/instant-deposits/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_instant_deposits', 'actioned', 1, '', NULL, NULL),
(1002, 20, 'get-started', 'Get started', 'https://woocommerce.com/products/google-listings-and-ads', 'actioned', 1, '', NULL, NULL),
(1003, 21, 'update-wc-subscriptions-3-0-15', 'View latest version', 'http://gordon:8888/wp-admin/admin.php?page=wc-admin&page=wc-addons&section=helper', 'actioned', 1, '', NULL, NULL),
(1004, 22, 'update-wc-core-5-4-0', 'How to update WooCommerce', 'https://docs.woocommerce.com/document/how-to-update-woocommerce/', 'actioned', 1, '', NULL, NULL),
(1005, 25, 'get-woo-commerce-payments', 'Get WooCommerce Payments', 'admin.php?page=wc-admin&action=setup-woocommerce-payments', 'actioned', 1, '', NULL, NULL),
(1006, 26, 'get-woocommerce-payments', 'Get WooCommerce Payments', 'admin.php?page=wc-admin&action=setup-woocommerce-payments', 'actioned', 1, '', NULL, NULL),
(1007, 27, 'ppxo-pps-install-paypal-payments-1', 'View upgrade guide', 'https://docs.woocommerce.com/document/woocommerce-paypal-payments/paypal-payments-upgrade-guide/', 'actioned', 1, '', NULL, NULL),
(1008, 28, 'ppxo-pps-install-paypal-payments-2', 'View upgrade guide', 'https://docs.woocommerce.com/document/woocommerce-paypal-payments/paypal-payments-upgrade-guide/', 'actioned', 1, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_category_lookup`
--

CREATE TABLE `fmn_wc_category_lookup` (
  `category_tree_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_wc_category_lookup`
--

INSERT INTO `fmn_wc_category_lookup` (`category_tree_id`, `category_id`) VALUES
(15, 15);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_customer_lookup`
--

CREATE TABLE `fmn_wc_customer_lookup` (
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `username` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `date_last_active` timestamp NULL DEFAULT NULL,
  `date_registered` timestamp NULL DEFAULT NULL,
  `country` char(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `postcode` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `city` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `state` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_download_log`
--

CREATE TABLE `fmn_wc_download_log` (
  `download_log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_ip_address` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_order_coupon_lookup`
--

CREATE TABLE `fmn_wc_order_coupon_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `coupon_id` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `discount_amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_order_product_lookup`
--

CREATE TABLE `fmn_wc_order_product_lookup` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `variation_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `product_qty` int(11) NOT NULL,
  `product_net_revenue` double NOT NULL DEFAULT '0',
  `product_gross_revenue` double NOT NULL DEFAULT '0',
  `coupon_amount` double NOT NULL DEFAULT '0',
  `tax_amount` double NOT NULL DEFAULT '0',
  `shipping_amount` double NOT NULL DEFAULT '0',
  `shipping_tax_amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_order_stats`
--

CREATE TABLE `fmn_wc_order_stats` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `num_items_sold` int(11) NOT NULL DEFAULT '0',
  `total_sales` double NOT NULL DEFAULT '0',
  `tax_total` double NOT NULL DEFAULT '0',
  `shipping_total` double NOT NULL DEFAULT '0',
  `net_total` double NOT NULL DEFAULT '0',
  `returning_customer` tinyint(1) DEFAULT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_order_tax_lookup`
--

CREATE TABLE `fmn_wc_order_tax_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shipping_tax` double NOT NULL DEFAULT '0',
  `order_tax` double NOT NULL DEFAULT '0',
  `total_tax` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_product_meta_lookup`
--

CREATE TABLE `fmn_wc_product_meta_lookup` (
  `product_id` bigint(20) NOT NULL,
  `sku` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `virtual` tinyint(1) DEFAULT '0',
  `downloadable` tinyint(1) DEFAULT '0',
  `min_price` decimal(19,4) DEFAULT NULL,
  `max_price` decimal(19,4) DEFAULT NULL,
  `onsale` tinyint(1) DEFAULT '0',
  `stock_quantity` double DEFAULT NULL,
  `stock_status` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT 'instock',
  `rating_count` bigint(20) DEFAULT '0',
  `average_rating` decimal(3,2) DEFAULT '0.00',
  `total_sales` bigint(20) DEFAULT '0',
  `tax_status` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT 'taxable',
  `tax_class` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_wc_product_meta_lookup`
--

INSERT INTO `fmn_wc_product_meta_lookup` (`product_id`, `sku`, `virtual`, `downloadable`, `min_price`, `max_price`, `onsale`, `stock_quantity`, `stock_status`, `rating_count`, `average_rating`, `total_sales`, `tax_status`, `tax_class`) VALUES
(52, '', 0, 0, '111.0000', '111.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(61, '', 0, 0, '248.0000', '248.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(62, '', 0, 0, '77.0000', '77.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(63, '', 0, 0, '103.0000', '103.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(64, '', 0, 0, '200.0000', '200.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(65, '', 0, 0, '0.0000', '0.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(66, '', 0, 0, '180.0000', '180.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', '');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_reserved_stock`
--

CREATE TABLE `fmn_wc_reserved_stock` (
  `order_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `stock_quantity` double NOT NULL DEFAULT '0',
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expires` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_tax_rate_classes`
--

CREATE TABLE `fmn_wc_tax_rate_classes` (
  `tax_rate_class_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_wc_tax_rate_classes`
--

INSERT INTO `fmn_wc_tax_rate_classes` (`tax_rate_class_id`, `name`, `slug`) VALUES
(1, 'Reduced rate', 'reduced-rate'),
(2, 'Zero rate', 'zero-rate');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_webhooks`
--

CREATE TABLE `fmn_wc_webhooks` (
  `webhook_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `delivery_url` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `secret` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `topic` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT '0',
  `pending_delivery` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_api_keys`
--

CREATE TABLE `fmn_woocommerce_api_keys` (
  `key_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_520_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_attribute_taxonomies`
--

CREATE TABLE `fmn_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_label` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `attribute_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_orderby` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_downloadable_product_permissions`
--

CREATE TABLE `fmn_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `download_id` varchar(36) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_log`
--

CREATE TABLE `fmn_woocommerce_log` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `context` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_order_itemmeta`
--

CREATE TABLE `fmn_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_order_items`
--

CREATE TABLE `fmn_woocommerce_order_items` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_payment_tokenmeta`
--

CREATE TABLE `fmn_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `payment_token_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_payment_tokens`
--

CREATE TABLE `fmn_woocommerce_payment_tokens` (
  `token_id` bigint(20) UNSIGNED NOT NULL,
  `gateway_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_sessions`
--

CREATE TABLE `fmn_woocommerce_sessions` (
  `session_id` bigint(20) UNSIGNED NOT NULL,
  `session_key` char(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_expiry` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_woocommerce_sessions`
--

INSERT INTO `fmn_woocommerce_sessions` (`session_id`, `session_key`, `session_value`, `session_expiry`) VALUES
(1, '1', 'a:8:{s:4:\"cart\";s:6:\"a:0:{}\";s:11:\"cart_totals\";s:367:\"a:15:{s:8:\"subtotal\";i:0;s:12:\"subtotal_tax\";i:0;s:14:\"shipping_total\";i:0;s:12:\"shipping_tax\";i:0;s:14:\"shipping_taxes\";a:0:{}s:14:\"discount_total\";i:0;s:12:\"discount_tax\";i:0;s:19:\"cart_contents_total\";i:0;s:17:\"cart_contents_tax\";i:0;s:19:\"cart_contents_taxes\";a:0:{}s:9:\"fee_total\";i:0;s:7:\"fee_tax\";i:0;s:9:\"fee_taxes\";a:0:{}s:5:\"total\";i:0;s:9:\"total_tax\";i:0;}\";s:15:\"applied_coupons\";s:6:\"a:0:{}\";s:22:\"coupon_discount_totals\";s:6:\"a:0:{}\";s:26:\"coupon_discount_tax_totals\";s:6:\"a:0:{}\";s:21:\"removed_cart_contents\";s:821:\"a:2:{s:32:\"9a1158154dfa42caddbd0694a4e9bdc8\";a:11:{s:3:\"key\";s:32:\"9a1158154dfa42caddbd0694a4e9bdc8\";s:10:\"product_id\";i:52;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:14;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:1554;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:1554;s:8:\"line_tax\";i:0;}s:32:\"7f39f8317fbdb1988ef4c628eba02591\";a:11:{s:3:\"key\";s:32:\"7f39f8317fbdb1988ef4c628eba02591\";s:10:\"product_id\";i:61;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:4;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:992;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:992;s:8:\"line_tax\";i:0;}}\";s:8:\"customer\";s:729:\"a:26:{s:2:\"id\";s:1:\"1\";s:13:\"date_modified\";s:25:\"2021-07-11T18:08:56+03:00\";s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:9:\"address_1\";s:0:\"\";s:7:\"address\";s:0:\"\";s:9:\"address_2\";s:0:\"\";s:5:\"state\";s:0:\"\";s:7:\"country\";s:2:\"IL\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:0:\"\";s:16:\"shipping_country\";s:2:\"IL\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:0:\"\";s:9:\"last_name\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:5:\"email\";s:15:\"maxf@leos.co.il\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";s:10:\"wc_notices\";N;}', 1626185115);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_shipping_zones`
--

CREATE TABLE `fmn_woocommerce_shipping_zones` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `zone_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `zone_order` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_shipping_zone_locations`
--

CREATE TABLE `fmn_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_shipping_zone_methods`
--

CREATE TABLE `fmn_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `instance_id` bigint(20) UNSIGNED NOT NULL,
  `method_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `method_order` bigint(20) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_tax_rates`
--

CREATE TABLE `fmn_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_country` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_tax_rate_locations`
--

CREATE TABLE `fmn_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_yith_wcwl`
--

CREATE TABLE `fmn_yith_wcwl` (
  `ID` bigint(20) NOT NULL,
  `prod_id` bigint(20) NOT NULL,
  `quantity` int(11) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `wishlist_id` bigint(20) DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  `original_price` decimal(9,3) DEFAULT NULL,
  `original_currency` char(3) DEFAULT NULL,
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `on_sale` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_yith_wcwl_lists`
--

CREATE TABLE `fmn_yith_wcwl_lists` (
  `ID` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  `wishlist_slug` varchar(200) NOT NULL,
  `wishlist_name` text,
  `wishlist_token` varchar(64) NOT NULL,
  `wishlist_privacy` tinyint(1) NOT NULL DEFAULT '0',
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expiration` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fmn_yith_wcwl_lists`
--

INSERT INTO `fmn_yith_wcwl_lists` (`ID`, `user_id`, `session_id`, `wishlist_slug`, `wishlist_name`, `wishlist_token`, `wishlist_privacy`, `is_default`, `dateadded`, `expiration`) VALUES
(1, 1, NULL, '', '', '9BMHUXY1FR05', 0, 1, '2021-07-13 08:32:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_yoast_indexable`
--

CREATE TABLE `fmn_yoast_indexable` (
  `id` int(11) UNSIGNED NOT NULL,
  `permalink` longtext COLLATE utf8mb4_unicode_520_ci,
  `permalink_hash` varchar(40) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `object_id` bigint(20) DEFAULT NULL,
  `object_type` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `object_sub_type` varchar(32) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `author_id` bigint(20) DEFAULT NULL,
  `post_parent` bigint(20) DEFAULT NULL,
  `title` text COLLATE utf8mb4_unicode_520_ci,
  `description` mediumtext COLLATE utf8mb4_unicode_520_ci,
  `breadcrumb_title` text COLLATE utf8mb4_unicode_520_ci,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_public` tinyint(1) DEFAULT NULL,
  `is_protected` tinyint(1) DEFAULT '0',
  `has_public_posts` tinyint(1) DEFAULT NULL,
  `number_of_pages` int(11) UNSIGNED DEFAULT NULL,
  `canonical` longtext COLLATE utf8mb4_unicode_520_ci,
  `primary_focus_keyword` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `primary_focus_keyword_score` int(3) DEFAULT NULL,
  `readability_score` int(3) DEFAULT NULL,
  `is_cornerstone` tinyint(1) DEFAULT '0',
  `is_robots_noindex` tinyint(1) DEFAULT '0',
  `is_robots_nofollow` tinyint(1) DEFAULT '0',
  `is_robots_noarchive` tinyint(1) DEFAULT '0',
  `is_robots_noimageindex` tinyint(1) DEFAULT '0',
  `is_robots_nosnippet` tinyint(1) DEFAULT '0',
  `twitter_title` text COLLATE utf8mb4_unicode_520_ci,
  `twitter_image` longtext COLLATE utf8mb4_unicode_520_ci,
  `twitter_description` longtext COLLATE utf8mb4_unicode_520_ci,
  `twitter_image_id` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `twitter_image_source` text COLLATE utf8mb4_unicode_520_ci,
  `open_graph_title` text COLLATE utf8mb4_unicode_520_ci,
  `open_graph_description` longtext COLLATE utf8mb4_unicode_520_ci,
  `open_graph_image` longtext COLLATE utf8mb4_unicode_520_ci,
  `open_graph_image_id` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `open_graph_image_source` text COLLATE utf8mb4_unicode_520_ci,
  `open_graph_image_meta` mediumtext COLLATE utf8mb4_unicode_520_ci,
  `link_count` int(11) DEFAULT NULL,
  `incoming_link_count` int(11) DEFAULT NULL,
  `prominent_words_version` int(11) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `blog_id` bigint(20) NOT NULL DEFAULT '1',
  `language` varchar(32) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `region` varchar(32) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `schema_page_type` varchar(64) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `schema_article_type` varchar(64) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `has_ancestors` tinyint(1) DEFAULT '0',
  `estimated_reading_time_minutes` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_yoast_indexable`
--

INSERT INTO `fmn_yoast_indexable` (`id`, `permalink`, `permalink_hash`, `object_id`, `object_type`, `object_sub_type`, `author_id`, `post_parent`, `title`, `description`, `breadcrumb_title`, `post_status`, `is_public`, `is_protected`, `has_public_posts`, `number_of_pages`, `canonical`, `primary_focus_keyword`, `primary_focus_keyword_score`, `readability_score`, `is_cornerstone`, `is_robots_noindex`, `is_robots_nofollow`, `is_robots_noarchive`, `is_robots_noimageindex`, `is_robots_nosnippet`, `twitter_title`, `twitter_image`, `twitter_description`, `twitter_image_id`, `twitter_image_source`, `open_graph_title`, `open_graph_description`, `open_graph_image`, `open_graph_image_id`, `open_graph_image_source`, `open_graph_image_meta`, `link_count`, `incoming_link_count`, `prominent_words_version`, `created_at`, `updated_at`, `blog_id`, `language`, `region`, `schema_page_type`, `schema_article_type`, `has_ancestors`, `estimated_reading_time_minutes`) VALUES
(1, 'http://gordon:8888/author/dev_admin/', '36:a29aaa8d9b7d1a885cf76a34ad25c82d', 1, 'user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 'https://0.gravatar.com/avatar/f1fe2068092e42d4a763e35621a332de?s=500&d=mm&r=g', NULL, NULL, 'gravatar-image', NULL, NULL, 'https://0.gravatar.com/avatar/f1fe2068092e42d4a763e35621a332de?s=500&d=mm&r=g', NULL, 'gravatar-image', NULL, NULL, NULL, NULL, '2021-07-11 22:43:17', '2021-07-13 08:52:47', 1, NULL, NULL, NULL, NULL, 0, NULL),
(2, 'http://gordon:8888/2021/07/13/125/', '34:d263000464bd7391acd395c8a15b3f20', 125, 'post', 'nav_menu_item', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-11 22:43:17', '2021-07-13 03:40:07', 1, NULL, NULL, NULL, NULL, 0, NULL),
(3, 'http://gordon:8888/2021/07/13/72/', '33:e5232ba6463b98a7dd58cf2a0b223d9b', 72, 'post', 'nav_menu_item', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-11 22:43:22', '2021-07-13 03:40:07', 1, NULL, NULL, NULL, NULL, 0, NULL),
(4, 'http://gordon:8888/2021/07/13/73/', '33:4f81a436127aeeca0163e8153f11db42', 73, 'post', 'nav_menu_item', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-11 22:43:22', '2021-07-13 03:40:07', 1, NULL, NULL, NULL, NULL, 0, NULL),
(5, 'http://gordon:8888/2021/07/13/74/', '33:a5d03b1ae36bf57ec8003848f7d32f7b', 74, 'post', 'nav_menu_item', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-11 22:43:22', '2021-07-13 03:40:07', 1, NULL, NULL, NULL, NULL, 0, NULL),
(6, 'http://gordon:8888/2021/07/13/75/', '33:113e4da9809ae2211dc06976eb67e50a', 75, 'post', 'nav_menu_item', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-11 22:43:22', '2021-07-13 03:40:07', 1, NULL, NULL, NULL, NULL, 0, NULL),
(8, 'http://gordon:8888/2021/07/13/77/', '33:fc71f78bc29b0a55d16f3f04d33e472c', 77, 'post', 'nav_menu_item', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-11 22:43:22', '2021-07-13 03:40:07', 1, NULL, NULL, NULL, NULL, 0, NULL),
(9, 'http://gordon:8888/', '19:3028c699e258c768245d9c1574f9b5e4', 67, 'post', 'page', 1, 0, NULL, NULL, 'דף הבית', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-11 22:43:37', '2021-07-13 03:17:00', 1, NULL, NULL, NULL, NULL, 0, NULL),
(10, 'http://gordon:8888/', '19:3028c699e258c768245d9c1574f9b5e4', NULL, 'home-page', NULL, NULL, NULL, '%%sitename%% %%page%% %%sep%% %%sitedesc%%', 'אתר וורדפרס חדש', 'דף הבית', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '%%sitename%%', '', '', '0', NULL, NULL, NULL, NULL, NULL, '2021-07-11 22:43:37', '2021-07-11 19:43:37', 1, NULL, NULL, NULL, NULL, 0, NULL),
(11, 'http://gordon:8888/%d7%a6%d7%95%d7%a8-%d7%a7%d7%a9%d7%a8/', '57:aa714dcc8dfdbe0134f215e8b3ee6a73', 49, 'post', 'page', 1, 0, NULL, NULL, 'צור קשר', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 90, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/contact-img.png', NULL, '126', 'featured-image', NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/contact-img.png', '126', 'featured-image', '{\n    \"width\": 370,\n    \"height\": 483,\n    \"url\": \"http://gordon:8888/wp-content/uploads/2021/07/contact-img.png\",\n    \"path\": \"/Users/kathappens/sites/gordon/www/wp-content/uploads/2021/07/contact-img.png\",\n    \"size\": \"full\",\n    \"id\": 126,\n    \"alt\": \"\",\n    \"pixels\": 178710,\n    \"type\": \"image/png\"\n}', 0, NULL, NULL, '2021-07-11 22:43:42', '2021-07-12 04:48:25', 1, NULL, NULL, NULL, NULL, 0, 1),
(12, 'http://gordon:8888/?post_type=wpcf7_contact_form&p=12', '53:1494e7224aa0e1ed0f64369c784338ba', 12, 'post', 'wpcf7_contact_form', 1, 0, NULL, NULL, 'צור קשר', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-11 22:45:46', '2021-07-11 19:54:13', 1, NULL, NULL, NULL, NULL, 0, NULL),
(13, 'http://gordon:8888/cart/', '24:fdeb93947f3f345426c2031228e87639', 8, 'post', 'page', 1, 0, NULL, NULL, 'Cart', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-11 22:46:20', '2021-07-11 19:46:20', 1, NULL, NULL, NULL, NULL, 0, NULL),
(14, 'http://gordon:8888/checkout/', '28:913b4d6e3b6d7f04ad628fc7553b1571', 9, 'post', 'page', 1, 0, NULL, NULL, 'Checkout', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-11 22:46:20', '2021-07-11 19:46:20', 1, NULL, NULL, NULL, NULL, 0, NULL),
(15, 'http://gordon:8888/my-account/', '30:29a58f8c0fb7a1f237dbc5b6ffc04430', 10, 'post', 'page', 1, 0, NULL, NULL, 'My account', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-11 22:46:20', '2021-07-11 19:46:20', 1, NULL, NULL, NULL, NULL, 0, NULL),
(16, 'http://gordon:8888/shop/', '24:e4fb1f50364e2d81d1e766bc22cd35a8', 7, 'post', 'page', 1, 0, NULL, NULL, 'חנות', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-11 22:46:20', '2021-07-13 08:50:11', 1, NULL, NULL, NULL, NULL, 0, NULL),
(17, 'http://gordon:8888/?page_id=3', '29:51361aab865563c60b6538182551214a', 3, 'post', 'page', 1, 0, NULL, NULL, 'מדיניות פרטיות', 'draft', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-11 22:46:20', '2021-07-11 19:46:20', 1, NULL, NULL, NULL, NULL, 0, NULL),
(18, 'http://gordon:8888/sample-page/', '31:91cfb79f4f58e6c357af1627cda83795', 2, 'post', 'page', 1, 0, NULL, NULL, 'עמוד לדוגמא', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-11 22:46:20', '2021-07-11 19:46:20', 1, NULL, NULL, NULL, NULL, 0, NULL),
(19, 'http://gordon:8888/wishlist/', '28:4d316e4d1512f37335f93cd8278f1a8a', 98, 'post', 'page', 1, 0, NULL, NULL, 'רשימת משאלות', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-11 22:46:20', '2021-07-11 19:46:20', 1, NULL, NULL, NULL, NULL, 0, NULL),
(20, 'http://gordon:8888/wp-content/uploads/2021/07/contact-img.png', '61:7939b1b2f77ebd4b1f0c39fdffa3a7d2', 126, 'post', 'attachment', 1, 49, NULL, NULL, 'contact-img', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/contact-img.png', NULL, '126', 'attachment-image', NULL, NULL, NULL, '126', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-11 22:46:40', '2021-07-11 19:46:40', 1, NULL, NULL, NULL, NULL, 0, NULL),
(21, 'http://gordon:8888/?post_type=acf-field&p=129', '45:716638dfa109457399856aedd471545c', 129, 'post', 'acf-field', 1, 128, NULL, NULL, 'כותרת של בלוק עם מפה', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 07:42:19', '2021-07-12 04:42:19', 1, NULL, NULL, NULL, NULL, 1, NULL),
(22, 'http://gordon:8888/?post_type=acf-field-group&p=128', '51:29afc866db7cb9bac9893b4a37b0b698', 128, 'post', 'acf-field-group', 1, 0, NULL, NULL, 'צור קשר', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 07:42:19', '2021-07-12 04:43:30', 1, NULL, NULL, NULL, NULL, 0, NULL),
(23, 'http://gordon:8888/?post_type=acf-field&p=130', '45:c4d0d16f82ddacbf184c752f55f2bc67', 130, 'post', 'acf-field', 1, 128, NULL, NULL, 'טקסט בבלוק עם לקוחות', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 07:43:30', '2021-07-12 04:43:30', 1, NULL, NULL, NULL, NULL, 1, NULL),
(24, 'http://gordon:8888/?post_type=acf-field&p=131', '45:59be3464ecbdea9d680683f0aa91e1e0', 131, 'post', 'acf-field', 1, 128, NULL, NULL, 'לקוחות', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 07:43:30', '2021-07-12 04:43:30', 1, NULL, NULL, NULL, NULL, 1, NULL),
(25, 'http://gordon:8888/?post_type=acf-field-group&p=21', '50:8d7969773604d2a7d6bf59a0563d0189', 21, 'post', 'acf-field-group', 1, 0, NULL, NULL, 'תוספות תוכן', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 07:44:14', '2021-07-13 08:53:02', 1, NULL, NULL, NULL, NULL, 0, NULL),
(26, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו.png', '58:ee9578f578ae9e6091347ffe00976e4f', 132, 'post', 'attachment', 1, 49, NULL, NULL, 'לוגו', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו.png', NULL, '132', 'attachment-image', NULL, NULL, NULL, '132', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 07:46:58', '2021-07-12 04:46:58', 1, NULL, NULL, NULL, NULL, 0, NULL),
(27, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו-1.png', '60:452139ffa346c8ec7b61b6f502cae790', 133, 'post', 'attachment', 1, 49, NULL, NULL, 'לוגו', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו-1.png', NULL, '133', 'attachment-image', NULL, NULL, NULL, '133', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 07:47:08', '2021-07-12 04:47:08', 1, NULL, NULL, NULL, NULL, 0, NULL),
(28, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו-2.png', '60:8ba52f4b0da45cd807a84737e2866235', 134, 'post', 'attachment', 1, 49, NULL, NULL, 'לוגו', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו-2.png', NULL, '134', 'attachment-image', NULL, NULL, NULL, '134', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 07:47:23', '2021-07-12 04:47:23', 1, NULL, NULL, NULL, NULL, 0, NULL),
(29, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו-3.png', '60:0217b106572a3116663e47677ab77931', 135, 'post', 'attachment', 1, 49, NULL, NULL, 'לוגו', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו-3.png', NULL, '135', 'attachment-image', NULL, NULL, NULL, '135', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 07:47:40', '2021-07-12 04:47:40', 1, NULL, NULL, NULL, NULL, 0, NULL),
(30, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו-4.png', '60:34c8060bd21c5b8f5f4b86c3052f696d', 136, 'post', 'attachment', 1, 49, NULL, NULL, 'לוגו', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו-4.png', NULL, '136', 'attachment-image', NULL, NULL, NULL, '136', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 07:47:48', '2021-07-12 04:47:48', 1, NULL, NULL, NULL, NULL, 0, NULL),
(31, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו-5.png', '60:15b65a97fee9bb2e3e3214db5b3428e1', 137, 'post', 'attachment', 1, 49, NULL, NULL, 'לוגו', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו-5.png', NULL, '137', 'attachment-image', NULL, NULL, NULL, '137', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 07:47:58', '2021-07-12 04:47:58', 1, NULL, NULL, NULL, NULL, 0, NULL),
(32, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו-6.png', '60:65f885267fa7c1e5fb2685f5ef27ff84', 138, 'post', 'attachment', 1, 49, NULL, NULL, 'לוגו', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו-6.png', NULL, '138', 'attachment-image', NULL, NULL, NULL, '138', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 07:48:06', '2021-07-12 04:48:06', 1, NULL, NULL, NULL, NULL, 0, NULL),
(33, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו-7.png', '60:dcd7cba66d6d9bd0b8b9e47e655e8c67', 139, 'post', 'attachment', 1, 49, NULL, NULL, 'לוגו', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/לוגו-7.png', NULL, '139', 'attachment-image', NULL, NULL, NULL, '139', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 07:48:15', '2021-07-12 04:48:15', 1, NULL, NULL, NULL, NULL, 0, NULL),
(34, 'http://gordon:8888/product/%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-test/', '138:e1c467d97f938c449e762445dfab4851', 52, 'post', 'product', 1, 0, NULL, NULL, 'שם המוצר לורם איפסום TEST', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/test-prod-1.png', NULL, '145', 'featured-image', NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/test-prod-1.png', '145', 'featured-image', '{\n    \"width\": 533,\n    \"height\": 615,\n    \"url\": \"http://gordon:8888/wp-content/uploads/2021/07/test-prod-1.png\",\n    \"path\": \"/Users/kathappens/sites/gordon/www/wp-content/uploads/2021/07/test-prod-1.png\",\n    \"size\": \"full\",\n    \"id\": 145,\n    \"alt\": \"\",\n    \"pixels\": 327795,\n    \"type\": \"image/png\"\n}', 0, NULL, NULL, '2021-07-12 08:38:56', '2021-07-13 07:09:28', 1, NULL, NULL, NULL, NULL, 0, NULL),
(35, 'http://gordon:8888/shop/', '24:e4fb1f50364e2d81d1e766bc22cd35a8', NULL, 'post-type-archive', 'product', NULL, NULL, 'ארכיון %%pt_plural%% %%page%% %%sep%% %%sitename%%', '', 'מוצרים', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-12 08:38:56', '2021-07-12 05:38:56', 1, NULL, NULL, NULL, NULL, 0, NULL),
(36, 'http://gordon:8888/product/%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-6/', '135:22772c5bae339592f13c9396f8ec8e3d', 66, 'post', 'product', 1, 0, NULL, NULL, 'שם המוצר לורם איפסום 6', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/product-8.png', NULL, '60', 'featured-image', NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/product-8.png', '60', 'featured-image', '{\n    \"width\": 186,\n    \"height\": 203,\n    \"url\": \"http://gordon:8888/wp-content/uploads/2021/07/product-8.png\",\n    \"path\": \"/Users/kathappens/sites/gordon/www/wp-content/uploads/2021/07/product-8.png\",\n    \"size\": \"full\",\n    \"id\": 60,\n    \"alt\": \"\",\n    \"pixels\": 37758,\n    \"type\": \"image/png\"\n}', 0, NULL, NULL, '2021-07-12 08:39:34', '2021-07-13 07:10:29', 1, NULL, NULL, NULL, NULL, 0, NULL),
(37, 'http://gordon:8888/product/%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-5/', '135:ca13ca2b58fad5af73281093007f0aae', 65, 'post', 'product', 1, 0, NULL, NULL, 'שם המוצר לורם איפסום 5', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/product-6.png', NULL, '58', 'featured-image', NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/product-6.png', '58', 'featured-image', '{\n    \"width\": 267,\n    \"height\": 185,\n    \"url\": \"http://gordon:8888/wp-content/uploads/2021/07/product-6.png\",\n    \"path\": \"/Users/kathappens/sites/gordon/www/wp-content/uploads/2021/07/product-6.png\",\n    \"size\": \"full\",\n    \"id\": 58,\n    \"alt\": \"\",\n    \"pixels\": 49395,\n    \"type\": \"image/png\"\n}', 0, NULL, NULL, '2021-07-12 08:39:34', '2021-07-13 07:10:21', 1, NULL, NULL, NULL, NULL, 0, NULL),
(38, 'http://gordon:8888/product/%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-4/', '135:a73310e9c2a661dc4a755fbcef3b6055', 64, 'post', 'product', 1, 0, NULL, NULL, 'שם המוצר לורם איפסום 4', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/product-5.png', NULL, '57', 'featured-image', NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/product-5.png', '57', 'featured-image', '{\n    \"width\": 267,\n    \"height\": 181,\n    \"url\": \"http://gordon:8888/wp-content/uploads/2021/07/product-5.png\",\n    \"path\": \"/Users/kathappens/sites/gordon/www/wp-content/uploads/2021/07/product-5.png\",\n    \"size\": \"full\",\n    \"id\": 57,\n    \"alt\": \"\",\n    \"pixels\": 48327,\n    \"type\": \"image/png\"\n}', 0, NULL, NULL, '2021-07-12 08:39:34', '2021-07-13 07:10:13', 1, NULL, NULL, NULL, NULL, 0, NULL),
(39, 'http://gordon:8888/product/%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-3/', '135:f67a54226c1fd8dc81f592f7babbaf2b', 63, 'post', 'product', 1, 0, NULL, NULL, 'שם המוצר לורם איפסום 3', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/product-4.png', NULL, '56', 'featured-image', NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/product-4.png', '56', 'featured-image', '{\n    \"width\": 186,\n    \"height\": 203,\n    \"url\": \"http://gordon:8888/wp-content/uploads/2021/07/product-4.png\",\n    \"path\": \"/Users/kathappens/sites/gordon/www/wp-content/uploads/2021/07/product-4.png\",\n    \"size\": \"full\",\n    \"id\": 56,\n    \"alt\": \"\",\n    \"pixels\": 37758,\n    \"type\": \"image/png\"\n}', 0, NULL, NULL, '2021-07-12 08:39:34', '2021-07-13 07:10:04', 1, NULL, NULL, NULL, NULL, 0, NULL),
(40, 'http://gordon:8888/product/%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-2/', '135:7bc91791ff92db51d5a63da37b80f430', 62, 'post', 'product', 1, 0, NULL, NULL, 'שם המוצר לורם איפסום 2', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/product-3.png', NULL, '55', 'featured-image', NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/product-3.png', '55', 'featured-image', '{\n    \"width\": 205,\n    \"height\": 198,\n    \"url\": \"http://gordon:8888/wp-content/uploads/2021/07/product-3.png\",\n    \"path\": \"/Users/kathappens/sites/gordon/www/wp-content/uploads/2021/07/product-3.png\",\n    \"size\": \"full\",\n    \"id\": 55,\n    \"alt\": \"\",\n    \"pixels\": 40590,\n    \"type\": \"image/png\"\n}', 0, NULL, NULL, '2021-07-12 08:39:34', '2021-07-13 07:09:55', 1, NULL, NULL, NULL, NULL, 0, NULL),
(41, 'http://gordon:8888/product/%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-1/', '135:bae1d916be3b0f77a0b8363f54b954d6', 61, 'post', 'product', 1, 0, NULL, NULL, 'שם המוצר לורם איפסום 1', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/product-2.png', NULL, '54', 'featured-image', NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/product-2.png', '54', 'featured-image', '{\n    \"width\": 267,\n    \"height\": 185,\n    \"url\": \"http://gordon:8888/wp-content/uploads/2021/07/product-2.png\",\n    \"path\": \"/Users/kathappens/sites/gordon/www/wp-content/uploads/2021/07/product-2.png\",\n    \"size\": \"full\",\n    \"id\": 54,\n    \"alt\": \"\",\n    \"pixels\": 49395,\n    \"type\": \"image/png\"\n}', 0, NULL, NULL, '2021-07-12 08:39:35', '2021-07-13 07:09:39', 1, NULL, NULL, NULL, NULL, 0, NULL),
(42, 'http://gordon:8888/?post_type=acf-field&p=142', '45:b99656dde4b688df3fb846cd0fccedb1', 142, 'post', 'acf-field', 1, 141, NULL, NULL, 'מידע', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 08:45:20', '2021-07-12 05:45:20', 1, NULL, NULL, NULL, NULL, 1, NULL),
(43, 'http://gordon:8888/?post_type=acf-field-group&p=141', '51:8d77a040c2844dd4b1677484d43bb8d4', 141, 'post', 'acf-field-group', 1, 0, NULL, NULL, 'מוצר', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 08:45:20', '2021-07-12 07:28:54', 1, NULL, NULL, NULL, NULL, 0, NULL),
(44, 'http://gordon:8888/?post_type=acf-field&p=143', '45:d108d4a1d5b4f05f2bde1bacb43fc196', 143, 'post', 'acf-field', 1, 142, NULL, NULL, 'כותרת', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 08:45:20', '2021-07-12 05:45:20', 1, NULL, NULL, NULL, NULL, 1, NULL),
(45, 'http://gordon:8888/?post_type=acf-field&p=144', '45:5e4bf95beec846b59b8ea30e164c7889', 144, 'post', 'acf-field', 1, 142, NULL, NULL, 'תוכן', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 08:45:20', '2021-07-12 05:45:20', 1, NULL, NULL, NULL, NULL, 1, NULL),
(46, 'http://gordon:8888/wp-content/uploads/2021/07/test-prod-1.png', '61:55be6f823d1c50999eb5d4d124e1256e', 145, 'post', 'attachment', 1, 52, NULL, NULL, 'test-prod-1', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/test-prod-1.png', NULL, '145', 'attachment-image', NULL, NULL, NULL, '145', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 09:00:22', '2021-07-12 17:01:31', 1, NULL, NULL, NULL, NULL, 1, NULL),
(47, 'http://gordon:8888/wp-content/uploads/2021/07/test-prod-2.png', '61:2653b54e649edcdcbb3d083d54463382', 146, 'post', 'attachment', 1, 52, NULL, NULL, 'test-prod-2', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/test-prod-2.png', NULL, '146', 'attachment-image', NULL, NULL, NULL, '146', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 09:00:25', '2021-07-12 17:01:31', 1, NULL, NULL, NULL, NULL, 1, NULL),
(48, 'http://gordon:8888/wp-content/uploads/2021/07/test-prod-3.png', '61:6a697f9028253142c137bd78f6ec622a', 147, 'post', 'attachment', 1, 52, NULL, NULL, 'test-prod-3', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/test-prod-3.png', NULL, '147', 'attachment-image', NULL, NULL, NULL, '147', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 09:00:27', '2021-07-12 17:01:31', 1, NULL, NULL, NULL, NULL, 1, NULL),
(49, 'http://gordon:8888/wp-content/uploads/2021/07/test-prod-4.png', '61:f7e100ed46a22ba71af0ae73dd9a5a57', 148, 'post', 'attachment', 1, 52, NULL, NULL, 'test-prod-4', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/test-prod-4.png', NULL, '148', 'attachment-image', NULL, NULL, NULL, '148', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 09:00:28', '2021-07-12 17:01:31', 1, NULL, NULL, NULL, NULL, 1, NULL),
(50, 'http://gordon:8888/wp-content/uploads/2021/07/test-prod-5.png', '61:63c08e4e119e9a575069d744ad8dbcb1', 149, 'post', 'attachment', 1, 52, NULL, NULL, 'test-prod-5', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/test-prod-5.png', NULL, '149', 'attachment-image', NULL, NULL, NULL, '149', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 09:00:30', '2021-07-12 17:01:31', 1, NULL, NULL, NULL, NULL, 1, NULL),
(51, 'http://gordon:8888/?post_type=acf-field&p=150', '45:d7795db2f30778edb01a21e209d2be8e', 150, 'post', 'acf-field', 1, 141, NULL, NULL, 'סליידר עם מוצרים נוספים', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 09:05:46', '2021-07-12 07:28:54', 1, NULL, NULL, NULL, NULL, 1, NULL),
(52, 'http://gordon:8888/?post_type=acf-field&p=151', '45:69a7476e145c21caa086a6299241bc36', 151, 'post', 'acf-field', 1, 141, NULL, NULL, 'כותרת', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 09:09:14', '2021-07-12 06:09:14', 1, NULL, NULL, NULL, NULL, 1, NULL),
(53, 'http://gordon:8888/?post_type=acf-field&p=152', '45:0b6eb1d0a304fdeb8ec309f2f0dd841b', 152, 'post', 'acf-field', 1, 141, NULL, NULL, 'טקסט', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 09:09:26', '2021-07-12 07:28:54', 1, NULL, NULL, NULL, NULL, 1, NULL),
(54, 'http://gordon:8888/?post_type=acf-field-group&p=154', '51:1a6ba5bfd17b4653fc340bdeb568258b', 154, 'post', 'acf-field-group', 1, 0, NULL, NULL, 'תוספות תוכן בעמוד מוצר', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 10:24:31', '2021-07-12 07:29:10', 1, NULL, NULL, NULL, NULL, 0, NULL),
(55, 'http://gordon:8888/?post_type=acf-field&p=155', '45:ceca5a771daee621097120cad0c99884', 155, 'post', 'acf-field', 1, 154, NULL, NULL, 'סליידר SEO', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 10:25:06', '2021-07-12 07:28:08', 1, NULL, NULL, NULL, NULL, 1, NULL),
(56, 'http://gordon:8888/?post_type=acf-field&p=156', '45:7aafa0faaed0d4e31028ff4e43d9d22c', 156, 'post', 'acf-field', 1, 155, NULL, NULL, 'תוכן', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 10:25:06', '2021-07-13 08:53:02', 1, NULL, NULL, NULL, NULL, 1, NULL),
(57, 'http://gordon:8888/?post_type=acf-field&p=27', '44:9f0db7536cf666ca0257044a59959d62', 27, 'post', 'acf-field', 1, 21, NULL, NULL, 'תמונה', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 10:25:06', '2021-07-13 08:53:02', 1, NULL, NULL, NULL, NULL, 1, NULL),
(58, 'http://gordon:8888/?post_type=acf-field&p=157', '45:d4c18966dff4b4c9118dc62f63a82d0c', 157, 'post', 'acf-field', 1, 154, NULL, NULL, 'תמונה', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 10:25:21', '2021-07-12 07:28:19', 1, NULL, NULL, NULL, NULL, 1, NULL),
(59, 'http://gordon:8888/?post_type=acf-field&p=158', '45:b160cfe1c93ab77b8a22324342c66e90', 158, 'post', 'acf-field', 1, 154, NULL, NULL, 'כותרת של תמונה', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 10:27:07', '2021-07-12 07:28:27', 1, NULL, NULL, NULL, NULL, 1, NULL),
(60, 'http://gordon:8888/?post_type=acf-field&p=159', '45:8560f6403658f6deb1c4f686b2f3a6a6', 159, 'post', 'acf-field', 1, 154, NULL, NULL, 'טקסט בתמונה', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 10:27:07', '2021-07-12 07:28:41', 1, NULL, NULL, NULL, NULL, 1, NULL),
(61, 'http://gordon:8888/?post_type=acf-field&p=160', '45:0d87a39c065f5d7d63492e181795365d', 160, 'post', 'acf-field', 1, 154, NULL, NULL, 'קישור', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 10:27:07', '2021-07-12 07:28:49', 1, NULL, NULL, NULL, NULL, 1, NULL),
(62, 'http://gordon:8888/wp-content/uploads/2021/07/slider-img.png', '60:2737205c3beb48a063f6e2d3247df8ca', 161, 'post', 'attachment', 1, 67, NULL, NULL, 'slider-img', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/slider-img.png', NULL, '161', 'attachment-image', NULL, NULL, NULL, '161', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 11:00:04', '2021-07-12 08:00:04', 1, NULL, NULL, NULL, NULL, 0, NULL),
(63, 'http://gordon:8888/wp-content/uploads/2021/07/prod-slider-img.png', '65:8ebb07cc2e7a5f9d8007b2d10e228dc6', 163, 'post', 'attachment', 1, 52, NULL, NULL, 'prod-slider-img', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/prod-slider-img.png', NULL, '163', 'attachment-image', NULL, NULL, NULL, '163', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 11:18:30', '2021-07-12 17:01:31', 1, NULL, NULL, NULL, NULL, 1, NULL),
(64, 'http://gordon:8888/?post_type=acf-field&p=166', '45:205ca8655a6e16622a1169f7edac912d', 166, 'post', 'acf-field', 1, 165, NULL, NULL, 'בלוק ראשי', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:36:23', '2021-07-12 16:36:23', 1, NULL, NULL, NULL, NULL, 1, NULL),
(65, 'http://gordon:8888/?post_type=acf-field-group&p=165', '51:0451ea9c126d22f2fe56d8c0a350832f', 165, 'post', 'acf-field-group', 1, 0, NULL, NULL, 'דף הבית', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:36:23', '2021-07-12 17:51:05', 1, NULL, NULL, NULL, NULL, 0, NULL),
(66, 'http://gordon:8888/?post_type=acf-field&p=167', '45:0b7ec54a103c155c770cc11becf462ce', 167, 'post', 'acf-field', 1, 165, NULL, NULL, 'תמונה', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:36:23', '2021-07-12 16:36:23', 1, NULL, NULL, NULL, NULL, 1, NULL),
(67, 'http://gordon:8888/?post_type=acf-field&p=168', '45:94d198a559ae1d9aee03378cf1933087', 168, 'post', 'acf-field', 1, 165, NULL, NULL, 'כותרת קטנה', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:36:23', '2021-07-12 16:36:23', 1, NULL, NULL, NULL, NULL, 1, NULL),
(68, 'http://gordon:8888/?post_type=acf-field&p=169', '45:8d2c3663c9f5ca68c6ad1243575b8814', 169, 'post', 'acf-field', 1, 165, NULL, NULL, 'כותרת גדולה', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:36:23', '2021-07-12 16:36:23', 1, NULL, NULL, NULL, NULL, 1, NULL),
(69, 'http://gordon:8888/?post_type=acf-field&p=170', '45:6f96189a1dc8d3493ded67a8466157f1', 170, 'post', 'acf-field', 1, 165, NULL, NULL, 'קישור', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:36:23', '2021-07-12 16:36:23', 1, NULL, NULL, NULL, NULL, 1, NULL),
(70, 'http://gordon:8888/?post_type=acf-field&p=171', '45:08653a873fbce5bb993dbe892c91ba3e', 171, 'post', 'acf-field', 1, 165, NULL, NULL, 'מבחר שירותיים ומוצרים', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:38:50', '2021-07-12 16:38:50', 1, NULL, NULL, NULL, NULL, 1, NULL),
(71, 'http://gordon:8888/?post_type=acf-field&p=172', '45:51ad646de6d7da27cd793578b7239a28', 172, 'post', 'acf-field', 1, 165, NULL, NULL, 'טקסט', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:38:50', '2021-07-12 16:38:50', 1, NULL, NULL, NULL, NULL, 1, NULL),
(72, 'http://gordon:8888/?post_type=acf-field&p=173', '45:e3b454391ced2e74d5a2e2e9bd86f3fb', 173, 'post', 'acf-field', 1, 165, NULL, NULL, 'פריט', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:38:50', '2021-07-12 16:38:50', 1, NULL, NULL, NULL, NULL, 1, NULL),
(73, 'http://gordon:8888/?post_type=acf-field&p=174', '45:00fc82516e1eaa4f3d0336d2b1df24f9', 174, 'post', 'acf-field', 1, 173, NULL, NULL, 'תמונה', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:38:50', '2021-07-12 16:38:50', 1, NULL, NULL, NULL, NULL, 1, NULL),
(74, 'http://gordon:8888/?post_type=acf-field&p=175', '45:745863d84f5e4c95dfd9d2b02b67e809', 175, 'post', 'acf-field', 1, 173, NULL, NULL, 'כותרת קטנה', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:38:50', '2021-07-12 16:38:51', 1, NULL, NULL, NULL, NULL, 1, NULL),
(75, 'http://gordon:8888/?post_type=acf-field&p=176', '45:669e510f55ef00b9154925616ef370c1', 176, 'post', 'acf-field', 1, 173, NULL, NULL, 'כותרת גדולה', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:38:51', '2021-07-12 16:38:51', 1, NULL, NULL, NULL, NULL, 1, NULL),
(76, 'http://gordon:8888/?post_type=acf-field&p=177', '45:16970085882220b039374b632287aed6', 177, 'post', 'acf-field', 1, 173, NULL, NULL, 'קישור', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:38:51', '2021-07-12 16:38:51', 1, NULL, NULL, NULL, NULL, 1, NULL),
(77, 'http://gordon:8888/?post_type=acf-field&p=178', '45:9e15442cdcd455f2702f248076c613b3', 178, 'post', 'acf-field', 1, 165, NULL, NULL, 'בלוק עם חיפוש', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:41:33', '2021-07-12 16:41:33', 1, NULL, NULL, NULL, NULL, 1, NULL),
(78, 'http://gordon:8888/?post_type=acf-field&p=179', '45:a4c36ccd36f425beb50dcdb19057de1e', 179, 'post', 'acf-field', 1, 165, NULL, NULL, 'טקסט', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:41:33', '2021-07-12 16:41:33', 1, NULL, NULL, NULL, NULL, 1, NULL),
(79, 'http://gordon:8888/?post_type=acf-field&p=180', '45:28933c0d3d38473b417a8a34435d45cb', 180, 'post', 'acf-field', 1, 165, NULL, NULL, 'סליידר עם באנרים', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:41:33', '2021-07-12 16:41:33', 1, NULL, NULL, NULL, NULL, 1, NULL),
(80, 'http://gordon:8888/?post_type=acf-field&p=181', '45:a2319f2b2463d6b37b1dab54fe08abfa', 181, 'post', 'acf-field', 1, 165, NULL, NULL, 'סליידר עם באנרים', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:41:33', '2021-07-12 16:41:33', 1, NULL, NULL, NULL, NULL, 1, NULL),
(81, 'http://gordon:8888/?post_type=acf-field&p=182', '45:11d541d6f89cef404cced0fe2a5b9103', 182, 'post', 'acf-field', 1, 181, NULL, NULL, 'תמונות', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:41:33', '2021-07-12 16:41:33', 1, NULL, NULL, NULL, NULL, 1, NULL),
(83, 'http://gordon:8888/?post_type=acf-field&p=184', '45:5b42d68f3676dc05f49c4bae62c6ed83', 184, 'post', 'acf-field', 1, 165, NULL, NULL, 'בלוק עם מוצרים', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:45:20', '2021-07-12 16:45:20', 1, NULL, NULL, NULL, NULL, 1, NULL),
(84, 'http://gordon:8888/?post_type=acf-field&p=185', '45:9026aab5073e790f24a3bbcf2616f75c', 185, 'post', 'acf-field', 1, 165, NULL, NULL, 'טקסט', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:45:20', '2021-07-12 16:45:20', 1, NULL, NULL, NULL, NULL, 1, NULL),
(85, 'http://gordon:8888/?post_type=acf-field&p=186', '45:a0594f214e197155a28177ed1b0460f6', 186, 'post', 'acf-field', 1, 165, NULL, NULL, 'קישור לכל המוצרים', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:45:20', '2021-07-12 16:45:20', 1, NULL, NULL, NULL, NULL, 1, NULL),
(86, 'http://gordon:8888/?post_type=acf-field&p=187', '45:8db538a0a0e1be40dc475c0bb808d5a6', 187, 'post', 'acf-field', 1, 165, NULL, NULL, 'מוצרים custom', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:45:20', '2021-07-12 16:45:20', 1, NULL, NULL, NULL, NULL, 1, NULL),
(87, 'http://gordon:8888/?post_type=acf-field&p=188', '45:8970e10e5a5feaec61f276841ad4beac', 188, 'post', 'acf-field', 1, 187, NULL, NULL, 'תמונה', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:45:20', '2021-07-12 16:45:20', 1, NULL, NULL, NULL, NULL, 1, NULL),
(88, 'http://gordon:8888/?post_type=acf-field&p=189', '45:8121208773be45936a178242064dff18', 189, 'post', 'acf-field', 1, 187, NULL, NULL, 'כותרת', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:45:20', '2021-07-12 16:45:20', 1, NULL, NULL, NULL, NULL, 1, NULL),
(89, 'http://gordon:8888/?post_type=acf-field&p=190', '45:9cccb5e7a83fe8da1f38a2d96be97961', 190, 'post', 'acf-field', 1, 187, NULL, NULL, 'קישור', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:45:20', '2021-07-12 16:45:20', 1, NULL, NULL, NULL, NULL, 1, NULL),
(90, 'http://gordon:8888/?post_type=acf-field&p=191', '45:4ac76c9b08d4f4a1cdee01c3623b8ee7', 191, 'post', 'acf-field', 1, 165, NULL, NULL, 'קורסים', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:56:44', '2021-07-12 16:56:44', 1, NULL, NULL, NULL, NULL, 1, NULL),
(91, 'http://gordon:8888/?post_type=acf-field&p=192', '45:4ca152287738a3c23e16e9507d832fff', 192, 'post', 'acf-field', 1, 165, NULL, NULL, 'טקסט', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:56:44', '2021-07-12 16:56:44', 1, NULL, NULL, NULL, NULL, 1, NULL),
(92, 'http://gordon:8888/?post_type=acf-field&p=193', '45:950711127a0c496c6d893d2531a994bb', 193, 'post', 'acf-field', 1, 165, NULL, NULL, 'תמונה', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:56:44', '2021-07-12 16:56:44', 1, NULL, NULL, NULL, NULL, 1, NULL),
(93, 'http://gordon:8888/?post_type=acf-field&p=194', '45:01d684db76adf40c582f2eb67f62660e', 194, 'post', 'acf-field', 1, 165, NULL, NULL, 'קורסים', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:56:44', '2021-07-12 16:56:44', 1, NULL, NULL, NULL, NULL, 1, NULL),
(94, 'http://gordon:8888/?post_type=acf-field&p=195', '45:21bb508b6791738707cd6e226f895161', 195, 'post', 'acf-field', 1, 194, NULL, NULL, 'תמונה', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:56:44', '2021-07-12 16:56:44', 1, NULL, NULL, NULL, NULL, 1, NULL),
(95, 'http://gordon:8888/?post_type=acf-field&p=196', '45:643cfb0595de582cd9e0ce45c6cd6203', 196, 'post', 'acf-field', 1, 194, NULL, NULL, 'כותרת', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:56:44', '2021-07-12 16:56:44', 1, NULL, NULL, NULL, NULL, 1, NULL),
(96, 'http://gordon:8888/?post_type=acf-field&p=197', '45:01b28c4113888e6c5dd6f4d4fa152b76', 197, 'post', 'acf-field', 1, 194, NULL, NULL, 'קישור', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:56:44', '2021-07-12 16:56:44', 1, NULL, NULL, NULL, NULL, 1, NULL),
(97, 'http://gordon:8888/?post_type=acf-field&p=198', '45:a81062da8db0f1612ecf0c29b9acf11a', 198, 'post', 'acf-field', 1, 165, NULL, NULL, 'קישור לכל הקורסים', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:56:44', '2021-07-12 16:56:44', 1, NULL, NULL, NULL, NULL, 1, NULL),
(98, 'http://gordon:8888/?post_type=acf-field&p=199', '45:fd74195206c198ef3de581489b632ad2', 199, 'post', 'acf-field', 1, 165, NULL, NULL, 'סליידר עם מוצרים', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:56:44', '2021-07-12 16:56:44', 1, NULL, NULL, NULL, NULL, 1, NULL),
(99, 'http://gordon:8888/?post_type=acf-field&p=200', '45:f4066ea897f910f898328afb8ac1ebb4', 200, 'post', 'acf-field', 1, 165, NULL, NULL, 'טקסט', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:56:44', '2021-07-12 16:56:44', 1, NULL, NULL, NULL, NULL, 1, NULL),
(100, 'http://gordon:8888/?post_type=acf-field&p=201', '45:3144514893095ff17a03f921fac9c2f4', 201, 'post', 'acf-field', 1, 165, NULL, NULL, 'קישור לכל המוצרים', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:56:44', '2021-07-12 16:56:44', 1, NULL, NULL, NULL, NULL, 1, NULL),
(101, 'http://gordon:8888/?post_type=acf-field&p=202', '45:7115995c33fe770cf064ea5aa6b085e9', 202, 'post', 'acf-field', 1, 165, NULL, NULL, 'סליידר עם מוצרים', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:56:44', '2021-07-12 16:56:44', 1, NULL, NULL, NULL, NULL, 1, NULL),
(102, 'http://gordon:8888/?post_type=acf-field&p=203', '45:e434143f1a70fa8a948ebc3dd5bd41fd', 203, 'post', 'acf-field', 1, 165, NULL, NULL, 'יתרונות', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:56:44', '2021-07-12 16:56:44', 1, NULL, NULL, NULL, NULL, 1, NULL),
(103, 'http://gordon:8888/?post_type=acf-field&p=204', '45:a2639c4e0038bfd23f6298ff344c59b6', 204, 'post', 'acf-field', 1, 165, NULL, NULL, 'סליידר עם יתרונות', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:56:44', '2021-07-12 17:51:05', 1, NULL, NULL, NULL, NULL, 1, NULL),
(104, 'http://gordon:8888/?post_type=acf-field&p=205', '45:9b997d6d990dfcfeb56833037ecccc7e', 205, 'post', 'acf-field', 1, 204, NULL, NULL, 'טקסט', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:56:44', '2021-07-12 16:56:44', 1, NULL, NULL, NULL, NULL, 1, NULL),
(105, 'http://gordon:8888/?post_type=acf-field&p=206', '45:8623c5dc1b06fdd20f1a92d7fe128752', 206, 'post', 'acf-field', 1, 165, NULL, NULL, 'תמונה', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 19:56:44', '2021-07-12 17:51:05', 1, NULL, NULL, NULL, NULL, 1, NULL),
(106, 'http://gordon:8888/wp-content/uploads/2021/07/course-1-1.png', '60:19052df16cee9d12e0d27a785e6698f0', 207, 'post', 'attachment', 1, 67, NULL, NULL, 'course-1', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/course-1-1.png', NULL, '207', 'attachment-image', NULL, NULL, NULL, '207', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 19:59:07', '2021-07-12 16:59:07', 1, NULL, NULL, NULL, NULL, 0, NULL),
(107, 'http://gordon:8888/wp-content/uploads/2021/07/course-2-1.png', '60:7497a068a637d010cab4d7b76d470fd4', 208, 'post', 'attachment', 1, 67, NULL, NULL, 'course-2', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/course-2-1.png', NULL, '208', 'attachment-image', NULL, NULL, NULL, '208', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 19:59:10', '2021-07-12 16:59:10', 1, NULL, NULL, NULL, NULL, 0, NULL),
(108, 'http://gordon:8888/wp-content/uploads/2021/07/course-3-1.png', '60:ade2c5f7081003b119fd014466cd589c', 209, 'post', 'attachment', 1, 67, NULL, NULL, 'course-3', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/course-3-1.png', NULL, '209', 'attachment-image', NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/course-3-1.png', '209', 'attachment-image', '{\n    \"width\": 351,\n    \"height\": 211,\n    \"url\": \"http://gordon:8888/wp-content/uploads/2021/07/course-3-1.png\",\n    \"path\": \"/Users/kathappens/sites/gordon/www/wp-content/uploads/2021/07/course-3-1.png\",\n    \"size\": \"full\",\n    \"id\": 209,\n    \"alt\": \"\",\n    \"pixels\": 74061,\n    \"type\": \"image/png\"\n}', NULL, NULL, NULL, '2021-07-12 19:59:13', '2021-07-12 17:11:09', 1, NULL, NULL, NULL, NULL, 0, NULL);
INSERT INTO `fmn_yoast_indexable` (`id`, `permalink`, `permalink_hash`, `object_id`, `object_type`, `object_sub_type`, `author_id`, `post_parent`, `title`, `description`, `breadcrumb_title`, `post_status`, `is_public`, `is_protected`, `has_public_posts`, `number_of_pages`, `canonical`, `primary_focus_keyword`, `primary_focus_keyword_score`, `readability_score`, `is_cornerstone`, `is_robots_noindex`, `is_robots_nofollow`, `is_robots_noarchive`, `is_robots_noimageindex`, `is_robots_nosnippet`, `twitter_title`, `twitter_image`, `twitter_description`, `twitter_image_id`, `twitter_image_source`, `open_graph_title`, `open_graph_description`, `open_graph_image`, `open_graph_image_id`, `open_graph_image_source`, `open_graph_image_meta`, `link_count`, `incoming_link_count`, `prominent_words_version`, `created_at`, `updated_at`, `blog_id`, `language`, `region`, `schema_page_type`, `schema_article_type`, `has_ancestors`, `estimated_reading_time_minutes`) VALUES
(109, 'http://gordon:8888/wp-content/uploads/2021/07/course-4-1.png', '60:ec532d8ccaf4fa722fa6bba17b882c92', 210, 'post', 'attachment', 1, 67, NULL, NULL, 'course-4', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/course-4-1.png', NULL, '210', 'attachment-image', NULL, NULL, NULL, '210', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 19:59:15', '2021-07-12 16:59:15', 1, NULL, NULL, NULL, NULL, 0, NULL),
(110, 'http://gordon:8888/wp-content/uploads/2021/07/courses-img.png', '61:b991819742750026597d93e1e995f9df', 211, 'post', 'attachment', 1, 67, NULL, NULL, 'courses-img', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/courses-img.png', NULL, '211', 'attachment-image', NULL, NULL, NULL, '211', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 20:00:00', '2021-07-12 17:00:00', 1, NULL, NULL, NULL, NULL, 0, NULL),
(111, 'http://gordon:8888/product-category/%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%aa-%d7%9e%d7%95%d7%a6%d7%a8%d7%99%d7%9d-%d7%9c%d7%98%d7%a1%d7%98/', '141:055a868b95719c2f329e23c1b56f23d7', 24, 'term', 'product_cat', NULL, NULL, NULL, NULL, 'קטגורית מוצרים לטסט', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-12 20:01:31', '2021-07-12 17:01:31', 1, NULL, NULL, NULL, NULL, 0, NULL),
(112, 'http://gordon:8888/wp-content/uploads/2021/07/slider-benefits-img.png', '69:83166cc50a993fce8e1e1333c2f269fd', 212, 'post', 'attachment', 1, 67, NULL, NULL, 'slider-benefits-img', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/slider-benefits-img.png', NULL, '212', 'attachment-image', NULL, NULL, NULL, '212', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 20:03:46', '2021-07-12 17:03:46', 1, NULL, NULL, NULL, NULL, 0, NULL),
(113, 'http://gordon:8888/wp-content/uploads/2021/07/main-img.png', '58:e2bd15f5df31fbc3734ca3958c800499', 213, 'post', 'attachment', 1, 67, NULL, NULL, 'main-img', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/main-img.png', NULL, '213', 'attachment-image', NULL, NULL, NULL, '213', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 20:06:07', '2021-07-12 17:06:07', 1, NULL, NULL, NULL, NULL, 0, NULL),
(114, 'http://gordon:8888/wp-content/uploads/2021/07/home-part-img-1.png', '65:060b225e34710016f06c07ffebb8ce53', 214, 'post', 'attachment', 1, 67, NULL, NULL, 'home-part-img-1', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/home-part-img-1.png', NULL, '214', 'attachment-image', NULL, NULL, NULL, '214', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 20:07:10', '2021-07-12 17:07:10', 1, NULL, NULL, NULL, NULL, 0, NULL),
(115, 'http://gordon:8888/wp-content/uploads/2021/07/home-part-img-2.png', '65:f45bdbf9b3b3c4655a770cb22d100372', 215, 'post', 'attachment', 1, 67, NULL, NULL, 'home-part-img-2', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/home-part-img-2.png', NULL, '215', 'attachment-image', NULL, NULL, NULL, '215', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 20:07:12', '2021-07-12 17:07:12', 1, NULL, NULL, NULL, NULL, 0, NULL),
(116, 'http://gordon:8888/wp-content/uploads/2021/07/baner-1.png', '57:24d80e3bbf5d8cd7eb889b116502ddb3', 216, 'post', 'attachment', 1, 67, NULL, NULL, 'baner-1', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/baner-1.png', NULL, '216', 'attachment-image', NULL, NULL, NULL, '216', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 20:10:42', '2021-07-12 17:10:42', 1, NULL, NULL, NULL, NULL, 0, NULL),
(117, 'http://gordon:8888/wp-content/uploads/2021/07/baner-2.png', '57:d4273d13b57da638269428a41c7f9469', 217, 'post', 'attachment', 1, 67, NULL, NULL, 'baner-2', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/baner-2.png', NULL, '217', 'attachment-image', NULL, NULL, NULL, '217', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 20:10:44', '2021-07-12 17:10:44', 1, NULL, NULL, NULL, NULL, 0, NULL),
(118, 'http://gordon:8888/wp-content/uploads/2021/07/baner-3.png', '57:7eed048af2f30475f266f7356c44529e', 218, 'post', 'attachment', 1, 67, NULL, NULL, 'baner-3', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/baner-3.png', NULL, '218', 'attachment-image', NULL, NULL, NULL, '218', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 20:10:46', '2021-07-12 17:10:46', 1, NULL, NULL, NULL, NULL, 0, NULL),
(119, 'http://gordon:8888/wp-content/uploads/2021/07/post-1-1.png', '58:c397c7c65c815a1a2b52965f1edfa4db', 219, 'post', 'attachment', 1, 67, NULL, NULL, 'post-1', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/post-1-1.png', NULL, '219', 'attachment-image', NULL, NULL, NULL, '219', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 20:11:47', '2021-07-12 17:11:47', 1, NULL, NULL, NULL, NULL, 0, NULL),
(120, 'http://gordon:8888/wp-content/uploads/2021/07/post-2-1.png', '58:9c8203fa8f203bd5b1ade36d22ecf4c0', 220, 'post', 'attachment', 1, 67, NULL, NULL, 'post-2', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/post-2-1.png', NULL, '220', 'attachment-image', NULL, NULL, NULL, '220', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 20:11:49', '2021-07-12 17:11:49', 1, NULL, NULL, NULL, NULL, 0, NULL),
(121, 'http://gordon:8888/wp-content/uploads/2021/07/post-3-1.png', '58:11ece449558009584a527f38b6b6b713', 221, 'post', 'attachment', 1, 67, NULL, NULL, 'post-3', 'inherit', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/post-3-1.png', NULL, '221', 'attachment-image', NULL, NULL, NULL, '221', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-12 20:11:52', '2021-07-12 17:11:52', 1, NULL, NULL, NULL, NULL, 0, NULL),
(122, 'http://gordon:8888/?post_type=acf-field&p=223', '45:4e33631a37dd9fdbba11d2bcd2fe7d38', 223, 'post', 'acf-field', 1, 165, NULL, NULL, 'כותרת של סליידר עם יתרונות', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-12 20:51:05', '2021-07-12 17:51:05', 1, NULL, NULL, NULL, NULL, 1, NULL),
(123, 'http://gordon:8888/?post_type=acf-field&p=227', '45:bd3e5dd68a3a44b9fd668934ae1ce85c', 227, 'post', 'acf-field', 1, 226, NULL, NULL, 'תמונות', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 06:37:23', '2021-07-13 03:37:23', 1, NULL, NULL, NULL, NULL, 1, NULL),
(124, 'http://gordon:8888/?post_type=acf-field-group&p=226', '51:3fa05d586989cc040c0d63dbf11e366e', 226, 'post', 'acf-field-group', 1, 0, NULL, NULL, 'גלריה', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 06:37:23', '2021-07-13 03:37:23', 1, NULL, NULL, NULL, NULL, 0, NULL),
(125, 'http://gordon:8888/%d7%92%d7%9c%d7%a8%d7%99%d7%94/', '50:d9a6a680b074a194ccf6f28238f60937', 228, 'post', 'page', 1, 0, NULL, NULL, 'גלריה', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 06:37:46', '2021-07-13 03:38:18', 1, NULL, NULL, NULL, NULL, 0, NULL),
(126, 'http://gordon:8888/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/', '56:b11296d9113ecb62fc2bbca1a9e5c45a', 231, 'post', 'page', 1, 0, NULL, NULL, 'מאמרים', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 06:39:35', '2021-07-13 08:26:56', 1, NULL, NULL, NULL, NULL, 0, NULL),
(127, 'http://gordon:8888/2021/07/13/233/', '34:504e9182c6b60c70f466567686c35c34', 233, 'post', 'nav_menu_item', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 06:39:57', '2021-07-13 03:40:07', 1, NULL, NULL, NULL, NULL, 0, NULL),
(128, 'http://gordon:8888/2021/07/13/234/', '34:fc71fd3759592186ea09bc3d5a8c81e2', 234, 'post', 'nav_menu_item', 1, 0, NULL, NULL, '', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 06:39:57', '2021-07-13 03:40:07', 1, NULL, NULL, NULL, NULL, 0, NULL),
(129, 'http://gordon:8888/2021/07/08/%d7%a9%d7%9c%d7%95%d7%9d-%d7%a2%d7%95%d7%9c%d7%9d/', '80:2f8abd3e939b43c02143492590a2413f', 1, 'post', 'post', 1, 0, NULL, NULL, 'שלום עולם! TEST', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/main-img.png', NULL, '213', 'featured-image', NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/main-img.png', '213', 'featured-image', '{\n    \"width\": 1920,\n    \"height\": 982,\n    \"url\": \"http://gordon:8888/wp-content/uploads/2021/07/main-img.png\",\n    \"path\": \"/Users/kathappens/sites/gordon/www/wp-content/uploads/2021/07/main-img.png\",\n    \"size\": \"full\",\n    \"id\": 213,\n    \"alt\": \"\",\n    \"pixels\": 1885440,\n    \"type\": \"image/png\"\n}', 0, NULL, NULL, '2021-07-13 06:46:07', '2021-07-13 08:53:02', 1, NULL, NULL, NULL, NULL, 0, 1),
(130, 'http://gordon:8888/?post_type=acf-field&p=236', '45:9844a2a667fd22cae94c4c041534ba8c', 236, 'post', 'acf-field', 1, 235, NULL, NULL, 'תמונות אחרי תוכן', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 07:17:21', '2021-07-13 04:17:21', 1, NULL, NULL, NULL, NULL, 1, NULL),
(131, 'http://gordon:8888/?post_type=acf-field-group&p=235', '51:cfa515d8414db8446e331e5087cf260d', 235, 'post', 'acf-field-group', 1, 0, NULL, NULL, 'מאמר', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 07:17:21', '2021-07-13 04:55:47', 1, NULL, NULL, NULL, NULL, 0, NULL),
(132, 'http://gordon:8888/?post_type=acf-field&p=237', '45:40a436c7545e178bdc89ce5775a9538d', 237, 'post', 'acf-field', 1, 235, NULL, NULL, 'קישורים בנושא', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 07:17:21', '2021-07-13 04:17:21', 1, NULL, NULL, NULL, NULL, 1, NULL),
(133, 'http://gordon:8888/?post_type=acf-field&p=238', '45:f2b12a38585a482719767aec254b96aa', 238, 'post', 'acf-field', 1, 235, NULL, NULL, 'כותרת', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 07:17:21', '2021-07-13 04:17:21', 1, NULL, NULL, NULL, NULL, 1, NULL),
(134, 'http://gordon:8888/?post_type=acf-field&p=239', '45:13966ab0ccc856881a60d6126b0e2988', 239, 'post', 'acf-field', 1, 235, NULL, NULL, 'קישורים בנושא', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 07:17:21', '2021-07-13 04:17:21', 1, NULL, NULL, NULL, NULL, 1, NULL),
(135, 'http://gordon:8888/?post_type=acf-field&p=240', '45:1cbad059638ed08f56f6c0586e3ee764', 240, 'post', 'acf-field', 1, 239, NULL, NULL, 'כותרת', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 07:17:21', '2021-07-13 04:17:21', 1, NULL, NULL, NULL, NULL, 1, NULL),
(136, 'http://gordon:8888/?post_type=acf-field&p=241', '45:e45f2e10fc6098a65ce08001955039ca', 241, 'post', 'acf-field', 1, 239, NULL, NULL, 'קישור', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 07:17:21', '2021-07-13 04:17:21', 1, NULL, NULL, NULL, NULL, 1, NULL),
(137, 'http://gordon:8888/2021/07/11/%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-5/', '138:21123e4c025a45ca002150b42efe8c8f', 47, 'post', 'post', 1, 0, NULL, NULL, 'שם המאמר לורם איפסום 5', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/course-1.png', NULL, '33', 'featured-image', NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/course-1.png', '33', 'featured-image', '{\n    \"width\": 352,\n    \"height\": 212,\n    \"url\": \"http://gordon:8888/wp-content/uploads/2021/07/course-1.png\",\n    \"path\": \"/Users/kathappens/sites/gordon/www/wp-content/uploads/2021/07/course-1.png\",\n    \"size\": \"full\",\n    \"id\": 33,\n    \"alt\": \"\",\n    \"pixels\": 74624,\n    \"type\": \"image/png\"\n}', 0, NULL, NULL, '2021-07-13 07:51:59', '2021-07-13 08:53:02', 1, NULL, NULL, NULL, NULL, 0, NULL),
(138, 'http://gordon:8888/2021/07/11/%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-4/', '138:1fc8221222db0f7c70e952aa66aef0f0', 45, 'post', 'post', 1, 0, NULL, NULL, 'שם המאמר לורם איפסום 4', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/course-2.png', NULL, '34', 'featured-image', NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/course-2.png', '34', 'featured-image', '{\n    \"width\": 351,\n    \"height\": 212,\n    \"url\": \"http://gordon:8888/wp-content/uploads/2021/07/course-2.png\",\n    \"path\": \"/Users/kathappens/sites/gordon/www/wp-content/uploads/2021/07/course-2.png\",\n    \"size\": \"full\",\n    \"id\": 34,\n    \"alt\": \"\",\n    \"pixels\": 74412,\n    \"type\": \"image/png\"\n}', 0, NULL, NULL, '2021-07-13 07:51:59', '2021-07-13 08:53:02', 1, NULL, NULL, NULL, NULL, 0, NULL),
(139, 'http://gordon:8888/2021/07/11/%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-3/', '138:0941dc2b5c9dd0d6e08c41028ab0d5bf', 43, 'post', 'post', 1, 0, NULL, NULL, 'שם המאמר לורם איפסום 3', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/course-3.png', NULL, '35', 'featured-image', NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/course-3.png', '35', 'featured-image', '{\n    \"width\": 351,\n    \"height\": 211,\n    \"url\": \"http://gordon:8888/wp-content/uploads/2021/07/course-3.png\",\n    \"path\": \"/Users/kathappens/sites/gordon/www/wp-content/uploads/2021/07/course-3.png\",\n    \"size\": \"full\",\n    \"id\": 35,\n    \"alt\": \"\",\n    \"pixels\": 74061,\n    \"type\": \"image/png\"\n}', 0, NULL, NULL, '2021-07-13 07:51:59', '2021-07-13 08:53:02', 1, NULL, NULL, NULL, NULL, 0, NULL),
(140, 'http://gordon:8888/2021/07/11/%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-2/', '138:1c27af7836b2d56873c5a3c34c67250a', 41, 'post', 'post', 1, 0, NULL, NULL, 'שם המאמר לורם איפסום 2', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 60, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/post-2.png', NULL, '38', 'featured-image', NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/post-2.png', '38', 'featured-image', '{\n    \"width\": 400,\n    \"height\": 288,\n    \"url\": \"http://gordon:8888/wp-content/uploads/2021/07/post-2.png\",\n    \"path\": \"/Users/kathappens/sites/gordon/www/wp-content/uploads/2021/07/post-2.png\",\n    \"size\": \"full\",\n    \"id\": 38,\n    \"alt\": \"\",\n    \"pixels\": 115200,\n    \"type\": \"image/png\"\n}', 0, NULL, NULL, '2021-07-13 07:51:59', '2021-07-13 08:53:02', 1, NULL, NULL, NULL, NULL, 0, NULL),
(141, 'http://gordon:8888/2021/07/11/%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-1/', '138:934b45ede550b3d35e5d116fd0434e1c', 32, 'post', 'post', 1, 0, NULL, NULL, 'שם המאמר לורם איפסום 1', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 30, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/post-3.png', NULL, '39', 'featured-image', NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/post-3.png', '39', 'featured-image', '{\n    \"width\": 295,\n    \"height\": 288,\n    \"url\": \"http://gordon:8888/wp-content/uploads/2021/07/post-3.png\",\n    \"path\": \"/Users/kathappens/sites/gordon/www/wp-content/uploads/2021/07/post-3.png\",\n    \"size\": \"full\",\n    \"id\": 39,\n    \"alt\": \"\",\n    \"pixels\": 84960,\n    \"type\": \"image/png\"\n}', 0, NULL, NULL, '2021-07-13 07:51:59', '2021-07-13 08:53:02', 1, NULL, NULL, NULL, NULL, 0, NULL),
(142, 'http://gordon:8888/?post_type=acf-field&p=244', '45:99f50ed6aea89025030f28ba488648c8', 244, 'post', 'acf-field', 1, 235, NULL, NULL, 'מאמרים נוספים', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 07:55:47', '2021-07-13 04:55:47', 1, NULL, NULL, NULL, NULL, 1, NULL),
(143, 'http://gordon:8888/?post_type=acf-field&p=245', '45:22774b34618bb20bcf33758ffb3667a4', 245, 'post', 'acf-field', 1, 235, NULL, NULL, 'כותרת', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 07:55:47', '2021-07-13 04:55:47', 1, NULL, NULL, NULL, NULL, 1, NULL),
(144, 'http://gordon:8888/?post_type=acf-field&p=246', '45:6c2dbccf40888673722b8dcde80647ac', 246, 'post', 'acf-field', 1, 235, NULL, NULL, 'מאמרים נוספים', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 07:55:47', '2021-07-13 04:55:47', 1, NULL, NULL, NULL, NULL, 1, NULL),
(145, 'http://gordon:8888/?post_type=acf-field&p=247', '45:e3ea1f6e81c135913f383656a0708889', 247, 'post', 'acf-field', 1, 115, NULL, NULL, 'פוסט', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 08:02:13', '2021-07-13 05:02:13', 1, NULL, NULL, NULL, NULL, 1, NULL),
(146, 'http://gordon:8888/?post_type=acf-field-group&p=115', '51:cabadc349e7d4ffd9eb0d289549d1f39', 115, 'post', 'acf-field-group', 1, 0, NULL, NULL, 'תוספות', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 08:02:13', '2021-07-13 05:02:14', 1, NULL, NULL, NULL, NULL, 0, NULL),
(147, 'http://gordon:8888/?post_type=acf-field&p=248', '45:8a38f69144ce92c13de2c3157af3dfa6', 248, 'post', 'acf-field', 1, 115, NULL, NULL, 'קישור לחנות', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 08:02:13', '2021-07-13 05:02:14', 1, NULL, NULL, NULL, NULL, 1, NULL),
(148, 'http://gordon:8888/?post_type=acf-field&p=249', '45:f685889cf1b6c7f64f61ab88352c422f', 249, 'post', 'acf-field', 1, 115, NULL, NULL, 'תמונה', 'publish', NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 08:02:14', '2021-07-13 05:02:14', 1, NULL, NULL, NULL, NULL, 1, NULL),
(149, 'http://gordon:8888/wp-content/uploads/2021/07/girls.png', '55:f77cb7c8469234ab90dab0893b824f77', 250, 'post', 'attachment', 1, 0, NULL, NULL, 'girls', 'inherit', 0, 0, 0, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 'http://gordon:8888/wp-content/uploads/2021/07/girls.png', NULL, '250', 'attachment-image', NULL, NULL, NULL, '250', 'attachment-image', NULL, NULL, NULL, NULL, '2021-07-13 08:03:01', '2021-07-13 05:03:01', 1, NULL, NULL, NULL, NULL, 0, NULL),
(150, 'http://gordon:8888/prod_area/%d7%aa%d7%97%d7%95%d7%9d-%d7%93%d7%a2%d7%aa-1/', '75:382a151a44b0f2882dd9c766ee9d392c', 27, 'term', 'prod_area', NULL, NULL, NULL, NULL, 'תחום דעת 1', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 10:07:18', '2021-07-13 07:07:18', 1, NULL, NULL, NULL, NULL, 0, NULL),
(151, 'http://gordon:8888/prod_area/%d7%aa%d7%97%d7%95%d7%9d-%d7%93%d7%a2%d7%aa-2/', '75:3be7034efe1bacfd43a186c33892676f', 28, 'term', 'prod_area', NULL, NULL, NULL, NULL, 'תחום דעת 2', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 10:07:23', '2021-07-13 07:07:23', 1, NULL, NULL, NULL, NULL, 0, NULL),
(152, 'http://gordon:8888/prod_area/%d7%aa%d7%97%d7%95%d7%9d-%d7%93%d7%a2%d7%aa-3/', '75:e971c6431687f0a5bc7a7d35ba06f242', 29, 'term', 'prod_area', NULL, NULL, NULL, NULL, 'תחום דעת 3', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 10:07:30', '2021-07-13 07:07:30', 1, NULL, NULL, NULL, NULL, 0, NULL),
(153, 'http://gordon:8888/prod_tech/%d7%98%d7%9b%d7%a0%d7%95%d7%9c%d7%95%d7%92%d7%99%d7%94-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-1/', '148:5a608c143da95a6127d5775a717ebd95', 30, 'term', 'prod_tech', NULL, NULL, NULL, NULL, 'טכנולוגיה לורם איפסום 1', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 10:07:52', '2021-07-13 07:07:52', 1, NULL, NULL, NULL, NULL, 0, NULL),
(154, 'http://gordon:8888/prod_tech/%d7%98%d7%9b%d7%a0%d7%95%d7%9c%d7%95%d7%92%d7%99%d7%94-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-2/', '148:9c43b0cd049072c0ecd03235ef4964cf', 31, 'term', 'prod_tech', NULL, NULL, NULL, NULL, 'טכנולוגיה לורם איפסום 2', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 10:07:56', '2021-07-13 07:07:56', 1, NULL, NULL, NULL, NULL, 0, NULL),
(155, 'http://gordon:8888/prod_tech/%d7%98%d7%9b%d7%a0%d7%95%d7%9c%d7%95%d7%92%d7%99%d7%94-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-3/', '148:b7fd05dfd0e6846c1e7d3bdb949f26a1', 32, 'term', 'prod_tech', NULL, NULL, NULL, NULL, 'טכנולוגיה לורם איפסום 3', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 10:08:00', '2021-07-13 07:08:00', 1, NULL, NULL, NULL, NULL, 0, NULL),
(156, 'http://gordon:8888/prod_age/%d7%92%d7%99%d7%9c-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-1/', '111:8fcbeaea69429634977361c8232843da', 33, 'term', 'prod_age', NULL, NULL, NULL, NULL, 'גיל לורם איפסום 1', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 10:08:19', '2021-07-13 07:08:19', 1, NULL, NULL, NULL, NULL, 0, NULL),
(157, 'http://gordon:8888/prod_age/%d7%92%d7%99%d7%9c-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-2/', '111:aef7aa730d622d55077078680d2416e1', 34, 'term', 'prod_age', NULL, NULL, NULL, NULL, 'גיל לורם איפסום 2', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 10:08:22', '2021-07-13 07:08:22', 1, NULL, NULL, NULL, NULL, 0, NULL),
(158, 'http://gordon:8888/prod_age/%d7%92%d7%99%d7%9c-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-3/', '111:e0e0c2726bca3d71b7d07a79874ef114', 35, 'term', 'prod_age', NULL, NULL, NULL, NULL, 'גיל לורם איפסום 3', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 10:08:25', '2021-07-13 07:08:25', 1, NULL, NULL, NULL, NULL, 0, NULL),
(159, 'http://gordon:8888/prod_age/%d7%92%d7%99%d7%9c-%d7%9c%d7%95%d7%a8%d7%9d-%d7%90%d7%99%d7%a4%d7%a1%d7%95%d7%9d-4/', '111:33b4b2369f0c8742cb451b383851af0d', 36, 'term', 'prod_age', NULL, NULL, NULL, NULL, 'גיל לורם איפסום 4', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 10:08:29', '2021-07-13 07:08:29', 1, NULL, NULL, NULL, NULL, 0, NULL),
(160, 'http://gordon:8888/category/%d7%9b%d7%9c%d7%9c%d7%99/', '53:b206994ac0992d49e917eb34feff14ec', 1, 'term', 'category', NULL, NULL, NULL, NULL, 'כללי', NULL, NULL, 0, NULL, NULL, NULL, 'כללי', 28, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2021-07-13 11:53:02', '2021-07-13 08:53:02', 1, NULL, NULL, NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_yoast_indexable_hierarchy`
--

CREATE TABLE `fmn_yoast_indexable_hierarchy` (
  `indexable_id` int(11) UNSIGNED NOT NULL,
  `ancestor_id` int(11) UNSIGNED NOT NULL,
  `depth` int(11) UNSIGNED DEFAULT NULL,
  `blog_id` bigint(20) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_yoast_indexable_hierarchy`
--

INSERT INTO `fmn_yoast_indexable_hierarchy` (`indexable_id`, `ancestor_id`, `depth`, `blog_id`) VALUES
(1, 0, 0, 1),
(2, 0, 0, 1),
(3, 0, 0, 1),
(4, 0, 0, 1),
(5, 0, 0, 1),
(6, 0, 0, 1),
(8, 0, 0, 1),
(9, 0, 0, 1),
(11, 0, 0, 1),
(12, 0, 0, 1),
(13, 0, 0, 1),
(14, 0, 0, 1),
(15, 0, 0, 1),
(16, 0, 0, 1),
(17, 0, 0, 1),
(18, 0, 0, 1),
(19, 0, 0, 1),
(20, 11, 1, 1),
(21, 22, 1, 1),
(22, 0, 0, 1),
(23, 22, 1, 1),
(24, 22, 1, 1),
(25, 0, 0, 1),
(26, 11, 1, 1),
(27, 11, 1, 1),
(28, 11, 1, 1),
(29, 11, 1, 1),
(30, 11, 1, 1),
(31, 11, 1, 1),
(32, 11, 1, 1),
(33, 11, 1, 1),
(34, 0, 0, 1),
(36, 0, 0, 1),
(37, 0, 0, 1),
(38, 0, 0, 1),
(39, 0, 0, 1),
(40, 0, 0, 1),
(41, 0, 0, 1),
(42, 43, 1, 1),
(43, 0, 0, 1),
(44, 42, 1, 1),
(44, 43, 2, 1),
(45, 42, 1, 1),
(45, 43, 2, 1),
(46, 34, 1, 1),
(47, 34, 1, 1),
(48, 34, 1, 1),
(49, 34, 1, 1),
(50, 34, 1, 1),
(51, 43, 1, 1),
(52, 43, 1, 1),
(53, 43, 1, 1),
(54, 0, 0, 1),
(55, 54, 1, 1),
(56, 54, 2, 1),
(56, 55, 1, 1),
(57, 25, 1, 1),
(58, 54, 1, 1),
(59, 54, 1, 1),
(60, 54, 1, 1),
(61, 54, 1, 1),
(62, 9, 1, 1),
(63, 34, 1, 1),
(64, 65, 1, 1),
(65, 0, 0, 1),
(66, 65, 1, 1),
(67, 65, 1, 1),
(68, 65, 1, 1),
(69, 65, 1, 1),
(70, 65, 1, 1),
(71, 65, 1, 1),
(72, 65, 1, 1),
(73, 65, 2, 1),
(73, 72, 1, 1),
(74, 65, 2, 1),
(74, 72, 1, 1),
(75, 65, 2, 1),
(75, 72, 1, 1),
(76, 65, 2, 1),
(76, 72, 1, 1),
(77, 65, 1, 1),
(78, 65, 1, 1),
(79, 65, 1, 1),
(80, 65, 1, 1),
(81, 65, 2, 1),
(81, 80, 1, 1),
(83, 65, 1, 1),
(84, 65, 1, 1),
(85, 65, 1, 1),
(86, 65, 1, 1),
(87, 65, 2, 1),
(87, 86, 1, 1),
(88, 65, 2, 1),
(88, 86, 1, 1),
(89, 65, 2, 1),
(89, 86, 1, 1),
(90, 65, 1, 1),
(91, 65, 1, 1),
(92, 65, 1, 1),
(93, 65, 1, 1),
(94, 65, 2, 1),
(94, 93, 1, 1),
(95, 65, 2, 1),
(95, 93, 1, 1),
(96, 65, 2, 1),
(96, 93, 1, 1),
(97, 65, 1, 1),
(98, 65, 1, 1),
(99, 65, 1, 1),
(100, 65, 1, 1),
(101, 65, 1, 1),
(102, 65, 1, 1),
(103, 65, 1, 1),
(104, 65, 2, 1),
(104, 103, 1, 1),
(105, 65, 1, 1),
(106, 9, 1, 1),
(107, 9, 1, 1),
(108, 9, 1, 1),
(109, 9, 1, 1),
(110, 9, 1, 1),
(111, 0, 0, 1),
(112, 9, 1, 1),
(113, 9, 1, 1),
(114, 9, 1, 1),
(115, 9, 1, 1),
(116, 9, 1, 1),
(117, 9, 1, 1),
(118, 9, 1, 1),
(119, 9, 1, 1),
(120, 9, 1, 1),
(121, 9, 1, 1),
(122, 65, 1, 1),
(123, 124, 1, 1),
(124, 0, 0, 1),
(125, 0, 0, 1),
(126, 0, 0, 1),
(127, 0, 0, 1),
(128, 0, 0, 1),
(129, 0, 0, 1),
(130, 131, 1, 1),
(131, 0, 0, 1),
(132, 131, 1, 1),
(133, 131, 1, 1),
(134, 131, 1, 1),
(135, 131, 2, 1),
(135, 134, 1, 1),
(136, 131, 2, 1),
(136, 134, 1, 1),
(137, 0, 0, 1),
(138, 0, 0, 1),
(139, 0, 0, 1),
(140, 0, 0, 1),
(141, 0, 0, 1),
(142, 131, 1, 1),
(143, 131, 1, 1),
(144, 131, 1, 1),
(145, 146, 1, 1),
(146, 0, 0, 1),
(147, 146, 1, 1),
(148, 146, 1, 1),
(149, 0, 0, 1),
(150, 0, 0, 1),
(151, 0, 0, 1),
(152, 0, 0, 1),
(153, 0, 0, 1),
(154, 0, 0, 1),
(155, 0, 0, 1),
(156, 0, 0, 1),
(157, 0, 0, 1),
(158, 0, 0, 1),
(159, 0, 0, 1),
(160, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_yoast_migrations`
--

CREATE TABLE `fmn_yoast_migrations` (
  `id` int(11) UNSIGNED NOT NULL,
  `version` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_yoast_migrations`
--

INSERT INTO `fmn_yoast_migrations` (`id`, `version`) VALUES
(1, '20171228151840'),
(2, '20171228151841'),
(3, '20190529075038'),
(4, '20191011111109'),
(5, '20200408101900'),
(6, '20200420073606'),
(7, '20200428123747'),
(8, '20200428194858'),
(9, '20200429105310'),
(10, '20200430075614'),
(11, '20200430150130'),
(12, '20200507054848'),
(13, '20200513133401'),
(14, '20200609154515'),
(15, '20200616130143'),
(16, '20200617122511'),
(17, '20200702141921'),
(18, '20200728095334'),
(19, '20201202144329'),
(20, '20201216124002'),
(21, '20201216141134');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_yoast_primary_term`
--

CREATE TABLE `fmn_yoast_primary_term` (
  `id` int(11) UNSIGNED NOT NULL,
  `post_id` bigint(20) DEFAULT NULL,
  `term_id` bigint(20) DEFAULT NULL,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `blog_id` bigint(20) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_yoast_primary_term`
--

INSERT INTO `fmn_yoast_primary_term` (`id`, `post_id`, `term_id`, `taxonomy`, `created_at`, `updated_at`, `blog_id`) VALUES
(1, 52, 33, 'prod_age', '2021-07-13 10:09:28', '2021-07-13 07:09:28', 1),
(2, 52, 30, 'prod_tech', '2021-07-13 10:09:28', '2021-07-13 07:09:28', 1),
(3, 52, 27, 'prod_area', '2021-07-13 10:09:28', '2021-07-13 07:09:28', 1),
(4, 61, 26, 'brand', '2021-07-13 10:09:39', '2021-07-13 07:09:39', 1),
(5, 61, 34, 'prod_age', '2021-07-13 10:09:39', '2021-07-13 07:09:39', 1),
(6, 61, 31, 'prod_tech', '2021-07-13 10:09:39', '2021-07-13 07:09:39', 1),
(7, 61, 28, 'prod_area', '2021-07-13 10:09:39', '2021-07-13 07:09:39', 1),
(8, 62, 25, 'brand', '2021-07-13 10:09:55', '2021-07-13 07:09:55', 1),
(9, 62, 35, 'prod_age', '2021-07-13 10:09:55', '2021-07-13 07:09:55', 1),
(10, 62, 32, 'prod_tech', '2021-07-13 10:09:55', '2021-07-13 07:09:55', 1),
(11, 62, 29, 'prod_area', '2021-07-13 10:09:55', '2021-07-13 07:09:55', 1),
(12, 62, 24, 'product_cat', '2021-07-13 10:09:55', '2021-07-13 07:09:55', 1),
(13, 63, 36, 'prod_age', '2021-07-13 10:10:04', '2021-07-13 07:10:04', 1),
(14, 63, 30, 'prod_tech', '2021-07-13 10:10:04', '2021-07-13 07:10:04', 1),
(15, 63, 28, 'prod_area', '2021-07-13 10:10:04', '2021-07-13 07:10:04', 1),
(16, 64, 36, 'prod_age', '2021-07-13 10:10:13', '2021-07-13 07:10:13', 1),
(17, 64, 31, 'prod_tech', '2021-07-13 10:10:13', '2021-07-13 07:10:13', 1),
(18, 64, 27, 'prod_area', '2021-07-13 10:10:13', '2021-07-13 07:10:13', 1),
(19, 65, 34, 'prod_age', '2021-07-13 10:10:21', '2021-07-13 07:10:21', 1),
(20, 65, 31, 'prod_tech', '2021-07-13 10:10:21', '2021-07-13 07:10:21', 1),
(21, 65, 27, 'prod_area', '2021-07-13 10:10:21', '2021-07-13 07:10:21', 1),
(22, 66, 33, 'prod_age', '2021-07-13 10:10:29', '2021-07-13 07:10:29', 1),
(23, 66, 31, 'prod_tech', '2021-07-13 10:10:29', '2021-07-13 07:10:29', 1),
(24, 66, 28, 'prod_area', '2021-07-13 10:10:29', '2021-07-13 07:10:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_yoast_seo_links`
--

CREATE TABLE `fmn_yoast_seo_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `post_id` bigint(20) UNSIGNED DEFAULT NULL,
  `target_post_id` bigint(20) UNSIGNED DEFAULT NULL,
  `type` varchar(8) DEFAULT NULL,
  `indexable_id` int(11) UNSIGNED DEFAULT NULL,
  `target_indexable_id` int(11) UNSIGNED DEFAULT NULL,
  `height` int(11) UNSIGNED DEFAULT NULL,
  `width` int(11) UNSIGNED DEFAULT NULL,
  `size` int(11) UNSIGNED DEFAULT NULL,
  `language` varchar(32) DEFAULT NULL,
  `region` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fmn_actionscheduler_actions`
--
ALTER TABLE `fmn_actionscheduler_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `hook` (`hook`),
  ADD KEY `status` (`status`),
  ADD KEY `scheduled_date_gmt` (`scheduled_date_gmt`),
  ADD KEY `args` (`args`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `last_attempt_gmt` (`last_attempt_gmt`),
  ADD KEY `claim_id` (`claim_id`);

--
-- Indexes for table `fmn_actionscheduler_claims`
--
ALTER TABLE `fmn_actionscheduler_claims`
  ADD PRIMARY KEY (`claim_id`),
  ADD KEY `date_created_gmt` (`date_created_gmt`);

--
-- Indexes for table `fmn_actionscheduler_groups`
--
ALTER TABLE `fmn_actionscheduler_groups`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `slug` (`slug`(191));

--
-- Indexes for table `fmn_actionscheduler_logs`
--
ALTER TABLE `fmn_actionscheduler_logs`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `action_id` (`action_id`),
  ADD KEY `log_date_gmt` (`log_date_gmt`);

--
-- Indexes for table `fmn_commentmeta`
--
ALTER TABLE `fmn_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `fmn_comments`
--
ALTER TABLE `fmn_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10)),
  ADD KEY `woo_idx_comment_type` (`comment_type`);

--
-- Indexes for table `fmn_links`
--
ALTER TABLE `fmn_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `fmn_options`
--
ALTER TABLE `fmn_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Indexes for table `fmn_postmeta`
--
ALTER TABLE `fmn_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `fmn_posts`
--
ALTER TABLE `fmn_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `fmn_termmeta`
--
ALTER TABLE `fmn_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `fmn_terms`
--
ALTER TABLE `fmn_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `fmn_term_relationships`
--
ALTER TABLE `fmn_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `fmn_term_taxonomy`
--
ALTER TABLE `fmn_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `fmn_usermeta`
--
ALTER TABLE `fmn_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `fmn_users`
--
ALTER TABLE `fmn_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `fmn_wc_admin_notes`
--
ALTER TABLE `fmn_wc_admin_notes`
  ADD PRIMARY KEY (`note_id`);

--
-- Indexes for table `fmn_wc_admin_note_actions`
--
ALTER TABLE `fmn_wc_admin_note_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `note_id` (`note_id`);

--
-- Indexes for table `fmn_wc_category_lookup`
--
ALTER TABLE `fmn_wc_category_lookup`
  ADD PRIMARY KEY (`category_tree_id`,`category_id`);

--
-- Indexes for table `fmn_wc_customer_lookup`
--
ALTER TABLE `fmn_wc_customer_lookup`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `fmn_wc_download_log`
--
ALTER TABLE `fmn_wc_download_log`
  ADD PRIMARY KEY (`download_log_id`),
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indexes for table `fmn_wc_order_coupon_lookup`
--
ALTER TABLE `fmn_wc_order_coupon_lookup`
  ADD PRIMARY KEY (`order_id`,`coupon_id`),
  ADD KEY `coupon_id` (`coupon_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `fmn_wc_order_product_lookup`
--
ALTER TABLE `fmn_wc_order_product_lookup`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `fmn_wc_order_stats`
--
ALTER TABLE `fmn_wc_order_stats`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `date_created` (`date_created`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `status` (`status`(191));

--
-- Indexes for table `fmn_wc_order_tax_lookup`
--
ALTER TABLE `fmn_wc_order_tax_lookup`
  ADD PRIMARY KEY (`order_id`,`tax_rate_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `fmn_wc_product_meta_lookup`
--
ALTER TABLE `fmn_wc_product_meta_lookup`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `virtual` (`virtual`),
  ADD KEY `downloadable` (`downloadable`),
  ADD KEY `stock_status` (`stock_status`),
  ADD KEY `stock_quantity` (`stock_quantity`),
  ADD KEY `onsale` (`onsale`),
  ADD KEY `min_max_price` (`min_price`,`max_price`);

--
-- Indexes for table `fmn_wc_reserved_stock`
--
ALTER TABLE `fmn_wc_reserved_stock`
  ADD PRIMARY KEY (`order_id`,`product_id`);

--
-- Indexes for table `fmn_wc_tax_rate_classes`
--
ALTER TABLE `fmn_wc_tax_rate_classes`
  ADD PRIMARY KEY (`tax_rate_class_id`),
  ADD UNIQUE KEY `slug` (`slug`(191));

--
-- Indexes for table `fmn_wc_webhooks`
--
ALTER TABLE `fmn_wc_webhooks`
  ADD PRIMARY KEY (`webhook_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `fmn_woocommerce_api_keys`
--
ALTER TABLE `fmn_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Indexes for table `fmn_woocommerce_attribute_taxonomies`
--
ALTER TABLE `fmn_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(20));

--
-- Indexes for table `fmn_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `fmn_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(16),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `user_order_remaining_expires` (`user_id`,`order_id`,`downloads_remaining`,`access_expires`);

--
-- Indexes for table `fmn_woocommerce_log`
--
ALTER TABLE `fmn_woocommerce_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `level` (`level`);

--
-- Indexes for table `fmn_woocommerce_order_itemmeta`
--
ALTER TABLE `fmn_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `fmn_woocommerce_order_items`
--
ALTER TABLE `fmn_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `fmn_woocommerce_payment_tokenmeta`
--
ALTER TABLE `fmn_woocommerce_payment_tokenmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `payment_token_id` (`payment_token_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `fmn_woocommerce_payment_tokens`
--
ALTER TABLE `fmn_woocommerce_payment_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `fmn_woocommerce_sessions`
--
ALTER TABLE `fmn_woocommerce_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD UNIQUE KEY `session_key` (`session_key`);

--
-- Indexes for table `fmn_woocommerce_shipping_zones`
--
ALTER TABLE `fmn_woocommerce_shipping_zones`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `fmn_woocommerce_shipping_zone_locations`
--
ALTER TABLE `fmn_woocommerce_shipping_zone_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `fmn_woocommerce_shipping_zone_methods`
--
ALTER TABLE `fmn_woocommerce_shipping_zone_methods`
  ADD PRIMARY KEY (`instance_id`);

--
-- Indexes for table `fmn_woocommerce_tax_rates`
--
ALTER TABLE `fmn_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`),
  ADD KEY `tax_rate_state` (`tax_rate_state`(2)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(10)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Indexes for table `fmn_woocommerce_tax_rate_locations`
--
ALTER TABLE `fmn_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `fmn_yith_wcwl`
--
ALTER TABLE `fmn_yith_wcwl`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `prod_id` (`prod_id`);

--
-- Indexes for table `fmn_yith_wcwl_lists`
--
ALTER TABLE `fmn_yith_wcwl_lists`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `wishlist_token` (`wishlist_token`),
  ADD KEY `wishlist_slug` (`wishlist_slug`);

--
-- Indexes for table `fmn_yoast_indexable`
--
ALTER TABLE `fmn_yoast_indexable`
  ADD PRIMARY KEY (`id`),
  ADD KEY `object_type_and_sub_type` (`object_type`,`object_sub_type`),
  ADD KEY `object_id_and_type` (`object_id`,`object_type`),
  ADD KEY `permalink_hash_and_object_type` (`permalink_hash`,`object_type`),
  ADD KEY `subpages` (`post_parent`,`object_type`,`post_status`,`object_id`),
  ADD KEY `prominent_words` (`prominent_words_version`,`object_type`,`object_sub_type`,`post_status`);

--
-- Indexes for table `fmn_yoast_indexable_hierarchy`
--
ALTER TABLE `fmn_yoast_indexable_hierarchy`
  ADD PRIMARY KEY (`indexable_id`,`ancestor_id`),
  ADD KEY `indexable_id` (`indexable_id`),
  ADD KEY `ancestor_id` (`ancestor_id`),
  ADD KEY `depth` (`depth`);

--
-- Indexes for table `fmn_yoast_migrations`
--
ALTER TABLE `fmn_yoast_migrations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fmn_yoast_migrations_version` (`version`);

--
-- Indexes for table `fmn_yoast_primary_term`
--
ALTER TABLE `fmn_yoast_primary_term`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_taxonomy` (`post_id`,`taxonomy`),
  ADD KEY `post_term` (`post_id`,`term_id`);

--
-- Indexes for table `fmn_yoast_seo_links`
--
ALTER TABLE `fmn_yoast_seo_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `link_direction` (`post_id`,`type`),
  ADD KEY `indexable_link_direction` (`indexable_id`,`type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fmn_actionscheduler_actions`
--
ALTER TABLE `fmn_actionscheduler_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `fmn_actionscheduler_claims`
--
ALTER TABLE `fmn_actionscheduler_claims`
  MODIFY `claim_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=239;

--
-- AUTO_INCREMENT for table `fmn_actionscheduler_groups`
--
ALTER TABLE `fmn_actionscheduler_groups`
  MODIFY `group_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fmn_actionscheduler_logs`
--
ALTER TABLE `fmn_actionscheduler_logs`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `fmn_commentmeta`
--
ALTER TABLE `fmn_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_comments`
--
ALTER TABLE `fmn_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fmn_links`
--
ALTER TABLE `fmn_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_options`
--
ALTER TABLE `fmn_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1754;

--
-- AUTO_INCREMENT for table `fmn_postmeta`
--
ALTER TABLE `fmn_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1796;

--
-- AUTO_INCREMENT for table `fmn_posts`
--
ALTER TABLE `fmn_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=254;

--
-- AUTO_INCREMENT for table `fmn_termmeta`
--
ALTER TABLE `fmn_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `fmn_terms`
--
ALTER TABLE `fmn_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `fmn_term_taxonomy`
--
ALTER TABLE `fmn_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `fmn_usermeta`
--
ALTER TABLE `fmn_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `fmn_users`
--
ALTER TABLE `fmn_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fmn_wc_admin_notes`
--
ALTER TABLE `fmn_wc_admin_notes`
  MODIFY `note_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `fmn_wc_admin_note_actions`
--
ALTER TABLE `fmn_wc_admin_note_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1009;

--
-- AUTO_INCREMENT for table `fmn_wc_customer_lookup`
--
ALTER TABLE `fmn_wc_customer_lookup`
  MODIFY `customer_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_wc_download_log`
--
ALTER TABLE `fmn_wc_download_log`
  MODIFY `download_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_wc_tax_rate_classes`
--
ALTER TABLE `fmn_wc_tax_rate_classes`
  MODIFY `tax_rate_class_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fmn_wc_webhooks`
--
ALTER TABLE `fmn_wc_webhooks`
  MODIFY `webhook_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_api_keys`
--
ALTER TABLE `fmn_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_attribute_taxonomies`
--
ALTER TABLE `fmn_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `fmn_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_log`
--
ALTER TABLE `fmn_woocommerce_log`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_order_itemmeta`
--
ALTER TABLE `fmn_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_order_items`
--
ALTER TABLE `fmn_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_payment_tokenmeta`
--
ALTER TABLE `fmn_woocommerce_payment_tokenmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_payment_tokens`
--
ALTER TABLE `fmn_woocommerce_payment_tokens`
  MODIFY `token_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_sessions`
--
ALTER TABLE `fmn_woocommerce_sessions`
  MODIFY `session_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_shipping_zones`
--
ALTER TABLE `fmn_woocommerce_shipping_zones`
  MODIFY `zone_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_shipping_zone_locations`
--
ALTER TABLE `fmn_woocommerce_shipping_zone_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_shipping_zone_methods`
--
ALTER TABLE `fmn_woocommerce_shipping_zone_methods`
  MODIFY `instance_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_tax_rates`
--
ALTER TABLE `fmn_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_tax_rate_locations`
--
ALTER TABLE `fmn_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_yith_wcwl`
--
ALTER TABLE `fmn_yith_wcwl`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fmn_yith_wcwl_lists`
--
ALTER TABLE `fmn_yith_wcwl_lists`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fmn_yoast_indexable`
--
ALTER TABLE `fmn_yoast_indexable`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;

--
-- AUTO_INCREMENT for table `fmn_yoast_migrations`
--
ALTER TABLE `fmn_yoast_migrations`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `fmn_yoast_primary_term`
--
ALTER TABLE `fmn_yoast_primary_term`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `fmn_yoast_seo_links`
--
ALTER TABLE `fmn_yoast_seo_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `fmn_wc_download_log`
--
ALTER TABLE `fmn_wc_download_log`
  ADD CONSTRAINT `fk_fmn_wc_download_log_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `fmn_woocommerce_downloadable_product_permissions` (`permission_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
